package helpers

import (
	"strings"
	"fmt"
)

//ArrayToString -
func ArrayToString(a []int, delim string) string {
    return strings.Trim(strings.Replace(fmt.Sprint(a), " ", delim, -1), "[]")
}