package helpers

import (
	"fmt"
	"time"
)

func StartOfDateNow() time.Time {
	bangkok, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		fmt.Println("Something wrong with Load Location")
	}
	now := time.Now().In(bangkok)
	year, month, day := now.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, now.Location())
}

func GetBankokTimeZone(t time.Time) time.Time {
	bangkok, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		fmt.Println("Something wrong with Load Location")
	}
	timeInBankok := t.In(bangkok)
	return timeInBankok
}

func GetBangkokDateFromString(str string) time.Time {
	parsedTime, _ := time.Parse(time.RFC3339, str)
	bangkokTime := GetBankokTimeZone(parsedTime)
	return bangkokTime
}
