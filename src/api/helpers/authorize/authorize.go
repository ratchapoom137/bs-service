package helpers

import (
	"api/base"
	"api/model"
	"api/service"
	"context"
	"reflect"
	"runtime"
	"strconv"

	"dv.co.th/hrms/core"
	"dv.co.th/hrms/core/util"
	"github.com/graphql-go/graphql"
	"github.com/thoas/go-funk"
	// "github.com/davecgh/go-spew/spew"
)

type AuthorizedHandle func(ctx context.Context) (bool, context.Context)
type AuthorizedGroupResponse func() (bool, context.Context)

type key string

const (
	// AccessTokenHeaderKey - It return key of header
	AccessTokenHeaderKey key = "access_token"

	// UserIDHeaderKey - It return key of header
	UserIDHeaderKey key = "user_id"

	// ClientIDHeaderKey - It return key of header
	ClientIDHeaderKey key = "client_id"

	// UserAccountHeaderKey - It return key of header
	UserAccountHeaderKey key = "user_account"

	// UserRoleHeaderKey - It return key of header
	UserRoleHeaderKey key = "user_role"
)

// Authorized is a middleware to provide the validation of resource to resolve an output for GraphQL responses
//
// Example
//
// helpers.Authorized(
// 	helpers.AuthorizedAny(&p)(
// 		helpers.AuthorizedGroup(p)(
// 			helpers.WithBearer("client"),
// 		),
// 		helpers.AuthorizedGroup(p)(
// 			helpers.WithBearer("user"),
// 			helpers.WithUserProfile(),
// 			helpers.WithPermissionAny(
// 				model.Permission{Module: "dashboard", Function: "read"},
// 				model.Permission{Module: "dashboard", Function: "all"},
// 			),
// 			helpers.WithPermissionAll(
// 				model.Permission{Module: "dashboard", Function: "deny"},
// 			),
// 		),
// 	),

// 	func(p graphql.ResolveParams) (interface{}, error) {
// 		..Handle success
// 	}
// 	func(p graphql.ResolveParams) (interface{}, error) {
// 		..Handle Fail
// 	},
// )(p)
func Authorized(
	isAuthorized bool,
	onSuccess func(p graphql.ResolveParams) (interface{}, error),
	onError func(p graphql.ResolveParams) (interface{}, error),
) func(p graphql.ResolveParams) (interface{}, error) {
	if !isAuthorized {
		return onError
	}

	return onSuccess
}

// AuthorizedAny -
func AuthorizedAny(
	p *graphql.ResolveParams,
) func(res ...AuthorizedGroupResponse) bool {
	return func(athRes ...AuthorizedGroupResponse) bool {
		for _, AuthorizeGroupResult := range athRes {
			if ok, ctx := AuthorizeGroupResult(); ok {
				_ = ctx
				p.Context = ctx
				return true
			}
		}

		return false
	}
}

// AuthorizedGroup is the wrapper fof each validation rules
func AuthorizedGroup(p graphql.ResolveParams) func(aths ...AuthorizedHandle) AuthorizedGroupResponse {
	app := base.GetApplication()
	ctx := p.Context

	return func(aths ...AuthorizedHandle) AuthorizedGroupResponse {
		var ok bool
		for _, ath := range aths {
			if ok, ctx = ath(ctx); !ok {
				app.Logger.Error("Validation failed from : ", GetFunctionName(ath))
				return func() (bool, context.Context) {
					return false, ctx
				}
			}

			p.Context = ctx
		}

		return func() (bool, context.Context) {
			return true, ctx
		}
	}
}

// WithBearer return true if header with Authorization field was passes
func WithBearer(BearerType string) AuthorizedHandle {
	app := core.GetApplication()
	return func(ctx context.Context) (isCan bool, octx context.Context) {
		octx = ctx

		var accessToken string
		var err error

		if accessToken, err = util.GetTokenFromHeader(ctx); err != nil {
			app.Logger.Info(err.Error())
			return
		}

		response, err := service.ValidateTokenFromAuthCode(
			accessToken,
		)

		if err != nil {
			app.Logger.Error("Request header doesn't have bearer")
			return
		}
		
		if BearerType != "client" && BearerType != "user"  {
			app.Logger.Error("Request bearer is not user or client bearer")
			return
		}

		if BearerType == "user" && response.UserID == "" {
			app.Logger.Error("Request bearer is not user bearer")
			return
		}

		if BearerType == "client" && response.ClientID == "" {
			app.Logger.Error("Request bearer is not user bearer")
			return
		}

		octx = context.WithValue(octx, AccessTokenHeaderKey, accessToken)
		octx = context.WithValue(octx, ClientIDHeaderKey, response.ClientID)
		octx = context.WithValue(octx, UserIDHeaderKey, response.UserID)

		isCan = true
		return
	}
}

// WithUserProfile return true if bearer in Authorization header was valid and reslovable in OAuth server
func WithUserProfile() AuthorizedHandle {
	app := core.GetApplication()
	return func(ctx context.Context) (isCan bool, octx context.Context) {
		octx = ctx

		app.Logger.Info("User id: ", ctx.Value(UserIDHeaderKey))

		UserID, _ := strconv.Atoi(ctx.Value(UserIDHeaderKey).(string))
		response, err := service.GetUserFromID(UserID)

		if err != nil {
			app.Logger.Error("Bearer cannot be resolved or doesn't have user id")
			return
		}

		if response.ID == 0 {
			app.Logger.Error("Bearer cannot be resolved or doesn't have user id")
			return
		}

		octx = context.WithValue(octx, UserAccountHeaderKey, response)
		octx = context.WithValue(octx, UserRoleHeaderKey, response.Role.Name)

		isCan = true
		return
	}
}

// WithNoUser return true if bearer in Authorization header was valid and reslovable in OAuth server
func WithNoUser() AuthorizedHandle {
	return func(ctx context.Context) (isCan bool, octx context.Context) {
		octx = ctx

		_, ok := ctx.Value(UserIDHeaderKey).(string)
		if !ok {
			isCan = true
			return
		}
		return
	}
}

// func WithPermission(Permissions ...model.Permission) AuthorizedHandle {
// 		userAccount := ctx.Value(UserAccountHeaderKey).(model.User)
// 	return func(ctx context.Context) (isCan bool, octx context.Context) {
// 	}
// }

// WithPermissionAll return true if user's permission match required rule
// helpers.WithPermissionAll(
// 	model.Permission{Module: "dashboard", Function: "read"},
// ),
func WithPermissionAll(Permissions ...model.Permission) AuthorizedHandle {
	// app := core.GetApplication()
	return func(ctx context.Context) (isCan bool, octx context.Context) {
		octx = ctx
		userAccount := octx.Value(UserAccountHeaderKey).(model.User)

		isCan = true
		funk.ForEach(Permissions, func(graphp model.Permission) {
			r := funk.Find(userAccount.Role.Permissions, func(userp model.Permission) bool {
				if userp.Function == graphp.Function && userp.Module == graphp.Module {
					return true
				}
				return false
			})

			if r == nil {
				isCan = false
				return
			}
		})

		return
	}
}

// WithPermissionAny return true if user's permission match required rule
func WithPermissionAny(Permissions ...model.Permission) AuthorizedHandle {
	app := core.GetApplication()
	return func(ctx context.Context) (isCan bool, octx context.Context) {
		octx = ctx
		userAccount := octx.Value(UserAccountHeaderKey).(model.User)

		funk.ForEach(userAccount.Role.Permissions, func(userp model.Permission) {
			funk.ForEach(Permissions, func(graphp model.Permission) {
				if userp.Function == graphp.Function && userp.Module == graphp.Module {
					isCan = true
					return
				}
			})
		})

		if !isCan {
			app.Logger.Errorf("User id %d was not have permission to access the resource", userAccount.ID)
		}

		return
	}
}

func GetFunctionName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}
