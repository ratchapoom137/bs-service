package helpers

//Paginator -
func Paginator(limit int, offset int, total int) (int, int, error) {
	start := 0
	end := total
	if offset <= total && limit <= total && limit != 1 {

		if offset != 1 {
			if limit*offset > total {
				start = limit
				end = total
			} else {
				start = (limit * (offset - 1))
				end = limit * offset
			}
		} else {
			end = limit
		}

	} else if offset <= total && limit <= total && limit == 1 {
		start = offset - 1
		end = offset
	}

	return start, end, nil
}
