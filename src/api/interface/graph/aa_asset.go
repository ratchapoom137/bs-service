package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"

	"github.com/graphql-go/graphql"
	_ "github.com/thoas/go-funk"
)

type AssetResMessage struct {
	Id      int    `json:"id"`
	Message string `json:"message"`
}

var AssetType *graphql.Object
var AssetResponseMessage *graphql.Object
var AssetListType *graphql.Object

func init() {
	AssetType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Asset",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"serial_number": &graphql.Field{
						Type: graphql.String,
					},
					"asset_category_id": &graphql.Field{
						Type: graphql.Int,
					},
					"residual": &graphql.Field{
						Type: graphql.Int,
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
					"asset_category": &graphql.Field{
						Type: AssetCategoryType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							catID := p.Source.(model.Asset).AssetCategoryID
							res, err := service.GetCategory(catID)
							return res, err
						},
					},
					"detail": &graphql.Field{
						Type: graphql.String,
					},
					"purchase_date": &graphql.Field{
						Type: graphql.String,
					},
					"expire_date": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	AssetResponseMessage = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "AssetResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	AssetListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Assets",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"assets": &graphql.Field{
						Type: graphql.NewList(AssetType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
}

// GetAssetByID -
func (c *Controller) GetAssetByID() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "asset", &graphql.Field{
			Type: AssetType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					res, err := service.GetAssetByID(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetAssetListByFilter -
func (c *Controller) GetAssetList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "assets", &graphql.Field{
			Type: AssetListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"asset_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"asset_category_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"serial_number": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"start_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"end_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"category_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					name, _ := p.Args["asset_name"].(string)
					status, _ := p.Args["status"].(string)
					asset_category_name, _ := p.Args["asset_category_name"].(string)
					serial_number, _ := p.Args["serial_number"].(string)
					start_date, _ := p.Args["start_date"].(string)
					end_date, _ := p.Args["end_date"].(string)
					categoryID, categoryOk := p.Args["category_id"].(int)
					companyID, companyOk := p.Args["company_id"].(int)

					if !limitOk {
						limit = -1
					}

					if !offsetOk {
						offset = -1
					}

					if !categoryOk {
						categoryID = -1
					}

					if !companyOk {
						companyID = -1
					}

					_, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					companyID = employee.CompanyID
					if companyID == 0 {
						return nil, errors.New("no company id")
					}

					res, total, err := service.GetAssetList(name, status, asset_category_name, serial_number, start_date, end_date, limit, offset, categoryID, companyID)
					output := model.AssetList{
						Assets: res,
						Total:  total,
					}

					if err != nil {
						return nil, err
					}

					return output, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostAsset
func (c *Controller) PostAsset() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_asset", &graphql.Field{
			Description: "Create new asset",
			Type:        AssetResponseMessage,
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"serial_number": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"detail": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"asset_category_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"residual": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"purchase_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"expire_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"created_by": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"updated_by": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var assetInput model.AssetInput

					jsonData, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonData, &assetInput)

					res, err := service.PostAsset(&assetInput)
					if err != nil {
						return nil, err
					}
					return AssetResMessage{
						Id:      res,
						Message: "Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutAsset -
func (c *Controller) PutAsset() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_asset", &graphql.Field{
			Description: "Edit asset",
			Type:        AssetResponseMessage,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"serial_number": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"detail": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"asset_category_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"residual": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"purchase_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"expire_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"updated_by": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var assetInput model.AssetInput

					jsonData, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonData, &assetInput)

					res, err := service.PutAsset(&assetInput)

					return AssetResMessage{
						Id:      res,
						Message: "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// DeleteAsset -
func (c *Controller) DeleteAsset() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_asset", &graphql.Field{
			Type: AssetResponseMessage,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					res, err := service.DeleteAsset(p.Args["id"].(int))

					if err != nil {
						return nil, err
					}

					return AssetResMessage{
						Id:      res,
						Message: "Delete Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
