package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"strconv"

	// "github.com/davecgh/go-spew/spew"

	"github.com/graphql-go/graphql"
)

var WorkPresetType *graphql.Object
var WorkListQueryType *graphql.Object

func init() {
	WorkPresetType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "WorkPreset",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.String,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"type": &graphql.Field{
						Type: graphql.String,
					},
					"start_time": &graphql.Field{
						Type: graphql.String,
					},
					"end_time": &graphql.Field{
						Type: graphql.String,
					},
					"work_day": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	WorkListQueryType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "WorkPresets",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"work_presets": &graphql.Field{
						Type: graphql.NewList(WorkPresetType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
}

// GetWorkPresetList -
func (c *Controller) GetWorkPresetList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "work_presets", &graphql.Field{
			Type: WorkListQueryType,
			Args: graphql.FieldConfigArgument{
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"work_type": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_preset", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userInfo, err := service.GetEmployeeByUserID(userID)
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					if err != nil && userAccount.(model.User).Role.Name != "admin" {
						return "fail", nil
					}

					if userAccount.(model.User).Role.Name != "admin" {
						return service.GetWorkPresetList(p.Args["limit"].(int), p.Args["offset"].(int), p.Args["work_type"].(string), userInfo.CompanyID)
					}
					return service.GetWorkPresetList(p.Args["limit"].(int), p.Args["offset"].(int), p.Args["work_type"].(string), p.Args["company_id"].(int))

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetWorkPreset -
func (c *Controller) GetWorkPreset() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "work_preset", &graphql.Field{
			Type: WorkPresetType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_preset", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					return service.GetWorkPresetDetail(p.Args["id"].(int))
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// DeleteWorkPreset -
func (c *Controller) DeleteWorkPreset() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_work_preset", &graphql.Field{
			Description: "Delete Work Preset",
			Type:        WorkPresetType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_preset", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					return service.DeleteWorkPreset(p.Args["id"].(int))
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)

			},
		}
	}
}

// PutWorkPreset -
func (c *Controller) PutWorkPreset() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_work_preset", &graphql.Field{
			Description: "Edit Work Preset",
			Type:        WorkPresetType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"type": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"start_time": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"end_time": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"work_day": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_preset", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userInfo, err := service.GetEmployeeByUserID(userID)

					if err != nil {
						return "fail", nil
					}

					p.Args["updated_by"] = userInfo.PersonalID

					return service.PutWorkPreset(p.Args)
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostWorkPreset -
func (c *Controller) PostWorkPreset() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_work_preset", &graphql.Field{
			Description: "Create Work Preset",
			Type:        WorkPresetType,
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"type": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"start_time": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"end_time": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"work_day": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_preset", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userInfo, err := service.GetEmployeeByUserID(userID)

					if err != nil {
						return "fail", nil
					}
					p.Args["created_by"] = userInfo.PersonalID
					p.Args["updated_by"] = userInfo.PersonalID
					p.Args["company_id"] = userInfo.CompanyID

					return service.PostWorkPreset(p.Args)
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
