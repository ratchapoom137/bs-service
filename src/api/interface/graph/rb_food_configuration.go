package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"
	"github.com/thoas/go-funk"
	// "time"
	// "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
)

//Types
var FoodApproveConfigurationType *graphql.Object
var FoodConfigurationType *graphql.Object
var HRType *graphql.Object
var HRListType *graphql.Object

//InputTypes
var FoodApproveConfigurationInputType *graphql.InputObject

func init() {
	HRListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "HRList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"total": &graphql.Field{
						Type: graphql.Int,
					},
					"hrs": &graphql.Field{
						Type: graphql.NewList(HRType),
					},
				}
			}),
		},
	)
	HRType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "HRType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"first_name": &graphql.Field{
						Type: graphql.String,
					},
					"last_name": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	FoodApproveConfigurationInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "FoodApproveConfigurationInputType",
			Fields: graphql.InputObjectConfigFieldMap{
				"approve_type_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)
	FoodApproveConfigurationType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "FoodApproveConfigurationType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"configuration_id": &graphql.Field{
						Type: graphql.Int,
					},
					"approve_type_id": &graphql.Field{
						Type: graphql.Int,
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	FoodConfigurationType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "FoodConfigurationType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"approve_type_id": &graphql.Field{
						Type: graphql.Int,
					},
					"approve_type_name": &graphql.Field{
						Type: graphql.String,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							approval, ok := p.Source.(model.FoodApproveConfiguration)
							if !ok {
								return nil, errors.New("Invalid approval")
							}
							getApprovalName, err := service.GetApprovalConfig(approval.ApproveTypeID)
							if err != nil {
								return nil, err
							}
							return getApprovalName.Name, err
						},
					},
				}
			}),
		},
	)
}

// PostFoodApproveConfiguration -
func (c *Controller) PostFoodApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_food_approve_configuration", &graphql.Field{
			Description: "Create new food approve configuration",
			Type:        FoodApproveConfigurationType,
			Args: graphql.FieldConfigArgument{
				"food_approve_configuration_input": &graphql.ArgumentConfig{
					Type: FoodApproveConfigurationInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					var foodApproveConfigurationInput model.FoodApproveConfiguration
					jsonData, _ := json.Marshal(p.Args["food_approve_configuration_input"])
					json.Unmarshal(jsonData, &foodApproveConfigurationInput)

					foodApproveConfigurationInput.CompanyID = employee.CompanyID
					foodApproveConfigurationInput.CreatedBy = employee.PersonalID

					res, err := service.CreateFoodApproveConfiguration(&foodApproveConfigurationInput)
					if err != nil {
						return nil, err
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetFoodApproveConfiguration -
func (c *Controller) GetFoodApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "food_approve_configuration", &graphql.Field{
			Description: "Get food configuration",
			Type:        FoodConfigurationType,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_type", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					res, err := service.GetFoodConfiguration(employee.CompanyID)
					if err != nil {
						return nil, err
					}

					return res, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutFoodApproveConfiguration -
func (c *Controller) PutFoodApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_food_approve_configuration", &graphql.Field{
			Type: FoodApproveConfigurationType,
			Args: graphql.FieldConfigArgument{
				"food_approve_configuration_input": &graphql.ArgumentConfig{
					Type: FoodApproveConfigurationInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_type", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					var foodApproveConfigurationInput model.FoodApproveConfiguration
					jsonData, _ := json.Marshal(p.Args["food_approve_configuration_input"])
					json.Unmarshal(jsonData, &foodApproveConfigurationInput)

					foodApproveConfigurationInput.CompanyID = employee.CompanyID
					foodApproveConfigurationInput.UpdatedBy = employee.PersonalID

					res, err := service.PutFoodApproveConfiguration(&foodApproveConfigurationInput)
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetFoodApproveConfiguration -
func (c *Controller) GetHR() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "hr_list", &graphql.Field{
			Description: "Get HR list",
			Type:        HRListType,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						// authorize.WithPermissionAny(
						// 	// model.Permission{Module: "reimbursement_type", Function: "create"},
						// ),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					type HRDetails struct {
						EmployeeID int    `json:"employee_id"`
						FirstName  string `json:"first_name"`
						LastName   string `json:"last_name"`
					}
					type HRListStruct struct {
						HRs   []HRDetails `json:"hrs"`
						Total int         `json:"total"`
					}
					_, employee, getErr := GetUserEmployeeFromSession(p)
					if getErr != nil {
						return HRListStruct{}, nil
					}
					filter := map[string]interface{}{
						"role_name":  "hr",
					}
					res, _, _ := service.GetUserList(filter)

					var filterList service.FilterList
					for _,item := range res{
						filterList.UserIDList = append(filterList.UserIDList, item.ID)
					}

					filterList.CompanyIDList = append(filterList.CompanyIDList, employee.CompanyID)
					userDetailFilter := map[string]interface{}{}
					userDetailFilter["user_id_list"] = filterList.UserIDList
					userDetailFilter["company_id_list"] = filterList.CompanyIDList
						
					userDetailList,_,_:=service.GetUserDetailList(userDetailFilter)

					var userList []model.UserDetail
					for _,item := range userDetailList {
						if item.EmployeeID != 0{
							userList = append(userList,item)
						}
					}

					HRList, ok := funk.Map(userList, func(eachUser model.UserDetail) HRDetails {
						empFilter := map[string]interface{}{
							"company_id": employee.CompanyID,
							"user_id":    eachUser.UserID,
							"limit":      -1,
							"offset":     -1,
						}
						empInfo, _, _ := service.GetEmployeeByFilter(empFilter)

						firstEmployeeInfo := empInfo[0]

						return HRDetails{
							EmployeeID: firstEmployeeInfo.ID,
							FirstName:  firstEmployeeInfo.EmployeePersonnelInfo.FirstNameEn,
							LastName:   firstEmployeeInfo.EmployeePersonnelInfo.LastNameEn,
						}

					}).([]HRDetails)

					if !ok {
						return nil, errors.New("Can't get emp details")
					}

					return HRListStruct{
						HRs:   HRList,
						Total: len(HRList),
					}, nil

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
