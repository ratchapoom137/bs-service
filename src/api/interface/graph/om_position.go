package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"time"
	
	"github.com/graphql-go/graphql"
)

var PositionType *graphql.Object
var PositionInputType *graphql.InputObject
var PositionListType *graphql.Object
var PositionResponseMessageType *graphql.Object

func init() {
	PositionType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Position",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"org_unit_id": &graphql.Field{
						Type: graphql.Int,
					},
					"org_unit": &graphql.Field{
						Type: OrgUnitType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							orgID := p.Source.(model.Position).OrgUnitID
							if orgID > 0 {
								res, err := service.GetOrgUnit(orgID)
								return res, err
							}
							return nil, nil
						},
					},
					"is_manager": &graphql.Field{
						Type: graphql.Boolean,
					},
					"effective_at": &graphql.Field{
						Type: graphql.String,
					},
					"terminate_at": &graphql.Field{
						Type: graphql.String,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"employee_personel_info": &graphql.Field{
						Type: PersonnelInfoType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							var generalInfo *model.EmployeePersonnelInfo
							if p.Source.(model.Position).EmployeeID != 0 {
								 testEmployee,_ := service.GetEmployeeByPersonalID(p.Source.(model.Position).EmployeeID)								
								 generalInfo = testEmployee.EmployeePersonnelInfo
							}
							return generalInfo, nil
						},
					},
				}
			}),
		},
	)
	PositionInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "PositionInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"description": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"org_unit_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"is_manager": &graphql.InputObjectFieldConfig{
					Type: graphql.Boolean,
				},
				"effective_at": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"terminate_at": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)
	PositionListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "PositionList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"positions": &graphql.Field{
						Type: graphql.NewList(PositionType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	PositionResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "PositionResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"position_id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

// GetPosition -
func (c *Controller) GetPosition() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "position", &graphql.Field{
			Type: PositionType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee_position", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetPosition(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetPositions -
func (c *Controller) GetPositions() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "positions", &graphql.Field{
			Type: PositionListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"org_unit_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"point_of_time": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"is_pending": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"is_available": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee_position", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					org_unit_id, _ := p.Args["org_unit_id"].(int)
					name, _ := p.Args["name"].(string)
					pointOfTime, timeOk := p.Args["point_of_time"].(string)
					isPending, isPendingOk := p.Args["is_pending"].(bool)
					isAvailable, isAvailableOk := p.Args["is_available"].(bool)

					if !limitOk {
						limit = 999
					}

					if !offsetOk {
						offset = 0
					}

					if !timeOk {
						pointOfTime = ""
					}

					if !isPendingOk {
						isPending = false
					}

					if !isAvailableOk {
						isAvailable = false
					}

					result, total, err := service.GetPositions(limit, offset, org_unit_id, name, pointOfTime, isPending, isAvailable)
					if err != nil {
						return nil, err
					}
					return model.PositionList{
						Positions: result,
						Total:     total,
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostPosition -
func (c *Controller) PostPosition() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_position", &graphql.Field{
			Type: PositionResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"position": &graphql.ArgumentConfig{
					Type: PositionInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee_position", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var position model.Position
					jsonPosition, _ := json.Marshal(p.Args["position"])
					json.Unmarshal(jsonPosition, &position)

					result, err := service.PostPosition(position)
					if err != nil || result == 0 {
						return nil, err
					}

					return model.PositionResponseMessage{
						PositionID: result,
						Message:    "Create Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutPosition -
func (c *Controller) PutPosition() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_position", &graphql.Field{
			Type: PositionResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"position": &graphql.ArgumentConfig{
					Type: PositionInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee_position", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var position model.Position
					jsonPosition, _ := json.Marshal(p.Args["position"])
					json.Unmarshal(jsonPosition, &position)

					result, err := service.PutPosition(position)
					if err != nil || result == 0 {
						return nil, err
					}

					return model.PositionResponseMessage{
						PositionID: result,
						Message:    "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//DeletePosition -
func (c *Controller) DeletePosition() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_position", &graphql.Field{
			Description: "Delete position",
			Type:        PositionResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"terminate_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee_position", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					posID, _ := p.Args["id"].(int)
					terminateDate, dateOk := p.Args["terminate_date"].(string)

					var terminateAt time.Time

					if !dateOk {
						year, month, day := time.Now().Date()
						terminateAt = time.Date(year, month, day, 0, 0, 0, 0, time.Now().Location())
					}

					position, err := service.GetPosition(posID)
					if err != nil {
						return nil, err
					}
					var result int
					if position.EffectiveAt.Before(time.Now()) {
						if terminateDate != "" && dateOk {
							terminateAt, _ = time.Parse(time.RFC3339, terminateDate)
						}
						position.TerminateAt = &terminateAt
						result, err = service.PutPosition(position)
						if err != nil || result == 0 {
							return nil, err
						}
					}
					if position.EffectiveAt.After(time.Now()) {
						result, err = service.DeletePosition(p.Args["id"].(int))
						if err != nil || result == 0 {
							return nil, err
						}
					}

					return model.PositionResponseMessage{
						PositionID: result,
						Message:    "Terminate Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
