package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"
	"strconv"
	_"github.com/mitchellh/mapstructure"
	"github.com/graphql-go/graphql"
)

var ApprovalConfigType *graphql.Object
var ApprovalConfigInputType *graphql.InputObject
var ApprovalConfigDetailType *graphql.Object
var ApprovalConfigListType *graphql.Object
var ApprovalConfigBudgetInputType *graphql.InputObject
var ApprovalConfigBudgetDetail *graphql.Object

var ApprovalConfigResponseMessageType *graphql.Object

// var ApprovalConfigListResponseMessageType *graphql.Object

func init() {
	ApprovalConfigType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ApprovalConfig",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"config_type": &graphql.Field{
						Type: graphql.String,
					},
					"approval_config_detail": &graphql.Field{
						Type: graphql.NewList(ApprovalConfigDetailType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							approvalConfigID := p.Source.(model.ApprovalConfig).ID
							result, err := service.GetApprovalConfigDetail(approvalConfigID)
							if err != nil {
								return nil, err
							}
							return result, err
						},
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	ApprovalConfigBudgetInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "ApprovalConfigBudgetInputType",
			Fields: graphql.InputObjectConfigFieldMap{
			
					"id": &graphql.InputObjectFieldConfig{
						Type: graphql.Int,
					},
					"approver_employee_level": &graphql.InputObjectFieldConfig{
						Type: graphql.String,
					},
					"budget": &graphql.InputObjectFieldConfig{
						Type: graphql.Float,
					},
				
			},
		},
	)
	ApprovalConfigInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "ApprovalConfigInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"company_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"config_type": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"value_id": &graphql.InputObjectFieldConfig{
					Type: graphql.NewList(graphql.Int),
				},
				"created_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"updated_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"employee_level_budget": &graphql.InputObjectFieldConfig{
					Type: graphql.NewList(ApprovalConfigBudgetInputType),
				},
			},
		},
	)
	ApprovalConfigDetailType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ApprovalConfigDetail",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"approval_config_id": &graphql.Field{
						Type: graphql.Int,
					},
					"value_id": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	ApprovalConfigBudgetDetail = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ApprovalConfigBudgetDetail",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"approver_employee_level": &graphql.Field{
						Type: graphql.String,
					},
					"budget": &graphql.Field{
						Type: graphql.Float,
					},
				}
			}),
		},
	)
	ApprovalConfigListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ApprovalConfigList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"approval_configs": &graphql.Field{
						Type: graphql.NewList(ApprovalConfigType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	ApprovalConfigResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ApprovalConfigResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"approval_config_id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

}

// GetApprovalConfig -
func (c *Controller) GetApprovalConfig() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "approval_config", &graphql.Field{
			Type: ApprovalConfigType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "approval_config", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetApprovalConfig(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetApprovalConfigList -
func (c *Controller) GetApprovalConfigList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "approval_configs", &graphql.Field{
			Type: ApprovalConfigListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "approval_config", Function: "read"},
						),
					),
				)

				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					companyID, companyIDOk := p.Args["company_id"].(int)
					name, _ := p.Args["name"].(string)

					if !limitOk {
						limit = 9999
					}
					if !offsetOk {
						offset = 0
					}
					if !companyIDOk {
						companyID = 0
					}

					userIDstr, userIDOk := p.Context.Value(authorize.UserIDHeaderKey).(string)
					if !userIDOk {
						return model.User{}, errors.New("invalid bearer")
					}
					userID, _ := strconv.Atoi(userIDstr)
					user, err := service.GetUserFromID(userID)

					employee, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err
					}

					if user.Role.Name == "hr" {
						companyID = employee.CompanyID
						result, total, err := service.GetApprovalConfigList(limit, offset, companyID, name)
						if err != nil {
							return nil, err
						}
						return model.ApprovalConfigList{
							ApprovalConfigs: result,
							Total:           total,
						}, err
					}

					return nil, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostApprovalConfig -
func (c *Controller) PostApprovalConfig() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_approval_config", &graphql.Field{
			Type: ApprovalConfigResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"approval_config_input": &graphql.ArgumentConfig{
					Type: ApprovalConfigInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "approval_config", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var approvalConfigInput model.ApprovalConfigInput

					jsonData, _ := json.Marshal(p.Args["approval_config_input"])
					json.Unmarshal(jsonData, &approvalConfigInput)
					// decodeError := mapstructure.Decode(p.Args["approval_config_input"], &approvalConfigInput)
					// if decodeError != nil {
					// 	return nil, decodeError
					// }
					user, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					if user.Role.Name == "hr" {
						approvalConfigInput.CompanyID = employee.CompanyID
						result, err := service.PostApprovalConfig(approvalConfigInput)
						if err != nil {
							return nil, err
						}
						return model.ApprovalConfigResponseMessage{
							ApprovalConfigID: result,
							Message:          "Create Success",
						}, nil
					}
					return nil, errors.New("no permission")
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutApprovalConfig -
func (c *Controller) PutApprovalConfig() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_approval_config", &graphql.Field{
			Type: ApprovalConfigResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"approval_config_input": &graphql.ArgumentConfig{
					Type: ApprovalConfigInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "approval_config", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var approvalConfigInput model.ApprovalConfigInput

					jsonData, _ := json.Marshal(p.Args["approval_config_input"])
					json.Unmarshal(jsonData, &approvalConfigInput)

					result, err := service.PutApprovalConfig(approvalConfigInput)
					if err != nil {
						return nil, err
					}

					return model.ApprovalConfigResponseMessage{
						ApprovalConfigID: result,
						Message:          "Edit Success",
					}, nil
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetApprovalPositions -
func (c *Controller) GetApprovalPositions() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "approval_positions", &graphql.Field{
			Type: graphql.NewList(PositionType),
			Args: graphql.FieldConfigArgument{
				"position_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"approval_type_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "approval_config", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					positionID, _ := p.Args["position_id"].(int)
					approvalID, _ := p.Args["approval_type_id"].(int)

					approvalPositions, _ := service.GetApprovalPositions(positionID, approvalID)

					return approvalPositions, nil
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// DeleteApprovalConfig -
func (c *Controller) DeleteApprovalConfig() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_approval_config", &graphql.Field{
			Type: ApprovalConfigResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "approval_config", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.DeleteApprovalConfig(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}

					return model.ApprovalConfigResponseMessage{
						ApprovalConfigID: result,
						Message:          "Delete Success",
					}, nil
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)

			},
		}
	}
}

// GetApprovalConfig Budget Detail
func (c *Controller) GetApprovalConfigBudgetByID() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "approval_config_budget", &graphql.Field{
			Type: ApprovalConfigBudgetDetail,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "approval_config", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetApprovalConfigBudget(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}