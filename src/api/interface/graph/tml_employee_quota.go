package graph

import (
	authorize "api/helpers/authorize"
	"api/service"
	"api/model"
	"api/request"
	"encoding/json"
	"errors"
	"strings"
	"time"
	"strconv"
	"github.com/graphql-go/graphql"
	"github.com/thoas/go-funk"
)

// EmployeeQuotaType -
var EmployeeQuotaType *graphql.Object

// EmployeeQuotaInputType -
var EmployeeQuotaInputType *graphql.InputObject

// EmployeeQuotaDetailType -
var EmployeeQuotaDetailType *graphql.Object

// EmployeeQuotasType -
var EmployeeQuotasType *graphql.Object

func init() {
	EmployeeQuotaType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "EmployeeQuota",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee": &graphql.Field{
						Type: EmployeeType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							id := p.Source.(model.EmployeeQuota).EmployeeID
							res, err := service.GetEmployeeByPersonalID(id)
							if err != nil {
								return nil, err
							}
							return res, err
						},
					},
					"leave_type_id": &graphql.Field{
						Type: graphql.Int,
					},
					"leave_type": &graphql.Field{
						Type: LeaveType,
					},
					"quota_days": &graphql.Field{
						Type: graphql.Float,
					},
					"quota_hours": &graphql.Field{
						Type: graphql.Float,
					},
					"taken_days": &graphql.Field{
						Type: graphql.Float,
					},
					"taken_hours": &graphql.Field{
						Type: graphql.Float,
					},
					"carried_days": &graphql.Field{
						Type: graphql.Float,
					},
					"carried_hours": &graphql.Field{
						Type: graphql.Float,
					},
					"times_left": &graphql.Field{
						Type: graphql.Int,
					},
					"times_taken": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	EmployeeQuotaInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "EmployeeQuotaInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"leave_type_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"quota_hours": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
				"times_left": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	EmployeeQuotaDetailType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "EmployeeQuotaDetail",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"employee": &graphql.Field{
						Type: EmployeeType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							id := p.Source.(model.EmployeeQuotaDetail).EmployeeID
							res, err := service.GetEmployeeByPersonalID(id)
							if err != nil {
								return nil, err
							}
							return res, err
						},
					},
					"quotas": &graphql.Field{
						Type: graphql.NewList(EmployeeQuotaType),
					},
				}
			}),
		},
	)

	EmployeeQuotasType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "EmployeeQuotaWithPagination",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"employee_quotas": &graphql.Field{
						Type: graphql.NewList(EmployeeQuotaType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
}

// InitEmployeeQuota -
func (c *Controller) InitEmployeeQuota() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_employee_quota", &graphql.Field{
			Type: EmployeeQuotaDetailType,
			Description: "Initialize employee quota by employee id",
			Args: graphql.FieldConfigArgument{
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee_quota", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					employeeID, _ := p.Args["employee_id"].(int)

					userInfo, userInfoErr := service.GetEmployeeByPersonalID(employeeID)
					if userInfoErr != nil {
						return nil, userInfoErr
					}

					empGender := strings.ToLower(userInfo.EmployeePersonnelInfo.Gender)
					empCompanyID := userInfo.EmployeeGeneralInfo.CompanyID

					res, err := service.InitEmployeeQuotaByID(employeeID, empGender, empCompanyID)
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetEmployeeQuota -
func (c *Controller) GetEmployeeQuota() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "employee_quota", &graphql.Field{
			Type: EmployeeQuotaDetailType,
			Description: "Get employee quota by employee id",
			Args: graphql.FieldConfigArgument{
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee_quota", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err 
					}

					var status string

					status = model.ACTIVE.String()

					quotaRes, quotaResErr := service.GetEmployeeQuotaByID(userResult.PersonalID, status)
					if quotaResErr != nil {
						return nil, quotaResErr
					}

					var res model.EmployeeQuotaDetail
					employeeID, employeeIDOk := p.Args["employee_id"].(int)

					if userAccount.(model.User).Role.Name != "hr" {
						return quotaRes, quotaResErr
					}

					if !employeeIDOk {
						return quotaRes, quotaResErr
					}

					empFilter := map[string]interface{}{
						"limit": 30000,
						"offset": 0,
						"company_id": userResult.CompanyID,
					}

					employeeInCompany, _, employeeInCompanyErr := service.GetEmployeeByFilter(empFilter)
					if employeeInCompanyErr != nil {
						return nil, employeeInCompanyErr
					}

					employeeInCompanyIDs := funk.Map(employeeInCompany, func(employee model.Employee) int {
						return employee.ID
					}).([]int)

					if !funk.Contains(employeeInCompanyIDs, employeeID) {
						return nil, errors.New("can't found employee quota")
					}

					status = model.ALL_LEAVE_TYPE.String()

					res, err = service.GetEmployeeQuotaByID(employeeID, status)

					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetEmployeeQuotas -
func (c *Controller) GetEmployeeQuotas() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "employee_quotas", &graphql.Field{
			Type: EmployeeQuotasType,
			Description: "Get all employee quota list",
			Args: graphql.FieldConfigArgument{
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"year": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_report", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err 
					}

					filter := map[string]interface{}{
						"limit": 30000,
						"offset": 0,
						"company_id": userResult.CompanyID,
					}

					employeeInCompany, _, employeeInCompanyErr := service.GetEmployeeByFilter(filter)
					if employeeInCompanyErr != nil {
						return nil, employeeInCompanyErr
					}

					employeeInCompanyIDs := funk.Map(employeeInCompany, func(employee model.Employee) int {
						return employee.ID
					}).([]int)
						
					status, _ := p.Args["status"].(string)
					empInputIDs, empInputIDsOk := p.Args["employee_id"].([]interface{})
					var employeeIDs []int

					if empInputIDsOk {
						if len(empInputIDs) == 0 {
							return model.EmployeeQuotaList{}, nil
						}
						funk.ForEach(empInputIDs, func(item interface{}) {
							if !funk.Contains(employeeInCompanyIDs, item.(int)) {
								return
							}
							employeeIDs = append(employeeIDs, item.(int))
						})
						if employeeIDs == nil {
							return model.EmployeeQuotaList{}, nil
						}
					}

					if len(employeeIDs) == 0 {
						funk.ForEach(employeeInCompanyIDs, func(item int) {
							employeeIDs = append(employeeIDs, item)
						})
					}

					year, yearOk := p.Args["year"].(string) 

					currentYear := strconv.Itoa(time.Now().Year())

					if !yearOk {
						year = currentYear
					}

					res, err := service.GetEmployeeQuotaList(employeeIDs, year, status)

					if err != nil {
						return nil, err
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// EditEmployeeQuota -
func (c *Controller) EditEmployeeQuota() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_employee_quota", &graphql.Field{
			Type:        EmployeeQuotaDetailType,
			Description: "Edit Employee Quota by Employee id",
			Args: graphql.FieldConfigArgument{
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"quotas": &graphql.ArgumentConfig{
					Type: graphql.NewList(EmployeeQuotaInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee_quota", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var employeeQuota []model.EmployeeQuota

					jsonEmployeeQuota, _ := json.Marshal(p.Args["quotas"])
					json.Unmarshal(jsonEmployeeQuota, &employeeQuota)

					res, err := service.EditEmployeeQuotaByID(p.Args["employee_id"].(int), employeeQuota)
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// ResetEmployeeQuota -
func (c *Controller) ResetEmployeeQuota() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "reset_employee_quota", &graphql.Field{
			Type: graphql.NewList(EmployeeQuotaType),
			Description: "Reset employee quota by employee id",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				filter := map[string]interface{}{
					"limit": 30000,
					"offset": 0,
				}

				allEmployees, _, allEmployeesErr := service.GetEmployeeByFilter(filter)
				if allEmployeesErr != nil {
					return nil, allEmployeesErr
				}

				employees := funk.Map(allEmployees, func(employee model.Employee) request.APITMLInitEmployeeQuota {
					employeeInput := request.APITMLInitEmployeeQuota {
						EmployeeID: employee.ID,
						Gender:  strings.ToLower(employee.EmployeePersonnelInfo.Gender),
						CompanyID:  employee.EmployeeGeneralInfo.CompanyID,
					}
					return employeeInput
				}).([]request.APITMLInitEmployeeQuota)

				res, err := service.ResetEmployeeQuotaByID(employees)
				if err != nil {
					return nil, err
				}
				return res, err
			},
		}
	}
}
