package graph

import "github.com/graphql-go/graphql"

// Controller -
type Controller struct{}
type ControllersReturn func() (string, *graphql.Fields)
type ControllerReturn func() (string, *graphql.Field)

// GetQueryMapping -
func (c *Controller) GetQueryMapping() []ControllerReturn {
	return []ControllerReturn{
		// Auth service
		c.BearerAuth(),
		c.PostUserLogin(),
		c.GetToken(),
		c.GetUserInfo(),
		c.ValidateToken(),
		c.GetRoles(),
		c.GetUserAndUserDetail(),
		c.GetUserAndUserDetailList(),
		c.DuplicateFieldInAuthen(),
		c.ValidateSecret(),

		// PA
		c.GetEmployeeList(),
		c.GetEmployeeByPersonalID(),
		c.GetUserDetail(),
		c.GetUserDetailList(),

		// AA
		c.GetCategoryList(),
		c.GetCategory(),
		c.GetAssetByID(),
		c.GetAssetList(),
		c.GetAssetOperation(),
		c.GetAssetOperations(),

		// TM (Attendance)
		c.GetHolidayPresetDetail(),
		c.GetHolidayPresetList(),
		c.GetHolidayCompanyList(),
		c.GetHolidayCompanyDetail(),
		c.GetChargeCodeList(),
		c.GetChargeCodeDetail(),
		c.GetWorkPresetList(),
		c.GetWorkPreset(),
		c.GetWorkReportByEmployeeIDPeriodID(),
		c.GetWorkAdjustmentByEmployeeIDPeriodID(),
		c.GetEmployeeWorkSubmissions(),
		c.GetAvailablePeriod(),
		c.GetWorkReport(),
		c.GetReportCodeSetList(),
		c.GetReportCodeSetDetail(),
		c.GetMyWorkSubmissions(),

		// OM
		c.GetCompanyDetail(),
		c.GetCompanyList(),
		c.GetAddress(),
		c.GetAddressList(),
		c.GetOrganizationLevel(),
		c.GetOrganizationLevels(),
		c.GetOrgUnit(),
		c.GetOrgUnits(),
		c.GetPosition(),
		c.GetPositions(),
		c.GetApprovalConfig(),
		c.GetApprovalConfigList(),
		c.GetApprovalPositions(),
		c.GetRegistrationRequest(),
		c.GetRegistrationRequests(),
		c.GetApprovalConfigBudgetByID(),
		c.CheckDuplicateTaxID(),
		c.GetTerminationRequest(),
		c.GetTerminationRequestList(),
		c.CheckTerminationRequestExist(),
		c.GetRequestFile(),
		c.GetRequestFileList(),
		c.GetInbox(),
		c.GetInboxList(),
		c.GetRegistrationRequestBySecret(),

		//TML (Leave)
		c.GetLeaveTypeDetail(),
		c.GetLeaveTypeList(),
		c.GetLeaveRequestList(),
		c.GetLeaveRequestDetail(),
		c.GetApprovalLeaveRequestList(),
		c.GetEmployeeQuota(),
		c.GetEmployeeQuotas(),

		//RB
		c.GetFoodReimbursementRequestList(),
		c.GetFoodReimbursementRequestByID(),
		c.GetFoodApproveConfiguration(),
		c.GetMyReimbursementRequestList(),
		c.GetReimbursementReportList(),

		c.GetPerdiemApproveConfiguration(),
		c.GetMyPerdiemRate(),
		c.GetPerdiemReimbursementRequestByID(),
		c.GetHR(),

		c.GetTravelApproveConfiguration(),
		c.GetMyTravelExpense(),
		c.GetTravelAllowanceReimbursementRequestByID(),

		c.GetOtherApproveConfiguration(),
		c.GetOthersReimbursementRequestByID(),
	}
}

// GetMutationMapping -
func (c *Controller) GetMutationMapping() []ControllerReturn {
	return []ControllerReturn{
		// AA
		c.PostCategory(),
		c.PutAssetCategory(),
		c.DeleteAssetCategory(),
		c.PostAsset(),
		c.PutAsset(),
		c.DeleteAsset(),
		c.CreateAssetOperation(),
		c.EditAssetOperation(),
		c.CreateAssetOperations(),
		c.EditAssetOperations(),

		// TM (Attendance)
		c.PostHolidayPreset(),
		c.PutHolidayPreset(),
		c.DeleteHolidayPreset(),
		c.PostHolidayCompany(),
		c.PutHolidayCompany(),
		c.DeleteHolidayCompany(),

		c.PostChargeCode(),
		c.PutChargeCode(),
		c.DeleteChargeCode(),

		c.PutWorkPreset(),
		c.PostWorkPreset(),
		c.DeleteWorkPreset(),

		c.PostWorkReport(),
		c.PostWorkAdjustment(),
		c.PutWorkReport(),
		c.PutWorkAdjustment(),
		c.PostSubmitWorkStatus(),

		c.PostReportCodeSet(),
		c.PutReportCodeSet(),
		c.DeleteReportCodeSet(),

		// OM
		c.PostCreateCompany(),
		c.PostCompanyAddress(),
		c.PostOrganizationLevel(),
		c.PutOrganizationLevel(),
		c.DeleteOrganizationLevel(),
		c.PutCompanyDetail(),
		c.PutCompanyAddress(),
		c.DeleteCompanyAddress(),
		c.PostOrgUnit(),
		c.PutOrgUnit(),
		c.DeleteOrgUnit(),
		c.PostPosition(),
		c.PutPosition(),
		c.DeletePosition(),
		c.PostApprovalConfig(),
		c.PutApprovalConfig(),
		c.DeleteApprovalConfig(),
		c.TerminateOrgUnit(),
		c.TransferOrgUnit(),
		c.PostRegistrationRequest(),
		c.PutRegistrationRequest(),
		c.CreateCompanyFromRequest(),
		c.PostTerminationRequest(),
		c.PutTerminationRequest(),
		c.PostRequestFiles(),
		c.DeleteRequestFile(),
		c.PostInbox(),
		c.PostInboxGreeting(),
		c.PostInboxReject(),
		c.PostInboxResubmit(),
		c.PostInboxApprove(),
		c.PostInboxResetPassword(),
		c.PostInboxForgotPassword(),
		c.PutRegistrationRequestByGuest(),
		c.PostCreateUserInbox(),

		//TML (Leave)
		c.CreateLeaveType(),
		c.EditLeaveType(),

		c.PostLeaveRequest(),
		c.EditLeaveRequest(),
		c.CancelLeaveRequest(),

		c.ApproveLeaveRequest(),
		c.RejectLeaveRequest(),

		c.InitEmployeeQuota(),
		c.ResetEmployeeQuota(),
		c.EditEmployeeQuota(),

		// PA
		c.PostEmployee(),
		c.PutEmployee(),
		c.PostUser(),
		c.PutUserDetail(),
		c.DeleteUser(),

		//RB
		c.PostFoodReimbursementRequest(),
		c.PutFoodReimbursementRequest(),
		c.PostFoodApproveConfiguration(),
		c.PutFoodApproveConfiguration(),
		c.PutStatusFoodReimbursementRequest(),

		c.PutPerdiemApproveConfiguration(),
		c.PostPerdiemApproveConfiguration(),
		c.PostTravelApproveConfiguration(),

		c.PostPerDiemReimbursementRequest(),
		c.PutPerDiemReimbursementRequest(),
		c.PostTravelReimbursementRequest(),
		c.PutTravelApproveConfiguration(),
		c.PutTravelAllowanceReimbursementRequest(),
		c.PostOtherApproveConfiguration(),
		c.PutOtherApproveConfiguration(),

		// Authen
		c.PutUser(),
		c.PutUserAndEmployee(),
		c.PostOthersReimbursementRequest(),
		c.PutOthersReimbursementRequest(),
		c.SetPassword(),
		c.ChangePassword(),
		c.ResetPassword(),
		c.ForgotPassword(),
	}
}
