package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	_ "encoding/json"
	"errors"
	_ "strconv"

	_ "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
)

var TravelAllowanceInputType *graphql.InputObject
var TravelReimbursementRequestType *graphql.Object
var TravelAllowanceDetailsType *graphql.Object

func init() {
	TravelReimbursementRequestType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "TravelReimbursementRequest",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"request_id": &graphql.Field{
						Type: graphql.Int,
					},
					"charge_code": &graphql.Field{
						Type: graphql.String,
					},
					"type": &graphql.Field{
						Type: graphql.String,
					},
					"cost": &graphql.Field{
						Type: graphql.Float,
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
					"approver_id": &graphql.Field{
						Type: graphql.Int,
					},
					"processed_date": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"remark": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"approvers": &graphql.Field{
						Type: graphql.NewList(ApproversType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							approval, ok := p.Source.(model.TravelReimbursementRequest)
							if !ok {
								return nil, errors.New("Invalid approval")
							}
							approversResult := GetApproverOrReviewerDetails(ParseApproverIDsToInt(approval.ApproverIDs))
							return approversResult, nil
						},
					},
					"reviewers": &graphql.Field{
						Type: graphql.NewList(ApproversType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {

							reviewer, ok := p.Source.(model.TravelReimbursementRequest)
							if !ok {
								return nil, errors.New("Invalid reviewer")
							}
							reviewersResult := GetApproverOrReviewerDetails(ParseReviewerIDsToInt(reviewer.ReviewerIDs))
							return reviewersResult, nil
						},
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
					"travel_allowances": &graphql.Field{
						Type: graphql.NewList(TravelAllowanceDetailsType),
					},
					"total_distance": &graphql.Field{
						Type: graphql.Float,
					},
					"file_ids": &graphql.Field{
						Type: graphql.NewList(RequestFileType),
					},
					"activity_log": &graphql.Field{
						Type: graphql.NewList(ActivityLogType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							return GetTravelActivityLog(p)
						},
					},
				}
			}),
		},
	)

	TravelAllowanceInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "TravelAllowanceInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"travel_description": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"distance": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
				"expend": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
				"date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	TravelAllowanceDetailsType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "TravelAllowanceDetailsType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"request_id": &graphql.Field{
						Type: graphql.Int,
					},
					"travel_description": &graphql.Field{
						Type: graphql.String,
					},
					"distance": &graphql.Field{
						Type: graphql.Float,
					},
					"expend": &graphql.Field{
						Type: graphql.Float,
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}
func GetTravelActivityLog(p graphql.ResolveParams) (interface{}, error) {

	reimbursementRequest, ok := p.Source.(model.TravelReimbursementRequest)

	if !ok {
		return nil, errors.New("Invalid reviewer")
	}
	logOut := GetLogs(reimbursementRequest.ID)

	return logOut, nil
}
func (c *Controller) PostTravelReimbursementRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_travel_allowance_reimbursement_request", &graphql.Field{
			Description: "Create travel reimbursement request",
			Type:        RequestMessageType,
			Args: graphql.FieldConfigArgument{
				"charge_code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"type": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"cost": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"approver_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"travel_allowances": &graphql.ArgumentConfig{
					Type: graphql.NewList(TravelAllowanceInputType),
				},
				"file_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(RequestFileInputType),
				},
				"reviewers": &graphql.ArgumentConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
				"total_distance": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					p.Args["created_by"] = employee.PersonalID
					p.Args["requester_id"] = employee.PersonalID
					p.Args["updated_by"] = employee.PersonalID
					p.Args["company_id"] = employee.CompanyID

					res, err := service.PostTravelAllowanceReimbursementRequest(p.Args, employee)
					if err != nil {
						return nil, err
					}
					return ReimbursementRequestMessage{
						Id:      res,
						Message: "Create Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetPerdiemReimbursementRequestByID
func (c *Controller) GetTravelAllowanceReimbursementRequestByID() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "travel_allowance_reimbursement_request", &graphql.Field{
			Type: TravelReimbursementRequestType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "read"},
							model.Permission{Module: "my_reimbursement_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetTravelAllowanceReimbursementRequestByID(p.Args["id"].(int))
					return result, err
				}

				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) PutTravelAllowanceReimbursementRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_travel_allowance_reimbursement_request", &graphql.Field{
			Description: "Edit travel allowance reimbursement request",
			Type:        RequestMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"cost": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"approver_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"remark": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"file_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(RequestFileInputType),
				},
				"reviewers": &graphql.ArgumentConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
				"travel_allowances": &graphql.ArgumentConfig{
					Type: graphql.NewList(TravelAllowanceInputType),
				},
				"total_distance": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "edit"},
							model.Permission{Module: "my_reimbursement_request", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					p.Args["created_by"] = employee.PersonalID
					p.Args["request_id"] = employee.PersonalID
					p.Args["updated_by"] = employee.PersonalID
					p.Args["company_id"] = employee.CompanyID

					res, err := service.PutTravelAllowanceReimbursementRequestWithLog(p.Args, employee)

					return ReimbursementRequestMessage{
						Id:      res,
						Message: "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
