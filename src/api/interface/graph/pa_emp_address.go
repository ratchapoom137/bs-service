package graph

import (
	"github.com/graphql-go/graphql"
)

var AddressInfoType *graphql.Object
var AddressInfoInputType *graphql.InputObject
var AddressInfoInputTypeEdit *graphql.InputObject

func init() {
	AddressInfoType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "AdressInfo",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_info_id": &graphql.Field{
						Type: graphql.Int,
					},
					"address_type": &graphql.Field{
						Type: graphql.String,
					},
					"country": &graphql.Field{
						Type: graphql.String,
					},
					"province": &graphql.Field{
						Type: graphql.String,
					},
					"address": &graphql.Field{
						Type: graphql.String,
					},
					"postal_code": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	AddressInfoInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "AdressInfoInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"address_type": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"country": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"province": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"address": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"postal_code": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"created_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"updated_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	AddressInfoInputTypeEdit = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "AddressInfoInputTypeEdit",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"address_type": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"country": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"province": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"address": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"postal_code": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"created_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"updated_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

}
