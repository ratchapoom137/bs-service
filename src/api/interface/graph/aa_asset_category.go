package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"errors"
	"strconv"

	"github.com/graphql-go/graphql"
)

type AssetCategoryResMessage struct {
	Id      int    `json:"id"`
	Message string `json:"message"`
}

var AssetCategoryType *graphql.Object
var AssetCategoryListType *graphql.Object
var AssetCategoryResponseMessage *graphql.Object

func init() {
	AssetCategoryType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "AssetCategory",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	AssetCategoryListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "AssetCategories",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"categories": &graphql.Field{
						Type: graphql.NewList(AssetCategoryType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	AssetCategoryResponseMessage = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "AssetCategoryResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

func (c *Controller) PostCategory() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_asset_category", &graphql.Field{
			Description: "Create new asset Category",
			Type:        AssetCategoryResponseMessage,
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"created_by": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"updated_by": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_category", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userIDstr, userIDOk := p.Context.Value(authorize.UserIDHeaderKey).(string)
					if !userIDOk {
						return model.User{}, errors.New("invalid bearer")
					}
					userID, _ := strconv.Atoi(userIDstr)

					employee, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err
					}
					categoryType := model.AssetCategory{
						Name:      p.Args["name"].(string),
						CreatedBy: userID,
						UpdatedBy: userID,
						CompanyID: employee.CompanyID,
					}
					res, err := service.PostCategory(&categoryType)
					if err != nil {
						return nil, err
					}
					return AssetCategoryResMessage{
						Id:      res,
						Message: "Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetCategoryList -
func (c *Controller) GetCategoryList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "asset_categories", &graphql.Field{
			Type: AssetCategoryListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_category", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					name, _ := p.Args["name"].(string)
					companyID, companyOk := p.Args["company_id"].(int)

					if !limitOk {
						limit = -1
					}

					if !offsetOk {
						offset = -1
					}

					if !companyOk {
						companyID = -1
					}

					_, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}
					companyID = employee.CompanyID
					if companyID == 0 {
						return nil, errors.New("no company id")
					}

					res, total, err := service.GetCategoryList(name, limit, offset, companyID)

					output := model.AssetCategoryList{
						Categories: res,
						Total:      total,
					}

					if err != nil {
						return nil, err
					}

					return output, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetCategory -
func (c *Controller) GetCategory() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "asset_category", &graphql.Field{
			Type: AssetCategoryType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_category", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					res, err := service.GetCategory(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return res, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) PutAssetCategory() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_asset_category", &graphql.Field{
			Type: AssetCategoryResponseMessage,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"updated_by": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_category", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					categoryType := model.AssetCategory{
						ID:        p.Args["id"].(int),
						Name:      p.Args["name"].(string),
						UpdatedBy: p.Args["updated_by"].(int),
					}
					res, err := service.PutAssetCategory(&categoryType)

					if err != nil {
						return nil, err
					}
					return AssetCategoryResMessage{
						Id:      res,
						Message: "Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// DeleteAssetCategory -
func (c *Controller) DeleteAssetCategory() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_asset_category", &graphql.Field{
			Type: AssetCategoryResponseMessage,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_category", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					res, err := service.DeleteAssetCategory(p.Args["id"].(int))

					if err != nil {
						return nil, err
					}
					return AssetCategoryResMessage{
						Message: res,
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
