package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"

	"github.com/graphql-go/graphql"
)

var RegistrationRequestType *graphql.Object
var RegistrationRequestInputType *graphql.InputObject
var RegistrationRequestListType *graphql.Object
var RegistrationRequestResponseMessageType *graphql.Object
var RegistrationRequestFileInputType *graphql.InputObject
var RegistrationRequestFileResponseMessageType *graphql.Object
var RegistrationRequestFileListResponseMessageType *graphql.Object
var RegistrationRequestFileType *graphql.Object
var RegistrationRequestFileListType *graphql.Object

func init() {
	RegistrationRequestType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "RegistrationRequest",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"first_name": &graphql.Field{
						Type: graphql.String,
					},
					"last_name": &graphql.Field{
						Type: graphql.String,
					},
					"email": &graphql.Field{
						Type: graphql.String,
					},
					"phone_number": &graphql.Field{
						Type: graphql.String,
					},
					"company_name_th": &graphql.Field{
						Type: graphql.String,
					},
					"company_name_en": &graphql.Field{
						Type: graphql.String,
					},
					"company_code": &graphql.Field{
						Type: graphql.String,
					},
					"company_email": &graphql.Field{
						Type: graphql.String,
					},
					"company_telephone": &graphql.Field{
						Type: graphql.String,
					},
					"tax_id": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
					"request_date": &graphql.Field{
						Type: graphql.String,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
					"deleted_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	RegistrationRequestInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "RegistrationRequestInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"first_name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"last_name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"email": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"phone_number": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"company_name_th": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"company_name_en": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"company_code": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"company_email": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"company_telephone": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"tax_id": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"description": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"status": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	RegistrationRequestListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "RegistrationRequestList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"registration_requests": &graphql.Field{
						Type: graphql.NewList(RegistrationRequestType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	RegistrationRequestResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "RegistrationRequestResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	RegistrationRequestFileInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "RegistrationRequestFileInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"file_source": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)
	RegistrationRequestFileResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "RegistrationRequestFileResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	RegistrationRequestFileListResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "RegistrationRequestFileListResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"request_files_id": &graphql.Field{
						Type: graphql.NewList(graphql.Int),
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	RegistrationRequestFileType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "RegistrationRequestFile",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"request_id": &graphql.Field{
						Type: graphql.Int,
					},
					"file_source": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	RegistrationRequestFileListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "RegistrationRequestFileList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"request_files": &graphql.Field{
						Type: graphql.NewList(RegistrationRequestFileType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
}

// GetRegistrationRequest -
func (c *Controller) GetRegistrationRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "registration_request", &graphql.Field{
			Type: RegistrationRequestType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "registration_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetRegistrationRequest(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetRegistrationRequestBySecret -
func (c *Controller) GetRegistrationRequestBySecret() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "registration_request_by_secret", &graphql.Field{
			Type: RegistrationRequestListType,
			Args: graphql.FieldConfigArgument{
				"secret": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				filter := map[string]interface{}{}
				filter["secret"], _ = p.Args["secret"].(string)
				filter["limit"] = -1
				filter["offset"] = -1

				result, total, err := service.GetRegistrationRequests(filter)
				if err != nil {
					return nil, err
				}
				return model.RegistrationRequestList{
					RegistrationRequests: result,
					Total:                total,
				}, err
			},
		}
	}
}

// GetRegistrationRequests -
func (c *Controller) GetRegistrationRequests() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "registration_requests", &graphql.Field{
			Type: RegistrationRequestListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"request_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "registration_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					if !limitOk || !offsetOk {
						limit = -1
						offset = -1
					}

					filter := map[string]interface{}{}
					filter["limit"] = limit
					filter["offset"] = offset
					filter["company_name"], _ = p.Args["company_name"].(string)
					filter["request_date"], _ = p.Args["request_date"].(string)
					filter["status"], _ = p.Args["status"].(string)

					result, total, err := service.GetRegistrationRequests(filter)
					if err != nil {
						return nil, err
					}
					return model.RegistrationRequestList{
						RegistrationRequests: result,
						Total:                total,
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostRegistrationRequest -
func (c *Controller) PostRegistrationRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_registration_request", &graphql.Field{
			Type: RegistrationRequestResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"registration_request_input": &graphql.ArgumentConfig{
					Type: RegistrationRequestInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				var request model.RegistrationRequestInput
				jsonRequest, _ := json.Marshal(p.Args["registration_request_input"])
				json.Unmarshal(jsonRequest, &request)

				result, err := service.PostRegistrationRequest(request)
				if err != nil || result == 0 {
					return nil, err
				}

				return model.RegistrationRequestResponseMessage{
					RegistrationRequestID: result,
					Message:               "Create Registration Request Success",
				}, err
			},
		}
	}
}

// PutRegistrationRequest -
func (c *Controller) PutRegistrationRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_registration_request", &graphql.Field{
			Type: RegistrationRequestResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"registration_request_input": &graphql.ArgumentConfig{
					Type: RegistrationRequestInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "registration_request", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var request model.RegistrationRequestInput
					jsonRequest, _ := json.Marshal(p.Args["registration_request_input"])
					json.Unmarshal(jsonRequest, &request)

					result, err := service.PutRegistrationRequest(request)
					if err != nil || result == 0 {
						return nil, err
					}

					return model.RegistrationRequestResponseMessage{
						RegistrationRequestID: result,
						Message:               "Edit Request Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutRegistrationRequestByGuest -
func (c *Controller) PutRegistrationRequestByGuest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_registration_request_by_guest", &graphql.Field{
			Type: RegistrationRequestResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"registration_request_input": &graphql.ArgumentConfig{
					Type: RegistrationRequestInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				var request model.RegistrationRequestInput
				jsonRequest, _ := json.Marshal(p.Args["registration_request_input"])
				json.Unmarshal(jsonRequest, &request)

				result, err := service.PutRegistrationRequest(request)
				if err != nil || result == 0 {
					return nil, err
				}

				return model.RegistrationRequestResponseMessage{
					RegistrationRequestID: result,
					Message:               "Edit Request Success",
				}, err
			},
		}
	}
}

// PostRequestFiles -
func (c *Controller) PostRequestFiles() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_request_files", &graphql.Field{
			Type: RegistrationRequestFileListResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"request_file_input": &graphql.ArgumentConfig{
					Type: graphql.NewList(RegistrationRequestFileInputType),
				},
				"request_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				filter := map[string]interface{}{}
				filter["request_file_input"] = p.Args["request_file_input"]
				filter["request_id"] = p.Args["request_id"]

				result, err := service.PostRequestFiles(filter)
				if err != nil {
					return nil, err
				}

				return model.RegistrationRequestFileListResponseMessage{
					RequestFilesID: result,
					Message:        "Create Request File Success",
				}, nil
			},
		}
	}
}

// GetRequestFile -
func (c *Controller) GetRequestFile() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "get_request_file", &graphql.Field{
			Type: RegistrationRequestFileType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				result, err := service.GetRequestFile(p.Args["id"].(int))
				if err != nil {
					return nil, err
				}

				return result, nil
			},
		}
	}
}

// GetRequestFileList -
func (c *Controller) GetRequestFileList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "get_request_file_list", &graphql.Field{
			Type: RegistrationRequestFileListType,
			Args: graphql.FieldConfigArgument{
				"request_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				result, total, err := service.GetRequestFileList(p.Args["request_id"].(int))
				if err != nil {
					return nil, err
				}

				return model.RegistrationRequestFileList{
					RequestFiles: result,
					Total:        total,
				}, nil
			},
		}
	}
}

// DeleteRequestFile -
func (c *Controller) DeleteRequestFile() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_request_file", &graphql.Field{
			Type: RegistrationRequestFileResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				result, err := service.DeleteRequestFile(p.Args["id"].(int))
				if err != nil {
					return nil, err
				}

				return model.RegistrationRequestFileResponseMessage{
					RequestFileID: result,
					Message:       "Delete Request File Success",
				}, nil
			},
		}
	}
}
