package graph

import (
	"github.com/graphql-go/graphql"
)

// LeaveRequestDate -
var LeaveRequestDateType *graphql.Object

// LeaveRequestDateInputType - For Create
var LeaveRequestDateInputType *graphql.InputObject

func init() {
	LeaveRequestDateType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "LeaveRequestDate",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"leave_request_id": &graphql.Field{
						Type: graphql.Int,
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
					"period": &graphql.Field{
						Type: graphql.String,
					},
					"hour": &graphql.Field{
						Type: graphql.Float,
					},
				}
			}),
		},
	)

	LeaveRequestDateInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "LeaveRequestDateInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"period": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"hour": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
			},
		},
	)
}
