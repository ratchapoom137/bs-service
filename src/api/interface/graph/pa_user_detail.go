package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"

	"github.com/graphql-go/graphql"
)

var UserDetailType *graphql.Object
var UserDetailListType *graphql.Object
var UserDetailsInputType *graphql.InputObject
var UserDetailResponseType *graphql.Object

func init() {
	UserDetailType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserDetail",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"user_id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"first_name": &graphql.Field{
						Type: graphql.String,
					},
					"last_name": &graphql.Field{
						Type: graphql.String,
					},
					"deactivated_date": &graphql.Field{
						Type: graphql.String,
					},
					"expired_date": &graphql.Field{
						Type: graphql.String,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
					"deleted_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	UserDetailListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserDetailList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"user_details": &graphql.Field{
						Type: graphql.NewList(UserDetailType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	UserDetailsInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "UserDetailInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"user_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"first_name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"last_name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"deactivated_date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)
	UserDetailResponseType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserDetailResponse",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

// GetUserDetail -
func (c *Controller) GetUserDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "user_detail", &graphql.Field{
			Type: UserDetailType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "account", Function: "read"},
							model.Permission{Module: "my_account", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					loginUser, _, err := GetUserEmployeeFromSession(p)

					id := loginUser.ID
					if loginUser.Role.Name == "admin" || loginUser.Role.Name == "back_office_admin" {
						id = p.Args["id"].(int)
					}

					result, err := service.GetUserDetailByID(id)
					if err != nil {
						return nil, err
					}
					
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetUserDetailList -
func (c *Controller) GetUserDetailList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "user_details", &graphql.Field{
			Type: UserDetailListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"user_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "account", Function: "read"},
							model.Permission{Module: "my_account", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					loginUser, _, err := GetUserEmployeeFromSession(p)
					
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					user_id, _ := p.Args["user_id"].(int)
					company_id, _ := p.Args["company_id"].(int)
					name, _ := p.Args["name"].(string)
					filter := map[string]interface{}{}

					if !limitOk {
						filter["limit"] = 10
					}
					if limit > 0 {
						filter["limit"] = limit
					}
					if !offsetOk {
						filter["offset"] = 0
					}
					if offset >= 0 {
						filter["offset"] = offset
					}
					if user_id > 0 {
						filter["user_id"] = user_id
					}
					if company_id > 0 {
						filter["company_id"] = company_id
					}
					if name != "" {
						filter["name"] = name
					}
					if loginUser.Role.Name != "admin" && loginUser.Role.Name != "back_office_admin" {
						filter["user_id"] = loginUser.ID
					}
					res, total, err := service.GetUserDetailList(filter)

					return model.UserDetailList{
						UserDetails: res,
						Total:       total,
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//PutUserDetail -
func (c *Controller) PutUserDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_user_detail", &graphql.Field{
			Type: UserDetailResponseType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"user_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"first_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"last_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"deactivated_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "account", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var userDetailInput model.UserDetailInput
					jsonUserDetail, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonUserDetail, &userDetailInput)

					res, err := service.PutUserDetail(userDetailInput)
					if err != nil || res == 0 {
						return nil, err
					}

					return model.UserDetailResponse{
						ID      : res,
						Message : "Edit User Detail Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}


