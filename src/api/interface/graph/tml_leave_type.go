package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/request"
	"api/service"
	"encoding/json"
	"strings"
	"strconv"
	"time"
	"github.com/graphql-go/graphql"
	"github.com/thoas/go-funk"
)

// LeaveType -
var LeaveType *graphql.Object

// LeaveTypeList -
var LeaveTypeList *graphql.Object

func init() {
	LeaveType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "LeaveType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"gender": &graphql.Field{
						Type: graphql.String,
					},
					"marital_status": &graphql.Field{
						Type: graphql.String,
					},
					"is_leave_after_absence": &graphql.Field{
						Type: graphql.Boolean,
					},
					"is_holiday_calculate": &graphql.Field{
						Type: graphql.Boolean,
					},
					"is_per_times": &graphql.Field{
						Type: graphql.Boolean,
					},
					"is_per_year": &graphql.Field{
						Type: graphql.Boolean,
					},
					"times": &graphql.Field{
						Type: graphql.Int,
					},
					"quota_day_per_times": &graphql.Field{
						Type: graphql.Float,
					},
					"quota_hour_per_times": &graphql.Field{
						Type: graphql.Float,
					},
					"quota_day_per_year": &graphql.Field{
						Type: graphql.Float,
					},
					"quota_hour_per_year": &graphql.Field{
						Type: graphql.Float,
					},
					"max_carried_day": &graphql.Field{
						Type: graphql.Float,
					},
					"max_carried_hour": &graphql.Field{
						Type: graphql.Float,
					},
					"approval_config_id": &graphql.Field{
						Type: graphql.Int,
					},
					"charge_code_id": &graphql.Field{
						Type: graphql.Int,
					},
					"status": &graphql.Field{
						Type: graphql.Boolean,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	LeaveTypeList = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "LeaveTypes",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"leave_types": &graphql.Field{
						Type: graphql.NewList(LeaveType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
}

// GetLeaveTypeList -
func (c *Controller) GetLeaveTypeList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "leave_types", &graphql.Field{
			Type: LeaveTypeList,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"year": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"charge_code_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"order_by": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"sort": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_type", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err 
					}

					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					name, _ := p.Args["name"].(string)
					status, _ := p.Args["status"].(string)
					year, yearOk := p.Args["year"].(string)
					orderBy, _ := p.Args["order_by"].(string)
					sort, _ := p.Args["sort"].(string)
					companyID := userResult.CompanyID
					chargeCodeID, _ := p.Args["charge_code_id"].(int)

					if !limitOk {
						limit = 999
					}

					if !offsetOk {
						offset = 0
					}

					if !yearOk {
						currentYear := strconv.Itoa(time.Now().Year())
						year = currentYear
					}

					res, err := service.GetLeaveTypeList(limit, offset, name, status, year, orderBy, sort, companyID, chargeCodeID)
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetLeaveTypeDetail -
func (c *Controller) GetLeaveTypeDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "leave_type", &graphql.Field{
			Type: LeaveType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_type", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					res, err := service.GetLeaveType(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// CreateLeaveType -
func (c *Controller) CreateLeaveType() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_leave_type", &graphql.Field{
			Type:        LeaveType,
			Description: "Create new leave type",
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"gender": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"marital_status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"is_leave_after_absence": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"is_holiday_calculate": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"is_per_times":&graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"is_per_year": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"times": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"quota_day_per_times": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"quota_hour_per_times": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"quota_day_per_year": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"quota_hour_per_year": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"max_carried_day": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"max_carried_hour": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"approval_config_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"charge_code_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Boolean),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_type", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err 
					}
					
					var leaveType model.LeaveType
					jsonLeaveType, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonLeaveType, &leaveType)

					leaveType.CompanyID = userResult.CompanyID

					res, err := service.CreateLeaveType(leaveType)
					if err != nil {
						return nil, err
					}

					companyID := res.CompanyID

					userFilter := map[string]interface{}{
						"limit": 30000,
						"offset": 0,
						"company_id": companyID,
					}

					employeeInfos, _, employeeInfosErr := service.GetEmployeeByFilter(userFilter)
					if employeeInfosErr != nil {
						return nil, employeeInfosErr
					}

					empDetails := funk.Map(employeeInfos, func(e model.Employee) request.TMLEmployeeInfo {
						empDetail := request.TMLEmployeeInfo{
							EmployeeID: e.ID,
							Gender: strings.ToLower(e.EmployeePersonnelInfo.Gender),
						}
						return empDetail
					}).([]request.TMLEmployeeInfo)

					employeeQuotaRequest := request.TMLCreateEmployeeQuota{
							LeaveTypeID: res.ID,
							Employees: empDetails,
					}

					_, employeeQuotaListErr := service.CreateEmployeeQuotaAfterLeaveType(employeeQuotaRequest)
					if employeeQuotaListErr != nil {
						return nil, employeeQuotaListErr
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// EditLeaveType -
func (c *Controller) EditLeaveType() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_leave_type", &graphql.Field{
			Type:        LeaveType,
			Description: "Edit leave type by id",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"gender": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"marital_status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"is_leave_after_absence": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"is_holiday_calculate": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"is_per_times": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"is_per_year": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"times": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"quota_day_per_times": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"quota_hour_per_times": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"quota_day_per_year": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"quota_hour_per_year": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"max_carried_day": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"max_carried_hour": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"approval_config_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"charge_code_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_type", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err 
					}

					var leaveType model.LeaveType
					jsonLeaveType, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonLeaveType, &leaveType)

					companyID := userResult.CompanyID

					userFilter := map[string]interface{}{
						"limit": 30000,
						"offset": 0,
						"company_id": companyID,
					}

					employeeInfos, _, employeeInfosErr := service.GetEmployeeByFilter(userFilter)
					if employeeInfosErr != nil {
						return nil, employeeInfosErr
					}

					empDetails := funk.Map(employeeInfos, func(e model.Employee) request.TMLEmployeeInfo {
						empDetail := request.TMLEmployeeInfo{
							EmployeeID: e.ID,
							Gender: strings.ToLower(e.EmployeePersonnelInfo.Gender),
						}
						return empDetail
					}).([]request.TMLEmployeeInfo)

					employeeQuotaRequest := request.TMLEditEmployeeQuotaAfterLeaveType{
							LeaveType: leaveType,
							Employees: empDetails,
					}

					_, employeeQuotaListErr := service.EditEmployeeQuotasAfterLeaveType(employeeQuotaRequest)
					if employeeQuotaListErr != nil {
						return nil, employeeQuotaListErr
					}

					res, err := service.EditLeaveType(p.Args["id"].(int), leaveType)
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
