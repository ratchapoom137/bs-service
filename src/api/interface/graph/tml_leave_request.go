package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/request"
	"api/service"
	"errors"
	"github.com/graphql-go/graphql"
	"github.com/thoas/go-funk"
	"time"
)

//LeaveRequestType -
var LeaveRequestType *graphql.Object

//LeaveRequestWithPaginationType -
var LeaveRequestWithPaginationType *graphql.Object

func init() {
	LeaveRequestType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "LeaveRequest",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee": &graphql.Field{
						Type: EmployeeType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							empID := p.Source.(model.LeaveRequest).EmployeeID
							res, err := service.GetEmployeeByPersonalID(empID)
							if err != nil {
								return nil, err
							}
							return res, err
						},
					},
					"leave_type_id": &graphql.Field{
						Type: graphql.Int,
					},
					"leave_type": &graphql.Field{
						Type: LeaveType,
					},
					"leave_request_dates": &graphql.Field{
						Type: graphql.NewList(LeaveRequestDateType),
					},
					"start_date": &graphql.Field{
						Type: graphql.String,
					},
					"end_date": &graphql.Field{
						Type: graphql.String,
					},
					"total_date": &graphql.Field{
						Type: graphql.Float,
					},
					"total_hour": &graphql.Field{
						Type: graphql.Float,
					},
					"reason": &graphql.Field{
						Type: graphql.String,
					},
					"participants": &graphql.Field{
						Type: graphql.NewList(ParticipantType),
					},
					"attachments": &graphql.Field{
						Type: graphql.NewList(AttachmentType),
					},
					"approval_tasks": &graphql.Field{
						Type: graphql.NewList(ApprovalTask),
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	LeaveRequestWithPaginationType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "LeaveRequestList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"leave_requests": &graphql.Field{
						Type: graphql.NewList(LeaveRequestType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
}

// GetLeaveRequestList -
func (c *Controller) GetLeaveRequestList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "leave_requests", &graphql.Field{
			Type:        LeaveRequestWithPaginationType,
			Description: "Get all my leave request",
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"leave_type_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"charge_code_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err
					}

					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					leaveTypeID, _ := p.Args["leave_type_id"].(int)
					chargeCodeID, _ := p.Args["charge_code_id"].(int)
					status, _ := p.Args["status"].(string)
					employeeID, employeeIDOk := p.Args["employee_id"].(int)

					if userAccount.(model.User).Role.Name != "hr" && userAccount.(model.User).Role.Name != "manager" {
						employeeID = userResult.PersonalID
					}

					if !employeeIDOk {
						employeeID = userResult.PersonalID
					}

					if !limitOk {
						limit = 999
					}

					if !offsetOk {
						offset = 0
					}

					res, err := service.GetLeaveRequestList(limit, offset, leaveTypeID, chargeCodeID, employeeID, status)

					if err != nil {
						return nil, err
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostLeaveRequest -
func (c *Controller) PostLeaveRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_leave_request", &graphql.Field{
			Type:        LeaveRequestType,
			Description: "Create new leave Request",
			Args: graphql.FieldConfigArgument{
				"leave_type_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"leave_request_dates": &graphql.ArgumentConfig{
					Type: graphql.NewList(LeaveRequestDateInputType),
				},
				"start_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"end_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"total_date": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Float),
				},
				"total_hour": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Float),
				},
				"reason": &graphql.ArgumentConfig{
					Type:         graphql.String,
					DefaultValue: "",
				},
				"participants": &graphql.ArgumentConfig{
					Type: graphql.NewList(ParticipantInputType),
				},
				"attachments": &graphql.ArgumentConfig{
					Type: graphql.NewList(AttachmentInputType),
				},
				"status": &graphql.ArgumentConfig{
					Type:         graphql.String,
					DefaultValue: "pending",
				},
				"created_by": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"updated_by": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_request", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err
					}

					userInfo, userInfoErr := service.GetEmployeeByPersonalID(userResult.PersonalID)
					if userInfoErr != nil {
						return nil, userInfoErr
					}

					leaveTypeInfo, leaveTypeInfoErr := service.GetLeaveType(p.Args["leave_type_id"].(int))
					if leaveTypeInfoErr != nil {
						return nil, leaveTypeInfoErr
					}

					positionID := userInfo.EmployeeGeneralInfo.PositionID
					approvalID := leaveTypeInfo.ApprovalConfigID

					approvalPositions, approvalErr := service.GetApprovalPositions(positionID, approvalID)
					if approvalErr != nil {
						return nil, approvalErr
					}
					
					if approvalPositions == nil {
						return nil, errors.New("Not found approval list from om service")
					}

					var leaveRequestDate []model.LeaveRequestDate
					var participant []model.Participant
					var attachment []model.Attachment
					_, participantCheckOk := p.Args["participants"].([]interface{})
					_, attachmentCheckOk := p.Args["attachments"].([]interface{})

					funk.ForEach(p.Args["leave_request_dates"].([]interface{}), func(item interface{}) {
						dateParsed, _ := time.Parse(time.RFC3339, item.(map[string]interface{})["date"].(string))
						leaveRequestDate = append(leaveRequestDate,
							model.LeaveRequestDate{
								Date:   dateParsed,
								Period: item.(map[string]interface{})["period"].(string),
								Hour:   item.(map[string]interface{})["hour"].(float64),
							},
						)
					})

					if participantCheckOk {
						funk.ForEach(p.Args["participants"].([]interface{}), func(item interface{}) {
							participant = append(participant,
								model.Participant{
									Email: item.(map[string]interface{})["email"].(string),
								},
							)
						})
					}

					if attachmentCheckOk {
						funk.ForEach(p.Args["attachments"].([]interface{}), func(item interface{}) {
							attachment = append(attachment,
								model.Attachment{
									File: item.(map[string]interface{})["file"].(string),
								},
							)
						})
					}

					startDateParsed, err := time.Parse(time.RFC3339, p.Args["start_date"].(string))
					if err != nil {
						return model.LeaveRequest{}, errors.New("Wrong time format")
					}

					endDateParsed, err := time.Parse(time.RFC3339, p.Args["end_date"].(string))
					if err != nil {
						return model.LeaveRequest{}, errors.New("Wrong time format")
					}

					leaveRequest := model.LeaveRequest{
						EmployeeID:        userResult.PersonalID,
						LeaveTypeID:       p.Args["leave_type_id"].(int),
						LeaveRequestDates: leaveRequestDate,
						StartDate:         startDateParsed,
						EndDate:           endDateParsed,
						TotalDate:         p.Args["total_date"].(float64),
						TotalHour:         p.Args["total_hour"].(float64),
						Reason:            p.Args["reason"].(string),
						Participants:      participant,
						Attachments:       attachment,
						Status:            p.Args["status"].(string),
					}

					res, err := service.CreateLeaveRequest(leaveRequest)
					if err != nil {
						return nil, err
					}

					var approvals []request.TMLCreateApprovalList

					funk.ForEach(approvalPositions, func(result model.Position) {
						approvals = append(approvals, request.TMLCreateApprovalList{
							EmployeeID:     result.EmployeeID,
							LeaveRequestID: res.ID,
						})
					})

					approvalList, approvalListErr := service.CreateApprovalList(approvals)
					if approvalListErr != nil {
						return nil, approvalListErr
					}

					res.ApprovalTasks = approvalList

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// EditLeaveRequest -
func (c *Controller) EditLeaveRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_leave_request", &graphql.Field{
			Type:        LeaveRequestType,
			Description: "Edit leave request by id",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"leave_type_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"leave_request_dates": &graphql.ArgumentConfig{
					Type: graphql.NewList(LeaveRequestDateInputType),
				},
				"start_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"end_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"total_date": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Float),
				},
				"total_hour": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Float),
				},
				"reason": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"participants": &graphql.ArgumentConfig{
					Type: graphql.NewList(ParticipantInputType),
				},
				"attachments": &graphql.ArgumentConfig{
					Type: graphql.NewList(AttachmentInputType),
				},
				"status": &graphql.ArgumentConfig{
					Type:         graphql.String,
					DefaultValue: "pending",
				},
				"created_by": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"updated_by": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_request", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err
					}

					userInfo, userInfoErr := service.GetEmployeeByPersonalID(userResult.PersonalID)
					if userInfoErr != nil {
						return nil, userInfoErr
					}

					leaveTypeInfo, leaveTypeInfoErr := service.GetLeaveType(p.Args["leave_type_id"].(int))
					if leaveTypeInfoErr != nil {
						return nil, leaveTypeInfoErr
					}

					positionID := userInfo.EmployeeGeneralInfo.PositionID
					approvalID := leaveTypeInfo.ApprovalConfigID

					approvalPositions, approvalErr := service.GetApprovalPositions(positionID, approvalID)
					if approvalErr != nil {
						return nil, approvalErr
					}
					if approvalPositions == nil {
						return nil, errors.New("Not found approval list from om service")
					}

					var leaveRequestDate []model.LeaveRequestDate
					var participant []model.Participant
					var attachment []model.Attachment
					_, participantCheckOk := p.Args["participants"].([]interface{})
					_, attachmentCheckOk := p.Args["attachments"].([]interface{})

					funk.ForEach(p.Args["leave_request_dates"].([]interface{}), func(item interface{}) {
						dateParsed, _ := time.Parse(time.RFC3339, item.(map[string]interface{})["date"].(string))
						leaveRequestDate = append(leaveRequestDate,
							model.LeaveRequestDate{
								Date:   dateParsed,
								Period: item.(map[string]interface{})["period"].(string),
								Hour:   item.(map[string]interface{})["hour"].(float64),
							},
						)
					})

					if participantCheckOk {
						funk.ForEach(p.Args["participants"].([]interface{}), func(item interface{}) {
							participant = append(participant,
								model.Participant{
									Email: item.(map[string]interface{})["email"].(string),
								},
							)
						})
					}

					if attachmentCheckOk {
						funk.ForEach(p.Args["attachments"].([]interface{}), func(item interface{}) {
							attachment = append(attachment,
								model.Attachment{
									File: item.(map[string]interface{})["file"].(string),
								},
							)
						})
					}

					startDateParsed, err := time.Parse(time.RFC3339, p.Args["start_date"].(string))
					if err != nil {
						return model.LeaveRequest{}, errors.New("Wrong time format")
					}

					endDateParsed, err := time.Parse(time.RFC3339, p.Args["end_date"].(string))
					if err != nil {
						return model.LeaveRequest{}, errors.New("Wrong time format")
					}

					leaveRequest := model.LeaveRequest{
						EmployeeID:        userResult.PersonalID,
						LeaveTypeID:       p.Args["leave_type_id"].(int),
						LeaveRequestDates: leaveRequestDate,
						StartDate:         startDateParsed,
						EndDate:           endDateParsed,
						TotalDate:         p.Args["total_date"].(float64),
						TotalHour:         p.Args["total_hour"].(float64),
						Reason:            p.Args["reason"].(string),
						Participants:      participant,
						Attachments:       attachment,
						Status:            p.Args["status"].(string),
					}

					res, err := service.EditLeaveRequest(p.Args["id"].(int), leaveRequest)
					if err != nil {
						return nil, err
					}

					var approvals []request.TMLCreateApprovalList

					funk.ForEach(approvalPositions, func(result model.Position) {
						approvals = append(approvals, request.TMLCreateApprovalList{
							EmployeeID:     result.EmployeeID,
							LeaveRequestID: res.ID,
						})
					})

					approvalList, approvalListErr := service.CreateApprovalList(approvals)
					if approvalListErr != nil {
						return nil, approvalListErr
					}

					res.ApprovalTasks = approvalList

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// CancelLeaveRequest -
func (c *Controller) CancelLeaveRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "cancel_leave_request", &graphql.Field{
			Type: LeaveRequestType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_request", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					res, err := service.CancelLeaveRequest(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetLeaveRequestDetail -
func (c *Controller) GetLeaveRequestDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "leave_request_detail", &graphql.Field{
			Type: LeaveRequestType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					res, err := service.GetLeaveRequestByID(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
