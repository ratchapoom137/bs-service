package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"

	_ "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
)

//Types
var PerdiemConfigurationType *graphql.Object
var PerdiemApproveConfigurationTableType *graphql.Object

//InputTypes
var PerdiemApproveConfigurationInputTableType *graphql.InputObject
var PerdiemApproveConfigurationInputType *graphql.InputObject

func init() {

	PerdiemApproveConfigurationTableType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "PerdiemApproveConfigurationTableType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_level_id": &graphql.Field{
						Type: graphql.Int,
					},
					"rate": &graphql.Field{
						Type: graphql.Float,
					},
				}
			}),
		},
	)

	PerdiemConfigurationType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "PerdiemConfigurationType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"approve_type_id": &graphql.Field{
						Type: graphql.Int,
					},
					"approve_type_name": &graphql.Field{
						Type: graphql.String,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							approval, ok := p.Source.(model.PerDiemApproveConfiguration)
							if !ok {
								return nil, errors.New("Invalid approval")
							}
							getApprovalName, err := service.GetApprovalConfig(approval.ApproveTypeID)
							if err != nil {
								return nil, err
							}
							return getApprovalName.Name, err
						},
					},
					"perdiem_table": &graphql.Field{
						Type: graphql.NewList(PerdiemApproveConfigurationTableType),
					},
				}
			}),
		},
	)

	PerdiemApproveConfigurationInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "PerdiemApproveConfigurationInputType",
			Fields: graphql.InputObjectConfigFieldMap{
				"approve_type_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	PerdiemApproveConfigurationInputTableType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "PerdiemApproveConfigurationInputTableType",
			Fields: graphql.InputObjectConfigFieldMap{
				"employee_level_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"rate": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

}

//PostPerdiemApproveConfiguration
func (c *Controller) PostPerdiemApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_perdiem_approve_configuration", &graphql.Field{
			Description: "Create new perdiem approve configuration",
			Type:        PerdiemConfigurationType,
			Args: graphql.FieldConfigArgument{
				"perdiem_approve_configuration_input": &graphql.ArgumentConfig{
					Type: PerdiemApproveConfigurationInputType,
				},
				"perdiem_table": &graphql.ArgumentConfig{
					Type: graphql.NewList(PerdiemApproveConfigurationInputTableType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					var perdiemApproveConfigurationInput model.PerDiemApproveConfiguration
					jsonData, _ := json.Marshal(p.Args["perdiem_approve_configuration_input"])
					json.Unmarshal(jsonData, &perdiemApproveConfigurationInput)

					perdiemApproveConfigurationInput.CompanyID = employee.CompanyID
					perdiemApproveConfigurationInput.CreatedBy = employee.PersonalID

					var perdiemApproveConfigurationInputTables []model.PerDiemApproveConfigurationTable
					for _, item := range p.Args["perdiem_table"].([]interface{}) {
						var perdiemApproveConfigurationInputTable model.PerDiemApproveConfigurationTable
						jsonDatatable, _ := json.Marshal(item)
						json.Unmarshal(jsonDatatable, &perdiemApproveConfigurationInputTable)
						perdiemApproveConfigurationInputTable.CompanyID = employee.CompanyID
						perdiemApproveConfigurationInputTable.CreatedBy = employee.PersonalID
						perdiemApproveConfigurationInputTables = append(perdiemApproveConfigurationInputTables, perdiemApproveConfigurationInputTable)
					}

					res, err := service.CreatePerdiemApproveConfiguration(&perdiemApproveConfigurationInput, perdiemApproveConfigurationInputTables)
					if err != nil {
						return nil, err
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetPerdiemApproveConfiguration -
func (c *Controller) GetPerdiemApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "perdiem_approve_configuration", &graphql.Field{
			Description: "Get perdiem configuration",
			Type:        PerdiemConfigurationType,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_type", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					res, err := service.GetPerdiemConfiguration(employee.CompanyID)
					
					if err != nil {
						return nil, err
					}

					return res, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//PutPerdiemApproveConfiguration
func (c *Controller) PutPerdiemApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_perdiem_approve_configuration", &graphql.Field{
			Description: "Edit new perdiem approve configuration",
			Type:        PerdiemConfigurationType,
			Args: graphql.FieldConfigArgument{
				"perdiem_approve_configuration_input": &graphql.ArgumentConfig{
					Type: PerdiemApproveConfigurationInputType,
				},
				"perdiem_table": &graphql.ArgumentConfig{
					Type: graphql.NewList(PerdiemApproveConfigurationInputTableType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_type", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					var perdiemApproveConfigurationInput model.PerDiemApproveConfiguration
					jsonData, _ := json.Marshal(p.Args["perdiem_approve_configuration_input"])
					json.Unmarshal(jsonData, &perdiemApproveConfigurationInput)

					perdiemApproveConfigurationInput.CompanyID = employee.CompanyID
					perdiemApproveConfigurationInput.UpdatedBy = employee.PersonalID

					var perdiemApproveConfigurationInputTables []model.PerDiemApproveConfigurationTable
					for _, item := range p.Args["perdiem_table"].([]interface{}) {
						var perdiemApproveConfigurationInputTable model.PerDiemApproveConfigurationTable
						jsonDatatable, _ := json.Marshal(item)
						json.Unmarshal(jsonDatatable, &perdiemApproveConfigurationInputTable)
						perdiemApproveConfigurationInputTable.CompanyID = employee.CompanyID
						perdiemApproveConfigurationInputTable.UpdatedBy = employee.PersonalID
						perdiemApproveConfigurationInputTables = append(perdiemApproveConfigurationInputTables, perdiemApproveConfigurationInputTable)
					}

					res, err := service.PutPerdiemApproveConfiguration(&perdiemApproveConfigurationInput, perdiemApproveConfigurationInputTables)
					if err != nil {
						return nil, err
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetMyPerdiemRate
func (c *Controller) GetMyPerdiemRate() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "my_perdiem_rate", &graphql.Field{
			Description: "Get my perdiem rate",
			Type:        PerdiemConfigurationType,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "my_perdiem_rate", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					res, err := service.GetMyPerdiemRate(employee.CompanyID)
					
					if err != nil {
						return nil, err
					}

					return res, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}