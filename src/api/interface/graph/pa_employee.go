package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"
	"strconv"
	// "time"

	"github.com/graphql-go/graphql"
	// "github.com/davecgh/go-spew/spew"
)

//Types
var EmployeeType *graphql.Object
var PersonnelInfoType *graphql.Object
var GeneralInfoType *graphql.Object
var EmergencyContactInfoType *graphql.Object
var ContactInfoType *graphql.Object
var MobilePhoneInfoType *graphql.Object
var EmployeeListType *graphql.Object
var ChangelogEditByType *graphql.Object
var ChangelogFieldType *graphql.Object
var ChangelogRecordType *graphql.Object
var ChangelogListType *graphql.Object
var ChangelogContentType *graphql.Object

//InputTypes
var PersonnelInfoInputType *graphql.InputObject
var GeneralInfoInputType *graphql.InputObject
var EmergencyContactInfoInputType *graphql.InputObject
var ContactInfoInputType *graphql.InputObject
var MobilePhoneInfoInputType *graphql.InputObject
var PersonnelInfoInputTypeEdit *graphql.InputObject
var ContactInfoInputTypeEdit *graphql.InputObject
var MobilePhoneInfoInputTypeEdit *graphql.InputObject
var EmergencyContactInfoInputTypeEdit *graphql.InputObject
var GeneralInfoInputTypeEdit *graphql.InputObject

func init() {
	//Type
	PersonnelInfoType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "PersonnelInfo",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_info_id": &graphql.Field{
						Type: graphql.Int,
					},
					"titlename_th": &graphql.Field{
						Type: graphql.String,
					},
					"firstname_th": &graphql.Field{
						Type: graphql.String,
					},
					"lastname_th": &graphql.Field{
						Type: graphql.String,
					},
					"titlename_en": &graphql.Field{
						Type: graphql.String,
					},
					"firstname_en": &graphql.Field{
						Type: graphql.String,
					},
					"lastname_en": &graphql.Field{
						Type: graphql.String,
					},
					"date_of_birth": &graphql.Field{
						Type: graphql.String,
					},
					"gender": &graphql.Field{
						Type: graphql.String,
					},
					"height": &graphql.Field{
						Type: graphql.Float,
					},
					"weight": &graphql.Field{
						Type: graphql.Float,
					},
					"blood_type": &graphql.Field{
						Type: graphql.String,
					},
					"pregnancy_status": &graphql.Field{
						Type: graphql.String,
					},
					"nationality": &graphql.Field{
						Type: graphql.String,
					},
					"religion": &graphql.Field{
						Type: graphql.String,
					},
					"marital_status": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	GeneralInfoType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "GeneralInfoType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_info_id": &graphql.Field{
						Type: graphql.Int,
					},
					"company_employee_id": &graphql.Field{
						Type: graphql.String,
					},
					"type_of_staff": &graphql.Field{
						Type: graphql.String,
					},
					"employee_level": &graphql.Field{
						Type: graphql.String,
					},
					"work_type_id": &graphql.Field{
						Type: graphql.Int,
					},
					"work_type": &graphql.Field{
						Type: WorkPresetType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							workPresetID := p.Source.(*model.EmployeeGeneralInfo).WorkTypeID
							res, _ := service.GetWorkPresetDetail(workPresetID)
							return res, nil
						},
					},
					"probation_end_date": &graphql.Field{
						Type: graphql.String,
					},
					"position_id": &graphql.Field{
						Type: graphql.Int,
					},
					"position": &graphql.Field{
						Type: PositionType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							positionID := p.Source.(*model.EmployeeGeneralInfo).PositionID
							res, _ := service.GetPosition(positionID)
							return res, nil
						},
					},
					"org_unit_id": &graphql.Field{
						Type: graphql.Int,
					},
					"org_unit": &graphql.Field{
						Type: OrgUnitType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							orgUnitID := p.Source.(*model.EmployeeGeneralInfo).OrgUnitID
							res, _ := service.GetOrgUnit(orgUnitID)
							return res, nil
						},
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"company": &graphql.Field{
						Type: CompanyType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							companyID := p.Source.(*model.EmployeeGeneralInfo).CompanyID
							res, _ := service.GetCompanyDetail(companyID)
							return res, nil
						},
					},
					"company_branch_id": &graphql.Field{
						Type: graphql.Int,
					},
					"company_branch": &graphql.Field{
						Type: CompanyAddressType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							companyBranchID := p.Source.(*model.EmployeeGeneralInfo).CompanyBranchID
							res, _ := service.GetAddress(companyBranchID)
							return res, nil
						},
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	EmergencyContactInfoType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "EmergencyContactInfo",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_info_id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"relative": &graphql.Field{
						Type: graphql.String,
					},
					"mobile_phone_number": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	ContactInfoType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ContactInfo",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_info_id": &graphql.Field{
						Type: graphql.Int,
					},
					"company_email": &graphql.Field{
						Type: graphql.String,
					},
					"contact_info_mobile_phone_info": &graphql.Field{
						Type: graphql.NewList(MobilePhoneInfoType),
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	EmployeeType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Employee",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"user_id": &graphql.Field{
						Type: graphql.Int,
					},
					"citizen_id": &graphql.Field{
						Type: graphql.String,
					},
					"personnel_info": &graphql.Field{
						Type: PersonnelInfoType,
					},
					"address_info": &graphql.Field{
						Type: graphql.NewList(AddressInfoType),
					},
					"contact_info": &graphql.Field{
						Type: ContactInfoType,
					},
					"emergency_contact_info": &graphql.Field{
						Type: graphql.NewList(EmergencyContactInfoType),
					},
					"general_info": &graphql.Field{
						Type: GeneralInfoType,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
					"start_date": &graphql.Field{
						Type: graphql.String,
					},
					"end_date": &graphql.Field{
						Type: graphql.String,
					},
					"changelogs": &graphql.Field{
						Type: ChangelogListType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							contentId := p.Source.(model.Employee).ID

							res, total, err := service.GetChangelogList(service.EMPLOYEE_CONTENT, contentId)
							output := service.ChangelogList{
								Changelog: res,
								Total:     total,
							}
							return output, err
						},
					},
				}
			}),
		},
	)

	EmployeeListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Employees",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"employee_list": &graphql.Field{
						Type: graphql.NewList(EmployeeType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	ChangelogEditByType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ChangelogEditByType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.String,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	ChangelogFieldType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ChangelogFieldType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"field": &graphql.Field{
						Type: graphql.String,
					},
					"from": &graphql.Field{
						Type: graphql.String,
					},
					"to": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	ChangelogRecordType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ChangelogRecordType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"hash": &graphql.Field{
						Type: graphql.String,
					},
					"action": &graphql.Field{
						Type: graphql.String,
					},
					"edit_by": &graphql.Field{
						Type: ChangelogEditByType,
					},
					"timestamp": &graphql.Field{
						Type: graphql.String,
					},
					"logs": &graphql.Field{
						Type: graphql.NewList(ChangelogFieldType),
					},
				}
			}),
		},
	)

	ChangelogListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ChangelogListType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"changelog": &graphql.Field{
						Type: ChangelogContentType,
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	ChangelogContentType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ChangelogContentType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.String,
					},
					"content_type": &graphql.Field{
						Type: graphql.String,
					},
					"content_id": &graphql.Field{
						Type: graphql.Int,
					},
					"record": &graphql.Field{
						Type: graphql.NewList(ChangelogRecordType),
					},
				}
			}),
		},
	)

	//InputType
	PersonnelInfoInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "PeronnelInfoInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"titlename_th": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"firstname_th": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"lastname_th": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"titlename_en": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"firstname_en": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"lastname_en": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"date_of_birth": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"gender": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	//Input personal type for edit action
	PersonnelInfoInputTypeEdit = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "PersonnelInfoInputTypeEdit",
			Fields: graphql.InputObjectConfigFieldMap{
				"titlename_th": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"firstname_th": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"lastname_th": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"titlename_en": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"firstname_en": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"lastname_en": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"date_of_birth": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"gender": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"height": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
				"weight": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
				"blood_type": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"pregnancy_status": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"nationality": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"religion": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"marital_status": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	GeneralInfoInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "GeneralInfoInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"company_employee_id": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"type_of_staff": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"employee_level": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"work_type_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"position_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"org_unit_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"company_branch_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)
	EmergencyContactInfoInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "EmergencyContactInfoInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"relative": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"mobile_phone_numbe": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)
	MobilePhoneInfoInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "MobilePhoneInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"mobile_phone_number": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	ContactInfoInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "ContactInfoInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"company_email": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"contact_info_mobile_phone_info": &graphql.InputObjectFieldConfig{
					Type: graphql.NewList(MobilePhoneInfoInputType),
				},
			},
		},
	)
	MobilePhoneInfoType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "MobilePhoneInfo",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"mobile_info_id": &graphql.Field{
						Type: graphql.Int,
					},
					"mobile_phone_number": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	MobilePhoneInfoInputTypeEdit = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "MobilePhoneInputTypeEdit",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"mobile_phone_number": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"created_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	ContactInfoInputTypeEdit = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "ContactInfoInputTypeEdit",
			Fields: graphql.InputObjectConfigFieldMap{
				"company_email": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"contact_info_mobile_phone_info": &graphql.InputObjectFieldConfig{
					Type: graphql.NewList(MobilePhoneInfoInputTypeEdit),
				},
			},
		},
	)

	EmergencyContactInfoInputTypeEdit = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "EmergencyContactInfoInputTypeEdit",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"relative": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"mobile_phone_number": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	GeneralInfoInputTypeEdit = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "GeneralInfoInputTypeEdit",
			Fields: graphql.InputObjectConfigFieldMap{
				"company_employee_id": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"type_of_staff": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"employee_level": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"work_type_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"probation_end_date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"position_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"org_unit_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"company_branch_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"start_date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

}

// PostEmployee -
func (c *Controller) PostEmployee() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_employee", &graphql.Field{
			Type:        EmployeeType,
			Description: "Create new employee",
			Args: graphql.FieldConfigArgument{
				"citizen_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"user": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(UserInputType),
				},
				"personnel_info": &graphql.ArgumentConfig{
					Type: PersonnelInfoInputType,
				},
				"address_info": &graphql.ArgumentConfig{
					Type: graphql.NewList(AddressInfoInputType),
				},
				"contact_info": &graphql.ArgumentConfig{
					Type: ContactInfoInputType,
				},
				"emergency_contact_info": &graphql.ArgumentConfig{
					Type: graphql.NewList(EmergencyContactInfoInputType),
				},
				"general_info": &graphql.ArgumentConfig{
					Type: GeneralInfoInputType,
				},
				"start_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					loginUser, userDetail, err := GetUserEmployeeFromSession(p)
					
					var res model.Employee

					var employeeParsed model.Employee
					jsonData, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonData, &employeeParsed)

					user, _ := p.Args["user"].(map[string]interface{})
					var userParsed service.UserInput
					jsonUserData, _ := json.Marshal(user)
					json.Unmarshal(jsonUserData, &userParsed)

					if loginUser.Role.Name != "admin" && err != nil {
						return nil, err
					}

					if loginUser.Role.Name == "employee" {
						return nil, err
					}
					if loginUser.Role.Name == "hr" || loginUser.Role.Name == "admin" {
						res, err = service.CreateEmployee(&employeeParsed, userParsed, userDetail)
						if err != nil {
							return nil, err
						}
						service.InitEmployeeQuotaByID(res.ID, res.EmployeePersonnelInfo.Gender, res.EmployeeGeneralInfo.CompanyID)
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetEmployeeByPersonalID information by personal id -
func (c *Controller) GetEmployeeByPersonalID() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "employee", &graphql.Field{
			Type: EmployeeType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee", Function: "read"},
							model.Permission{Module: "profile", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userIDstr, userIDOk := p.Context.Value(authorize.UserIDHeaderKey).(string)
					userID, _ := strconv.Atoi(userIDstr)
					var res model.Employee

					if !userIDOk {
						return model.User{}, errors.New("invalid bearer")
					}
					response, err := service.GetUserFromID(userID)

					id, idOk := p.Args["id"].(int)

					if !idOk {
						id = 0
					}

					result, err := service.GetEmployeeByUserID(userID)
					if response.Role.Name != "admin" && err != nil {
						return nil, err
					}

					userInfo, _ := service.GetEmployeeByPersonalID(result.PersonalID)

					if response.Role.Name == "employee" {
						return userInfo, err
					}
					if !idOk {
						return userInfo, nil
					}

					res, err = service.GetEmployeeByPersonalID(id)
					if err != nil {
						return nil, err
					}

					if res.EmployeeGeneralInfo.CompanyID != result.CompanyID && response.Role.Name != "admin" {
						return nil, err
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetEmployeeList -
func (c *Controller) GetEmployeeList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "employees", &graphql.Field{
			Type: EmployeeListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"company_employee_id": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"gender": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"position_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"point_of_time": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"citizen_id": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"company_email": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"work_type_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee", Function: "search"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userIDstr, userIDOk := p.Context.Value(authorize.UserIDHeaderKey).(string)
					userID, _ := strconv.Atoi(userIDstr)

					if !userIDOk {
						return model.User{}, errors.New("invalid bearer")
					}
					response, err := service.GetUserFromID(userID)
					if err != nil {
						return nil, err
					}

					limit, limitOk := p.Args["limit"].(int)
					offset := p.Args["offset"].(int)
					name, _ := p.Args["name"].(string)
					employee_id, _ := p.Args["company_employee_id"].(string)
					gender, _ := p.Args["gender"].(string)
					status, _ := p.Args["status"].(string)
					position_id, _ := p.Args["position_id"].(int)
					point_of_time, _ := p.Args["point_of_time"].(string)
					citizen_id, _ := p.Args["citizen_id"].(string)
					company_email, _ := p.Args["company_email"].(string)
					work_type_id, _ := p.Args["work_type_id"].(int)
					filter := map[string]interface{}{}

					result, err := service.GetEmployeeByUserID(userID)
					if response.Role.Name != "admin" && err != nil {
						return nil, err
					}

					if !limitOk {
						limit = 10
					}
					if limit > 0 {
						filter["limit"] = limit
					}
					if offset >= 0 {
						filter["offset"] = offset
					}
					if name != "" {
						filter["name"] = name
					}
					if employee_id != "" {
						filter["company_employee_id"] = employee_id
					}
					if result.CompanyID > 0 {
						filter["company_id"] = result.CompanyID
					}
					if gender != "" {
						filter["gender"] = gender
					}
					if status != "" {
						filter["status"] = status
					}
					if position_id > 0 {
						filter["position_id"] = position_id
					}
					if point_of_time != "" {
						filter["point_of_time"] = point_of_time
					}
					if citizen_id != "" {
						filter["citizen_id"] = citizen_id
					}
					if company_email != "" {
						filter["company_email"] = company_email
					}
					if work_type_id > 0 {
						filter["work_type_id"] = work_type_id
					}

					res, total, err := service.GetEmployeeByFilter(filter)

					output := service.EmployeeList{
						Employees: res,
						Total:     total,
					}
					return output, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//Put
func (c *Controller) PutEmployee() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_employee", &graphql.Field{
			Type:        EmployeeType,
			Description: "Edit employee information",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"personnel_info": &graphql.ArgumentConfig{
					Type: PersonnelInfoInputTypeEdit,
				},
				"contact_info": &graphql.ArgumentConfig{
					Type: ContactInfoInputTypeEdit,
				},
				"address_info": &graphql.ArgumentConfig{
					Type: graphql.NewList(AddressInfoInputTypeEdit),
				},
				"emergncy_contact_info": &graphql.ArgumentConfig{
					Type: graphql.NewList(EmergencyContactInfoInputTypeEdit),
				},
				"general_info": &graphql.ArgumentConfig{
					Type: GeneralInfoInputTypeEdit,
				},
				"end_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					loginUser, userDetail, userErr := GetUserEmployeeFromSession(p)
					if loginUser.Role.Name != "admin" && userErr != nil {
						return nil, userErr
					}

					var employee model.Employee
					id, _ := p.Args["id"].(int)
					inputPersonalInfo, inputPersonalInfoOK := p.Args["personnel_info"].(map[string]interface{})
					inputContactInfo, inputContactInfoOK := p.Args["contact_info"].(map[string]interface{})
					inputAddressInfo, inputAddressInfoOK := p.Args["address_info"].([]interface{})
					inputEmergencyContactInfo, inputEmergencyContactInfoOK := p.Args["emergncy_contact_info"].([]interface{})
					inputGeneralInfo, inputGeneralInfoInfoOK := p.Args["general_info"].(map[string]interface{})
					_, inputEndDateOK := p.Args["end_date"].(string)

					var res interface{}
					var err error

					if loginUser.Role.Name == "employee" {
						return nil, userErr
					}
					if inputPersonalInfoOK {
						res, err = service.PutEmployeePersonalInformationWithChangelog(id, inputPersonalInfo, userDetail)
						// var resParsed model.EmployeePersonnelInfo
						// jsonResData, _ := json.Marshal(res)
						// json.Unmarshal(jsonResData, &resParsed)
						// employee.EmployeePersonnelInfo = &resParsed
						return res, err
					}
					if inputContactInfoOK {
						res, err = service.PutEmployeeContactInformation(id, inputContactInfo, userDetail)
						var resParsed model.EmployeeContactInfo
						jsonResData, _ := json.Marshal(res)
						json.Unmarshal(jsonResData, &resParsed)
						employee.EmployeeContactInfo = &resParsed
					}
					if inputAddressInfoOK {
						res, err = service.PutEmployeeAddressInformation(id, inputAddressInfo, userDetail)
						var resParsed []model.EmployeeAddressInfo
						jsonResData, _ := json.Marshal(res)
						json.Unmarshal(jsonResData, &resParsed)
						employee.EmployeeAddressInfo = resParsed
					}
					if inputEmergencyContactInfoOK {
						res, err = service.PutEmployeeEmergencyContactInformation(id, inputEmergencyContactInfo, userDetail)
						var resParsed []model.EmployeeEmergencyContactInfo
						jsonResData, _ := json.Marshal(res)
						json.Unmarshal(jsonResData, &resParsed)
						employee.EmployeeEmergencyContactInfo = resParsed
					}
					if inputEndDateOK {
						res, err = service.PutEmployeeByAction(id, p.Args, userDetail)
						var resParsed model.Employee
						jsonResData, _ := json.Marshal(res)
						json.Unmarshal(jsonResData, &resParsed)
						employee = resParsed
					}
					if inputGeneralInfoInfoOK {
						res, err = service.PutEmployeeGeneralInformation(id, inputGeneralInfo, userDetail)
						var resParsed model.EmployeeGeneralInfo
						jsonResData, _ := json.Marshal(res)
						json.Unmarshal(jsonResData, &resParsed)
						employee.EmployeeGeneralInfo = &resParsed
					}

					if err != nil {
						return nil, err
					}

					return employee, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
