package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"

	_ "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
)

var ReimbursementReportType *graphql.Object
var ReimbursementReportListType *graphql.Object

func init() {
	ReimbursementReportType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ReimbursementReport",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"code": &graphql.Field{
						Type: graphql.String,
					},
					"food": &graphql.Field{
						Type: graphql.NewList(graphql.Float),
					},
					"travel": &graphql.Field{
						Type: graphql.NewList(graphql.Float),
					},
					"perdiem": &graphql.Field{
						Type: graphql.NewList(graphql.Float),
					},
					"other": &graphql.Field{
						Type: graphql.NewList(graphql.Float),
					},
				}
			}),
		},
	)

	ReimbursementReportListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ReimbursementReportTypeList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"total": &graphql.Field{
						Type: graphql.Int,
					},
					"reimbursement_requests": &graphql.Field{
						Type: graphql.NewList(ReimbursementReportType),
					},
				}
			}),
		},
	)
}

//GetMyReimbursementRequestList
func (c *Controller) GetReimbursementReportList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "reimbursement_report", &graphql.Field{
			Type: ReimbursementReportListType,
			Args: graphql.FieldConfigArgument{
				"start_period": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"end_period": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"search_code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_report", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					queryObject := service.ReimbursementReportQuery{
						StartPeriod: p.Args["start_period"].(string),
						EndPeriod:   p.Args["end_period"].(string),
						CompanyID:   employee.CompanyID,
					}

					if p.Args["search_code"] != nil {
						queryObject.SearchCode = p.Args["search_code"].(string)
					}

					res, total, err := service.GetReimbursementReportList(queryObject)

					output := model.ReimbursementReportList{
						Reports: res,
						Total:   total,
					}

					if err != nil {
						return nil, err
					}

					return output, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
