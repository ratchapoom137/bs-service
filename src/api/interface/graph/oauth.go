package graph

import (
	"api/service"
	"os"

	"github.com/graphql-go/graphql"
)

var AccessTokenType, AccessTokenInfoType *graphql.Object

func init() {
	AccessTokenType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OAuthToken",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"access_token": &graphql.Field{
						Type: graphql.String,
					},
					"refresh_token": &graphql.Field{
						Type: graphql.String,
					},
					"expires_in": &graphql.Field{
						Type: graphql.Int,
					},
					"scope": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	AccessTokenInfoType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OAuthTokenInfo",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"user_id": &graphql.Field{
						Type: graphql.Int,
					},
					"client_id": &graphql.Field{
						Type: graphql.String,
					},
					"expires_in": &graphql.Field{
						Type: graphql.Int,
					},
					"scope": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

// GetToken -
func (c *Controller) GetToken() ControllerReturn {
	// http://go-oauth-server:9096/token?grant_type=authorization_code&client_id=admin_hrms&client_secret=999999&code=YOFOBBCMOGEU-MRVYZQREG&redirect_uri=http://hrms-admin:1100/oauth2
	// access_token: "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhZG1pbl9ocm1zIiwiZXhwIjoxNTY1MzUyNjk0LCJzdWIiOiIwMDAwMDEifQ.i-DxuLRg_CfC32ucTG_4KMx_iQtLYhtSSQ6e4Uv4wSUe7otPCXgC_eXigP40jZdKzF8mnliw-s5PrlGyAvDPAQ",
	// expires_in: 7200,
	// refresh_token: "VFQUJKYWUP-PFEPB-KR6ZQ",
	// scope: "admin",
	// token_type: "Bearer"
	return func() (string, *graphql.Field) {
		return "token", &graphql.Field{
			Type: AccessTokenType,
			Args: graphql.FieldConfigArgument{
				"code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"redirect_uri": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},

			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				code, _ := p.Args["code"].(string)
				redirectURI, _ := p.Args["redirect_uri"].(string)
				ClientID := os.Getenv("OAUTH_CLIENT_ID")
				ClientSecret := os.Getenv("OAUTH_CLIENT_SECRET")

				response, err := service.GetTokenFromAuthCode(
					code,
					redirectURI,
					ClientID,
					ClientSecret,
				)

				return response, err
			},
		}
	}
}

// ValidateToken -
func (c *Controller) ValidateToken() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "tokeninfo", &graphql.Field{
			Type: AccessTokenInfoType,
			Args: graphql.FieldConfigArgument{
				"access_token": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				accessToken := p.Args["access_token"].(string)
				response, err := service.ValidateTokenFromAuthCode(
					accessToken,
				)
				if response.UserID == "" {
					response.UserID = "0"
				}
				return response, err
			},
		}
	}
}
