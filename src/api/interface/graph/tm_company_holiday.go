package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"errors"
	"strconv"

	// "github.com/davecgh/go-spew/spew"

	"github.com/graphql-go/graphql"
	"github.com/thoas/go-funk"
)

// HolidayCompanyType -
var HolidayCompanyType *graphql.Object

// HolidayCompanyListType -
var HolidayCompanyListType *graphql.Object

// HolidayDetailType -
var HolidayDetailType *graphql.Object

// HolidayDetailInputType -
var HolidayDetailInputType *graphql.InputObject

func init() {
	HolidayDetailInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "HolidayCompanyInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"updated_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"created_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	HolidayDetailType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "HolidayDetail",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	HolidayCompanyType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "CompanyHoliday",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.String,
					},
					"holidaycompany_name": &graphql.Field{
						Type: graphql.String,
					},
					"holiday_amount": &graphql.Field{
						Type: graphql.Int,
					},
					"holidaydetails": &graphql.Field{
						Type: graphql.NewList(HolidayDetailType),
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	HolidayCompanyListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "CompanyHolidays",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"company_holidays": &graphql.Field{
						Type: graphql.NewList(HolidayCompanyType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							companyHolidayList, ok := p.Source.(service.HolidayCompanyList)
							companyHoliday := companyHolidayList.HolidayCompany

							if !ok {
								return nil, errors.New("Invalid company holiday")
							}

							var result []service.HolidayCompany
							result = funk.Map(companyHoliday, func(ch service.HolidayCompany) service.HolidayCompany {
								id := strconv.Itoa(ch.ID)

								result, err := service.GetHolidayCompanyDetail(id)
								if err != nil {
									return service.HolidayCompany{}
								}
								return result

							}).([]service.HolidayCompany)
							return result, nil
						},
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
}

// GetHolidayCompanyDetail -
func (c *Controller) GetHolidayCompanyDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "company_holiday", &graphql.Field{
			Type: HolidayCompanyType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company_holiday", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetHolidayCompanyDetail(p.Args["id"].(string))
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)

			},
		}
	}
}

// GetHolidayCompanyList -
func (c *Controller) GetHolidayCompanyList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "company_holidays", &graphql.Field{
			Type: HolidayCompanyListType,
			Args: graphql.FieldConfigArgument{
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company_holiday", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}

					holidayQuery := service.CompanyHolidayQuery{
						Limit:  p.Args["limit"].(int),
						Offset: p.Args["offset"].(int),
					}

					if p.Args["name"] != nil {
						holidayQuery.Name = p.Args["name"].(string)
					}
					if userAccount.(model.User).Role.Name != "admin" {
						holidayQuery.CompanyID = userInfo.CompanyID

					}
					if userAccount.(model.User).Role.Name == "admin" {
						if p.Args["company_id"] != nil {
							holidayQuery.CompanyID = p.Args["company_id"].(int)
						}
					}

					return service.GetHolidayCompanyList(holidayQuery)
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostHolidayCompany -
func (c *Controller) PostHolidayCompany() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_company_holiday", &graphql.Field{
			Description: "Create new Holiday Company",
			Type:        HolidayCompanyType,
			Args: graphql.FieldConfigArgument{
				"holidaycompany_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"holiday_amount": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"holidaydetails": &graphql.ArgumentConfig{
					Type: graphql.NewList(HolidayDetailInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company_holiday", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))

					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}
					p.Args["created_by"] = userInfo.PersonalID
					p.Args["updated_by"] = userInfo.PersonalID
					p.Args["company_id"] = userInfo.CompanyID

					result, err := service.PostHolidayCompany(p.Args)
					if err == nil {
						return result, nil
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutHolidayCompany -
func (c *Controller) PutHolidayCompany() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_company_holiday", &graphql.Field{
			Description: "Edit Holiday Company",
			Type:        HolidayCompanyType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"holidaycompany_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"holiday_amount": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"holiday_details": &graphql.ArgumentConfig{
					Type: graphql.NewList(HolidayDetailInputType),
				},
				"delete_company_holiday_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company_holiday", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))

					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}

					p.Args["updated_by"] = userInfo.PersonalID
					p.Args["company_id"] = userInfo.CompanyID
					result, err := service.PutHolidayCompany(p.Args)
					if err == nil {
						return result, nil
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// DeleteHolidayCompany -
func (c *Controller) DeleteHolidayCompany() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_holiday_company", &graphql.Field{
			Description: "Delete Holiday Company",
			Type:        HolidayCompanyType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company_holiday", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					return service.DeleteHolidayCompany(p.Args["id"].(int))
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
