package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"
	"strconv"

	"github.com/graphql-go/graphql"
)

type OrganizationLevelResMessage struct {
	Id      int    `json:"id"`
	Message string `json:"message"`
}

var OrganizationLevelType *graphql.Object
var OrganizationLevelListType *graphql.Object
var OrganizationLevelResponseMessageType *graphql.Object

func init() {
	OrganizationLevelType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OrganizationLevel",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"value": &graphql.Field{
						Type: graphql.Int,
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	OrganizationLevelListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OrganizationLevels",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"org_levels": &graphql.Field{
						Type: graphql.NewList(OrganizationLevelType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	OrganizationLevelResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OrganizationLevelResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

func (c *Controller) PostOrganizationLevel() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_organization_level", &graphql.Field{
			Description: "Create new Organization Level from args",
			Type:        OrganizationLevelResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"value": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_level", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var orgLevelType model.OrgLevel
					jsonOrgLevel, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonOrgLevel, &orgLevelType)

					user, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					if user.Role.Name == "hr" {
						orgLevelType.CompanyID = employee.CompanyID
						result, err := service.PostLevel(orgLevelType)
						if err != nil || result == 0 {
							return nil, err
						}
						return OrganizationLevelResMessage{
							Id:      result,
							Message: "Create Success",
						}, err
					}
					return nil, errors.New("no permission")
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetOrganizationLevels -
func (c *Controller) GetOrganizationLevels() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "organization_levels", &graphql.Field{
			Type: OrganizationLevelListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_level", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					companyID, _ := p.Args["company_id"].(int)
					name, _ := p.Args["name"].(string)

					if !limitOk {
						limit = 999
					}

					if !offsetOk {
						offset = 0
					}

					userIDstr, userIDOk := p.Context.Value(authorize.UserIDHeaderKey).(string)
					if !userIDOk {
						return model.User{}, errors.New("invalid bearer")
					}
					userID, _ := strconv.Atoi(userIDstr)
					user, err := service.GetUserFromID(userID)

					employee, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err
					}

					if user.Role.Name == "hr" {
						companyID = employee.CompanyID
						result, total, err := service.GetLevelList(companyID, name, limit, offset)
						if err != nil {
							return nil, err
						}
						return model.OrgLevelList{
							OrgLevels: result,
							Total:     total,
						}, err
					}

					return nil, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetOrganizationLevel -
func (c *Controller) GetOrganizationLevel() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "organization_level", &graphql.Field{
			Type: OrganizationLevelType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_level", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetLevel(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return result, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) PutOrganizationLevel() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_organization_level", &graphql.Field{
			Type: OrganizationLevelResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"value": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_level", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var orgLevelType model.OrgLevel
					jsonOrgLevel, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonOrgLevel, &orgLevelType)

					result, err := service.PutOrganizationLevel(orgLevelType)
					if err != nil {
						return nil, err
					}
					return OrganizationLevelResMessage{
						Id:      result,
						Message: "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//DeleteOrganizationLevel -
func (c *Controller) DeleteOrganizationLevel() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_organization_level", &graphql.Field{
			Type: OrganizationLevelResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_level", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					res, err := service.DeleteOrganizationLevel(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}

					return OrganizationLevelResMessage{
						Id:      res,
						Message: "Delete Succese",
					}, nil
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
