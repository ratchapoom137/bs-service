package graph

import (
	"github.com/graphql-go/graphql"
)

// AttachmentType - with leave request
var AttachmentType *graphql.Object

// AttachmentInputType - For create
var AttachmentInputType *graphql.InputObject

func init() {
	AttachmentType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Attachment",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"leave_request_id": &graphql.Field{
						Type: graphql.Int,
					},
					"file": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	AttachmentInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "AttachmentInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"file": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)
}
