package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"strconv"
	"time"

	_ "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
)

//WorkReportInputType --
var WorkReportInputType *graphql.InputObject

//WorkProgressProjectType --
var WorkProgressProjectType *graphql.Object

//MonthlyReportType --
var MonthlyReportType *graphql.Object

//WorkProgressProjectListType --
var WorkProgressProjectListType *graphql.Object

//WorkAdjustmentsProjectListType --
var WorkAdjustmentsProjectListType *graphql.Object

//WorkSubmissionStatusListType --
var WorkSubmissionStatusListType *graphql.Object

//WorkSubmissionStatusType --
var WorkSubmissionStatusType *graphql.Object

//WorkProgressInputType --
var WorkProgressInputType *graphql.InputObject

//WorkAdjustmentInputType --
var WorkAdjustmentInputType *graphql.InputObject

//WorkAdjustmentType --
var WorkAdjustmentType *graphql.Object

//WorkAdjustmentsProjectType --
var WorkAdjustmentsProjectType *graphql.Object

//ReviewerInputType --
var ReviewerInputType *graphql.InputObject

//FileInputType --
var FileInputType *graphql.InputObject

//FileType --
var FileType *graphql.Object

//ReviewerType --
var ReviewerType *graphql.Object

//WorkProgressType --
var WorkProgressType *graphql.Object

//PeriodType --
var PeriodType *graphql.Object

// //WorkReportInputType --
// var WorkListQueryType *graphql.Object

func init() {

	MonthlyReportType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "MonthlyReportType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"code": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"hours": &graphql.Field{
						Type: graphql.NewList(graphql.Int),
					},
				}
			}),
		},
	)

	PeriodType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "PeriodType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"start_date": &graphql.Field{
						Type: graphql.String,
					},
					"end_date": &graphql.Field{
						Type: graphql.String,
					},
					"total_date": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	WorkSubmissionStatusType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "WorkSubmissionStatusType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"period_id": &graphql.Field{
						Type: graphql.Int,
					},
					"period": &graphql.Field{
						Type: PeriodType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							periodID := p.Source.(service.WorkReportStatusOutput).PeriodID
							result, _ := service.GetPeriodDetail(periodID)
							return result, nil
						},
					},
					"type": &graphql.Field{
						Type: graphql.Int,
					},
					"status": &graphql.Field{
						Type: graphql.Int,
					},
					"submit_date": &graphql.Field{
						Type: graphql.String,
					},
					"work_hour": &graphql.Field{
						Type: graphql.Int,
					},
					"leave_hour": &graphql.Field{
						Type: graphql.Int,
					},
					"total_hour": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	WorkSubmissionStatusListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "WorkSubmissionStatusListType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"work_submissions": &graphql.Field{
						Type: graphql.NewList(WorkSubmissionStatusType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	WorkAdjustmentType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "WorkAdjustmentType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"is_pending_delete": &graphql.Field{
						Type: graphql.Boolean,
					},
					"adjust_period": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"project_id": &graphql.Field{
						Type: graphql.Int,
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
					"original_work_hours": &graphql.Field{
						Type: graphql.Int,
					},
					"work_hours": &graphql.Field{
						Type: graphql.Int,
					},
					"submit_period_id": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	WorkAdjustmentsProjectType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "WorkAdjustmentsProjectType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{

					"status": &graphql.Field{
						Type: graphql.String,
					},
					"work_adjustments": &graphql.Field{
						Type: graphql.NewList(WorkAdjustmentType),
					},
					"period_id": &graphql.Field{
						Type: graphql.Int,
					},
					"files": &graphql.Field{
						Type: graphql.NewList(FileType),
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"reviewers": &graphql.Field{
						Type: graphql.NewList(ReviewerType),
					},
					"project_id": &graphql.Field{
						Type: graphql.Int,
					},
					"adjust_period": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	WorkAdjustmentsProjectListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "WorkAdjustmentsProjectListType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"work_adjustments": &graphql.Field{
						Type: graphql.NewList(WorkAdjustmentsProjectType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	WorkProgressProjectListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "WorkReportProgressListType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"work_reports": &graphql.Field{
						Type: graphql.NewList(WorkProgressProjectType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	WorkProgressProjectType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "WorkProgressProjectType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"status": &graphql.Field{
						Type: graphql.String,
					},
					"report_id": &graphql.Field{
						Type: graphql.Int,
					},
					"work_progresses": &graphql.Field{
						Type: graphql.NewList(WorkProgressType),
					},
					"period_id": &graphql.Field{
						Type: graphql.Int,
					},
					"files": &graphql.Field{
						Type: graphql.NewList(FileType),
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"reviewers": &graphql.Field{
						Type: graphql.NewList(ReviewerType),
					},
					"project_id": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	FileType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "FileType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"url": &graphql.Field{
						Type: graphql.String,
					},
					"period_id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"project_id": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	ReviewerType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ReviewerType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"reviewer_id": &graphql.Field{
						Type: graphql.Int,
					},
					"period_id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"project_id": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	WorkProgressType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "WorkProgressType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
					"work_hours": &graphql.Field{
						Type: graphql.Int,
					},
					"period_id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"project_id": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	//Input Schema

	WorkReportInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "WorkReport",
			Fields: graphql.InputObjectConfigFieldMap{
				"employee_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"project_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"period_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"work_progresses": &graphql.InputObjectFieldConfig{
					Type: graphql.NewList(WorkProgressInputType),
				},
				"reviewers": &graphql.InputObjectFieldConfig{
					Type: graphql.NewList(FileInputType),
				},
				"files": &graphql.InputObjectFieldConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
				"description": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	WorkProgressInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "WorkProgressInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"work_hours": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"period_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"project_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	ReviewerInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "ReviewerInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"period_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"project_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"reviewer_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	FileInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "FileInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"url": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"project_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"period_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	WorkAdjustmentInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "WorkAdjustmentInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"original_work_hours": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"work_hours": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)
}

// GetWorkReportByEmployeeIDPeriodID -
func (c *Controller) GetWorkReportByEmployeeIDPeriodID() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "work_reports", &graphql.Field{
			Type: WorkProgressProjectListType,
			Args: graphql.FieldConfigArgument{
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"period_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "time_sheet", Function: "read"},
						),
					),
				)

				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					// userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}
					queryObject := service.WorkReportQueryType{
						EmployeeID: userInfo.PersonalID,
						PeriodID:   p.Args["period_id"].(int),
					}
					if p.Args["employee_id"] != nil {
						queryObject.EmployeeID = p.Args["employee_id"].(int)
					}

					result, err := service.GetWorkReportByEmployeeIDPeriodID(queryObject)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetWorkAdjustmentByEmployeeIDPeriodID -
func (c *Controller) GetWorkAdjustmentByEmployeeIDPeriodID() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "work_adjustments", &graphql.Field{
			Type: WorkAdjustmentsProjectListType,
			Args: graphql.FieldConfigArgument{
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"period_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_adjustment", Function: "read"},
						),
					),
				)

				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					// userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}
					queryObject := service.WorkReportQueryType{
						EmployeeID: userInfo.PersonalID,
						PeriodID:   p.Args["period_id"].(int),
					}
					if p.Args["employee_id"] != nil {
						queryObject.EmployeeID = p.Args["employee_id"].(int)
					}
					result, err := service.GetWorkAdjustmentByEmployeeIDPeriodID(queryObject)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetEmployeeWorkSubmissions -
func (c *Controller) GetEmployeeWorkSubmissions() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "work_submissions", &graphql.Field{
			Type: WorkSubmissionStatusListType,
			Args: graphql.FieldConfigArgument{
				"period_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"is_reviewer": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"limit": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_submission", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}

					queryObject := service.WorkReportQueryType{
						Page:       p.Args["offset"].(int),
						Perpage:    p.Args["limit"].(int),
						EmployeeID: userInfo.PersonalID,
						CompanyID:  userInfo.CompanyID,
					}

					if p.Args["period_id"] != nil {
						queryObject.PeriodID = p.Args["period_id"].(int)
					}
					if p.Args["employee_id"] != nil {
						queryObject.EmployeeID = p.Args["employee_id"].(int)
					}
					if p.Args["is_reviewer"] != nil {
						queryObject.ReviewerID = userInfo.PersonalID
						queryObject.EmployeeID = 0
					}
					if p.Args["status"] != nil {
						queryObject.Status = p.Args["status"].(int)
					}
					if userAccount.(model.User).Role.Name == "hr" && p.Args["is_reviewer"] != nil {
						queryObject.ReviewerID = 0
						queryObject.EmployeeID = 0
					}

					result, err := service.GetEmployeeWorkSubmissions(queryObject)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetMyWorkSubmissions -
func (c *Controller) GetMyWorkSubmissions() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "my_work_submissions", &graphql.Field{
			Type: WorkSubmissionStatusListType,
			Args: graphql.FieldConfigArgument{
				"status": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"limit": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "my_work_submission", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}

					queryObject := service.WorkReportQueryType{
						Page:       p.Args["offset"].(int),
						Perpage:    p.Args["limit"].(int),
						EmployeeID: userInfo.PersonalID,
					}

					if p.Args["status"] != nil {
						queryObject.Status = p.Args["status"].(int)
					}

					result, err := service.GetMyWorkSubmissions(queryObject)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetAvailablePeriod -
func (c *Controller) GetAvailablePeriod() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "Periods", &graphql.Field{
			Type: graphql.NewList(PeriodType),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "period", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetAvailablePeriod()
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutWorkReport -
func (c *Controller) PutWorkReport() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_work_report", &graphql.Field{
			Description: "Edit Work Report",
			Type:        graphql.String,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"project_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"period_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"work_progresses": &graphql.ArgumentConfig{
					Type: graphql.NewList(WorkProgressInputType),
				},
				"reviewers": &graphql.ArgumentConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
				"files": &graphql.ArgumentConfig{
					Type: graphql.NewList(FileInputType),
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"delete_reviewer_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
				"delete_file_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "time_sheet", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}
					p.Args["employee_id"] = userInfo.PersonalID

					result, err := service.PutWorkReport(p.Args)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostWorkReport -
func (c *Controller) PostWorkReport() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_work_report", &graphql.Field{
			Description: "Create Work Report",
			Type:        graphql.String,
			Args: graphql.FieldConfigArgument{
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"project_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"period_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"work_progresses": &graphql.ArgumentConfig{
					Type: graphql.NewList(WorkProgressInputType),
				},
				"reviewers": &graphql.ArgumentConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
				"files": &graphql.ArgumentConfig{
					Type: graphql.NewList(FileInputType),
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},

			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "time_sheet", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					// userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}
					p.Args["employee_id"] = userInfo.PersonalID

					result, err := service.PostWorkReport(p.Args)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// SubmitWorkStatus -
func (c *Controller) PostSubmitWorkStatus() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_work_submit", &graphql.Field{
			Description: "Submit Work Draft and adjustment draft",
			Type:        graphql.String,
			Args: graphql.FieldConfigArgument{
				"period_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"report_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "my_work_submission", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					// userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}
					p.Args["employee_id"] = userInfo.PersonalID

					result, err := service.PostWorkSubmitStatus(p.Args)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostWorkAdjustment -
func (c *Controller) PostWorkAdjustment() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_work_adjustment", &graphql.Field{
			Description: "Create Work Adjustment",
			Type:        graphql.String,
			Args: graphql.FieldConfigArgument{
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"project_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"period_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"work_adjustments": &graphql.ArgumentConfig{
					Type: graphql.NewList(WorkAdjustmentInputType),
				},
				"adjust_period": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_adjustment", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}
					p.Args["employee_id"] = userInfo.PersonalID
					result, err := service.PostWorkAdjustment(p.Args)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutWorkAdjustment -
func (c *Controller) PutWorkAdjustment() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_work_adjustment", &graphql.Field{
			Description: "Edit Work Adjustment",
			Type:        graphql.String,
			Args: graphql.FieldConfigArgument{
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"project_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"period_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"work_adjustments": &graphql.ArgumentConfig{
					Type: graphql.NewList(WorkAdjustmentInputType),
				},
				"adjust_period": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_adjustment", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}
					p.Args["employee_id"] = userInfo.PersonalID
					result, err := service.PutWorkAdjustment(p.Args)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetWorkReport -
func (c *Controller) GetWorkReport() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "monthly_report", &graphql.Field{
			Type: graphql.NewList(MonthlyReportType),
			Args: graphql.FieldConfigArgument{
				"start_period": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"end_period": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"project_ids": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.NewList(graphql.Int)),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "time_sheet", Function: "read"},
						),
					),
				)
				var projectIDs []int
				for _, eachId := range p.Args["project_ids"].([]interface{}) {
					projectIDs = append(projectIDs, eachId.(int))
				}
				startPeriod, err := time.Parse(time.RFC3339, p.Args["start_period"].(string))
				endPeriod, err := time.Parse(time.RFC3339, p.Args["end_period"].(string))
				if err != nil {

				}
				queryObject := service.WorkReportQuery{
					ProjectIDs:  projectIDs,
					StartPeriod: startPeriod,
					EndPeriod:   endPeriod,
				}

				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetWorkReport(queryObject)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
