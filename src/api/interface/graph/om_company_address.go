package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"

	_ "github.com/davecgh/go-spew/spew"

	"github.com/graphql-go/graphql"
)

var CompanyAddressListResponseMessageType *graphql.Object
var CompanyAddressResponseMessageType *graphql.Object
var CompanyAddressListType *graphql.Object
var CompanyAddressType *graphql.Object
var CompanyAddressInputType *graphql.InputObject

func init() {
	CompanyAddressType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "CompanyAddress",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"detail": &graphql.Field{
						Type: graphql.String,
					},
					"postcode": &graphql.Field{
						Type: graphql.String,
					},
					"province": &graphql.Field{
						Type: graphql.String,
					},
					"country": &graphql.Field{
						Type: graphql.String,
					},
					"latitude": &graphql.Field{
						Type: graphql.Float,
					},
					"longitude": &graphql.Field{
						Type: graphql.Float,
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"is_head_office": &graphql.Field{
						Type: graphql.Boolean,
					},
				}
			}),
		},
	)
	CompanyAddressInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "CompanyAddressInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"detail": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"postcode": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"province": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"country": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"latitude": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
				"longitude": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
				"company_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)
	CompanyAddressListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "CompanyAddresses",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"addresses": &graphql.Field{
						Type: graphql.NewList(CompanyAddressType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	CompanyAddressResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "CompanyAddressResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"address_id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	CompanyAddressListResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "CompanyAddressListResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"addresses_id": &graphql.Field{
						Type: graphql.NewList(graphql.Int),
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

//GetAddress -
func (c *Controller) GetAddress() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "company_address", &graphql.Field{
			Type: CompanyAddressType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company", Function: "read"},
							model.Permission{Module: "my_company", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					user, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					result, err := service.GetAddress(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}

					if user.Role.Name != "admin" {
						if result.CompanyID != employee.CompanyID {
							return nil, errors.New("incorrect company")
						}
					}

					return result, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)

			},
		}
	}
}

// GetAddressList -
func (c *Controller) GetAddressList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "company_addresses", &graphql.Field{
			Type: CompanyAddressListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company", Function: "read"},
							model.Permission{Module: "my_company", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					compID, compIDOk := p.Args["company_id"].(int)

					if !compIDOk {
						return nil, errors.New("company id not found")
					}

					if !limitOk {
						limit = 10
					}

					if !offsetOk {
						offset = 0
					}

					user, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					if user.Role.Name != "admin" {
						compID = employee.CompanyID
					}

					result, total, err := service.GetAddressList(limit, offset, compID)

					if err != nil {
						return nil, err
					}
					return model.CompanyAddressList{
						Addresses: result,
						Total:     total,
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostCompanyAddress -
func (c *Controller) PostCompanyAddress() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_company_address", &graphql.Field{
			Description: "Create new Address from args",
			Type:        CompanyAddressListResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"head_office": &graphql.ArgumentConfig{
					Type: CompanyAddressInputType,
				},
				"sub_branch_office": &graphql.ArgumentConfig{
					Type: graphql.NewList(CompanyAddressInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company_address", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var headOffice model.CompanyAddress
					var subBranch []model.CompanyAddress

					jsonHeadOffice, _ := json.Marshal(p.Args["head_office"])
					json.Unmarshal(jsonHeadOffice, &headOffice)

					jsonSubBranch, _ := json.Marshal(p.Args["sub_branch_office"])
					json.Unmarshal(jsonSubBranch, &subBranch)

					user, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					var companyID int
					if user.Role.Name == "hr" {
						companyID = employee.CompanyID
					}

					result, err := service.PostAddress(headOffice, subBranch, companyID)
					if err != nil {
						return nil, err
					}

					return model.CompanyAddressListResponseMessage{
						AddressesID: result,
						Message:     "Create Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutCompanyAddress -
func (c *Controller) PutCompanyAddress() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_company_address", &graphql.Field{
			Description: "Edit company address by id",
			Type:        CompanyAddressListResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"addresses": &graphql.ArgumentConfig{
					Type: graphql.NewList(CompanyAddressInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var addresses []model.CompanyAddress
					jsonAddress, _ := json.Marshal(p.Args["addresses"])
					json.Unmarshal(jsonAddress, &addresses)

					user, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					if user.Role.Name == "hr" {
						for _, addr := range addresses {
							addr.CompanyID = employee.CompanyID
						}
					}

					result, err := service.PutAddress(addresses)
					if err != nil {
						return nil, err
					}
					return model.CompanyAddressListResponseMessage{
						AddressesID: result,
						Message:     "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//DeleteCompanyAddress -
func (c *Controller) DeleteCompanyAddress() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_company_address", &graphql.Field{
			Type: CompanyAddressListResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"address_id_list": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company_address", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var IDList []int
					for _, id := range p.Args["address_id_list"].([]interface{}) {
						IDList = append(IDList, id.(int))
					}

					user, _, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					if user.Role.Name == "hr" || user.Role.Name == "admin" {
						result, err := service.DeleteCompanyAddress(IDList)
						if err != nil {
							return nil, err
						}

						return model.CompanyAddressListResponseMessage{
							AddressesID: result,
							Message:     "Delete Success",
						}, nil
					}
					return nil, errors.New("not allowed")
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
