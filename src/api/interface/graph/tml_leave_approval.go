package graph

import (
	authorize "api/helpers/authorize"
	"api/service"
	"api/model"
	"github.com/graphql-go/graphql"
	"github.com/thoas/go-funk"
)

// ApprovalTask -
var ApprovalTask *graphql.Object

// ApprovalTasks -
var ApprovalTasks *graphql.Object

func init() {
	ApprovalTask = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ApprovalTask",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee": &graphql.Field{
						Type: EmployeeType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							id := p.Source.(model.ApprovalTask).EmployeeID
							res, err := service.GetEmployeeByPersonalID(id)
							if err != nil {
								return nil, err
							}
							return res, err
						},
					},
					"leave_request": &graphql.Field{
						Type: LeaveRequestType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							id := p.Source.(model.ApprovalTask).LeaveRequestID
							res, err := service.GetLeaveRequestByID(id)
							if err != nil {
								return nil, err
							}
							return res, err
						},
					},
					"leave_request_id": &graphql.Field{
						Type: graphql.Int,
					},
					"comment": &graphql.Field{
						Type: graphql.String,
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	ApprovalTasks = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ApprovalTasks",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"approval_tasks": &graphql.Field{
						Type: graphql.NewList(ApprovalTask),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
}

// GetApprovalLeaveRequestList -
func (c *Controller) GetApprovalLeaveRequestList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "approval_tasks", &graphql.Field{
			Type: ApprovalTasks,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"leave_type_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"employee_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_approval", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err 
					}

					filter := map[string]interface{}{
						"limit": 999,
						"offset": 0,
						"company_id": userResult.CompanyID,
					}

					employeeInCompany, _, employeeInCompanyErr := service.GetEmployeeByFilter(filter)
					if employeeInCompanyErr != nil {
						return nil, employeeInCompanyErr
					}

					employeeInCompanyIDs := funk.Map(employeeInCompany, func(employee model.Employee) int {
						return employee.ID
					}).([]int)

					employeeID := userResult.PersonalID

					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					leaveTypeID, _ := p.Args["leave_type_id"].(int)
					status, _ := p.Args["status"].(string)
					empInputIDs, empInputIDsOk := p.Args["employee_ids"].([]interface{})
					var employeeIDs []int

					if !limitOk {
						limit = 999
					}

					if !offsetOk {
						offset = 0
					}

					if empInputIDsOk {
						if len(empInputIDs) == 0 {
							return model.ApprovalTaskList{}, nil
						}
						funk.ForEach(empInputIDs, func(item interface{}) {
							if !funk.Contains(employeeInCompanyIDs, item.(int)) {
								return
							}
							employeeIDs = append(employeeIDs, item.(int))
						})
						if employeeIDs == nil {
							return model.ApprovalTaskList{}, nil
						}
					}

					if len(employeeIDs) == 0 {
						funk.ForEach(employeeInCompanyIDs, func(item int) {
							employeeIDs = append(employeeIDs, item)
						})
					}

					res, err := service.GetApprovalLeaveRequestList(employeeID, limit, offset, leaveTypeID, employeeIDs, status)
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// ApproveLeaveRequest -
func (c *Controller) ApproveLeaveRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "approve_leave_request", &graphql.Field{
			Type: ApprovalTask,
			Args: graphql.FieldConfigArgument{
				"leave_request_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"comment": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_approval", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err 
					}

					employeeID := userResult.PersonalID
					leaveRequestID, _ := p.Args["leave_request_id"].(int)
					comment, commentOk := p.Args["comment"].(string)

					if !commentOk {
						comment = ""
					}

					approvalTask := model.ApprovalTask{
						EmployeeID: 	employeeID,
						LeaveRequestID: p.Args["leave_request_id"].(int),
						Comment: 		comment,
					}

					res, err := service.ApproveLeaveRequest(employeeID, leaveRequestID, approvalTask)
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// RejectLeaveRequest -
func (c *Controller) RejectLeaveRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "reject_leave_request", &graphql.Field{
			Type: ApprovalTask,
			Args: graphql.FieldConfigArgument{
				"leave_request_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"comment": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "leave_approval", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					userID := userAccount.(model.User).ID

					userResult, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return nil, err 
					}

					employeeID := userResult.PersonalID
					leaveRequestID, _ := p.Args["leave_request_id"].(int)
					comment, commentOk := p.Args["comment"].(string)

					if !commentOk {
						comment = ""
					}

					approvalTask := model.ApprovalTask{
						EmployeeID: 	employeeID,
						LeaveRequestID: p.Args["leave_request_id"].(int),
						Comment: 		comment,
					}

					res, err := service.RejectLeaveRequest(employeeID, leaveRequestID, approvalTask)
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
