package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"strconv"

	"github.com/graphql-go/graphql"
)

// ReportCodeSetType -
var ReportCodeSetType *graphql.Object

// ReportCodeSetsType -
var ReportCodeSetsType *graphql.Object

// ChargeCodeReportType -
var ChargeCodeReportType *graphql.Object

func init() {

	ReportCodeSetType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ReportCodeSet",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"charge_codes": &graphql.Field{
						Type: graphql.NewList(ChargeCodeReportType),
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	ChargeCodeReportType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ChargeCodeReport",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"code": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	ReportCodeSetsType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ReportCodeSets",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"total": &graphql.Field{
						Type: graphql.Int,
					},
					"report_code_sets": &graphql.Field{
						Type: graphql.NewList(ReportCodeSetType),
					},
				}
			}),
		},
	)

}

//GetReportCodeSetList -
func (c *Controller) GetReportCodeSetList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "report_code_sets", &graphql.Field{
			Type: ReportCodeSetsType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"search_keyword": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_report", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					queryObject := service.ReportCodeSetQuery{
						Limit:  p.Args["limit"].(int),
						Offset: p.Args["offset"].(int),
					}

					if p.Args["search_keyword"] != nil {
						queryObject.SearchKeyword = p.Args["search_keyword"].(string)
					}
					return service.GetReportCodeSetList(queryObject)
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetReportCodeSetDetail -
func (c *Controller) GetReportCodeSetDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "report_code_set", &graphql.Field{
			Type: ReportCodeSetType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_report", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetReportCodeSetDetail(p.Args["id"].(int))
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostReportCodeSet -
func (c *Controller) PostReportCodeSet() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_report_code_set", &graphql.Field{
			Description: "Create new report code set",
			Type:        ReportCodeSetType,
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"charge_code_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_report", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))

					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}
					p.Args["created_by"] = userInfo.PersonalID
					p.Args["updated_by"] = userInfo.PersonalID
					p.Args["employee_id"] = userInfo.PersonalID

					result, err := service.PostReportCodeSet(p.Args)
					if err == nil {
						return result, nil
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutReportCodeSet -
func (c *Controller) PutReportCodeSet() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_report_code_set", &graphql.Field{
			Description: "Edit existing report code set",
			Type:        ReportCodeSetType,
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},

				"charge_code_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
				"charge_code_delete_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_report", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))

					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}
					p.Args["updated_by"] = userInfo.PersonalID

					result, err := service.PutReportCodeSet(p.Args)
					if err == nil {
						return result, nil
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// DeleteReportCodeSet -
func (c *Controller) DeleteReportCodeSet() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_report_code_set", &graphql.Field{
			Description: "Delete Report Code set",
			Type:        ReportCodeSetType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "work_report", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					return service.DeleteReportCodeSet(p.Args["id"].(int))
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
