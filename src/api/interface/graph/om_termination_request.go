package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"

	"github.com/graphql-go/graphql"
)

var TerminationRequestType *graphql.Object
var TerminationRequestInputType *graphql.InputObject
var TerminationRequestResponseType *graphql.Object
var TerminationRequestListType *graphql.Object
var TerminationRequestExistType *graphql.Object

type TerminationRequestExist struct {
	IsRequested bool   `json:"is_requested"`
	Message     string `json:"message"`
}

func init() {
	TerminationRequestType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "TerminationRequest",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"company": &graphql.Field{
						Type: CompanyType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							companyID := p.Source.(model.TerminationRequest).CompanyID
							res, _ := service.GetCompanyDetail(companyID)
							return res, nil
						},
					},
					"user_id": &graphql.Field{
						Type: graphql.Int,
					},
					"user": &graphql.Field{
						Type: UserAndUserDetailType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							userID := p.Source.(model.TerminationRequest).UserID
							res, _ := service.GetUserAndUserDetail(userID)
							return res, nil
						},
					},
					"requested_date": &graphql.Field{
						Type: graphql.String,
					},
					"approved_date": &graphql.Field{
						Type: graphql.String,
					},
					"cancel_date": &graphql.Field{
						Type: graphql.String,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
					"deleted_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	TerminationRequestResponseType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "TerminationRequestResponse",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	TerminationRequestListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "TerminationRequestList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"termination_requests": &graphql.Field{
						Type: graphql.NewList(RegistrationRequestType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	TerminationRequestListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "TerminationRequestList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"termination_requests": &graphql.Field{
						Type: graphql.NewList(TerminationRequestType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	TerminationRequestInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "TerminationRequestInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"user_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"requested_date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"approved_date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"cancel_date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)
	TerminationRequestExistType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "TerminationRequestExist",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"is_requested": &graphql.Field{
						Type: graphql.Boolean,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

// PostTerminationRequest -
func (c *Controller) PostTerminationRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_termination_request", &graphql.Field{
			Type: TerminationRequestResponseType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"user_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"requested_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"approved_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"cancel_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "termination_request", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userInfo, employee, err := GetUserEmployeeFromSession(p)
					var terminatedReqInput model.TerminationRequest
					terminatedReqJson, _ := json.Marshal(p.Args)
					json.Unmarshal(terminatedReqJson, &terminatedReqInput)

					terminatedReqInput.UserID = userInfo.ID
					terminatedReqInput.CompanyID = employee.CompanyID

					res, err := service.PostTerminationRequest(terminatedReqInput)
					if err != nil || res == 0 {
						return nil, err
					}

					return model.UserDetailResponse{
						ID:      res,
						Message: "Create Termination Request Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetTerminationRequest -
func (c *Controller) GetTerminationRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "termination_request", &graphql.Field{
			Type: TerminationRequestType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "termination_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					res, err := service.GetTerminationRequestByID(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return res, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetTerminationRequestList -
func (c *Controller) GetTerminationRequestList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "termination_request_list", &graphql.Field{
			Type: TerminationRequestListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"request_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "termination_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					if !limitOk || !offsetOk {
						limit = -1
						offset = -1
					}

					filter := map[string]interface{}{}
					filter["limit"] = limit
					filter["offset"] = offset
					filter["company_name"], _ = p.Args["company_name"].(string)
					filter["request_date"], _ = p.Args["request_date"].(string)
					filter["status"], _ = p.Args["status"].(string)

					user, _, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					if user.Role.Name == "admin" {
						result, total, err := service.GetTerminationRequestList(filter)
						if err != nil {
							return nil, err
						}

						return model.TerminationRequestList{
							TerminationRequests: result,
							Total:               total,
						}, err
					}

					return nil, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//PutTerminationRequest -
func (c *Controller) PutTerminationRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_termination_request", &graphql.Field{
			Type: TerminationRequestResponseType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"user_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"requested_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"approved_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"cancel_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "termination_request", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var terminateReqInput model.TerminationRequestInput
					terminateReqJson, _ := json.Marshal(p.Args)
					json.Unmarshal(terminateReqJson, &terminateReqInput)

					res, err := service.PutTerminationRequest(terminateReqInput)
					if err != nil || res == 0 {
						return nil, err
					}
					return model.TerminationRequestResponse{
						ID:      res,
						Message: "Update Termination Request Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// CheckTerminationRequestExist -
func (c *Controller) CheckTerminationRequestExist() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "check_termination_request_exist", &graphql.Field{
			Type: TerminationRequestExistType,
			Args: graphql.FieldConfigArgument{
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				// TODO: Edit Permission for termination request LIST
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company", Function: "read"},
							model.Permission{Module: "my_company", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					compID, _ := p.Args["company_id"].(int)
					if compID <= 0 {
						return TerminationRequestExist{
							IsRequested: false,
							Message:     "Empty Company ID",
						}, nil
					}

					result, err := service.CheckTerminationRequestExist(compID)
					if err != nil {
						return false, err
					}

					return TerminationRequestExist{
						IsRequested: result,
						Message:     "Check Successed",
					}, nil
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
