package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"

	// "github.com/thoas/go-funk"
	// "time"
	// "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
)

//Types
var OtherApproveConfigurationType *graphql.Object
var OtherConfigurationType *graphql.Object

//InputTypes
var OtherApproveConfigurationInputType *graphql.InputObject

func init() {
	OtherApproveConfigurationInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "OtherApproveConfigurationInputType",
			Fields: graphql.InputObjectConfigFieldMap{
				"approve_type_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)
	OtherApproveConfigurationType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OtherApproveConfigurationType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"configuration_id": &graphql.Field{
						Type: graphql.Int,
					},
					"approve_type_id": &graphql.Field{
						Type: graphql.Int,
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	OtherConfigurationType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OtherConfigurationType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"approve_type_id": &graphql.Field{
						Type: graphql.Int,
					},
					"approve_type_name": &graphql.Field{
						Type: graphql.String,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							approval, ok := p.Source.(model.OtherApproveConfiguration)
							if !ok {
								return nil, errors.New("Invalid approval")
							}
							getApprovalName, err := service.GetApprovalConfig(approval.ApproveTypeID)
							if err != nil {
								return nil, err
							}
							return getApprovalName.Name, err
						},
					},
				}
			}),
		},
	)
}

// PostOtherApproveConfiguration -
func (c *Controller) PostOtherApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_other_approve_configuration", &graphql.Field{
			Description: "Create new other approve configuration",
			Type:        OtherApproveConfigurationType,
			Args: graphql.FieldConfigArgument{
				"other_approve_configuration_input": &graphql.ArgumentConfig{
					Type: OtherApproveConfigurationInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					var otherApproveConfigurationInput model.OtherApproveConfiguration
					jsonData, _ := json.Marshal(p.Args["other_approve_configuration_input"])
					json.Unmarshal(jsonData, &otherApproveConfigurationInput)

					otherApproveConfigurationInput.CompanyID = employee.CompanyID
					otherApproveConfigurationInput.CreatedBy = employee.PersonalID

					res, err := service.CreateOtherApproveConfiguration(&otherApproveConfigurationInput)
					if err != nil {
						return nil, err
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetOthersApproveConfiguration -
func (c *Controller) GetOtherApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "other_approve_configuration", &graphql.Field{
			Description: "Get other configuration",
			Type:        OtherConfigurationType,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_type", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					res, err := service.GetOtherConfiguration(employee.CompanyID)
					if err != nil {
						return nil, err
					}

					return res, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutOthersApproveConfiguration -
func (c *Controller) PutOtherApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_other_approve_configuration", &graphql.Field{
			Type: OtherApproveConfigurationType,
			Args: graphql.FieldConfigArgument{
				"other_approve_configuration_input": &graphql.ArgumentConfig{
					Type: OtherApproveConfigurationInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_type", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					var otherApproveConfigurationInput model.OtherApproveConfiguration
					jsonData, _ := json.Marshal(p.Args["other_approve_configuration_input"])
					json.Unmarshal(jsonData, &otherApproveConfigurationInput)

					otherApproveConfigurationInput.CompanyID = employee.CompanyID
					otherApproveConfigurationInput.UpdatedBy = employee.PersonalID

					res, err := service.PutOtherApproveConfiguration(&otherApproveConfigurationInput)
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
