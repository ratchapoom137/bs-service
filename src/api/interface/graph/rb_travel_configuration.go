package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"

	// "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
)

//Types
var TravelConfigurationType *graphql.Object
var TravelApproveConfigurationExpenseType *graphql.Object

//InputTypes
var TravelApproveConfigurationInputType *graphql.InputObject
var TravelApproveConfigurationInputExpenseType *graphql.InputObject

func init() {

	TravelApproveConfigurationExpenseType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "TravelApproveConfigurationExpenseType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"expense": &graphql.Field{
						Type: graphql.Float,
					},
				}
			}),
		},
	)

	TravelConfigurationType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "TravelConfigurationType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"approve_type_id": &graphql.Field{
						Type: graphql.Int,
					},
					"approve_type_name": &graphql.Field{
						Type: graphql.String,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							approval, ok := p.Source.(model.TravelApproveConfiguration)
							if !ok {
								return nil, errors.New("Invalid approval")
							}
							getApprovalName, err := service.GetApprovalConfig(approval.ApproveTypeID)
							if err != nil {
								return nil, err
							}
							return getApprovalName.Name, err
						},
					},
					"travel_expense": &graphql.Field{
						Type: TravelApproveConfigurationExpenseType,
					},
				}
			}),
		},
	)

	TravelApproveConfigurationInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "TravelApproveConfigurationInputType",
			Fields: graphql.InputObjectConfigFieldMap{
				"approve_type_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	TravelApproveConfigurationInputExpenseType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "TravelApproveConfigurationInputExpenseType",
			Fields: graphql.InputObjectConfigFieldMap{
				"expense": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

}

//PostTravelApproveConfiguration
func (c *Controller) PostTravelApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_Travel_approve_configuration", &graphql.Field{
			Description: "Create new travel approve configuration",
			Type:        TravelConfigurationType,
			Args: graphql.FieldConfigArgument{
				"travel_approve_configuration_input": &graphql.ArgumentConfig{
					Type: TravelApproveConfigurationInputType,
				},
				"travel_expense": &graphql.ArgumentConfig{
					Type: TravelApproveConfigurationInputExpenseType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "employee", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					var travelApproveConfigurationInput model.TravelApproveConfiguration
					jsonData, _ := json.Marshal(p.Args["travel_approve_configuration_input"])
					json.Unmarshal(jsonData, &travelApproveConfigurationInput)
					travelApproveConfigurationInput.CompanyID = employee.CompanyID
					travelApproveConfigurationInput.CreatedBy = employee.PersonalID

					var travelApproveConfigurationInputExpense model.TravelApproveConfigurationExpense
					jsonDataExpense, _ := json.Marshal(p.Args["travel_expense"])
					json.Unmarshal(jsonDataExpense, &travelApproveConfigurationInputExpense)
					travelApproveConfigurationInputExpense.CompanyID = employee.CompanyID
					travelApproveConfigurationInputExpense.CreatedBy = employee.PersonalID

					res, err := service.CreateTravelApproveConfiguration(&travelApproveConfigurationInput, travelApproveConfigurationInputExpense)
					if err != nil {
						return nil, err
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetTravelApproveConfiguration -
func (c *Controller) GetTravelApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "travel_approve_configuration", &graphql.Field{
			Description: "Get travel configuration",
			Type:        TravelConfigurationType,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_type", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					res, err := service.GetTravelConfiguration(employee.CompanyID)

					if err != nil {
						return nil, err
					}

					return res, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//PutTravelApproveConfiguration
func (c *Controller) PutTravelApproveConfiguration() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_Travel_approve_configuration", &graphql.Field{
			Description: "edit new travel approve configuration",
			Type:        TravelConfigurationType,
			Args: graphql.FieldConfigArgument{
				"travel_approve_configuration_input": &graphql.ArgumentConfig{
					Type: TravelApproveConfigurationInputType,
				},
				"travel_expense": &graphql.ArgumentConfig{
					Type: TravelApproveConfigurationInputExpenseType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_type", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					var travelApproveConfigurationInput model.TravelApproveConfiguration
					jsonData, _ := json.Marshal(p.Args["travel_approve_configuration_input"])
					json.Unmarshal(jsonData, &travelApproveConfigurationInput)
					travelApproveConfigurationInput.CompanyID = employee.CompanyID
					travelApproveConfigurationInput.UpdatedBy = employee.PersonalID

					var travelApproveConfigurationInputExpense model.TravelApproveConfigurationExpense
					jsonDataExpense, _ := json.Marshal(p.Args["travel_expense"])
					json.Unmarshal(jsonDataExpense, &travelApproveConfigurationInputExpense)
					travelApproveConfigurationInputExpense.CompanyID = employee.CompanyID
					travelApproveConfigurationInputExpense.UpdatedBy = employee.PersonalID

					res, err := service.EditTravelApproveConfiguration(&travelApproveConfigurationInput, travelApproveConfigurationInputExpense)
					if err != nil {
						return nil, err
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetMyTravelExpense
func (c *Controller) GetMyTravelExpense() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "my_travel_expense", &graphql.Field{
			Description: "Get my travel rate",
			Type:        TravelConfigurationType,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "my_perdiem_rate", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)

					res, err := service.GetMyTravelExpense(employee.CompanyID)

					if err != nil {
						return nil, err
					}

					return res, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
