package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
)

var UserAuthResponseType *graphql.Object
var UserInfoType *graphql.Object
var RoleListType *graphql.Object
var RoleType *graphql.Object
var RolePermissionType *graphql.Object
var UserType *graphql.Object
var UserIDAndUserDetailIDType *graphql.Object
var UserListType *graphql.Object
var UserAndUserDetailType *graphql.Object
var UserDetailWithCompanyType *graphql.Object
var DuplicateFieldAuthenType *graphql.Object
var UserResponseType *graphql.Object
var UserCreationResponseType *graphql.Object
var SecretResponseType *graphql.Object
var SecretType *graphql.Object

var UserInputType *graphql.InputObject
var UserDetailInputType *graphql.InputObject
var PasswordInputType *graphql.InputObject

func init() {
	UserAuthResponseType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserAuth",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"user_id": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	UserInfoType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserInfo",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"username": &graphql.Field{
						Type: graphql.String,
					},
					"role": &graphql.Field{
						Type: RoleType,
					},
				}
			}),
		},
	)

	RoleListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Roles",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"roles": &graphql.Field{
						Type: graphql.NewList(RoleType),
					},
				}
			}),
		},
	)
	RoleType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Role",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"permissions": &graphql.Field{
						Type: graphql.NewList(RolePermissionType),
					},
				}
			}),
		},
	)
	RolePermissionType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "RolePermission",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"module": &graphql.Field{
						Type: graphql.String,
					},
					"function": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	UserIDAndUserDetailIDType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserIDAndUserDetailIDType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"user_id": &graphql.Field{
						Type: graphql.Int,
					},
					"user_detail_id": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	UserAndUserDetailType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserAndUserDetailType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"user": &graphql.Field{
						Type: UserType,
					},
					"user_detail": &graphql.Field{
						Type: UserDetailWithCompanyType,
					},
				}
			}),
		},
	)

	UserType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"username": &graphql.Field{
						Type: graphql.String,
					},
					"role": &graphql.Field{
						Type: RoleType,
					},
				}
			}),
		},
	)

	UserDetailWithCompanyType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserDetailWithCompanyType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"first_name": &graphql.Field{
						Type: graphql.String,
					},
					"last_name": &graphql.Field{
						Type: graphql.String,
					},
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"company": &graphql.Field{
						Type: CompanyType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							companyID := p.Source.(model.UserDetail).CompanyID
							res, _ := service.GetCompanyDetail(companyID)
							return res, nil
						},
					},
					"deactivated_date": &graphql.Field{
						Type: graphql.String,
					},
					"expired_date": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	SecretType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Secret",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"secret": &graphql.Field{
						Type: graphql.String,
					},
					"user_id": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	UserListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserListType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"user_list": &graphql.Field{
						Type: graphql.NewList(UserAndUserDetailType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	DuplicateFieldAuthenType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "DuplicateFieldAuthenType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"username": &graphql.Field{
						Type: graphql.Boolean,
					},
				}
			}),
		},
	)

	UserInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "UserInputType",
			Fields: graphql.InputObjectConfigFieldMap{
				"role_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"username": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	UserDetailInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "UserDetailInputType",
			Fields: graphql.InputObjectConfigFieldMap{
				"company_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	PasswordInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "PasswordInputType",
			Fields: graphql.InputObjectConfigFieldMap{
				"password": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	UserResponseType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserResponse",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	UserCreationResponseType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "UserCreationResponse",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"user_id": &graphql.Field{
						Type: graphql.Int,
					},
					"user_detail_id": &graphql.Field{
						Type: graphql.Int,
					},
					"password_session_id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	SecretResponseType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "SecretResponse",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"user": &graphql.Field{
						Type: graphql.NewList(SecretType),
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

// PostUserLogin -
func (c *Controller) PostUserLogin() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "authorize", &graphql.Field{
			Type: UserAuthResponseType,
			Args: graphql.FieldConfigArgument{
				"username": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"password": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				username, _ := p.Args["username"].(string)
				password, _ := p.Args["password"].(string)

				UserAuth, err := service.GetUserAuthFromLogin(username, password)

				return map[string]int{
					"user_id": UserAuth.ID,
				}, err
			},
		}
	}
}

// PostUser -
func (c *Controller) PostUser() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_user", &graphql.Field{
			Type:        UserCreationResponseType,
			Description: "Create new user",
			Args: graphql.FieldConfigArgument{
				"user": &graphql.ArgumentConfig{
					Type: UserInputType,
				},
				"user_detail": &graphql.ArgumentConfig{
					Type: UserDetailInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "account", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var userAndDetail service.UserIDAndUserDetailID
					loginUser, loginEmployee, err := GetUserEmployeeFromSession(p)

					var user service.UserInput
					jsonUserData, _ := json.Marshal(p.Args["user"])
					json.Unmarshal(jsonUserData, &user)

					var userDetail service.UserDetailInput
					jsonUserDetailData, _ := json.Marshal(p.Args["user_detail"])
					json.Unmarshal(jsonUserDetailData, &userDetail)

					if loginUser.Role.Name == "back_office_admin" {
						userDetail.CompanyID = loginEmployee.CompanyID
					}

					userID, createUserErr := service.CreateUser(user)
					if createUserErr != nil {
						return nil, createUserErr
					}

					// Create User Detail
					userAndDetail.UserID = userID
					userDetail.UserID = userID
					userDetailID, _ := service.CreateUserDetail(userDetail)
					userAndDetail.UserDetailID = userDetailID
					
					// Create Password Session
					var passwordSession model.PasswordSessionInput
					passwordSession.UserID = userID
					passwordSessionResult, _ := service.CreatePasswordSession(passwordSession)

					return model.UserCreationResponse{
						UserID:	userID,
						UserDetailID:	userDetailID,
						PasswordSession:	passwordSessionResult,
						Message:	"create user failed",
					}, err
					
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}


//DeleteUser -
func (c *Controller) DeleteUser() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_user", &graphql.Field{
			Type: UserResponseType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "account", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					userDetail, err := service.GetUserDetailByID(p.Args["id"].(int))
					if userDetail.EmployeeID > 0 && err != nil{
						return nil, err
					}

					if userDetail.EmployeeID <= 0 {
						_, err := service.DeleteUser(userDetail.UserID)
						if err != nil {
							return nil, err
						}
						res, errr := service.DeleteUserDetail(p.Args["id"].(int))
						if errr != nil {
							return nil, errr
						}
						return model.UserResponse{
							ID     : res,
							Message: "Delete user success",
						}, nil
					}
					return nil, errors.New("Cannot delete because the user still employee")
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// BearerAuth -
func (c *Controller) BearerAuth() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "bearer_authorization", &graphql.Field{
			Type: graphql.String,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				return authorize.Authorized(
					authorize.AuthorizedAny(&p)(
						authorize.AuthorizedGroup(p)(
							authorize.WithBearer("client"),
							authorize.WithNoUser(),
						),
						authorize.AuthorizedGroup(p)(
							authorize.WithBearer("user"),
							authorize.WithUserProfile(),
							authorize.WithPermissionAny(
								model.Permission{Module: "dashboard", Function: "read"},
								model.Permission{Module: "dashboard", Function: "all"},
							),
							authorize.WithPermissionAll(
							// model.Permission{Module: "dashboard", Function: "deny"}, // Always false
							),
						),
					),

					func(p graphql.ResolveParams) (interface{}, error) {
						accessToken := p.Context.Value(authorize.AccessTokenHeaderKey)
						clientID := p.Context.Value(authorize.ClientIDHeaderKey)
						userID := p.Context.Value(authorize.UserIDHeaderKey)
						userAccount := p.Context.Value(authorize.UserAccountHeaderKey)

						spew.Dump(accessToken, userID, clientID, userAccount)

						if userAccount.(model.User).Role.Name == "admin" {

						}
						if userAccount.(model.User).Role.Name == "user" {
							// check
						}

						return map[string]interface{}{
							"a":  accessToken,
							"u":  userID,
							"c":  clientID,
							"ua": userAccount,
						}, nil
					},
					func(p graphql.ResolveParams) (interface{}, error) {
						return "fail", nil
					},
				)(p)
			},
		}
	}
}

func (c *Controller) GetUserInfo() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "userinfo", &graphql.Field{
			Type: UserInfoType,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				return authorize.Authorized(
					authorize.AuthorizedAny(&p)(
						authorize.AuthorizedGroup(p)(
							authorize.WithBearer("user"),
							authorize.WithUserProfile(),
						),
					),

					func(p graphql.ResolveParams) (interface{}, error) {
						userIDstr, ok := p.Context.Value(authorize.UserIDHeaderKey).(string)
						userID, _ := strconv.Atoi(userIDstr)

						if !ok {
							return model.User{}, errors.New("invalid bearer")
						}

						response, err := service.GetUserFromID(userID)

						return response, err
					},
					func(p graphql.ResolveParams) (interface{}, error) {
						return model.User{}, errors.New("invalid bearer")
					},
				)(p)
			},
		}
	}
}

//GetUserAndUserDetail -
func (c *Controller) GetUserAndUserDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "get_user_and_user_detail", &graphql.Field{
			Type: UserAndUserDetailType,
			Args: graphql.FieldConfigArgument{
				"user_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "account", Function: "read"},
							model.Permission{Module: "my_account", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					loginUser, _, _ := GetUserEmployeeFromSession(p)
					userID, userIDOk := p.Args["user_id"].(int)

					if !userIDOk {
						userID = loginUser.ID
					}

					res, err := service.GetUserAndUserDetail(userID)
					if err != nil {
						return nil, err
					}

					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetUserAndUserDetailList -
func (c *Controller) GetUserAndUserDetailList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "get_user_and_user_detail_list", &graphql.Field{
			Type: UserListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"username": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"company_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "account", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					loginUser, loginEmployee, err := GetUserEmployeeFromSession(p)
					
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOK := p.Args["offset"].(int)
					username, _ := p.Args["username"].(string)
					name, _ := p.Args["name"].(string)
					companyName, _ := p.Args["company_name"].(string)
					status, _ := p.Args["status"].(string)
					filter := map[string]interface{}{}

					if !limitOk {
						limit = 10
					}
					if !offsetOK {
						offset = 0
					}
					if limit > 0 {
						filter["limit"] = limit
					}
					if offset >= 0 {
						filter["offset"] = offset
					}
					if username != "" {
						filter["search_username"] = username
					}
					if name != "" {
						filter["name"] = name
					}
					if companyName != "" && loginUser.Role.Name == "admin" {
						filter["company_name"] = companyName
					}
					if status != "" {
						filter["status"] = status
					}
					if loginUser.Role.Name == "back_office_admin" {
						filter["company_id"] = loginEmployee.CompanyID
					}
					if loginUser.Role.Name == "admin" {
						filter["role_name_list"] = []string{"admin", "back_office_admin"}
					}

					res, total, err := service.GetUserAndUserDetailList(filter)
					
					output := service.UserList{
						User: res,
						Total: total,
					}
					return output, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) DuplicateFieldInAuthen() ControllerReturn {
	return func () (string, *graphql.Field) {
		return "duplicate_field_in_authen", &graphql.Field{
			Type: DuplicateFieldAuthenType,
			Args: graphql.FieldConfigArgument{
				"username": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func (p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "account", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var result service.ValidateUser

					username, usernameOk := p.Args["username"].(string)
					filter := map[string]interface{}{}

					if username != "" {
						filter["username"] = username
					}

					_, total, err := service.GetUserList(filter)
					if total > 0 {
						if usernameOk {
							result.Username = true
						}
					}

					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) GetRoles() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "roles", &graphql.Field{
			Type: graphql.NewList(RoleType),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				response, err := service.GetRoles()

				return response, err
			},
		}
	}
}

func GetUserEmployeeFromSession(p graphql.ResolveParams) (user model.User, employee service.UserDetails, err error) {
	userIDstr, userIDOk := p.Context.Value(authorize.UserIDHeaderKey).(string)
	if !userIDOk {
		err = errors.New("invalid bearer")
		return
	}
	userID, _ := strconv.Atoi(userIDstr)

	if user, err = service.GetUserFromID(userID); err == nil {
		if employee, err = service.GetEmployeeByUserID(userID); err != nil {
			err = nil
		}
	}

	return
}

func (c *Controller) PutUser() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_user", &graphql.Field{
			Type: UserResponseType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"role_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"username": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"password": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "account", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var userInput model.UserInput
					jsonUser, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonUser, &userInput)

					res, err := service.PutUser(userInput)

					if err != nil || res == 0 {
						return nil, err
					}

					return model.UserResponse{
						ID      : res,
						Message : "Edit User Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) PutUserAndEmployee() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_user_and_employee", &graphql.Field{
			Type: UserResponseType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"role_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"username": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "account", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					// Update username
					var userInput model.UserInput 
					jsonUser, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonUser, &userInput)

					userResult, userErr := service.PutUser(userInput)

					// Get employee information				
					user_id, _ := p.Args["id"].(int)
					filter := map[string]interface{}{}
					if user_id > 0 {
						filter["user_id"] = user_id
					}

					employeeDetail, total, err := service.GetEmployeeByFilter(filter)
					if err != nil || total == 0 {
						return nil, err
					}

					// Update company email in employee contact information
					loginUser, userDetail, userErr := GetUserEmployeeFromSession(p)
					if loginUser.Role.Name != "back_office_admin" && userErr != nil {
						return nil, userErr
					}
					personal_id := employeeDetail[0].ID
					empInfo := map[string]interface{}{}
					empInfo["id"] = personal_id
					empInfo["company_email"] = p.Args["username"].(string)

					_, employeeErr := service.PutEmployeeContactInformation(personal_id, empInfo, userDetail)
					if employeeErr != nil && (userErr != nil || userResult == 0) {
						return nil, employeeErr
					}

					return model.UserResponse{
						ID      : userResult,
						Message : "Edit User and Employee Success",
					},userErr
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) SetPassword() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "set_password", &graphql.Field{
			Type: UserResponseType,
			Args: graphql.FieldConfigArgument{
				"secret": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"password": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				//Get password session
				filter := map[string]interface{}{}
				filter["secret"] = p.Args["secret"]
				pwdSessionList, _, pwdSessionErr := service.GetPasswordSessionList(filter)
				if pwdSessionErr != nil {
					return nil, pwdSessionErr
				}
				
				// Set Password by call the PutUser function 
				var userInput model.UserInput
				jsonUser, _ := json.Marshal(p.Args)
				json.Unmarshal(jsonUser, &userInput)
				userInput.ID = pwdSessionList[0].UserID

				err := service.VerifyPassword(userInput.Password)
				if err != nil {
					return nil, err
				}

				userResult, userErr := service.PutUser(userInput)
				if userErr != nil {
					return nil, userErr
				}

				// Update expired date by call the PutUserDetail function 
				var userDetail model.UserDetailInput
				now := time.Now()
				userDetail.ID = userInput.ID
				userDetail.ExpiredDate = now.AddDate(0, 0, 90)
				_, userDetailErr := service.PutUserDetail(userDetail)

				if userDetailErr != nil {
					return nil, userDetailErr
				}

				// Get Password Session Detail
				userFilter := map[string]interface{}{}
				userFilter["user_id"] = userResult
				passwordResult, total, passwordErr := service.GetPasswordSessionList(userFilter)
				if total <= 0 {
					return nil, passwordErr
				}

				// Delete Password Session
				_, secretErr := service.DeletePasswordSession(passwordResult[0].ID)
				if secretErr != nil {
					return nil, secretErr
				}

				return model.UserResponse{
					ID		: userResult,
					Message	: "Set Password Success",
				}, userErr
			},
		}
	}
}

func (c *Controller) ResetPassword() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "reset_password", &graphql.Field{
			Type: UserResponseType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "password", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var userInput model.UserInput
					jsonUser, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonUser, &userInput)

					userID, userErr := service.PutUserAndPasswordSession(userInput)

					if userErr != nil {
						return nil, userErr
					}

					return model.UserResponse{
						ID		: userID,
						Message	: "Reset Password Success",
					}, userErr
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) ForgotPassword() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "forgot_password", &graphql.Field{
			Type: UserResponseType,
			Args: graphql.FieldConfigArgument{
				"username": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				var userInput model.UserInput
				jsonUser, _ := json.Marshal(p.Args)
				json.Unmarshal(jsonUser, &userInput)

				filter := map[string]interface{}{}
				filter["username"] = userInput.Username
				userList, total, err := service.GetUserList(filter)
				if err != nil {
					return nil, err
				}

				if total >= 1 {
					_, err = service.PasswordSessionHelper(userList[0].ID)
				}

				if err != nil {
					return nil, err
				}

				return model.UserResponseMessage{
					Message	: "Forgot Password Success",
				}, err
			},
		}
	}
}

func (c *Controller) ChangePassword() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "change_password", &graphql.Field{
			Type: UserResponseType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"old_password": &graphql.ArgumentConfig{
					Type: PasswordInputType,
				},
				"new_password": &graphql.ArgumentConfig{
					Type: PasswordInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "password", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					loginUser, _, _ := GetUserEmployeeFromSession(p)

					// marshal old password
					var oldPassword model.UserPassword
					jsonOldPassword, _ := json.Marshal(p.Args["old_password"])
					json.Unmarshal(jsonOldPassword, &oldPassword)

					// marshal new password
					var newPassword model.UserInput
					jsonNewPassword, _ := json.Marshal(p.Args["new_password"])
					json.Unmarshal(jsonNewPassword, &newPassword)	
					
					res, err := service.ChangePasswordHelper(loginUser, newPassword, oldPassword)

					if err != nil {
						return nil, err
					}
					
					// Update expired date by call the PutUserDetail function 
					var userDetail model.UserDetailInput
					now := time.Now()
					userDetail.ID = loginUser.ID
					userDetail.ExpiredDate = now.AddDate(0, 0, 90)
					_, userDetailErr := service.PutUserDetail(userDetail)

					if userDetailErr != nil {
						return nil, userDetailErr
					}

					return model.UserResponse{
						ID		: res,
						Message	: "Change Password Success",
					}, err
				}	
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)			
			},
		}		
	}	
}

func (c *Controller) ValidateSecret() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "validate_secret", &graphql.Field{
			Type: SecretResponseType,
			Args: graphql.FieldConfigArgument{
				"secret": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {

				var secretInput model.PasswordSessionInput
				jsonSecret, _ := json.Marshal(p.Args)
				json.Unmarshal(jsonSecret, &secretInput)	

				filter := map[string]interface{}{}
				filter["secret"] = secretInput.Secret

				userResult, total, userErr := service.GetPasswordSessionList(filter)

				if userErr != nil {
					return 0, errors.New("Cannot get user by secret")
				}

				if total > 0 {
					return model.SecretResponse{
						User  : userResult,
						Message : "Get Password Session Success ",
						}, userErr
				}
				return userResult, userErr
			},
		}
	}
}
