package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"
	"strconv"

	_ "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
	"github.com/thoas/go-funk"
)

type ReimbursementRequestMessage struct {
	Id      int    `json:"id"`
	Message string `json:"message"`
}
type ActivityDetails struct {
	Date   string `json:"date" mapstructure:"date"`
	Action string `json:"action" mapstructure:"action"`
	EditBy string `json:"edit_by" mapstructure:"edit_by"`
	Remark string `json:"remark" mapstructure:"remark"`
}
type ApproverDetails struct {
	ID       int    `json:"id" mapstructure:"id"`
	Name     string `json:"display_name" mapstructure:"name"`
	Position string `json:"position" mapstructure:"position"`
}

var FoodReimbursementRequestType *graphql.Object
var FoodReimbursementRequestList *graphql.Object
var RequestMessageType *graphql.Object
var ApproversType *graphql.Object
var ActivityLogType *graphql.Object
var FoodAllowanceInputType *graphql.InputObject
var FoodAllowanceDetailsType *graphql.Object
var RequestFileInputType *graphql.InputObject
var RequestFileType *graphql.Object
var ReimbursementRequestType *graphql.Object
var ReimbursemetRequestsList *graphql.Object

func init() {
	FoodReimbursementRequestType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ReimbursementRequest",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"requester_id": &graphql.Field{
						Type: graphql.Int,
					},
					"charge_code": &graphql.Field{
						Type: graphql.String,
					},
					"type": &graphql.Field{
						Type: graphql.String,
					},
					"cost": &graphql.Field{
						Type: graphql.Float,
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
					"approver_id": &graphql.Field{
						Type: graphql.Int,
					},
					"processed_date": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"information": &graphql.Field{
						Type: graphql.String,
					},
					"remark": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"approvers": &graphql.Field{
						Type: graphql.NewList(ApproversType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {

							approval, ok := p.Source.(model.FoodReimbursementRequest)
							if !ok {
								return nil, errors.New("Invalid approval")
							}
							approversResult := GetApproverOrReviewerDetails(ParseApproverIDsToInt(approval.ApproverIDs))

							return approversResult, nil
						},
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
					"file_ids": &graphql.Field{
						Type: graphql.NewList(RequestFileType),
					},
					"reviewers": &graphql.Field{
						Type: graphql.NewList(ApproversType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {

							reviewer, ok := p.Source.(model.FoodReimbursementRequest)
							if !ok {
								return nil, errors.New("Invalid reviewer")
							}
							reviewersResult := GetApproverOrReviewerDetails(ParseReviewerIDsToInt(reviewer.ReviewerIDs))
							return reviewersResult, nil
						},
					},

					"activity_log": &graphql.Field{
						Type: graphql.NewList(ActivityLogType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							return GetFoodActivityLog(p)
						},
					},
				}
			}),
		},
	)
	ReimbursementRequestType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ReimbursementRequestType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"requester_id": &graphql.Field{
						Type: graphql.Int,
					},
					"charge_code": &graphql.Field{
						Type: graphql.String,
					},
					"type": &graphql.Field{
						Type: graphql.String,
					},
					"cost": &graphql.Field{
						Type: graphql.Float,
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
					"approver_id": &graphql.Field{
						Type: graphql.Int,
					},
					"processed_date": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"remark": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"approvers": &graphql.Field{
						Type: graphql.NewList(ApproversType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {

							request, ok := p.Source.(model.ReimbursementRequest)
							if !ok {
								return nil, errors.New("Invalid approval")
							}
							approversResult := GetApproverOrReviewerDetails(ParseApproverIDsToInt(request.ApproverIDs))
							return approversResult, nil
						},
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
					"file_ids": &graphql.Field{
						Type: graphql.NewList(RequestFileType),
					},
					"reviewers": &graphql.Field{
						Type: graphql.NewList(ApproversType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {

							request, ok := p.Source.(model.ReimbursementRequest)
							if !ok {
								return nil, errors.New("Invalid reviewer")
							}
							reviewersResult := GetApproverOrReviewerDetails(ParseReviewerIDsToInt(request.ReviewerIDs))
							return reviewersResult, nil
						},
					},
					"requester_name": &graphql.Field{
						Type: graphql.String,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							var generalInfo *model.EmployeePersonnelInfo
							var result string
							if p.Source.(model.ReimbursementRequest).RequesterID != 0 {
								resultEmployee, _ := service.GetEmployeeByPersonalID(p.Source.(model.ReimbursementRequest).RequesterID)
								generalInfo = resultEmployee.EmployeePersonnelInfo
								result = generalInfo.FirstNameEn + " " + generalInfo.LastNameEn
							}
							return result, nil
						},
					},
				}
			}),
		},
	)

	FoodReimbursementRequestList = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ReimbursementList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"total": &graphql.Field{
						Type: graphql.Int,
					},
					"reimbursement_requests": &graphql.Field{
						Type: graphql.NewList(FoodReimbursementRequestType),
					},
				}
			}),
		},
	)
	ReimbursemetRequestsList = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ReimbursementRequestTypeList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"total": &graphql.Field{
						Type: graphql.Int,
					},
					"reimbursement_requests": &graphql.Field{
						Type: graphql.NewList(ReimbursementRequestType),
					},
				}
			}),
		},
	)

	RequestMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ReimbursementRequestMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	FoodAllowanceDetailsType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "FoodAllowanceDetailsType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"requester_id": &graphql.Field{
						Type: graphql.Int,
					},
					"information": &graphql.Field{
						Type: graphql.String,
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	ApproversType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ApproversType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"display_name": &graphql.Field{
						Type: graphql.String,
					},
					"position": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	FoodAllowanceInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "FoodAllowanceInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"information": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	RequestFileInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "RequestFileInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"url": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	RequestFileType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "RequestFile",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"url": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	ReviewerInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "ReviewerInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)

	ActivityLogType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ActivityLog",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"date": &graphql.Field{
						Type: graphql.String,
					},
					"action": &graphql.Field{
						Type: graphql.String,
					},
					"edit_by": &graphql.Field{
						Type: graphql.String,
					},
					"remark": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

}

func GetFoodActivityLog(p graphql.ResolveParams) (interface{}, error) {

	reimbursementRequest, ok := p.Source.(model.FoodReimbursementRequest)

	if !ok {
		return nil, errors.New("Invalid reviewer")
	}
	logOut := GetLogs(reimbursementRequest.ID)

	return logOut, nil
}
func GetApproverOrReviewerDetails(idList []int) []ApproverDetails {
	return funk.Map(idList, func(id int) ApproverDetails {
		getEmployee, err := service.GetEmployeeByPersonalID(id)
		positionResult, positionErr := service.GetPosition(getEmployee.EmployeeGeneralInfo.PositionID)

		if positionErr != nil {
			return ApproverDetails{}
		}
		if err != nil {
			return ApproverDetails{}
		}
		name := getEmployee.EmployeePersonnelInfo.FirstNameEn + " " + getEmployee.EmployeePersonnelInfo.LastNameEn
		return ApproverDetails{
			ID:       id,
			Name:     name,
			Position: positionResult.Name,
		}
	}).([]ApproverDetails)
}
func ParseApproverIDsToInt(ApproverList []model.Approver) []int {
	return funk.Map(ApproverList, func(approver model.Approver) int {
		return approver.ID
	}).([]int)
}
func ParseReviewerIDsToInt(ReviewerList []model.ReimbursementRequestReviewer) []int {
	return funk.Map(ReviewerList, func(reviewer model.ReimbursementRequestReviewer) int {
		return reviewer.ID
	}).([]int)
}
func GetLogs(id int) []ActivityDetails {
	output := []ActivityDetails{}
	actionlogResult, _, errGet := service.GetChangelogList(service.REIMBURSEMENT_CONTENT, id)
	if errGet != nil {
		return output
	}
	funk.ForEach(actionlogResult.Record, func(record service.ChangelogRecordGet) {
		finalLog := ActivityDetails{
			Action: "Create",
			Date:   record.Timestamp,
			EditBy: record.EditBy.Name,
		}
		if record.Action != "create" {
			finalLog.Action = "Edit"
			funk.ForEach(record.Logs, func(log service.ChangelogFieldPost) {

				if log.Field == "FoodReimbursementRequest.Status" {
					finalLog.Action = GetAction(log)
				}
				if log.Field == "FoodReimbursementRequest.Remark" && log.To != "" {

					finalLog.Remark = log.To
				}
			})
		}
		output = append(output, finalLog)

	})
	return output
}

//PostReimbursementRequest
func (c *Controller) PostFoodReimbursementRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_food_reimbursement_request", &graphql.Field{
			Description: "Create food reimbursement request",
			Type:        RequestMessageType,
			Args: graphql.FieldConfigArgument{
				"charge_code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"type": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"cost": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"approver_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"information": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"file_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(RequestFileInputType),
				},
				"reviewers": &graphql.ArgumentConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					p.Args["created_by"] = employee.PersonalID
					p.Args["company_id"] = employee.CompanyID
					p.Args["requester_id"] = employee.PersonalID

					res, err := service.PostFoodReimbursementRequest(p.Args, employee)
					if err != nil {
						return nil, err
					}
					return ReimbursementRequestMessage{
						Id:      res,
						Message: "Create Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetReimbursementRequestList
func (c *Controller) GetFoodReimbursementRequestList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "reimbursement_requests", &graphql.Field{
			Type: ReimbursemetRequestsList,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"charge_code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"type": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"isApprover": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"isReviewer": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"isBackOffice": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					queryObject := service.ReimbursementQuery{
						Limit:  p.Args["limit"].(int),
						Offset: p.Args["offset"].(int),
					}
					_, employee, err := GetUserEmployeeFromSession(p)

					if p.Args["isApprover"] != nil {
						queryObject.ApproverID = employee.PersonalID
					}
					if p.Args["charge_code"] != nil {
						queryObject.Chargecode = p.Args["charge_code"].(string)
					}
					if p.Args["type"] != nil {
						queryObject.ReimbursementType = p.Args["type"].(string)
					}
					if p.Args["status"] != nil {
						queryObject.Status = p.Args["status"].(string)
					}
					if p.Args["isReviewer"] != nil {
						queryObject.ReviewerID = employee.PersonalID
					}
					if p.Args["isBackOffice"] != nil {
						queryObject.BackOfficeID = employee.PersonalID
						queryObject.CompanyID = employee.CompanyID
					}

					res, total, err := service.GetFoodReimbursementRequestList(queryObject)

					output := model.ReimbursementRequestList{
						Requests: res,
						Total:    total,
					}

					if err != nil {
						return nil, err
					}

					return output, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetReimbursementRequestByID
func (c *Controller) GetFoodReimbursementRequestByID() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "food_reimbursement_request", &graphql.Field{
			Type: FoodReimbursementRequestType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "read"},
							model.Permission{Module: "my_reimbursement_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetFoodReimbursementRequestByID(p.Args["id"].(int))
					return result, err
				}

				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//PutFoodReimbursementRequest
func (c *Controller) PutFoodReimbursementRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_food_reimbursement_request", &graphql.Field{
			Description: "Edit food reimbursement request",
			Type:        RequestMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"charge_code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"cost": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"approver_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"information": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"remark": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"file_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(RequestFileInputType),
				},
				"reviewers": &graphql.ArgumentConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "edit"},
							model.Permission{Module: "my_reimbursement_request", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					p.Args["updated_by"] = employee.PersonalID
					p.Args["company_id"] = employee.CompanyID

					res, err := service.PutFoodReimbursementRequestWithChangelog(p.Args, employee, employee.CompanyID)

					return ReimbursementRequestMessage{
						Id:      res,
						Message: "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
func GetAction(input service.ChangelogFieldPost) string {

	resultAction := "Edit"

	if input.To == "REQUEST_RESUBMIT" {
		resultAction = "Request More"
	}
	if input.To == "PENDING_APPROVAL" {
		resultAction = "Review Confirm"
	}
	if input.To == "APPROVED" {
		resultAction = "Approved"
	}
	if input.To == "REJECT_APPROVAL" {
		resultAction = "Rejected"
	}
	if input.To == "PROCESSED" {
		resultAction = "Processed"
	}
	if input.To == "CANCELED" {
		resultAction = "Canceled"
	}

	return resultAction
}

//PutFoodReimbursementRequest
func (c *Controller) PutStatusFoodReimbursementRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "put_status_reimbursement_request", &graphql.Field{
			Description: "Put Status Reimbursement Request",
			Type:        RequestMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"remark": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"processed_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "edit"},
							model.Permission{Module: "my_reimbursement_request", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var requestInput model.ReimbursementRequest
					_, employee, err := GetUserEmployeeFromSession(p)
					jsonData, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonData, &requestInput)
					requestInput.UpdatedBy = employee.PersonalID
					res, err := service.PutFoodReimbursementRequestStatusWithChangelog(&requestInput, employee)

					return ReimbursementRequestMessage{
						Id:      res,
						Message: "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetMyReimbursementRequestList
func (c *Controller) GetMyReimbursementRequestList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "my_reimbursement_requests", &graphql.Field{
			Type: ReimbursemetRequestsList,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"charge_code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"type": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "my_reimbursement_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userInfo, err := service.GetEmployeeByUserID(userID)

					if err != nil {
						return "fail", nil
					}

					queryObject := service.ReimbursementQuery{
						Limit:  p.Args["limit"].(int),
						Offset: p.Args["offset"].(int),
					}

					if p.Args["charge_code"] != nil {
						queryObject.Chargecode = p.Args["charge_code"].(string)
					}
					if p.Args["type"] != nil {
						queryObject.ReimbursementType = p.Args["type"].(string)
					}
					if p.Args["status"] != nil {
						queryObject.Status = p.Args["status"].(string)
					}

					if userInfo.PersonalID > 0 {
						queryObject.RequesterID = userInfo.PersonalID
					} else {
						queryObject.RequesterID = -2
					}

					res, total, err := service.GetMyReimbursementRequestList(queryObject)

					output := model.ReimbursementRequestList{
						Requests: res,
						Total:    total,
					}

					if err != nil {
						return nil, err
					}

					return output, err

				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
