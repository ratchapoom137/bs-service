package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	_ "encoding/json"
	"errors"
	_ "strconv"

	_ "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
)

var PerDiemReimbursementRequestType *graphql.Object
var PerDiemReimbursementRequestList *graphql.Object
var PerDiemInputType *graphql.InputObject
var PerDiemDetailsType *graphql.Object

func init() {
	PerDiemReimbursementRequestType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "PerDiemReimbursementRequest",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"request_id": &graphql.Field{
						Type: graphql.Int,
					},
					"charge_code": &graphql.Field{
						Type: graphql.String,
					},
					"type": &graphql.Field{
						Type: graphql.String,
					},
					"cost": &graphql.Field{
						Type: graphql.Float,
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
					"approver_id": &graphql.Field{
						Type: graphql.Int,
					},
					"processed_date": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"remark": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"approvers": &graphql.Field{
						Type: graphql.NewList(ApproversType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							approval, ok := p.Source.(model.PerDiemReimbursementRequest)
							if !ok {
								return nil, errors.New("Invalid approval")
							}
							approversResult := GetApproverOrReviewerDetails(ParseApproverIDsToInt(approval.ApproverIDs))
							return approversResult, nil
						},
					},
					"reviewers": &graphql.Field{
						Type: graphql.NewList(ApproversType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {

							reviewer, ok := p.Source.(model.PerDiemReimbursementRequest)
							if !ok {
								return nil, errors.New("Invalid reviewer")
							}
							reviewersResult := GetApproverOrReviewerDetails(ParseReviewerIDsToInt(reviewer.ReviewerIDs))
							return reviewersResult, nil
						},
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
					"perdiems": &graphql.Field{
						Type: graphql.NewList(PerDiemDetailsType),
					},
					"total_hours": &graphql.Field{
						Type: graphql.Int,
					},
					"file_ids": &graphql.Field{
						Type: graphql.NewList(RequestFileType),
					},
					"activity_log": &graphql.Field{
						Type: graphql.NewList(ActivityLogType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							return GetPerdiemActivityLog(p)
						},
					},
				}
			}),
		},
	)

	PerDiemReimbursementRequestList = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "PerDiemReimbursementRequestList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"total": &graphql.Field{
						Type: graphql.Int,
					},
					"per_diem_reimbursement_requests": &graphql.Field{
						Type: graphql.NewList(PerDiemReimbursementRequestType),
					},
				}
			}),
		},
	)

	PerDiemDetailsType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "PerDiemDetailsType",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"request_id": &graphql.Field{
						Type: graphql.Int,
					},
					"perdiem_description": &graphql.Field{
						Type: graphql.String,
					},
					"hour": &graphql.Field{
						Type: graphql.Int,
					},
					"expend": &graphql.Field{
						Type: graphql.Float,
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	PerDiemInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "PerDiemInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"perdiem_description": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"hour": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"expend": &graphql.InputObjectFieldConfig{
					Type: graphql.Float,
				},
				"date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)
}
func GetPerdiemActivityLog(p graphql.ResolveParams) (interface{}, error) {

	reimbursementRequest, ok := p.Source.(model.PerDiemReimbursementRequest)

	if !ok {
		return nil, errors.New("Invalid reviewer")
	}
	logOut := GetLogs(reimbursementRequest.ID)

	return logOut, nil
}
func (c *Controller) PostPerDiemReimbursementRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_perdiem_reimbursement_request", &graphql.Field{
			Description: "Create per diem reimbursement request",
			Type:        RequestMessageType,
			Args: graphql.FieldConfigArgument{
				"charge_code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"type": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"cost": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"approver_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"perdiems": &graphql.ArgumentConfig{
					Type: graphql.NewList(PerDiemInputType),
				},
				"file_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(RequestFileInputType),
				},
				"reviewers": &graphql.ArgumentConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
				"total_hours": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					p.Args["created_by"] = employee.PersonalID
					p.Args["requester_id"] = employee.PersonalID
					p.Args["updated_by"] = employee.PersonalID
					p.Args["company_id"] = employee.CompanyID

					res, err := service.PostPerDiemReimbursementRequest(p.Args, employee)
					if err != nil {
						return nil, err
					}
					return ReimbursementRequestMessage{
						Id:      res,
						Message: "Create Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//GetPerdiemReimbursementRequestByID
func (c *Controller) GetPerdiemReimbursementRequestByID() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "perdiem_reimbursement_request", &graphql.Field{
			Type: PerDiemReimbursementRequestType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "read"},
							model.Permission{Module: "my_reimbursement_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetPerdiemReimbursementRequestByID(p.Args["id"].(int))
					return result, err
				}

				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) PutPerDiemReimbursementRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_perdiem_reimbursement_request", &graphql.Field{
			Description: "Edit per diem reimbursement request",
			Type:        RequestMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"cost": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"approver_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"remark": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"file_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(RequestFileInputType),
				},
				"reviewers": &graphql.ArgumentConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
				"perdiems": &graphql.ArgumentConfig{
					Type: graphql.NewList(PerDiemInputType),
				},
				"total_hours": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "edit"},
							model.Permission{Module: "my_reimbursement_request", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					p.Args["created_by"] = employee.PersonalID
					p.Args["request_id"] = employee.PersonalID
					p.Args["updated_by"] = employee.PersonalID
					p.Args["company_id"] = employee.CompanyID

					res, err := service.PutPerDiemReimbursementRequestWithLog(p.Args, employee)

					return ReimbursementRequestMessage{
						Id:      res,
						Message: "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
