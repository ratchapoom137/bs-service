package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"errors"

	_ "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
)

var OthersReimbursementRequestType *graphql.Object

func init() {
	OthersReimbursementRequestType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OthersReimbursementRequest",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"requester_id": &graphql.Field{
						Type: graphql.Int,
					},
					"charge_code": &graphql.Field{
						Type: graphql.String,
					},
					"type": &graphql.Field{
						Type: graphql.String,
					},
					"cost": &graphql.Field{
						Type: graphql.Float,
					},
					"status": &graphql.Field{
						Type: graphql.String,
					},
					"approver_id": &graphql.Field{
						Type: graphql.Int,
					},
					"processed_date": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"information": &graphql.Field{
						Type: graphql.String,
					},
					"remark": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"approvers": &graphql.Field{
						Type: graphql.NewList(ApproversType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							approval, ok := p.Source.(model.OthersReimbursementRequest)
							if !ok {
								return nil, errors.New("Invalid approval")
							}
							approversResult := GetApproverOrReviewerDetails(ParseApproverIDsToInt(approval.ApproverIDs))
							return approversResult, nil
						},
					},
					"date": &graphql.Field{
						Type: graphql.String,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
					"file_ids": &graphql.Field{
						Type: graphql.NewList(RequestFileType),
					},
					"reviewers": &graphql.Field{
						Type: graphql.NewList(ApproversType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							reviewer, ok := p.Source.(model.OthersReimbursementRequest)
							if !ok {
								return nil, errors.New("Invalid reviewer")
							}
							reviewersResult := GetApproverOrReviewerDetails(ParseReviewerIDsToInt(reviewer.ReviewerIDs))
							return reviewersResult, nil
						},
					},
					"activity_log": &graphql.Field{
						Type: graphql.NewList(ActivityLogType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							return GetOtherActivityLog(p)
						},
					},
				}
			}),
		},
	)
}

func GetOtherActivityLog(p graphql.ResolveParams) (interface{}, error) {

	reimbursementRequest, ok := p.Source.(model.OthersReimbursementRequest)

	if !ok {
		return nil, errors.New("Invalid reviewer")
	}
	logOut := GetLogs(reimbursementRequest.ID)

	return logOut, nil
}

func (c *Controller) PostOthersReimbursementRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_others_reimbursement_request", &graphql.Field{
			Description: "Create others reimbursement request",
			Type:        RequestMessageType,
			Args: graphql.FieldConfigArgument{
				"charge_code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"type": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"cost": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"approver_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"information": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"file_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(RequestFileInputType),
				},
				"reviewers": &graphql.ArgumentConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					p.Args["created_by"] = employee.PersonalID
					p.Args["company_id"] = employee.CompanyID
					p.Args["requester_id"] = employee.PersonalID

					res, err := service.PostOthersReimbursementRequest(p.Args, employee)
					if err != nil {
						return nil, err
					}
					return ReimbursementRequestMessage{
						Id:      res,
						Message: "Create Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) GetOthersReimbursementRequestByID() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "others_reimbursement_request", &graphql.Field{
			Type: OthersReimbursementRequestType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "read"},
							model.Permission{Module: "my_reimbursement_request", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetOthersReimbursementRequestByID(p.Args["id"].(int))
					return result, err
				}

				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) PutOthersReimbursementRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_others_reimbursement_request", &graphql.Field{
			Description: "Edit others reimbursement request",
			Type:        RequestMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"charge_code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"cost": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"approver_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"information": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"remark": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"file_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(RequestFileInputType),
				},
				"reviewers": &graphql.ArgumentConfig{
					Type: graphql.NewList(ReviewerInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "reimbursement_request", Function: "edit"},
							model.Permission{Module: "my_reimbursement_request", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					_, employee, err := GetUserEmployeeFromSession(p)
					p.Args["updated_by"] = employee.PersonalID
					p.Args["company_id"] = employee.CompanyID

					res, err := service.PutOthersReimbursementRequestWithLog(p.Args, employee)
					return ReimbursementRequestMessage{
						Id:      res,
						Message: "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
