package graph

import (
	"api/model"
	"api/service"
	"encoding/json"

	"github.com/graphql-go/graphql"
)

var InboxType *graphql.Object
var InboxInputType *graphql.InputObject
var InboxListType *graphql.Object
var InboxResponseMessageType *graphql.Object

func init() {
	InboxType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Inbox",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"sender": &graphql.Field{
						Type: graphql.String,
					},
					"receiver": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"footer": &graphql.Field{
						Type: graphql.String,
					},
					"button_name": &graphql.Field{
						Type: graphql.String,
					},
					"link": &graphql.Field{
						Type: graphql.String,
					},
					"title": &graphql.Field{
						Type: graphql.String,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	InboxInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "InboxInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"sender": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"receiver": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"description": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"footer": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"button_name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"link": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"title": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

	InboxListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "InboxList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"inbox_list": &graphql.Field{
						Type: graphql.NewList(InboxType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)

	InboxResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "InboxResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"inbox_id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

// GetInbox -
func (c *Controller) GetInbox() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "get_inbox", &graphql.Field{
			Type: InboxType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				result, err := service.GetInbox(p.Args["id"].(int))
				if err != nil {
					return nil, err
				}

				return result, err
			},
		}
	}
}

// GetInboxList -
func (c *Controller) GetInboxList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "get_inbox_list", &graphql.Field{
			Type: InboxListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"receiver": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				limit, _ := p.Args["limit"].(int)
				offset, _ := p.Args["offset"].(int)
				receiver, _ := p.Args["receiver"].(string)

				filter := map[string]interface{}{}
				filter["limit"] = limit
				filter["offset"] = offset
				filter["receiver"] = receiver

				result, total, err := service.GetInboxList(filter)
				if err != nil {
					return nil, err
				}

				return model.InboxList{
					InboxList: result,
					Total:     total,
				}, err
			},
		}
	}
}

// PostInbox -
func (c *Controller) PostInbox() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "post_inbox", &graphql.Field{
			Type: InboxResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"inbox_input": &graphql.ArgumentConfig{
					Type: InboxInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				var inbox model.Inbox
				jsonData, _ := json.Marshal(p.Args["inbox_input"])
				json.Unmarshal(jsonData, &inbox)

				res, err := service.PostInbox(inbox)
				if err != nil || res == 0 {
					return nil, err
				}

				return model.InboxResponseMessage{
					InboxID: res,
					Message: "Create Inbox Success",
				}, err
			},
		}
	}
}

// PostInboxGreeting -
func (c *Controller) PostInboxGreeting() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "post_inbox_greeting", &graphql.Field{
			Type: InboxResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"request_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				request, err := service.GetRegistrationRequest(p.Args["request_id"].(int))
				if err != nil {
					return nil, err
				}

				inbox, err := GenerateGreetingInbox(request)
				if err != nil {
					return nil, err
				}

				res, err := service.PostInbox(inbox)
				if err != nil || res == 0 {
					return nil, err
				}

				return model.InboxResponseMessage{
					InboxID: res,
					Message: "Create Inbox Success",
				}, err
			},
		}
	}
}

// PostInboxReject -
func (c *Controller) PostInboxReject() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "post_inbox_reject", &graphql.Field{
			Type: InboxResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"request_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"reason": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				request, err := service.GetRegistrationRequest(p.Args["request_id"].(int))
				if err != nil {
					return nil, err
				}

				inbox, err := GenerateRejectInbox(request, p.Args["reason"].(string))
				if err != nil {
					return nil, err
				}

				res, err := service.PostInbox(inbox)
				if err != nil || res == 0 {
					return nil, err
				}

				return model.InboxResponseMessage{
					InboxID: res,
					Message: "Create Inbox Success",
				}, err
			},
		}
	}
}

// PostInboxResubmit -
func (c *Controller) PostInboxResubmit() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "post_inbox_resubmit", &graphql.Field{
			Type: InboxResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"request_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"reason": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				request, err := service.GetRegistrationRequest(p.Args["request_id"].(int))
				if err != nil {
					return nil, err
				}

				inbox, err := GenerateResubmitInbox(request, p.Args["reason"].(string))
				if err != nil {
					return nil, err
				}

				res, err := service.PostInbox(inbox)
				if err != nil || res == 0 {
					return nil, err
				}

				return model.InboxResponseMessage{
					InboxID: res,
					Message: "Create Inbox Success",
				}, err
			},
		}
	}
}

// PostInboxApprove -
func (c *Controller) PostInboxApprove() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "post_inbox_approve", &graphql.Field{
			Type: InboxResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"request_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				request, err := service.GetRegistrationRequest(p.Args["request_id"].(int))
				if err != nil {
					return nil, err
				}

				inbox, err := GenerateApproveInbox(request)
				if err != nil {
					return nil, err
				}

				res, err := service.PostInbox(inbox)
				if err != nil || res == 0 {
					return nil, err
				}

				return model.InboxResponseMessage{
					InboxID: res,
					Message: "Create Inbox Success",
				}, err
			},
		}
	}
}

// PostInboxResetPassword -
func (c *Controller) PostInboxResetPassword() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "post_inbox_reset_password", &graphql.Field{
			Type: InboxResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"user_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				userDet, err := service.GetUserAndUserDetail(p.Args["user_id"].(int))
				if err != nil {
					return nil, err
				}

				inbox, err := GenerateResetPasswordInbox(userDet)
				if err != nil {
					return nil, err
				}

				res, err := service.PostInbox(inbox)
				if err != nil || res == 0 {
					return nil, err
				}

				return model.InboxResponseMessage{
					InboxID: res,
					Message: "Create Inbox Success",
				}, err
			},
		}
	}
}

// PostInboxForgotPassword -
func (c *Controller) PostInboxForgotPassword() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "post_inbox_forgot_password", &graphql.Field{
			Type: InboxResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"username": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				filter := map[string]interface{}{}
				filter["username"], _ = p.Args["username"].(string)
				user, total, err := service.GetUserList(filter)
				if total <= 0 || err != nil {
					return model.InboxResponseMessage{
						InboxID: 0,
						Message: "Get User Failed",
					}, err
				}

				userDet, err := service.GetUserAndUserDetail(user[0].ID)
				if err != nil {
					return nil, err
				}

				inbox, err := GenerateForgotPasswordInbox(userDet)
				if err != nil {
					return nil, err
				}

				res, err := service.PostInbox(inbox)
				if err != nil || res == 0 {
					return nil, err
				}

				return model.InboxResponseMessage{
					InboxID: res,
					Message: "Create Inbox Success",
				}, err
			},
		}
	}
}

// PostCreateUserInbox -
func (c *Controller) PostCreateUserInbox() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "post_inbox_create_user", &graphql.Field{
			Type: InboxResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"username": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				inbox, err := GenerateCreateUserInbox(p.Args["username"].(string))
				if err != nil {
					return nil, err
				}

				res, err := service.PostInbox(inbox)
				if err != nil || res == 0 {
					return nil, err
				}

				return model.InboxResponseMessage{
					InboxID: res,
					Message: "Create Inbox Success",
				}, err
			},
		}
	}
}
