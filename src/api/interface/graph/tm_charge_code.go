package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"errors"
	_ "github.com/davecgh/go-spew/spew"
	"github.com/graphql-go/graphql"
	"strconv"
)

//ChargeCodeType -
var ChargeCodeType *graphql.Object
var EmployeeChargeCodeType *graphql.Object
var ChargeCodeQueryType *graphql.Object
var EmployeeChargeCodeInputType *graphql.InputObject
var EmployeeTypeEnum *graphql.Enum

func init() {
	EmployeeTypeEnum = graphql.NewEnum(graphql.EnumConfig{
		Name: "EmployeeType",
		Values: graphql.EnumValueConfigMap{
			"OWNER": &graphql.EnumValueConfig{
				Value: 1,
			},
			"MEMBER": &graphql.EnumValueConfig{
				Value: 2,
			},
		},
	})
	EmployeeChargeCodeInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "EmployeeChargeCodeInput",
			Fields: graphql.InputObjectConfigFieldMap{

				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"type": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"employee_charge_code_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"roll_on": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"roll_off": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)
	ChargeCodeType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ChargeCode",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"code": &graphql.Field{
						Type: graphql.String,
					},
					"start_date": &graphql.Field{
						Type: graphql.String,
					},
					"end_date": &graphql.Field{
						Type: graphql.String,
					},
					"is_active": &graphql.Field{
						Type: graphql.Boolean,
					},
					"employees": &graphql.Field{
						Type: graphql.NewList(EmployeeChargeCodeType),
					},
					"visibility_private": &graphql.Field{
						Type: graphql.Boolean,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"is_not_work": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	EmployeeChargeCodeType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "EmployeeChargeCode",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"type": &graphql.Field{
						Type: graphql.String,
					},
					"employee_charge_code_id": &graphql.Field{
						Type: graphql.Int,
					},
					"roll_on": &graphql.Field{
						Type: graphql.String,
					},
					"roll_off": &graphql.Field{
						Type: graphql.String,
					},
					"name": &graphql.Field{
						Type: graphql.String,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					
							EmpChargecode, ok := p.Source.(model.EmployeeChargeCode)
							if !ok {
								return nil, errors.New("Invalid employee details")
							}
							getEmployee, err := service.GetEmployeeByPersonalID(EmpChargecode.EmployeeID)
							if err != nil {
								return nil, errors.New("Invalid employee details")
							}
							return getEmployee.EmployeePersonnelInfo.FirstNameEn + " " + getEmployee.EmployeePersonnelInfo.LastNameEn, nil
						},
					},
				}
			}),
		},
	)

	ChargeCodeQueryType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "ChargeCodes",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"charge_codes": &graphql.Field{
						Type: graphql.NewList(ChargeCodeType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
}

//GetChargeCodeList -
func (c *Controller) GetChargeCodeList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "charge_codes", &graphql.Field{
			Type: ChargeCodeQueryType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"search_keyword": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "charge_code", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))
					userInfo, err := service.GetEmployeeByUserID(userID)
					userAccount := p.Context.Value(authorize.UserAccountHeaderKey)
					if err != nil {
						return "fail", nil
					}

					queryObject := service.ChargeCodeQuery{
						Limit:      p.Args["limit"].(int),
						Offset:     p.Args["offset"].(int),
						EmployeeID: userInfo.PersonalID,
						CompanyID:  userInfo.CompanyID,
					}
					if p.Args["search_keyword"] != nil {
						queryObject.SearchKeyword = p.Args["search_keyword"].(string)
					}
					if p.Args["status"] != nil {
						queryObject.Status = p.Args["status"].(string)
					}

					if userAccount.(model.User).Role.Name == "employee" || userAccount.(model.User).Role.Name == "manager" {
						return service.GetChargeCodeListByEmployeeID(queryObject)
					}
					if userAccount.(model.User).Role.Name == "admin" {
						queryObject.CompanyID = 0
					}

					//If have authen as admin return get all (admin)
					return service.GetChargeCodeList(queryObject)
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetHolidayPresetDetail -
func (c *Controller) GetChargeCodeDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "charge_code", &graphql.Field{
			Type: ChargeCodeType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "charge_code", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetChargeCodeDetail(p.Args["id"].(int))
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostChargeCode -
func (c *Controller) PostChargeCode() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_charge_code", &graphql.Field{
			Description: "Create new charge code",
			Type:        ChargeCodeType,
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"code": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"is_not_work": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"start_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"end_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"is_active": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"employees": &graphql.ArgumentConfig{
					Type: graphql.NewList(EmployeeChargeCodeInputType),
				},
				"visibility_private": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "charge_code", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))

					userInfo, err := service.GetEmployeeByUserID(userID)

					if err != nil {
						return "fail", nil
					}

					p.Args["created_by"] = userInfo.PersonalID
					p.Args["updated_by"] = userInfo.PersonalID
					p.Args["company_id"] = userInfo.CompanyID

					result, err := service.PostChargeCode(p.Args)
					if err == nil {
						return result, nil
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutChargeCode -
func (c *Controller) PutChargeCode() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_charge_code", &graphql.Field{
			Description: "Edit existing charge code",
			Type:        ChargeCodeType,
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"start_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"end_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"is_active": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"employees": &graphql.ArgumentConfig{
					Type: graphql.NewList(EmployeeChargeCodeInputType),
				},

				"visibility_private": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"description": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"deleted_employee_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "charge_code", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					userID, _ := strconv.Atoi(p.Context.Value(authorize.UserIDHeaderKey).(string))

					userInfo, err := service.GetEmployeeByUserID(userID)
					if err != nil {
						return "fail", nil
					}

					p.Args["updated_by"] = userInfo.PersonalID
					result, err := service.PutChargeCode(p.Args)
					if err == nil {
						return result, nil
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// DeleteHolidayPreset -
func (c *Controller) DeleteChargeCode() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_charge_code", &graphql.Field{
			Description: "Delete Charge Code",
			Type:        ChargeCodeType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},

			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "charge_code", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					return service.DeleteChargeCode(p.Args["id"].(int))
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
