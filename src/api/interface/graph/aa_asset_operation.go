package graph

import (
	"api/model"
	"api/service"
	"encoding/json"
	"errors"

	authorize "api/helpers/authorize"

	"github.com/graphql-go/graphql"
)

var AssetOperationType *graphql.Object
var AssetOperationInputType *graphql.InputObject
var AssetOperationListType *graphql.Object
var AssetOperationResponseMessageType *graphql.Object
var AssetOperationListResponseMessageType *graphql.Object

func init() {
	AssetOperationType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "AssetOperation",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"asset_id": &graphql.Field{
						Type: graphql.Int,
					},
					"asset": &graphql.Field{
						Type: AssetType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							assetID := p.Source.(model.AssetOperation).AssetID
							res, err := service.GetAssetByID(assetID)
							if err != nil {
								return nil, err
							}
							return res, err
						},
					},
					"employee_id": &graphql.Field{
						Type: graphql.Int,
					},
					"employee": &graphql.Field{
						Type: EmployeeType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							empID := p.Source.(model.AssetOperation).EmployeeID
							if empID <= 0 {
								return nil, nil
							}
							res, e := service.GetEmployeeByPersonalID(empID)
							if e != nil {
								return nil, e
							}
							return res, nil
						},
					},
					"start_date": &graphql.Field{
						Type: graphql.String,
					},
					"due_date": &graphql.Field{
						Type: graphql.String,
					},
					"return_date": &graphql.Field{
						Type: graphql.String,
					},
					"assign_note": &graphql.Field{
						Type: graphql.String,
					},
					"return_note": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	AssetOperationInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "AssetOperationInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"asset_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"start_date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"due_date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"return_date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"assign_note": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"return_note": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"created_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"updated_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)
	AssetOperationListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "AssetOperations",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"asset_operations": &graphql.Field{
						Type: graphql.NewList(AssetOperationType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	AssetOperationResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "AssetOperationResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"asset_operation_id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	AssetOperationListResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "AssetOperationListResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"asset_operations_id": &graphql.Field{
						Type: graphql.NewList(graphql.Int),
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

// GetAssetOperation -
func (c *Controller) GetAssetOperation() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "asset_operation", &graphql.Field{
			Type: AssetOperationType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_operation", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					res, err := service.GetAssetOperationByID(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return res, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetAssetOperations -
func (c *Controller) GetAssetOperations() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "asset_operations", &graphql.Field{
			Type: AssetOperationListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"employee_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"asset_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"start_date_from": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"start_date_to": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"due_date_from": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"due_date_to": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"is_return": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"is_due_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"asset_category_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"serial_number": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"asset_name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_operation", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var filter service.AssetOperationFilter

					jsonData, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonData, &filter)

					_, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					filter.CompanyID = employee.CompanyID
					if filter.CompanyID == 0 {
						return nil, errors.New("no company id")
					}
					res, total, err := service.GetAssetOperations(filter)
					if err != nil {
						return nil, err
					}
					return model.AssetOperationList{
						AssetOperations: res,
						Total:           total,
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// CreateAssetOperation -
func (c *Controller) CreateAssetOperation() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_asset_operation", &graphql.Field{
			Type: AssetOperationResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"asset_operation_input": &graphql.ArgumentConfig{
					Type: AssetOperationInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_operation", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var assetOpInput model.AssetOperationInput

					jsonData, _ := json.Marshal(p.Args["asset_operation_input"])
					json.Unmarshal(jsonData, &assetOpInput)

					res, err := service.CreateAssetOperation(&assetOpInput)
					if err != nil {
						return nil, err
					}

					return model.AssetOperationResponseMessage{
						AssetOperationID: res,
						Message:          "Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// CreateAssetOperations -
func (c *Controller) CreateAssetOperations() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_asset_operations", &graphql.Field{
			Type: AssetOperationListResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"asset_operation_input_list": &graphql.ArgumentConfig{
					Type: graphql.NewList(AssetOperationInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_operation", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var resultID []int

					for _, item := range p.Args["asset_operation_input_list"].([]interface{}) {
						var assetOp model.AssetOperationInput

						jsonData, _ := json.Marshal(item)
						json.Unmarshal(jsonData, &assetOp)

						res, err := service.CreateAssetOperation(&assetOp)
						if err != nil {
							return nil, err
						}
						resultID = append(resultID, res)
					}

					return model.AssetOperationListResponseMessage{
						AssetOperationsID: resultID,
						Message:           "Success",
					}, nil
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// EditAssetOperation -
func (c *Controller) EditAssetOperation() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_asset_operation", &graphql.Field{
			Type: AssetOperationResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"asset_operation_input": &graphql.ArgumentConfig{
					Type: AssetOperationInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_operation", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var assetOpInput model.AssetOperationInput

					jsonData, _ := json.Marshal(p.Args["asset_operation_input"])
					json.Unmarshal(jsonData, &assetOpInput)

					res, err := service.EditAssetOperation(&assetOpInput)
					if err != nil {
						return nil, err
					}

					return model.AssetOperationResponseMessage{
						AssetOperationID: res,
						Message:          "Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// EditAssetOperations -
func (c *Controller) EditAssetOperations() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_asset_operations", &graphql.Field{
			Type: AssetOperationListResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"asset_operation_input_list": &graphql.ArgumentConfig{
					Type: graphql.NewList(AssetOperationInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "asset_operation", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var resultID []int

					for _, item := range p.Args["asset_operation_input_list"].([]interface{}) {
						var assetOp model.AssetOperationInput

						jsonData, _ := json.Marshal(item)
						json.Unmarshal(jsonData, &assetOp)

						res, err := service.EditAssetOperation(&assetOp)
						if err != nil {
							return nil, err
						}
						resultID = append(resultID, res)
					}

					return model.AssetOperationListResponseMessage{
						AssetOperationsID: resultID,
						Message:           "Success",
					}, nil
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
