package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"errors"
	"strconv"

	_ "github.com/davecgh/go-spew/spew"

	"github.com/graphql-go/graphql"
	"github.com/thoas/go-funk"
)

// HolidayPresetType -
var HolidayPresetType *graphql.Object

// HolidayPresetsListType -
var HolidayPresetsListType *graphql.Object

// HolidayType -
var HolidayType *graphql.Object

// HolidayInputType -
var HolidayInputType *graphql.InputObject

func init() {
	HolidayInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "holidayInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"holiday_name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"holiday_date": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"updated_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"created_by": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
			},
		},
	)
	HolidayType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Holiday",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"holiday_name": &graphql.Field{
						Type: graphql.String,
					},
					"holiday_date": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	HolidayPresetType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "HolidayPreset",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.String,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"amount": &graphql.Field{
						Type: graphql.Int,
					},
					"holidays": &graphql.Field{
						Type: graphql.NewList(HolidayType),
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	HolidayPresetsListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "HolidayPresets",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"preset_list": &graphql.Field{
						Type: graphql.NewList(HolidayPresetType),

						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							hpList, ok := p.Source.(service.HolidayPresetList)

							Preset := hpList.Presets

							if !ok {
								return nil, errors.New("Invalid holiday preset")
							}
							var result []service.HolidayPresetOutput
							result = funk.Map(Preset, func(hp model.HolidayPreset) service.HolidayPresetOutput {
								id := strconv.Itoa(hp.ID)
								if hp.Amount > 0 {
									result, _ := service.GetHolidayPresetDetail(id)
									return result
								}
								return service.HolidayPresetOutput{}
							}).([]service.HolidayPresetOutput)
							return result, nil
						},
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
}

// GetHolidayPresetDetail -
func (c *Controller) GetHolidayPresetDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "holiday_preset", &graphql.Field{
			Type: HolidayPresetType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "holiday_preset", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetHolidayPresetDetail(p.Args["id"].(string))
					return map[string]interface{}{
						"id":         result.ID,
						"name":       result.Name,
						"amount":     result.Amount,
						"holidays":   result.Holidays,
						"created_at": result.CreatedAt,
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetHolidayPresetList -
func (c *Controller) GetHolidayPresetList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "holiday_presets", &graphql.Field{
			Type: HolidayPresetsListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "holiday_preset", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetHolidayPresetList(p.Args["limit"].(int), p.Args["offset"].(int))
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostHolidayPreset -
func (c *Controller) PostHolidayPreset() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_holiday_preset", &graphql.Field{
			Description: "Create new Holiday Preset",
			Type:        HolidayPresetType,
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"amount": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"holidays": &graphql.ArgumentConfig{
					Type: graphql.NewList(HolidayInputType),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "holiday_preset", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.PostHolidayPreset(p.Args)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PutHolidayPreset -
func (c *Controller) PutHolidayPreset() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_holiday_preset", &graphql.Field{
			Description: "Edit Holiday Preset",
			Type:        HolidayPresetType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"amount": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"holidays": &graphql.ArgumentConfig{
					Type: graphql.NewList(HolidayInputType),
				},
				"delete_holiday_ids": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "holiday_preset", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {

					p.Args["created_by"] = 0
					p.Args["updated_by"] = 0

					result, err := service.PutHolidayPreset(p.Args)
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// DeleteHolidayPreset -
func (c *Controller) DeleteHolidayPreset() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_holiday_preset", &graphql.Field{
			Description: "Delete Holiday Preset",
			Type:        HolidayPresetType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "holiday_preset", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.DeleteHolidayPreset(p.Args["id"].(int))
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}
				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
