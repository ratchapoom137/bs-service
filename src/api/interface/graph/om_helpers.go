package graph

import (
	"api/model"
	"api/service"
	"strings"
	"time"
)

func CompanyCreateHelper(companyType model.Company, user model.User, headOffice model.CompanyAddress) (compID int, err error) {
	infiniteDate, _ := time.Parse(time.RFC3339, "9999-12-31T00:00:00+00:00")
	companyType.DeactivatedDate = infiniteDate
	companyType.CreatedBy = user.ID
	companyType.UpdatedBy = user.ID
	// Create Company
	compID, err = service.PostCompany(companyType)
	if err != nil || compID == 0 {
		return 0, err
	}

	headOffice.CompanyID = compID
	//Create Head Office Address
	_, err = service.PostAddress(headOffice, []model.CompanyAddress{}, compID)
	if err != nil {
		return 0, err
	}

	// Create Default Org Level
	levelID, err := service.PostLevel(model.OrgLevel{
		CompanyID: compID,
		Name:      companyType.NameEN,
		Value:     0,
	})
	if err != nil {
		return 0, err
	}

	// Create Default OrgUnit
	now := time.Now()
	truncator := 24 * time.Hour
	effDate := now.Truncate(truncator)

	_, err = service.PostOrgUnit(model.OrgUnit{
		OrgLevelID:  levelID,
		Name:        companyType.NameEN,
		EffectiveAt: effDate,
		TerminateAt: &infiniteDate,
	})
	if err != nil {
		return 0, err
	}

	// Create Default Approval Configuration
	_, err = service.PostApprovalConfig(model.ApprovalConfigInput{
		Name:       "General Request",
		CompanyID:  compID,
		ConfigType: "MANAGER",
		CreatedBy:  user.ID,
		UpdatedBy:  user.ID,
	})
	if err != nil {
		return 0, err
	}

	return
}

func mapInboxWithTemplate(inboxTemplate model.InboxTemplate) (inbox model.Inbox) {
	inbox.Title = inboxTemplate.Title
	inbox.Sender = inboxTemplate.Sender
	inbox.Description = inboxTemplate.Description
	inbox.Footer = inboxTemplate.Footer
	inbox.ButtonName = inboxTemplate.ButtonName
	inbox.Link = inboxTemplate.Link

	return
}

func GenerateGreetingInbox(request model.RegistrationRequest) (inbox model.Inbox, err error) {
	// Greeting Template ID: 1
	inboxTemplate, err := service.GetInboxTemplate(1)
	if err != nil {
		return
	}

	inboxTemplate.Description = strings.Replace(inboxTemplate.Description, "--receiverName--", request.FirstName, 1)
	inboxTemplate.Description = strings.Replace(inboxTemplate.Description, "--receiverEmail--", request.Email, 1)

	inbox = mapInboxWithTemplate(inboxTemplate)

	inbox.Receiver = request.Email

	return
}

func GenerateRejectInbox(request model.RegistrationRequest, reason string) (inbox model.Inbox, err error) {
	// Reject Template ID: 2
	inboxTemplate, err := service.GetInboxTemplate(2)
	if err != nil {
		return
	}

	inboxTemplate.Description = strings.Replace(inboxTemplate.Description, "--receiverName--", request.FirstName, 1)
	inboxTemplate.Description = strings.Replace(inboxTemplate.Description, "--reason--", reason, 1)

	inbox = mapInboxWithTemplate(inboxTemplate)

	inbox.Receiver = request.Email

	return
}

func GenerateResubmitInbox(request model.RegistrationRequest, reason string) (inbox model.Inbox, err error) {
	// Resubmit Template ID: 3
	inboxTemplate, err := service.GetInboxTemplate(3)
	if err != nil {
		return
	}

	inboxTemplate.Description = strings.Replace(inboxTemplate.Description, "--receiverName--", request.FirstName, 1)
	inboxTemplate.Description = strings.Replace(inboxTemplate.Description, "--moreInformation--", reason, 1)

	inbox = mapInboxWithTemplate(inboxTemplate)

	inbox.Receiver = request.Email
	inbox.Link = inbox.Link + "secret?" + request.SecretCode

	return
}

func GenerateApproveInbox(request model.RegistrationRequest) (inbox model.Inbox, err error) {
	// Approve Template ID: 4
	inboxTemplate, err := service.GetInboxTemplate(4)
	if err != nil {
		return
	}

	inboxTemplate.Description = strings.Replace(inboxTemplate.Description, "--receiverName--", request.FirstName, 1)
	inboxTemplate.Description = strings.Replace(inboxTemplate.Description, "--username--", request.Email, 1)

	inbox = mapInboxWithTemplate(inboxTemplate)

	inbox.Receiver = request.Email
	//TODO: เพิ่ม secret จาก password session
	filter := map[string]interface{}{}
	filter["username"] = request.Email
	user, _, _ := service.GetUserList(filter)
	filter["user_id"] = user[0].ID
	pwds, _, _ := service.GetPasswordSessionList(filter)
	secret := pwds[0].Secret

	inbox.Link = inbox.Link + "secret?" + secret

	return
}

func GenerateResetPasswordInbox(userDet service.UserAndUserDetail) (inbox model.Inbox, err error) {
	// Reset Template ID: 5
	inboxTemplate, err := service.GetInboxTemplate(5)
	if err != nil {
		return
	}

	inboxTemplate.Description = inboxTemplate.Description

	inbox = mapInboxWithTemplate(inboxTemplate)

	inbox.Receiver = userDet.User.Username
	//TODO: เพิ่ม secret จาก password session
	filter := map[string]interface{}{}
	filter["username"] = userDet.User.Username
	user, _, _ := service.GetUserList(filter)
	filter["user_id"] = user[0].ID
	pwds, _, _ := service.GetPasswordSessionList(filter)
	secret := pwds[0].Secret

	inbox.Link = inbox.Link + "secret?" + secret

	return
}

func GenerateForgotPasswordInbox(userDet service.UserAndUserDetail) (inbox model.Inbox, err error) {
	// Forgot Template ID: 6
	inboxTemplate, err := service.GetInboxTemplate(6)
	if err != nil {
		return
	}

	inboxTemplate.Description = inboxTemplate.Description

	inbox = mapInboxWithTemplate(inboxTemplate)

	inbox.Receiver = userDet.User.Username
	//TODO: เพิ่ม secret จาก password session
	filter := map[string]interface{}{}
	filter["username"] = userDet.User.Username
	user, _, _ := service.GetUserList(filter)
	filter["user_id"] = user[0].ID
	pwds, _, _ := service.GetPasswordSessionList(filter)
	secret := pwds[0].Secret

	inbox.Link = inbox.Link + "secret?" + secret

	return
}

func GenerateCreateUserInbox(username string) (inbox model.Inbox, err error) {
	// Create user Template ID: 7
	inboxTemplate, err := service.GetInboxTemplate(7)
	if err != nil {
		return
	}

	inboxTemplate.Description = strings.Replace(inboxTemplate.Description, "--username--", username, 1)

	inbox = mapInboxWithTemplate(inboxTemplate)

	inbox.Receiver = username
	//TODO: เพิ่ม secret จาก password session
	filter := map[string]interface{}{}
	filter["username"] = username
	user, _, _ := service.GetUserList(filter)
	filter["user_id"] = user[0].ID
	pwds, _, _ := service.GetPasswordSessionList(filter)
	secret := pwds[0].Secret

	inbox.Link = inbox.Link + "secret?" + secret

	return
}
