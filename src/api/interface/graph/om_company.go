package graph

import (
	"api/helpers"
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"

	"github.com/graphql-go/graphql"
)

var CompanyType *graphql.Object
var CompanyListType *graphql.Object
var CompanyResponseMessageType *graphql.Object
var IsDuplicatedTaxType *graphql.Object

type IsDuplicatedTax struct {
	IsDuplicated bool   `json:"is_duplicated"`
	Message      string `json:"message"`
}

func init() {
	CompanyType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Company",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name_th": &graphql.Field{
						Type: graphql.String,
					},
					"name_en": &graphql.Field{
						Type: graphql.String,
					},
					"code": &graphql.Field{
						Type: graphql.String,
					},
					"tax": &graphql.Field{
						Type: graphql.String,
					},
					"email": &graphql.Field{
						Type: graphql.String,
					},
					"website": &graphql.Field{
						Type: graphql.String,
					},
					"telephone": &graphql.Field{
						Type: graphql.String,
					},
					"fax": &graphql.Field{
						Type: graphql.String,
					},
					"about": &graphql.Field{
						Type: graphql.String,
					},
					"deactivated_date": &graphql.Field{
						Type: graphql.String,
					},
					"created_by": &graphql.Field{
						Type: graphql.Int,
					},
					"updated_by": &graphql.Field{
						Type: graphql.Int,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
					"updated_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	CompanyListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Companies",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"companies": &graphql.Field{
						Type: graphql.NewList(CompanyType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	CompanyResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "CompanyResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"company_id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	IsDuplicatedTaxType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "IsDuplicatedTax",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"is_duplicated": &graphql.Field{
						Type: graphql.Boolean,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

// GetCompanyDetail -
func (c *Controller) GetCompanyDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "company", &graphql.Field{
			Type: CompanyType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company", Function: "read"},
							model.Permission{Module: "my_company", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					user, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					var companyID int
					switch user.Role.Name {
					case "admin":
						companyID, _ = p.Args["id"].(int)
						break
					default:
						companyID = employee.CompanyID
					}

					result, err := service.GetCompanyDetail(companyID)
					if err != nil {
						return nil, err
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetCompanyList -
func (c *Controller) GetCompanyList() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "companies", &graphql.Field{
			Type: CompanyListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"status": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					name, _ := p.Args["name"].(string)
					status, _ := p.Args["status"].(string)

					if !limitOk {
						limit = -1
					}
					if !offsetOk {
						offset = -1
					}

					user, _, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					if user.Role.Name == "admin" {
						result, total, err := service.GetCompanyList(limit, offset, name, status)
						if err != nil {
							return nil, err
						}

						return model.CompanyList{
							Companies: result,
							Total:     total,
						}, err
					}

					return nil, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// PostCreateCompany -
func (c *Controller) PostCreateCompany() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_company", &graphql.Field{
			Type:        CompanyResponseMessageType,
			Description: "Create new Company",
			Args: graphql.FieldConfigArgument{
				"name_th": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"name_en": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"code": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"tax": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"email": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"website": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"telephone": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"fax": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"about": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"created_by": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"updated_by": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"head_office": &graphql.ArgumentConfig{
					Type: CompanyAddressInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					user, _, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}
					
					if user.Role.Name == "admin" {
						var companyType model.Company
						jsonCompany, _ := json.Marshal(p.Args)
						json.Unmarshal(jsonCompany, &companyType)

						var headOffice model.CompanyAddress
						jsonAddr, _ := json.Marshal(p.Args["head_office"])
						json.Unmarshal(jsonAddr, &headOffice)

						headOffice.Name = companyType.NameEN

						// Create Company Helper
						compID, err := CompanyCreateHelper(companyType, user, headOffice)
						if err != nil || compID == 0 {
							return nil, err
						}

						return model.CompanyResponseMessage{
							CompanyID: compID,
							Message:   "Create Success",
						}, err
					}

					return nil, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// CreateCompanyFromRequest -
func (c *Controller) CreateCompanyFromRequest() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_company_from_request", &graphql.Field{
			Type:        CompanyResponseMessageType,
			Description: "Create new Company from Registration Request",
			Args: graphql.FieldConfigArgument{
				"request_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					user, _, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					if user.Role.Name == "admin" {
						requestID, _ := p.Args["request_id"].(int)

						request, err := service.GetRegistrationRequest(requestID)
						if err != nil {
							return nil, err
						}

						companyType := model.Company{
							NameTH:    request.CompanyNameTH,
							NameEN:    request.CompanyNameEN,
							Code:      request.CompanyCode,
							Tax:       request.TaxID,
							Email:     request.CompanyEmail,
							Telephone: request.CompanyTelephone,
							About:     request.Description,
						}

						headOffice := model.CompanyAddress{
							Name: companyType.NameEN,
						}

						// Create Company Helper
						compID, err := CompanyCreateHelper(companyType, user, headOffice)
						if err != nil || compID == 0 {
							return nil, err
						}

						// Get id of back office admin role
						roles, err := service.GetRoles()
						var roleID int
						for _, item := range roles {
							if item.Name == "back_office_admin" {
								roleID = int(item.ID)
							}
						}

						userInput := service.UserInput{
							Username: request.Email,
							RoleID: roleID,
						}
						userDetailInput := service.UserDetailInput{
							CompanyID: compID,
						}

						// Create Back office admin
						_, err = service.CreateUserAndUserDetail(userInput, userDetailInput)
						if err != nil {
							return nil, err
						}

						return model.CompanyResponseMessage{
							CompanyID: compID,
							Message:   "Create From Request Success",
						}, err
					}

					return nil, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

func (c *Controller) PutCompanyDetail() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_company_detail", &graphql.Field{
			Type: CompanyResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"name_th": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"name_en": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"code": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"tax": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"email": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"website": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"telephone": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"fax": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"about": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"updated_by": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"deactivated_date": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var companyType model.Company
					jsonCompany, _ := json.Marshal(p.Args)
					json.Unmarshal(jsonCompany, &companyType)

					user, employee, err := GetUserEmployeeFromSession(p)
					if err != nil {
						return nil, err
					}

					companyType.DeactivatedDate = helpers.GetBangkokDateFromString(p.Args["deactivated_date"].(string))
					companyType.UpdatedBy = user.ID

					if user.Role.Name != "admin" {
						if companyType.ID != employee.CompanyID {
							return nil, errors.New("incorrect company")
						}
					}

					result, err := service.PutCompany(companyType)
					if err != nil || result == 0 {
						return nil, err
					}

					return model.CompanyResponseMessage{
						CompanyID: result,
						Message:   "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// CheckDuplicateTaxID -
func (c *Controller) CheckDuplicateTaxID() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "check_duplicated_tax", &graphql.Field{
			Type: IsDuplicatedTaxType,
			Args: graphql.FieldConfigArgument{
				"tax": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "company", Function: "read"},
							model.Permission{Module: "my_company", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					taxID, _ := p.Args["tax"].(string)
					if taxID == "" {
						return IsDuplicatedTax{
							IsDuplicated: false,
							Message:      "Cannot Check Empty Tax",
						}, nil
					}
					result, err := service.CheckDuplicateTaxID(taxID)
					if err != nil {
						return nil, err
					}
					return IsDuplicatedTax{
						IsDuplicated: result,
						Message:      "Check Successed",
					}, nil
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
