package graph

import (
	"github.com/graphql-go/graphql"
)

// ParticipantType - with leave request
var ParticipantType *graphql.Object

// ParticipantInputType - For create
var ParticipantInputType *graphql.InputObject

func init() {
	ParticipantType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Participant",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"leave_request_id": &graphql.Field{
						Type: graphql.Int,
					},
					"email": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)

	ParticipantInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "ParticipantInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"email": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)

}
