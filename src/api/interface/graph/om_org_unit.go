package graph

import (
	authorize "api/helpers/authorize"
	"api/model"
	"api/service"
	"encoding/json"
	"errors"
	"strconv"

	"github.com/graphql-go/graphql"
)

var OrgUnitType *graphql.Object
var OrgUnitListType *graphql.Object
var OrgUnitInputType *graphql.InputObject
var OrgUnitResponseMessageType *graphql.Object

func init() {
	OrgUnitType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OrgUnit",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"id": &graphql.Field{
						Type: graphql.Int,
					},
					"name": &graphql.Field{
						Type: graphql.String,
					},
					"description": &graphql.Field{
						Type: graphql.String,
					},
					"org_level_id": &graphql.Field{
						Type: graphql.Int,
					},
					"org_level": &graphql.Field{
						Type: OrganizationLevelType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							orgID := p.Source.(model.OrgUnit).ID
							if orgID == 0 {
								return model.OrgLevel{}, nil
							}
							levelID := p.Source.(model.OrgUnit).OrgLevelID
							res, err := service.GetLevel(levelID)
							return res, err
						},
					},
					"parent_org_id": &graphql.Field{
						Type: graphql.Int,
					},
					"parent_org": &graphql.Field{
						Type: OrgUnitType,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							orgID := p.Source.(model.OrgUnit).ParentOrgID
							if orgID == 0 {
								return model.OrgUnit{}, nil
							}
							res, err := service.GetOrgUnit(orgID)
							return res, err
						},
					},
					"children_org": &graphql.Field{
						Type: graphql.NewList(OrgUnitType),
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							orgID := p.Source.(model.OrgUnit).ID
							ret, _, _ := service.GetOrgUnits(9999, 0, 0, 0, "", -1, -1, "", true, orgID)
							return ret, nil
						},
					},
					"has_employee": &graphql.Field{
						Type: graphql.Boolean,
						Resolve: func(p graphql.ResolveParams) (interface{}, error) {
							orgID := p.Source.(model.OrgUnit).ID
							res := service.IsOrgContainsEmployeeRecur(orgID)
							return res, nil
						},
					},
					"effective_at": &graphql.Field{
						Type: graphql.String,
					},
					"terminate_at": &graphql.Field{
						Type: graphql.String,
					},
					"created_at": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
	OrgUnitListType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OrgUnitList",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"org_units": &graphql.Field{
						Type: graphql.NewList(OrgUnitType),
					},
					"total": &graphql.Field{
						Type: graphql.Int,
					},
				}
			}),
		},
	)
	OrgUnitInputType = graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name: "OrgUnitInput",
			Fields: graphql.InputObjectConfigFieldMap{
				"id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"name": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"description": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"org_level_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"parent_org_id": &graphql.InputObjectFieldConfig{
					Type: graphql.Int,
				},
				"effective_at": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
				"terminate_at": &graphql.InputObjectFieldConfig{
					Type: graphql.String,
				},
			},
		},
	)
	OrgUnitResponseMessageType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "OrgUnitResponseMessage",
			Fields: graphql.FieldsThunk(func() graphql.Fields {
				return graphql.Fields{
					"org_unit_id": &graphql.Field{
						Type: graphql.Int,
					},
					"message": &graphql.Field{
						Type: graphql.String,
					},
				}
			}),
		},
	)
}

// GetOrgUnit -
func (c *Controller) GetOrgUnit() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "organization_unit", &graphql.Field{
			Type: OrgUnitType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_unit", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.GetOrgUnit(p.Args["id"].(int))
					if err != nil {
						return nil, err
					}
					return result, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

// GetOrgUnits -
func (c *Controller) GetOrgUnits() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "organization_units", &graphql.Field{
			Type: OrgUnitListType,
			Args: graphql.FieldConfigArgument{
				"limit": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"offset": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"company_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"org_level_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"min_org_level": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"max_org_level": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"point_of_time": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"is_pending": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
				"parent_org_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_unit", Function: "read"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					limit, limitOk := p.Args["limit"].(int)
					offset, offsetOk := p.Args["offset"].(int)
					companyID, _ := p.Args["company_id"].(int)
					orgLevelID, _ := p.Args["org_level_id"].(int)
					name, _ := p.Args["name"].(string)
					minOrgLevel, minLevelOk := p.Args["min_org_level"].(int)
					maxOrgLevel, maxLevelOk := p.Args["max_org_level"].(int)
					pointOfTime, timeOk := p.Args["point_of_time"].(string)
					isPending, isPendingOk := p.Args["is_pending"].(bool)
					parentOrgID, isParentIDOk := p.Args["parent_org_id"].(int)

					if !limitOk {
						limit = 9999
					}

					if !offsetOk {
						offset = 0
					}

					if !timeOk {
						pointOfTime = ""
					}

					if !isPendingOk {
						isPending = false
					}

					if !minLevelOk {
						minOrgLevel = -1
					}
					if !maxLevelOk {
						maxOrgLevel = -1
					}

					if !isParentIDOk {
						parentOrgID = 0
					}

					userIDstr, userIDOk := p.Context.Value(authorize.UserIDHeaderKey).(string)
					if !userIDOk {
						return model.User{}, errors.New("invalid bearer")
					}
					userID, _ := strconv.Atoi(userIDstr)
					user, err := service.GetUserFromID(userID)

					if user.Role.Name == "hr" {
						employee, err := service.GetEmployeeByUserID(userID)
						if err != nil {
							return nil, err
						}
						companyID = employee.CompanyID
					}
					result, total, err := service.GetOrgUnits(limit, offset, companyID, orgLevelID, name, minOrgLevel, maxOrgLevel, pointOfTime, isPending, parentOrgID)
					if err != nil {
						return nil, err
					}
					return model.OrgUnitList{
						OrgUnits: result,
						Total:    total,
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//PostOrgUnit -
func (c *Controller) PostOrgUnit() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "create_organization_unit", &graphql.Field{
			Description: "Create new Org Unit from args",
			Type:        OrgUnitResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"org_unit": &graphql.ArgumentConfig{
					Type: OrgUnitInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_unit", Function: "create"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var orgUnit model.OrgUnit
					jsonOrgUnit, _ := json.Marshal(p.Args["org_unit"])
					json.Unmarshal(jsonOrgUnit, &orgUnit)

					result, err := service.PostOrgUnit(orgUnit)
					if err != nil || result == 0 {
						return nil, err
					}

					return model.OrgUnitResponseMessage{
						OrgUnitID: result,
						Message:   "Create Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//PutOrgUnit -
func (c *Controller) PutOrgUnit() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "edit_organization_unit", &graphql.Field{
			Description: "Edit Org Unit from args",
			Type:        OrgUnitResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"org_unit": &graphql.ArgumentConfig{
					Type: OrgUnitInputType,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_unit", Function: "edit"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					var orgUnit model.OrgUnit
					jsonOrgUnit, _ := json.Marshal(p.Args["org_unit"])
					json.Unmarshal(jsonOrgUnit, &orgUnit)

					result, err := service.PutOrgUnit(orgUnit)
					if err != nil || result == 0 {
						return nil, err
					}

					return model.OrgUnitResponseMessage{
						OrgUnitID: result,
						Message:   "Edit Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//DeleteOrgUnit -
func (c *Controller) DeleteOrgUnit() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "delete_organization_unit", &graphql.Field{
			Description: "Delete Org Unit from args",
			Type:        OrgUnitResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_unit", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					result, err := service.DeleteOrgUnit(p.Args["id"].(int))
					if err != nil || result == 0 {
						return nil, err
					}

					return model.OrgUnitResponseMessage{
						OrgUnitID: result,
						Message:   "Delete Success",
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//TerminateOrgUnit -
func (c *Controller) TerminateOrgUnit() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "terminate_organization_unit", &graphql.Field{
			Type: OrgUnitResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"terminate_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_unit", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					orgUnitID := p.Args["id"].(int)
					terminateDate := p.Args["terminate_date"].(string)
					result, message, err := service.TerminateOrgUnit(orgUnitID, terminateDate)
					if err != nil {
						return nil, err
					}

					return model.OrgUnitResponseMessage{
						OrgUnitID: result,
						Message:   message,
					}, err
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}

//TransferOrgUnit -
func (c *Controller) TransferOrgUnit() ControllerReturn {
	return func() (string, *graphql.Field) {
		return "transfer_organization_unit", &graphql.Field{
			Type: OrgUnitResponseMessageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"target_parent_org_id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"process_date": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				auth := authorize.AuthorizedAny(&p)(
					authorize.AuthorizedGroup(p)(
						authorize.WithBearer("user"),
						authorize.WithUserProfile(),
						authorize.WithPermissionAny(
							model.Permission{Module: "org_unit", Function: "delete"},
						),
					),
				)
				resolve := func(p graphql.ResolveParams) (interface{}, error) {
					orgUnitID := p.Args["id"].(int)
					processDate := p.Args["process_date"].(string)
					targetParentOrgID, targetOK := p.Args["target_parent_org_id"].(int)

					if targetOK {
						result, message, err := service.TransferOrgUnit(orgUnitID, targetParentOrgID, processDate)
						if err != nil {
							return nil, err
						}
						return model.OrgUnitResponseMessage{
							OrgUnitID: result,
							Message:   message,
						}, err
					}

					return model.OrgUnitResponseMessage{
						OrgUnitID: 0,
						Message:   "target org missing",
					}, errors.New("transfer failed")
				}
				onFail := func(p graphql.ResolveParams) (interface{}, error) {
					return "fail", nil
				}

				return authorize.Authorized(auth, resolve, onFail)(p)
			},
		}
	}
}
