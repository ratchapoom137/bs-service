package router

import (
	"api/base"
	"api/interface/graph"

	"dv.co.th/hrms/core/middleware"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"

	"github.com/thoas/go-funk"

	"github.com/kataras/iris"
)

var controller = new(graph.Controller)

// GraphQLRouter -
func GraphQLRouter(irisApp *iris.Application) *iris.Application {
	app := base.GetApplication()

	// Schema
	queryFields := graphql.Fields{
		"hello": &graphql.Field{
			Type: graphql.String,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				return "world", nil
			},
		},
	}

	mutationFields := graphql.Fields{}

	queryMapping := controller.GetQueryMapping()
	mutationMapping := controller.GetMutationMapping()

	funk.ForEach(queryMapping, func(cr graph.ControllerReturn) {
		name, field := cr()
		queryFields[name] = field
	})

	funk.ForEach(mutationMapping, func(cr graph.ControllerReturn) {
		name, field := cr()
		mutationFields[name] = field
	})

	rootQuery := graphql.ObjectConfig{Name: "RootQuery", Fields: queryFields}
	rootMutation := graphql.ObjectConfig{Name: "RootMutation", Fields: mutationFields}

	schemaConfig := graphql.SchemaConfig{
		Query:    graphql.NewObject(rootQuery),
		Mutation: graphql.NewObject(rootMutation),
	}

	schema, _ := graphql.NewSchema(schemaConfig)

	h := handler.New(&handler.Config{
		Schema:     &schema,
		Pretty:     true,
		GraphiQL:   false,
		Playground: true,
	})

	cors := middleware.Cors(irisApp)
	cors.Post(app.Env.UriBasePath+"/graphql", iris.FromStd(middleware.HTTPHeader(h)))
	cors.Post("/graphql", iris.FromStd(middleware.HTTPHeader(h)))

	return irisApp
}
