package router

import (
	"api/base"

	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
)

// RestRouter -
func RestRouter(irisApp *iris.Application) *iris.Application {
	app := base.GetApplication()
	crs := cors.New(cors.Options{
		AllowedMethods:   []string{"HEAD", "GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})

	cors := irisApp.Party(app.Env.UriBasePath+"/", crs).AllowMethods(iris.MethodOptions)

	{
		cors.StaticWeb("/docs", "./docs")
	}

	return irisApp
}
