package main

import (
	"api/router"

	"dv.co.th/hrms/core"
	"github.com/kataras/iris"
)

func main() {
	app := core.GetApplication()

	app.Logger.Info("Main - ", *app)

	irisApp := router.New()

	// listen and serve on http://0.0.0.0:8080.
	irisApp.Run(iris.Addr(":8080"))
}
