package service

import (
	"api/model"
	"fmt"
	"errors"
	"dv.co.th/hrms/core/rest"
	// "github.com/mitchellh/mapstructure"
	// "github.com/davecgh/go-spew/spew"
)

// CreateOtherApproveConfiguration -
func CreateOtherApproveConfiguration(input *model.OtherApproveConfiguration) (interface{}, error) {
	url := fmt.Sprintf(URIEnvMapping.RBInternalOtherApproveConfiguration)

	res, err := rest.CallRestAPIWithOption("POST", url, input)
	if err != nil {
		return nil, err
	}
	result, _ := res.Data.(interface{})
	return result, err
}

// GetOthersConfiguration -
func GetOtherConfiguration(id int) (model.OtherApproveConfiguration, error) {
	var otherconfig model.OtherApproveConfiguration
	url := fmt.Sprintf(URIEnvMapping.RBInternalOtherApproveConfiguration+"?&company_id=%d", id)
	res, err := rest.CallRestAPIWithOption("GET", url, &otherconfig)
	if err != nil {
		return model.OtherApproveConfiguration{}, errors.New("cannot get other approve configuration")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.OtherApproveConfiguration{}, errors.New("not found: get other approve configuration")
	}

	return model.OtherApproveConfiguration{
		ID:            int(result["id"].(float64)),
		ApproveTypeID: int(result["approve_type_id"].(float64)),
		CreatedBy:     int(result["created_by"].(float64)),
		UpdatedBy:     int(result["updated_by"].(float64)),
	}, nil
}

// PutOtherApproveConfiguration -
func PutOtherApproveConfiguration(input *model.OtherApproveConfiguration) (interface{}, error) {
	url := fmt.Sprintf(URIEnvMapping.RBInternalOtherApproveConfiguration)

	res, err := rest.CallRestAPIWithOption("PUT", url, input)
	if err != nil {
		return nil, err
	}
	result, _ := res.Data.(interface{})
	return result, err
}
