package service

import (
	"api/helpers"
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	_ "net/url"
	"time"

	"dv.co.th/hrms/core/rest"
	_ "github.com/davecgh/go-spew/spew"
	_ "github.com/mitchellh/mapstructure"
	"github.com/r3labs/diff"
	"github.com/thoas/go-funk"
)

func PostPerDiemReimbursementRequest(input map[string]interface{},userLogin UserDetails) (requestID int, err error) {
	urlAddPerDiem := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest) + "/perdiem"

	var inputPerDiemRequest model.PerDiemReimbursementRequest
	jsonData, _ := json.Marshal(input)
	json.Unmarshal(jsonData, &inputPerDiemRequest)

	var totals = SumPerDiemTotalRequestTotals(inputPerDiemRequest.PerDiems)
	input["total_hours"] = totals.Hour
	input["cost"] = totals.Expend
	requester := input["requester_id"].(int)
	currentrequestID, err := PostReimbursementRequestOnly(input)
	if err != nil {
		return 0, errors.New("cannot create request")
	}

	for _, item := range inputPerDiemRequest.PerDiems {
		var inputPerDiem model.PerDiem
		jsonDataPerDiem, _ := json.Marshal(item)
		json.Unmarshal(jsonDataPerDiem, &inputPerDiem)
		now := time.Now()
		bankokTime := helpers.GetBankokTimeZone(now).Format("2006-01-02T15:04:05+00:00")
		currentTime, _ := time.Parse(time.RFC3339, bankokTime)

		inputPerDiem.RequestID = currentrequestID
		inputPerDiem.CreatedBy = requester
		inputPerDiem.CreatedAt = currentTime
		inputPerDiem.UpdatedAt = currentTime

		_, err = rest.CallRestAPIWithOption("POST", urlAddPerDiem, &inputPerDiem)
		if err != nil {
			return 0, errors.New("cannot create perDiem")
		}
	}
	oldReimbursementRequest := model.PerDiemReimbursementRequest{}
	difObject, err := diff.Diff(oldReimbursementRequest, model.PerDiemReimbursementRequest{
		ID: currentrequestID,
	})

	errCreateChangelog := PostChangelog(difObject, currentrequestID, CREATE_ACTLOG, REIMBURSEMENT_REPORT, REIMBURSEMENT_CONTENT, userLogin)
	if errCreateChangelog != nil {
		return 0, errors.New("cannot create perdiem")
	}

	return currentrequestID, nil
}

func checkRequestApprovers(personalID int, companyID int, cost float64, typeRequest string) (result []model.Approver, err error) {
	var finalResultApprover []model.Approver
	var configApprovalID int
	currentPosition, err := GetEmployeeByPersonalID(personalID)
	if err != nil {
		return []model.Approver{}, errors.New("Cannot get current position")
	}
	switch typeRequest {
	case "FOOD":
		configuration, err := GetFoodConfiguration(companyID)
		if err != nil {
			return []model.Approver{}, errors.New("Cannot get food configuration")
		}
		configApprovalID = configuration.ApproveTypeID

		break
	case "PERDIEM":
		configuration, err := GetPerdiemConfiguration(companyID)
		if err != nil {
			return []model.Approver{}, errors.New("Cannot get perdiem configuration")
		}
		configApprovalID = configuration.ApproveTypeID
		break
	case "TRAVEL":
		configuration, err := GetTravelConfiguration(companyID)
		if err != nil {
			return []model.Approver{}, errors.New("Cannot get tralvel configuration")
		}
		configApprovalID = configuration.ApproveTypeID
		break
	case "OTHER":
		configuration, err := GetOtherConfiguration(companyID)
		if err != nil {
			return []model.Approver{}, errors.New("Cannot get other configuration")
		}
		configApprovalID = configuration.ApproveTypeID
		break
	default:
		break
	}

	approvalType, err := GetApprovalConfig(configApprovalID)
	if err != nil {

		return []model.Approver{}, errors.New("Cannot get approval type")
	}

	approverConfigDetails, err := GetApprovalConfigDetail(configApprovalID)
	if err != nil {
		return []model.Approver{}, errors.New("Cannot get approval detail")
	}

	allApprovers, err := GetApprovalPositions(currentPosition.EmployeeGeneralInfo.PositionID, configApprovalID)
	if err != nil {
		return []model.Approver{}, errors.New("Cannot get all  approvers")
	}

	if approvalType.ConfigType == "EMPLOYEE_LEVEL" {
		approvers, err := getEmployeeLevelApprovals(approverConfigDetails, cost, allApprovers)
		if err != nil {
			return []model.Approver{}, errors.New("Cannot get approval config budget")
		}
		finalResultApprover, _ = getDefaultApprovers(approvers)
	} else {
		finalResultApprover, _ = getDefaultApprovers(allApprovers)
	}

	result = finalResultApprover
	return result, nil
}

// Get apprvers in case request type is employee level
func getEmployeeLevelApprovals(approvalDetails []model.ApprovalConfigDetail, requestCost float64, allPositions []model.Position) (result []model.Position, err error) {
	var lastLevel int
	var lowerBudget float64
	var boundLevel int
	for _, eachValue := range approvalDetails {
		budgetDetails, err := GetApprovalConfigBudget(eachValue.ValueID)
		if err != nil {
			return []model.Position{}, errors.New("Cannot get approval config budget")
		}

		if budgetDetails.Budget >= requestCost {
			currentLevel := compareEmployeeLevel(budgetDetails.ApproverEmployeeLevel)
			if currentLevel >= lastLevel {
				if lowerBudget <= budgetDetails.Budget || budgetDetails.Budget != 0.0 {
					lowerBudget = budgetDetails.Budget
					lastLevel = currentLevel
				}
			}
		}
	}
	result = funk.Filter(allPositions, func(manager model.Position) bool {
		positionLevel := getEmployeeLevelByPosition(manager.EmployeeID)
		for i := 4; i > 0; i = i - 1 {
			if positionLevel == lastLevel {
				boundLevel = positionLevel
			} else if positionLevel == lastLevel-i && boundLevel == 0 {
				boundLevel = positionLevel
			}
		}
		return positionLevel == boundLevel
	}).([]model.Position)

	return result, nil
}

//GetPerdiemReimbursementRequestByID
func GetPerdiemReimbursementRequestByID(id int) (model.PerDiemReimbursementRequest, error) {
	var perdiems model.PerDiemReimbursementRequest
	url := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/", id)

	res, err := rest.CallRestAPIWithOption("GET", url, &perdiems)

	if err != nil {
		return model.PerDiemReimbursementRequest{}, errors.New("cannot get perdiem by ID")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.PerDiemReimbursementRequest{}, errors.New("not found: get perdiem By ID")
	}

	var perdiem model.PerDiemReimbursementRequest
	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &perdiem)

	var perdiemTable []model.PerDiem

	urlTable := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/perdiem/", id)
	resTable, err := rest.CallRestAPIWithOption("GET", urlTable, nil)

	if err != nil {
		return model.PerDiemReimbursementRequest{}, errors.New("cannot get perdiem by id")
	}
	jsonData2, _ := json.Marshal(resTable.Data)
	json.Unmarshal(jsonData2, &perdiemTable)
	perdiem.PerDiems = perdiemTable

	return perdiem, nil
}

func PutPerDiemReimbursementRequestWithLog(input map[string]interface{},loginUser UserDetails) (requestID int, err error) {

	// Step 1: Get old information
	before := func() (interface{}, error) {
		return GetPerdiemReimbursementRequestByID(input["id"].(int))
	}

	// Step 2: The service update or create process
	process := func() (interface{}, error) {
		return PutPerDiemReimbursementRequest(input)
	}

	// Step 3: Get new information
	after := func() (interface{}, error) {
		return GetPerdiemReimbursementRequestByID(input["id"].(int))
	}

	// Changelog Option
	var changelogOption = ChangelogOption{
		ContentID:   input["id"].(int),
		ActionType:  UPDATE_ACTLOG,
		InfoType:    REIMBURSEMENT_REPORT,
		ContentType: REIMBURSEMENT_CONTENT,
	}

	success, _ := ChangelogProcess(
		before,
		process,
		after,
		loginUser,
		changelogOption,
	)
	if success == false {
		err = errors.New("changelog process is fail")
		return
	}
	requestID=input["id"].(int)
	return
}

func PutPerDiemReimbursementRequest(input map[string]interface{}) (requestID int, err error) {

	var inputPerDimeem model.PerDiemReimbursementRequest
	jsonData, _ := json.Marshal(input)
	json.Unmarshal(jsonData, &inputPerDimeem)

	currentRequest, err := GetPerdiemReimbursementRequestByID(inputPerDimeem.ID)
	if err != nil {
		return 0, errors.New("Can not get per diem reimbursement request by id")
	}

	var newPerDiemsDs []int
	for _, item := range inputPerDimeem.PerDiems {
		if item.ID > 0 {
			newPerDiemsDs = append(newPerDiemsDs, item.ID)
		}
	}
	for _, item := range currentRequest.PerDiems {
		if item.ID > 0 {
			if !funk.Contains(newPerDiemsDs, item.ID) {
				DeletePerdiem(item.ID)
			}

		}
	}
	currentRequest.PerDiems = inputPerDimeem.PerDiems
	var totals = SumPerDiemTotalRequestTotals(currentRequest.PerDiems)
	input["total_hours"] = totals.Hour
	input["cost"] = totals.Expend
	requester := input["request_id"].(int)

	resEditRequestID, err := PutReimbursementRequestOnly(input)
	if err != nil {
		return 0, errors.New("Can not edit reimbursement request by id")
	}

	urlPerDiem := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest) + "/perdiem"

	for _, perDiem := range currentRequest.PerDiems {
		var inputPerDiem model.PerDiem
		jsonDataPerDiem, _ := json.Marshal(perDiem)
		json.Unmarshal(jsonDataPerDiem, &inputPerDiem)
		now := time.Now()
		bankokTime := helpers.GetBankokTimeZone(now).Format("2006-01-02T15:04:05+00:00")
		currentTime, _ := time.Parse(time.RFC3339, bankokTime)

		inputPerDiem.RequestID = resEditRequestID
		inputPerDiem.CreatedBy = requester
		inputPerDiem.CreatedAt = currentTime
		inputPerDiem.UpdatedAt = currentTime

		if perDiem.ID != 0 {
			_, err = rest.CallRestAPIWithOption("PUT", urlPerDiem, &inputPerDiem)
			if err != nil {
				return 0, errors.New("cannot edit perdiem detail")
			}
		} else {
			_, err = rest.CallRestAPIWithOption("POST", urlPerDiem, &inputPerDiem)
			if err != nil {
				return 0, errors.New("cannot add perdiem detail")
			}
		}

	}

	return resEditRequestID, nil
}

func DeletePerdiem(id int) (ok bool) {
	urlTable := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/perdiem/", id)
	_, err := rest.CallRestAPIWithOption("DELETE", urlTable, nil)

	if err != nil {
		return
	}
	ok = true
	return
}
