package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"dv.co.th/hrms/core/rest"
)

type AssetOperationFilter struct {
	Offset            int    `json:"offset"`
	Limit             int    `json:"limit"`
	CompanyID         int    `json:"company_id"`
	EmployeeID        int    `json:"employee_id"`
	AssetID           int    `json:"asset_id"`
	StartDateFrom     string `json:"start_date_from"`
	StartDateTo       string `json:"start_date_to"`
	DueDateFrom       string `json:"due_date_from"`
	DueDateTo         string `json:"due_date_to"`
	IsReturn          string `json:"is_return"`
	IsDueDate         string `json:"is_due_date"`
	AssetName         string `json:"asset_name"`
	AssetCategoryName string `json:"asset_category_name"`
	SerialNumber      string `json:"serial_number"`
}

// GetAssetOperationByID -
func GetAssetOperationByID(id int) (model.AssetOperation, error) {
	url := fmt.Sprintf(URIEnvMapping.AAInternalAssetOperation+"/%d", id)

	res, err := rest.CallRestAPIWithOption("GET", url, nil)
	if err != nil {
		return model.AssetOperation{}, errors.New("cannot get asset operation")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.AssetOperation{}, errors.New("not found: get asset operation")
	}

	var assetOperationOut model.AssetOperation

	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &assetOperationOut)

	return assetOperationOut, nil
}

// GetAssetOperations -
func GetAssetOperations(filter AssetOperationFilter) ([]model.AssetOperation, int, error) {
	pathURL, _ := url.Parse(URIEnvMapping.AAInternalAssetOperations)
	q := pathURL.Query()

	if filter.Offset != 0 {
		q.Set("offset", fmt.Sprintf("%d", filter.Offset))
	}
	if filter.Limit != 0 {
		q.Set("limit", fmt.Sprintf("%d", filter.Limit))
	}
	if filter.CompanyID > 0 {
		q.Set("company_id", fmt.Sprintf("%d", filter.CompanyID))
	}
	if filter.EmployeeID > 0 {
		q.Set("employee_id", fmt.Sprintf("%d", filter.EmployeeID))
	}
	if filter.AssetID > 0 {
		q.Set("asset_id", fmt.Sprintf("%d", filter.AssetID))
	}
	if filter.StartDateFrom != "" {
		q.Set("start_date_from", filter.StartDateFrom)
	}
	if filter.StartDateTo != "" {
		q.Set("start_date_to", filter.StartDateTo)
	}
	if filter.DueDateFrom != "" {
		q.Set("due_date_from", filter.DueDateFrom)
	}
	if filter.DueDateTo != "" {
		q.Set("due_date_to", filter.DueDateTo)
	}
	if filter.IsReturn != "" {
		q.Set("is_return", filter.IsReturn)
	}
	if filter.IsDueDate != "" {
		q.Set("is_due_date", filter.IsDueDate)
	}
	if filter.AssetName != "" {
		q.Set("asset_name", filter.AssetName)
	}
	if filter.AssetCategoryName != "" {
		q.Set("asset_category_name", filter.AssetCategoryName)
	}
	if filter.SerialNumber != "" {
		q.Set("serial_number", filter.SerialNumber)
	}

	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), nil)
	if err != nil {
		return []model.AssetOperation{}, 0, errors.New("cannot get asset operation list")
	}

	result, ok := res.Data.([]interface{})
	if !ok {
		return []model.AssetOperation{}, 0, errors.New("not found: get asset operation list")
	}

	total := res.Total

	var assetOperationsOut []model.AssetOperation

	for _, assetOp := range result {
		var tmp model.AssetOperation
		jsonData, _ := json.Marshal(assetOp)
		json.Unmarshal(jsonData, &tmp)
		assetOperationsOut = append(assetOperationsOut, tmp)
	}

	return assetOperationsOut, total, nil
}

// CreateAssetOperation -
func CreateAssetOperation(input *model.AssetOperationInput) (int, error) {
	url := fmt.Sprintf(URIEnvMapping.AAInternalAssetOperation)

	res, err := rest.CallRestAPIWithOption("POST", url, input)

	if err != nil {
		return 0, errors.New("cannot create asset operation")
	}

	result := int(res.Data.(float64))

	return result, nil
}

// EditAssetOperation -
func EditAssetOperation(input *model.AssetOperationInput) (int, error) {
	url := fmt.Sprintf(URIEnvMapping.AAInternalAssetOperation+"/%d", input.ID)

	res, err := rest.CallRestAPIWithOption("PUT", url, input)
	if err != nil {
		return 0, errors.New("cannot create asset operation")
	}

	result := int(res.Data.(float64))

	return result, nil
}
