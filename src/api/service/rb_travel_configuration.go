package service

import (
	"api/model"
	"encoding/json"
	"errors"

	"dv.co.th/hrms/core/rest"
	// "github.com/davecgh/go-spew/spew"

	// "errors"
	"fmt"
)

//CreateTravelApproveConfiguration
func CreateTravelApproveConfiguration(input *model.TravelApproveConfiguration, inputExpense model.TravelApproveConfigurationExpense) (model.TravelApproveConfiguration, error) {
	urlTravel := fmt.Sprintf(URIEnvMapping.RBInternalTravelApproveConfiguration)
	urlTravelExpense := fmt.Sprintf(URIEnvMapping.RBInternalTravelApproveConfiguration) + "/expense"

	var travelConfiguration model.TravelApproveConfiguration
	resTravel, err := rest.CallRestAPIWithOption("POST", urlTravel, input)
	if err != nil {
		return model.TravelApproveConfiguration{}, err
	}
	result, _ := resTravel.Data.(map[string]interface{})
	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &travelConfiguration)

	var travelExpenseConfiguration model.TravelApproveConfigurationExpense
	resTravelExpense, err := rest.CallRestAPIWithOption("POST", urlTravelExpense, inputExpense)
	if err != nil {
		return model.TravelApproveConfiguration{}, err
	}
	resultExpens, _ := resTravelExpense.Data.(map[string]interface{})
	jsonDataExpens, _ := json.Marshal(resultExpens)
	json.Unmarshal(jsonDataExpens, &travelExpenseConfiguration)

	travelConfiguration.TravelExpense = travelExpenseConfiguration

	return travelConfiguration, nil

}

// GetTravelConfiguration -
func GetTravelConfiguration(id int) (model.TravelApproveConfiguration, error) {
	var travelconfig model.TravelApproveConfiguration
	url := fmt.Sprintf(URIEnvMapping.RBInternalTravelApproveConfiguration+"?&company_id=%d", id)
	res, err := rest.CallRestAPIWithOption("GET", url, &travelconfig)

	if err != nil {
		return model.TravelApproveConfiguration{}, errors.New("cannot get travel approve configuration")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.TravelApproveConfiguration{}, errors.New("not found: get travel approve configuration")
	}

	var configuration model.TravelApproveConfiguration
	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &configuration)

	var configurationExpense model.TravelApproveConfigurationExpense

	urlExpense := fmt.Sprintf(URIEnvMapping.RBInternalTravelApproveConfiguration+"/expense"+"?&company_id=%d", id)
	resExpense, err := rest.CallRestAPIWithOption("GET", urlExpense, nil)

	if err != nil {

		return model.TravelApproveConfiguration{}, errors.New("cannot get travel approve configuration")
	}

	jsonData2, _ := json.Marshal(resExpense.Data)
	json.Unmarshal(jsonData2, &configurationExpense)
	configuration.TravelExpense = configurationExpense
	return configuration, nil
}

//EditTravelApproveConfiguration
func EditTravelApproveConfiguration(input *model.TravelApproveConfiguration, inputExpense model.TravelApproveConfigurationExpense) (model.TravelApproveConfiguration, error) {
	urlTravel := fmt.Sprintf(URIEnvMapping.RBInternalTravelApproveConfiguration)
	urlTravelExpense := fmt.Sprintf(URIEnvMapping.RBInternalTravelApproveConfiguration) + "/expense"

	var travelConfiguration model.TravelApproveConfiguration
	resTravel, err := rest.CallRestAPIWithOption("PUT", urlTravel, input)
	if err != nil {
		return model.TravelApproveConfiguration{}, err
	}
	result, _ := resTravel.Data.(map[string]interface{})
	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &travelConfiguration)

	var travelExpenseConfiguration model.TravelApproveConfigurationExpense
	resTravelExpense, err := rest.CallRestAPIWithOption("PUT", urlTravelExpense, inputExpense)
	if err != nil {
		return model.TravelApproveConfiguration{}, err
	}
	resultExpens, _ := resTravelExpense.Data.(map[string]interface{})
	jsonDataExpens, _ := json.Marshal(resultExpens)
	json.Unmarshal(jsonDataExpens, &travelExpenseConfiguration)

	travelConfiguration.TravelExpense = travelExpenseConfiguration

	return travelConfiguration, nil

}

//GetMyTravelExpense
func GetMyTravelExpense(id int) (model.TravelApproveConfiguration, error) {
	var travelconfig model.TravelApproveConfiguration
	url := fmt.Sprintf(URIEnvMapping.RBInternalTravelApproveConfiguration+"?&company_id=%d", id)
	res, err := rest.CallRestAPIWithOption("GET", url, &travelconfig)

	if err != nil {
		return model.TravelApproveConfiguration{}, errors.New("cannot get travel approve configuration")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.TravelApproveConfiguration{}, errors.New("not found: get travel approve configuration")
	}

	var configuration model.TravelApproveConfiguration
	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &configuration)

	var configurationTable model.TravelApproveConfigurationExpense

	urlTable := fmt.Sprintf(URIEnvMapping.RBInternalTravelApproveConfiguration+"/expense"+"?&company_id=%d", id)
	resTable, err := rest.CallRestAPIWithOption("GET", urlTable, nil)

	if err != nil {

		return model.TravelApproveConfiguration{}, errors.New("cannot get travel approve configuration")
	}

	jsonData2, _ := json.Marshal(resTable.Data)
	json.Unmarshal(jsonData2, &configurationTable)
	configuration.TravelExpense = configurationTable

	return configuration, nil
}
