package service

import (
	"api/helpers"
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"time"

	"dv.co.th/hrms/core/rest"
	coreRest "dv.co.th/hrms/core/rest"
	_ "github.com/davecgh/go-spew/spew"
	"github.com/mitchellh/mapstructure"
	"github.com/r3labs/diff"
	_ "github.com/thoas/go-funk"
)

type ReimbursementQuery struct {
	Limit             int
	Offset            int
	Chargecode        string
	ReimbursementType string
	Status            string
	RequesterID       int
	ApproverID        int
	ReviewerID        int
	BackOfficeID      int
	CompanyID         int
}
type ReimbursementReportQuery struct {
	CompanyID   int    `json:"company_id"`
	StartPeriod string `json:"start_period"`
	EndPeriod   string `json:"end_period"`
	SearchCode  string `json:"search_code"`
}

func PostFoodReimbursementRequest(input map[string]interface{}, userLogin UserDetails) (requestID int, err error) {
	urlAddFood := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest) + "/food"
	currentrequestID, err := PostReimbursementRequestOnly(input)
	if err != nil {
		return 0, errors.New("cannot create request")
	}

	var foodDetail model.FoodAllowance
	jsonData, _ := json.Marshal(input)
	json.Unmarshal(jsonData, &foodDetail)

	now := time.Now()
	bankokTime := helpers.GetBankokTimeZone(now).Format("2006-01-02T15:04:05+00:00")
	foodDetail.CreatedAt = bankokTime
	foodDetail.UpdatedAt = bankokTime
	foodDetail.RequestID = currentrequestID

	_, err = rest.CallRestAPIWithOption("POST", urlAddFood, &foodDetail)

	oldReimbursementRequest := model.FoodReimbursementRequest{}
	difObject, err := diff.Diff(oldReimbursementRequest, model.FoodReimbursementRequest{
		ID: currentrequestID,
	})

	errCreateChangelog := PostChangelog(difObject, currentrequestID, CREATE_ACTLOG, REIMBURSEMENT_REPORT, REIMBURSEMENT_CONTENT, userLogin)
	if errCreateChangelog != nil {
		return 0, errors.New("cannot create food detail")
	}
	if err != nil {
		return 0, errors.New("cannot create food detail")
	}

	return currentrequestID, nil
}

//GetReimbursementRequestList
func GetFoodReimbursementRequestList(inputQuery ReimbursementQuery) (result []model.ReimbursementRequest, total int, err error) {
	var reimbursementType []model.ReimbursementRequest

	path := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequests)
	pathURL, _ := url.Parse(path)
	pathQuery := pathURL.Query()
	pathQuery.Set("offset", fmt.Sprintf("%d", inputQuery.Offset))
	pathQuery.Set("limit", fmt.Sprintf("%d", inputQuery.Limit))

	if inputQuery.Chargecode != "" {
		pathQuery.Set("charge_code", fmt.Sprintf("%s", inputQuery.Chargecode))
	}

	if inputQuery.ReimbursementType != "" {
		pathQuery.Set("type", fmt.Sprintf("%s", inputQuery.ReimbursementType))
	}
	if inputQuery.Status != "" {
		pathQuery.Set("status", fmt.Sprintf("%s", inputQuery.Status))
	}
	if inputQuery.ApproverID > 0 {
		pathQuery.Set("approver_id", fmt.Sprintf("%d", inputQuery.ApproverID))
	}
	if inputQuery.ReviewerID > 0 {
		pathQuery.Set("reviewer_id", fmt.Sprintf("%d", inputQuery.ReviewerID))
	}
	if inputQuery.BackOfficeID > 0 {
		pathQuery.Set("backoffice_id", fmt.Sprintf("%d", inputQuery.BackOfficeID))
		pathQuery.Set("company_id", fmt.Sprintf("%d", inputQuery.CompanyID))
	}

	pathURL.RawQuery = pathQuery.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), &reimbursementType)

	if err != nil {

		err = errors.New("Can not reimbursement list")
		return
	}

	data, ok := res.Data.([]interface{})

	if !ok {
		return []model.ReimbursementRequest{}, 0, errors.New("Can not get requests")
	}

	for _, item := range data {
		var request model.ReimbursementRequest

		jsonData, _ := json.Marshal(item)
		json.Unmarshal(jsonData, &request)

		result = append(result, request)
	}

	total = res.Total
	return
}

//GetReimbursementRequestByID
func GetFoodReimbursementRequestByID(id int) (model.FoodReimbursementRequest, error) {
	var reimbursementType model.FoodReimbursementRequest
	inputQuery := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/", id)

	res, err := coreRest.CallRestAPIWithOption("GET", inputQuery, &reimbursementType)

	if err != nil {
		return model.FoodReimbursementRequest{}, errors.New("Can not get reimbursement request by id1")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.FoodReimbursementRequest{}, errors.New("Can not get request by id")
	}

	decodeError := mapstructure.Decode(result, &reimbursementType)

	if decodeError != nil {
		return model.FoodReimbursementRequest{}, errors.New("Cannot parse request")
	}
	inputQueryFood := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/food/", id)

	response, err := coreRest.CallRestAPIWithOption("GET", inputQueryFood, nil)
	if err != nil {
		return model.FoodReimbursementRequest{}, errors.New("Can not get reimbursement request by id2")
	}

	resultFood, ok := response.Data.(map[string]interface{})
	if !ok {
		return model.FoodReimbursementRequest{}, errors.New("Can not get food request by id")
	}

	reimbursementType.Date = fmt.Sprintf("%v", resultFood["date"])
	reimbursementType.Information = fmt.Sprintf("%v", resultFood["information"])
	reimbursementType.UpdatedAt = fmt.Sprintf("%v", resultFood["updated_at"])
	reimbursementType.CreatedAt = fmt.Sprintf("%v", resultFood["created_at"])

	return reimbursementType, nil

}

//PutFoodReimbursementRequestWithChangelog -
func PutFoodReimbursementRequestWithChangelog(input map[string]interface{}, loginUser UserDetails, CompanyID int) (requestID int, err error) {

	// Step 1: Get old information
	before := func() (interface{}, error) {
		return GetFoodReimbursementRequestByID(input["id"].(int))
	}

	// Step 2: The service update or create process
	process := func() (interface{}, error) {
		return PutFoodReimbursementRequest(input)
	}

	// Step 3: Get new information
	after := func() (interface{}, error) {
		return GetFoodReimbursementRequestByID(input["id"].(int))
	}

	// Changelog Option
	var changelogOption = ChangelogOption{
		ContentID:   input["id"].(int),
		ActionType:  UPDATE_ACTLOG,
		InfoType:    REIMBURSEMENT_REPORT,
		ContentType: REIMBURSEMENT_CONTENT,
	}

	success, _ := ChangelogProcess(
		before,
		process,
		after,
		loginUser,
		changelogOption,
	)
	if success == false {
		err = errors.New("changelog process is fail")
		return
	}
	requestID = input["id"].(int)
	return
}

func PutFoodReimbursementRequest(input map[string]interface{}) (requestID int, err error) {

	var inputFood model.FoodReimbursementRequest
	jsonData, _ := json.Marshal(input)
	json.Unmarshal(jsonData, &inputFood)

	_, err = GetFoodReimbursementRequestByID(inputFood.ID)
	if err != nil {
		return 0, errors.New("Can not get food reimbursement request by id")
	}

	resEditRequestID, err := PutReimbursementRequestOnly(input)
	if err != nil {
		return 0, errors.New("Can not edit reimbursement request by id")
	}

	urlEditFood := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest + "/food")
	inputQueryFood := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/food/", resEditRequestID)

	resFoodDetail, err := rest.CallRestAPIWithOption("GET", inputQueryFood, nil)
	if err != nil {
		return 0, errors.New("cannot edit food detail")
	}

	responeFood, ok := resFoodDetail.Data.(map[string]interface{})
	if !ok {
		return 0, errors.New("Invalid food detail param")
	}

	var foodDetail model.FoodAllowance
	jsonDataFoodAllowance, _ := json.Marshal(responeFood)
	json.Unmarshal(jsonDataFoodAllowance, &foodDetail)

	now := time.Now()
	bankokTime := helpers.GetBankokTimeZone(now).Format("2006-01-02T15:04:05+00:00")
	foodDetail.UpdatedAt = bankokTime
	foodDetail.Information = inputFood.Information
	if inputFood.Date != "" {
		foodDetail.Date = inputFood.Date
	}
	foodDetail.UpdatedBy = inputFood.UpdatedBy
	_, err = rest.CallRestAPIWithOption("PUT", urlEditFood, &foodDetail)
	if err != nil {
		return 0, errors.New("cannot create food detail")
	}
	return resEditRequestID, nil
}

//PostoodAllowance
func PostFoodAllowance(foodlAllowance *model.FoodAllowance) (foodlAllowanceID int, err error) {
	url := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest)
	url += fmt.Sprintf("/food")
	resultFoodAllowance, err := rest.CallRestAPIWithOption("POST", url, &foodlAllowance)

	result, ok := resultFoodAllowance.Data.(map[string]interface{})
	if !ok {
		return 0, errors.New("Invalid param")
	}

	foodlAllowanceID = int(result["id"].(float64))

	return foodlAllowanceID, nil
}

func getDefaultApprovers(positions []model.Position) (positionsID []model.Approver, err error) {

	for i := 0; i < len(positions); i++ {
		var result model.Approver
		result.ID = positions[i].EmployeeID
		positionsID = append(positionsID, result)
	}

	return positionsID, nil
}

//GetMyReimbursementRequestList
func GetMyReimbursementRequestList(inputQuery ReimbursementQuery) (result []model.ReimbursementRequest, total int, err error) {
	var reimbursementType []model.ReimbursementRequest

	path := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequests)
	pathURL, _ := url.Parse(path)
	pathQuery := pathURL.Query()
	pathQuery.Set("offset", fmt.Sprintf("%d", inputQuery.Offset))
	pathQuery.Set("limit", fmt.Sprintf("%d", inputQuery.Limit))

	if inputQuery.Chargecode != "" {
		pathQuery.Set("charge_code", fmt.Sprintf("%s", inputQuery.Chargecode))
	}

	if inputQuery.ReimbursementType != "" {
		pathQuery.Set("type", fmt.Sprintf("%s", inputQuery.ReimbursementType))
	}
	if inputQuery.Status != "" {
		pathQuery.Set("status", fmt.Sprintf("%s", inputQuery.Status))
	}
	if inputQuery.RequesterID != 0 {
		pathQuery.Set("requester_id", fmt.Sprintf("%d", inputQuery.RequesterID))
	}

	pathURL.RawQuery = pathQuery.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), &reimbursementType)

	if err != nil {

		err = errors.New("Can not reimbursement list")
		return
	}

	data, ok := res.Data.([]interface{})

	if !ok {
		return []model.ReimbursementRequest{}, 0, errors.New("Can not get requests")
	}

	for _, item := range data {
		var request model.ReimbursementRequest

		jsonData, _ := json.Marshal(item)
		json.Unmarshal(jsonData, &request)

		result = append(result, request)
	}

	total = res.Total
	return
}

//PutFoodReimbursementRequestStatusWithChangelog -
func PutFoodReimbursementRequestStatusWithChangelog(request *model.ReimbursementRequest, loginUser UserDetails) (requestID int, err error) {

	// Step 1: Get old information
	before := func() (interface{}, error) {
		return GetReimbursementRequestOnly(request.ID)
	}

	// Step 2: The service update or create process
	process := func() (interface{}, error) {
		return PutFoodReimbursementRequestStatus(request)
	}

	// Step 3: Get new information
	after := func() (interface{}, error) {
		return GetReimbursementRequestOnly(request.ID)
	}

	// Changelog Option
	var changelogOption = ChangelogOption{
		ContentID:   request.ID,
		ActionType:  UPDATE_ACTLOG,
		InfoType:    REIMBURSEMENT_REPORT,
		ContentType: REIMBURSEMENT_CONTENT,
	}

	success, _ := ChangelogProcess(
		before,
		process,
		after,
		loginUser,
		changelogOption,
	)
	if success == false {
		err = errors.New("changelog process is fail")
		return
	}
	requestID = request.ID
	return
}

func PutFoodReimbursementRequestStatus(requestInput *model.ReimbursementRequest) (requestID int, err error) {
	urlPutRequest := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest)
	requester := requestInput.UpdatedBy
	request, _ := GetReimbursementRequestOnly(requestInput.ID)

	request.Status = requestInput.Status
	request.UpdatedBy = requester
	request.ApproverID = requester

	createAtString := helpers.GetBankokTimeZone(request.CreatedAt).Format("2006-01-02T15:04:05+00:00")
	updateAtString := helpers.GetBankokTimeZone(request.UpdatedAt).Format("2006-01-02T15:04:05+00:00")
	newCreateAt, _ := time.Parse("2006-01-02T15:04:05+00:00", createAtString)
	newUpdateAt, _ := time.Parse("2006-01-02T15:04:05+00:00", updateAtString)
	//TODO check process if canceled by back office
	if requestInput.ProcessedDate != "" {
		request.ProcessedDate = requestInput.ProcessedDate
	} else {
		request.ProcessedDate = "9999-12-31T00:00:00Z"
	}

	request.CreatedAt = newCreateAt
	request.UpdatedAt = newUpdateAt
	request.Remark = requestInput.Remark
	var newRequest model.ReimbursementRequest

	resEditStatusRequest, err := coreRest.CallRestAPIWithOption("PUT", urlPutRequest, &request)
	if err != nil {

		return 0, errors.New("Can not edit reimbursement request status")
	}

	resultEdit, ok := resEditStatusRequest.Data.(map[string]interface{})

	if !ok {
		return 0, errors.New("Can not edit request")
	}

	decodeErrorEditRequest := mapstructure.Decode(resultEdit, &newRequest)

	if decodeErrorEditRequest != nil {

		return 0, errors.New("Cannot parse request")
	}

	requestID = newRequest.ID
	return requestID, nil
}

func PostReimbursementRequestOnly(input map[string]interface{}) (requestID int, err error) {
	urlAddRequest := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest)

	infiniteTime, _ := time.Parse(time.RFC3339, "9999-12-31T00:00:00+00:00")

	var inputReimbursementRequest model.ReimbursementRequest
	jsonData, _ := json.Marshal(input)
	json.Unmarshal(jsonData, &inputReimbursementRequest)

	typeRequest := inputReimbursementRequest.Type
	cost := input["cost"].(float64)
	resultApprover, err := checkRequestApprovers(input["created_by"].(int), input["company_id"].(int), cost, typeRequest)
	if err != nil {
		return 0, err
	}

	inputReimbursementRequest.ApproverIDs = resultApprover
	inputReimbursementRequest.ProcessedDate = helpers.GetBankokTimeZone(infiniteTime).Format("2006-01-02T15:04:05+00:00")

	for i := 0; i < len(inputReimbursementRequest.FileIDs); i++ {
		inputReimbursementRequest.FileIDs[i].CreatedBy = inputReimbursementRequest.CreatedBy
		inputReimbursementRequest.FileIDs[i].UpdatedBy = inputReimbursementRequest.CreatedBy
	}

	resAddRequest, err := rest.CallRestAPIWithOption("POST", urlAddRequest, &inputReimbursementRequest)
	if err != nil {
		return 0, errors.New("cannot create request")
	}

	resultAddrequest, ok := resAddRequest.Data.(map[string]interface{})
	if !ok {
		return 0, errors.New("Invalid param request")
	}
	var newRequest model.ReimbursementRequest
	jsonData2, _ := json.Marshal(resultAddrequest)
	json.Unmarshal(jsonData2, &newRequest)
	currentrequestID := newRequest.ID

	return currentrequestID, nil
}

func PutReimbursementRequestOnly(input map[string]interface{}) (requestID int, err error) {
	urlPutRequest := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest)

	var inputRequest model.ReimbursementRequest
	jsonData, _ := json.Marshal(input)
	json.Unmarshal(jsonData, &inputRequest)

	inputQueryRequest := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/", inputRequest.ID)
	resRequest, err := coreRest.CallRestAPIWithOption("GET", inputQueryRequest, nil)
	if err != nil {
		return 0, errors.New("Can not get the reimbursement request by id")
	}
	result, ok := resRequest.Data.(map[string]interface{})
	if !ok {
		return 0, errors.New("Can not get request")
	}

	var currentRequest model.ReimbursementRequest
	decodeErrorRequest := mapstructure.Decode(result, &currentRequest)
	if decodeErrorRequest != nil {
		return 0, errors.New("Cannot parse request")
	}

	newProcessDateTime, _ := time.Parse("2006-01-02T15:04:05+00:00", currentRequest.ProcessedDate)

	currentRequest.Description = inputRequest.Description
	currentRequest.Status = inputRequest.Status
	currentRequest.UpdatedBy = inputRequest.UpdatedBy

	currentRequest.ProcessedDate = helpers.GetBankokTimeZone(newProcessDateTime).Format("2006-01-02T15:04:05+00:00")

	var finalResultApprover []model.Approver
	typeRequest := currentRequest.Type
	finalResultApprover, err = checkRequestApprovers(currentRequest.UpdatedBy, input["company_id"].(int), inputRequest.Cost, typeRequest)

	if err != nil {
		return 0, err
	}
	currentRequest.ApproverIDs = finalResultApprover

	currentRequest.Cost = inputRequest.Cost
	currentRequest.RequesterID = inputRequest.RequesterID
	currentRequest.ReviewerIDs = inputRequest.ReviewerIDs
	currentRequest.FileIDs = inputRequest.FileIDs
	if currentRequest.Type == "TRAVEL" {
		currentRequest.TotalDistance = inputRequest.TotalDistance
	}
	if currentRequest.Type == "PERDIEM" {
		currentRequest.TotalHours = inputRequest.TotalHours
	}

	resEditStatusRequest, err := coreRest.CallRestAPIWithOption("PUT", urlPutRequest, &currentRequest)
	if err != nil {
		return 0, errors.New("Can not edit reimbursement request status")
	}
	resultEditrequest, ok := resEditStatusRequest.Data.(map[string]interface{})
	if !ok {
		return 0, errors.New("Invalid param request")
	}

	var newRequest model.ReimbursementRequest
	jsonData2, _ := json.Marshal(resultEditrequest)
	json.Unmarshal(jsonData2, &newRequest)
	currentrequestID := newRequest.ID

	return currentrequestID, nil
}

//GetReimbursementReportList
func GetReimbursementReportList(inputQuery ReimbursementReportQuery) (result []model.ReimbursementReport, total int, err error) {

	path := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementReports)
	pathURL, _ := url.Parse(path)
	pathQuery := pathURL.Query()
	pathURL.RawQuery = pathQuery.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), &inputQuery)

	if err != nil {

		err = errors.New("Can not get reimbursement report")
		return
	}

	data, ok := res.Data.([]interface{})

	if !ok {
		return []model.ReimbursementReport{}, 0, errors.New("Can not get report")
	}

	for _, item := range data {
		var request model.ReimbursementReport

		jsonData, _ := json.Marshal(item)
		json.Unmarshal(jsonData, &request)

		result = append(result, request)
	}

	total = res.Total
	return
}

//GetReimbursementRequestByID
func GetReimbursementRequestOnly(id int) (model.ReimbursementRequest, error) {

	inputQueryRequest := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/", id)
	resRequest, err := coreRest.CallRestAPIWithOption("GET", inputQueryRequest, nil)
	if err != nil {
		return model.ReimbursementRequest{}, errors.New("Can not get the reimbursement request by id")
	}
	result, ok := resRequest.Data.(map[string]interface{})
	if !ok {
		return model.ReimbursementRequest{}, errors.New("Can not get request")
	}

	var currentRequest model.ReimbursementRequest
	decodeErrorRequest := mapstructure.Decode(result, &currentRequest)
	if decodeErrorRequest != nil {
		return model.ReimbursementRequest{}, errors.New("Cannot parse request")
	}
	return currentRequest, nil

}
