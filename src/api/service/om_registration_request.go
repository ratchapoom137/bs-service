package service

import (
	"api/helpers"
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/url"

	"dv.co.th/hrms/core/rest"
	"github.com/sethvargo/go-password/password"
)

func GetRegistrationRequest(id int) (model.RegistrationRequest, error) {
	var request model.RegistrationRequest
	url := fmt.Sprintf(URIEnvMapping.OMInternalRegistrationRequest+"/%d", id)

	res, err := rest.CallRestAPIWithOption("GET", url, model.RegistrationRequest{})
	if err != nil {
		return model.RegistrationRequest{}, errors.New("cannot get registration requests")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.RegistrationRequest{}, errors.New("no such file: get registration request")
	}

	jsonRequest, _ := json.Marshal(result)
	json.Unmarshal(jsonRequest, &request)

	return request, nil
}

func GetRegistrationRequests(filter map[string]interface{}) (requests []model.RegistrationRequest, total int, err error) {
	pathURL, _ := url.Parse(URIEnvMapping.OMInternalRegistrationRequests)
	q := pathURL.Query()

	if offset, ok := filter["offset"].(int); ok && offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit, ok := filter["limit"].(int); ok && limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if status, ok := filter["status"].(string); ok && status != "" {
		q.Set("status", status)
	}
	if secret, ok := filter["secret"].(string); ok && secret != "" {
		q.Set("secret", secret)
	}
	if requestDate, ok := filter["request_date"].(string); ok && requestDate != "" {
		q.Set("request_date", requestDate)
	}
	if companyName, ok := filter["company_name"].(string); ok && companyName != "" {
		q.Set("company_name", companyName)
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), []model.RegistrationRequest{})
	if err != nil {
		err = errors.New("cannot get request list")
		return
	}
	data, ok := res.Data.([]interface{})
	if !ok {
		err = errors.New("no such file: get request list")
		return
	}

	for _, request := range data {
		var tmpRequest model.RegistrationRequest

		jsonData, _ := json.Marshal(request)
		json.Unmarshal(jsonData, &tmpRequest)

		requests = append(requests, tmpRequest)
	}
	total = res.Total

	return
}

func PostRegistrationRequest(request model.RegistrationRequestInput) (int, error) {
	request.RequestDate = helpers.StartOfDateNow().Format("2006-01-02T15:04:05+00:00")
	request.Status = "PENDING"
	secret, err := password.Generate(32, 10, 0, false, false)
	if err != nil {
		log.Fatal(err)
	}
	request.SecretCode = secret

	url := URIEnvMapping.OMInternalRegistrationRequest
	res, err := rest.CallRestAPIWithOption("POST", url, &request)

	if err != nil {
		return 0, errors.New("cannot create registration request")
	}

	result := int(res.Data.(float64))

	return result, nil
}

func PutRegistrationRequest(request model.RegistrationRequestInput) (int, error) {
	if request.RequestDate == "" {
		request.RequestDate = helpers.StartOfDateNow().Format("2006-01-02T15:04:05+00:00")
	}

	url := fmt.Sprintf(URIEnvMapping.OMInternalRegistrationRequest+"/%d", request.ID)
	res, err := rest.CallRestAPIWithOption("PUT", url, &request)

	if err != nil {
		return 0, errors.New("cannot edit registration request")
	}

	result := int(res.Data.(float64))

	return result, nil
}

func PostRequestFiles(filter map[string]interface{}) ([]int, error) {
	requestId, _ := filter["request_id"].(int)

	var requestFilesInput []model.RegistrationRequestFile
	jsonRequest, _ := json.Marshal(filter["request_file_input"])
	json.Unmarshal(jsonRequest, &requestFilesInput)

	currentRequestFiles, _, _ := GetRequestFileList(requestId)
	for _, currFile := range currentRequestFiles {
		isKeep := false
		for _, inFile := range requestFilesInput {
			if currFile.ID == inFile.ID {
				isKeep = true
				break
			}
		}
		if !isKeep {
			DeleteRequestFile(currFile.ID)
		}
	}

	fileId := []int{}

	for _, reqFile := range requestFilesInput {
		if reqFile.ID == 0 {
			reqFile.RequestID = requestId

			url := URIEnvMapping.OMInternalRegistrationRequestFile
			res, err := rest.CallRestAPIWithOption("POST", url, &reqFile)
			if err != nil {
				return []int{}, errors.New("cannot create save file: " + reqFile.Name)
			}

			fileId = append(fileId, int(res.Data.(float64)))
		}
	}
	return fileId, nil
}

func GetRequestFile(id int) (model.RegistrationRequestFile, error) {

	url := fmt.Sprintf(URIEnvMapping.OMInternalRegistrationRequestFile+"/%d", id)
	res, _ := rest.CallRestAPIWithOption("GET", url, &model.RegistrationRequestFile{})

	var requestFile model.RegistrationRequestFile
	jsonRequest, _ := json.Marshal(res.Data)
	json.Unmarshal(jsonRequest, &requestFile)

	return requestFile, nil
}

func GetRequestFileList(requestId int) ([]model.RegistrationRequestFile, int, error) {
	pathURL, _ := url.Parse(URIEnvMapping.OMInternalRegistrationRequestFiles)
	q := pathURL.Query()

	if requestId > 0 {
		q.Set("request_id", fmt.Sprintf("%d", requestId))
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), []model.RegistrationRequestFile{})
	if err != nil {
		return []model.RegistrationRequestFile{}, 0, errors.New("cannot get request file list")
	}
	data, ok := res.Data.([]interface{})
	if !ok {
		return []model.RegistrationRequestFile{}, 0, errors.New("no such file: get request file list")
	}

	requestFiles := []model.RegistrationRequestFile{}

	for _, file := range data {
		var tmpfile model.RegistrationRequestFile
		jsonData, _ := json.Marshal(file)
		json.Unmarshal(jsonData, &tmpfile)

		requestFiles = append(requestFiles, tmpfile)
	}
	total := res.Total

	return requestFiles, total, nil
}

func DeleteRequestFile(id int) (int, error) {

	url := fmt.Sprintf(URIEnvMapping.OMInternalRegistrationRequestFile+"/%d", id)
	res, _ := rest.CallRestAPIWithOption("DELETE", url, &model.RegistrationRequestFile{})

	resId := int(res.Data.(float64))

	return resId, nil
}
