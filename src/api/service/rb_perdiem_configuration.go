package service

import (
	"api/model"
	"dv.co.th/hrms/core/rest"
	"encoding/json"
	"errors"
	"fmt"
	_ "github.com/davecgh/go-spew/spew"
)

//CreatePerdiemApproveConfiguration
func CreatePerdiemApproveConfiguration(input *model.PerDiemApproveConfiguration, inputTable []model.PerDiemApproveConfigurationTable) (model.PerDiemApproveConfiguration, error) {
	urlPerdiem := fmt.Sprintf(URIEnvMapping.RBInternalPerDiemApproveConfiguration)
	urlPerdiemTable := fmt.Sprintf(URIEnvMapping.RBInternalPerDiemApproveConfiguration) + "/table"

	var perDiemConfiguration model.PerDiemApproveConfiguration
	resPerdiem, err := rest.CallRestAPIWithOption("POST", urlPerdiem, input)
	if err != nil {
		return model.PerDiemApproveConfiguration{}, err
	}

	result, _ := resPerdiem.Data.(map[string]interface{})

	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &perDiemConfiguration)

	configId := perDiemConfiguration.ConfigurationID

	var newTable []model.PerDiemApproveConfigurationTable

	for i := 0; i < len(inputTable); i++ {
		inputTable[i].ConfigurationID = configId
		resPerdiemTable, err := rest.CallRestAPIWithOption("POST", urlPerdiemTable, inputTable[i])
		if err != nil {
			return model.PerDiemApproveConfiguration{}, err
		}
		var eachRowConfig model.PerDiemApproveConfigurationTable
		jsonData, _ := json.Marshal(resPerdiemTable.Data)
		json.Unmarshal(jsonData, &eachRowConfig)
		newTable = append(newTable, eachRowConfig)
	}
	perDiemConfiguration.PerDiemTable = newTable
	return perDiemConfiguration, nil

}

// GetPerdiemConfiguration -
func GetPerdiemConfiguration(id int) (model.PerDiemApproveConfiguration, error) {
	var perdiemconfig model.PerDiemApproveConfiguration
	url := fmt.Sprintf(URIEnvMapping.RBInternalPerDiemApproveConfiguration+"?&company_id=%d", id)
	res, err := rest.CallRestAPIWithOption("GET", url, &perdiemconfig)

	if err != nil {
		return model.PerDiemApproveConfiguration{}, errors.New("cannot get perdiem approve configuration")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.PerDiemApproveConfiguration{}, errors.New("not found: get perdiem approve configuration")
	}

	var configuration model.PerDiemApproveConfiguration
	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &configuration)

	var configurationTable []model.PerDiemApproveConfigurationTable

	urlTable := fmt.Sprintf(URIEnvMapping.RBInternalPerDiemApproveConfiguration+"/table"+"?&company_id=%d", id)
	resTable, err := rest.CallRestAPIWithOption("GET", urlTable, nil)
	if err != nil {

		return model.PerDiemApproveConfiguration{}, errors.New("cannot get perdiem approve configuration")
	}

	jsonData2, _ := json.Marshal(resTable.Data)
	json.Unmarshal(jsonData2, &configurationTable)
	configuration.PerDiemTable = configurationTable

	return configuration, nil
}

//PutPerdiemApproveConfiguration
func PutPerdiemApproveConfiguration(input *model.PerDiemApproveConfiguration, inputTable []model.PerDiemApproveConfigurationTable) (model.PerDiemApproveConfiguration, error) {
	urlPerdiem := fmt.Sprintf(URIEnvMapping.RBInternalPerDiemApproveConfiguration)
	urlPerdiemTable := fmt.Sprintf(URIEnvMapping.RBInternalPerDiemApproveConfiguration) + "/table"

	var perDiemConfiguration model.PerDiemApproveConfiguration
	resPerdiem, err := rest.CallRestAPIWithOption("PUT", urlPerdiem, input)
	if err != nil {
		return model.PerDiemApproveConfiguration{}, err
	}

	result, _ := resPerdiem.Data.(map[string]interface{})

	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &perDiemConfiguration)

	configId := perDiemConfiguration.ConfigurationID

	var newTable []model.PerDiemApproveConfigurationTable

	for i := 0; i < len(inputTable); i++ {
		inputTable[i].ConfigurationID = configId
		resPerdiemTable, err := rest.CallRestAPIWithOption("PUT", urlPerdiemTable, inputTable[i])
		if err != nil {
			return model.PerDiemApproveConfiguration{}, err
		}
		var eachRowConfig model.PerDiemApproveConfigurationTable
		jsonData, _ := json.Marshal(resPerdiemTable.Data)
		json.Unmarshal(jsonData, &eachRowConfig)
		newTable = append(newTable, eachRowConfig)
	}
	perDiemConfiguration.PerDiemTable = newTable
	return perDiemConfiguration, nil

}

//GetMyPerdiemRate
func GetMyPerdiemRate(id int) (model.PerDiemApproveConfiguration, error) {
	var perdiemconfig model.PerDiemApproveConfiguration
	url := fmt.Sprintf(URIEnvMapping.RBInternalPerDiemApproveConfiguration+"?&company_id=%d", id)
	res, err := rest.CallRestAPIWithOption("GET", url, &perdiemconfig)

	if err != nil {
		return model.PerDiemApproveConfiguration{}, errors.New("cannot get perdiem approve configuration")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.PerDiemApproveConfiguration{}, errors.New("not found: get perdiem approve configuration")
	}

	var configuration model.PerDiemApproveConfiguration
	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &configuration)

	var configurationTable []model.PerDiemApproveConfigurationTable

	urlTable := fmt.Sprintf(URIEnvMapping.RBInternalPerDiemApproveConfiguration+"/table"+"?&configuration_id=%d", configuration.ConfigurationID)

	resTable, err := rest.CallRestAPIWithOption("GET", urlTable, nil)

	if err != nil {

		return model.PerDiemApproveConfiguration{}, errors.New("cannot get perdiem approve configuration")
	}

	jsonData2, _ := json.Marshal(resTable.Data)
	json.Unmarshal(jsonData2, &configurationTable)
	configuration.PerDiemTable = configurationTable

	return configuration, nil
}
