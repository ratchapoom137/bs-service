package service

import (
	"api/model"
	coreRest "dv.co.th/hrms/core/rest"
	"errors"
	"net/url"
	"fmt"
	"time"
	"github.com/mitchellh/mapstructure"
	"github.com/thoas/go-funk"
)

// GetLeaveRequestList -
func GetLeaveRequestList(limit int, offset int, leaveTypeID int, chargeCodeID int, employeeID int, status string) (model.LeaveRequestList, error) {
	pathURL, _ := url.Parse(URIEnvMapping.TMLInternalLeaveRequests)
	q := pathURL.Query()

	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if leaveTypeID > 0 {
		q.Set("leave_type_id", fmt.Sprintf("%d", leaveTypeID))
	}
	if chargeCodeID > 0 {
		q.Set("charge_code_id", fmt.Sprintf("%d", chargeCodeID))
	}
	if employeeID > 0 {
		q.Set("employee_id", fmt.Sprintf("%d", employeeID))
	}
	if status != "" {
		q.Set("status", status)
	}
	pathURL.RawQuery = q.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), nil)

	if err != nil {
		return model.LeaveRequestList{}, errors.New("Can't get leave request list")
	}

	results := res.Data.([]interface{})

	var leaveRequests []model.LeaveRequest
	var total int
	var leaveErr error
	var approvalTasksErr error

	funk.ForEach(results, func(result interface{}) {
		var leaveType model.LeaveType
		var approvalTasks []model.ApprovalTask
		var leaveRequestDate []model.LeaveRequestDate
		
		if leaveErr = mapstructure.Decode(result.(map[string]interface{})["leave_type"], &leaveType); leaveErr != nil {
			leaveErr = errors.New("Can't prase leave type")
			return
		}

		if approvalTasksErr = mapstructure.Decode(result.(map[string]interface{})["approval_tasks"], &approvalTasks); approvalTasksErr != nil {
			approvalTasksErr = errors.New("Can't prase approval list")
			return
		}

		funk.ForEach(result.(map[string]interface{})["leave_request_dates"], func(item interface{}) {
			dateParsed, _ := time.Parse(time.RFC3339, item.(map[string]interface{})["date"].(string))
			leaveRequestDate = append(leaveRequestDate,
				model.LeaveRequestDate{
					ID:             int(item.(map[string]interface{})["id"].(float64)),
					LeaveRequestID: int(item.(map[string]interface{})["leave_request_id"].(float64)),
					Date:           dateParsed,
					Period:         item.(map[string]interface{})["period"].(string),
					Hour:           item.(map[string]interface{})["hour"].(float64),
				},
			)
		})

		item := result.(map[string]interface{})
		startDate, _ := time.Parse(time.RFC3339, item["start_date"].(string))
		endDate, _ := time.Parse(time.RFC3339, item["end_date"].(string))
		createdAt, _ := time.Parse(time.RFC3339, item["created_at"].(string))
		leaveRequests = append(leaveRequests,
			model.LeaveRequest{
				ID:            int(item["id"].(float64)),
				EmployeeID:    int(item["employee_id"].(float64)),
				LeaveTypeID:   int(item["leave_type_id"].(float64)),
				LeaveType:     &leaveType,
				StartDate:     startDate,
				EndDate:       endDate,
				LeaveRequestDates: leaveRequestDate,
				TotalDate:     item["total_date"].(float64),
				TotalHour:     item["total_hour"].(float64),
				Status:        item["status"].(string),
				ApprovalTasks: approvalTasks,
				CreatedAt:     createdAt,
			},
		)
	})

	if leaveErr != nil {
		return model.LeaveRequestList{}, leaveErr
	}

	if approvalTasksErr != nil {
		return model.LeaveRequestList{}, approvalTasksErr
	}

	if leaveTotalErr := mapstructure.Decode(res.Total, &total); leaveTotalErr != nil {
		return model.LeaveRequestList{}, errors.New("Can't not get leave request list")
	}

	return model.LeaveRequestList{
		LeaveRequests: leaveRequests,
		Total:         total,
	}, nil
}

// CreateLeaveRequest -
func CreateLeaveRequest(leaveRequest model.LeaveRequest) (model.LeaveRequest, error) {
	var leaveRequestDate []model.LeaveRequestDate
	var participants []model.Participant
	var attachments []model.Attachment
	var leaveType model.LeaveType

	url := URIEnvMapping.TMLInternalLeaveRequest
	res, err := coreRest.CallRestAPIWithOption("POST", url, &leaveRequest)
	if err != nil {
		return model.LeaveRequest{}, errors.New("Can't not create leave request")
	}

	result := res.Data.(map[string]interface{})

	startDateParsed, err := time.Parse(time.RFC3339, result["start_date"].(string))
	if err != nil {
		return model.LeaveRequest{}, errors.New("Wrong time format")
	}

	endDateParsed, err := time.Parse(time.RFC3339, result["end_date"].(string))
	if err != nil {
		return model.LeaveRequest{}, errors.New("Wrong time format")
	}

	if leaveTypeErr := mapstructure.Decode(result["leave_type"].(map[string]interface{}), &leaveType); leaveTypeErr != nil {
		return model.LeaveRequest{}, errors.New("Can't not prase leave type")
	}

	funk.ForEach(result["leave_request_dates"].([]interface{}), func(item interface{}) {
		dateParsed, _ := time.Parse(time.RFC3339, item.(map[string]interface{})["date"].(string))
		leaveRequestDate = append(leaveRequestDate,
			model.LeaveRequestDate{
				ID:             int(item.(map[string]interface{})["id"].(float64)),
				LeaveRequestID: int(item.(map[string]interface{})["leave_request_id"].(float64)),
				Date:           dateParsed,
				Period:         item.(map[string]interface{})["period"].(string),
				Hour:           item.(map[string]interface{})["hour"].(float64),
			},
		)
	})

	_, participantCheckOk := result["participants"].([]interface{})
	if participantCheckOk {
		if participantsErr := mapstructure.Decode(result["participants"].([]interface{}), &participants); participantsErr != nil {
			return model.LeaveRequest{}, errors.New("Can't not map leave request participants")
		}
	}

	_, attachmentCheckOk := result["attachments"].([]interface{})
	if attachmentCheckOk {
		if attachmentCheckOk {
			if attachmentsErr := mapstructure.Decode(result["attachments"].([]interface{}), &attachments); attachmentsErr != nil {
				return model.LeaveRequest{}, errors.New("Can't not map leave request attachments")
			}
		}
	}

	leaveRequest = model.LeaveRequest{
		ID:                int(result["id"].(float64)),
		EmployeeID:        int(result["employee_id"].(float64)),
		LeaveTypeID:       int(result["leave_type_id"].(float64)),
		LeaveType:		   &leaveType,
		LeaveRequestDates: leaveRequestDate,
		StartDate:         startDateParsed,
		EndDate:           endDateParsed,
		TotalDate:         result["total_date"].(float64),
		TotalHour:         result["total_hour"].(float64),
		Reason:            result["reason"].(string),
		Participants:      participants,
		Attachments:       attachments,
		Status:            result["status"].(string),
	}

	return leaveRequest, nil
}

// EditLeaveRequest -
func EditLeaveRequest(id int, leaveRequest model.LeaveRequest) (model.LeaveRequest, error) {
	var leaveRequestDate []model.LeaveRequestDate
	var participants []model.Participant
	var attachments []model.Attachment
	var leaveType model.LeaveType

	url := fmt.Sprintf(URIEnvMapping.TMLInternalLeaveRequest+"/%d", id)
	res, err := coreRest.CallRestAPIWithOption("PUT", url, &leaveRequest)
	if err != nil {
		return model.LeaveRequest{}, errors.New("Can't not get leave request")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.LeaveRequest{}, errors.New("leave request not found")
	}

	result := res.Data.(map[string]interface{})

	if leaveTypeErr := mapstructure.Decode(result["leave_type"].(map[string]interface{}), &leaveType); leaveTypeErr != nil {
		return model.LeaveRequest{}, errors.New("Can't not prase leave type")
	}

	startDateParsed, startErr := time.Parse(time.RFC3339, result["start_date"].(string))
	if startErr != nil {
		return model.LeaveRequest{}, errors.New("Wrong time format")
	}

	endDateParsed, endErr := time.Parse(time.RFC3339, result["end_date"].(string))
	if endErr != nil {
		return model.LeaveRequest{}, errors.New("Wrong time format")
	}

	createdAt, _ := time.Parse(time.RFC3339, result["created_at"].(string))
	updatedAt, _ := time.Parse(time.RFC3339, result["updated_at"].(string))

	funk.ForEach(result["leave_request_dates"].([]interface{}), func(item interface{}) {
		dateParsed, _ := time.Parse(time.RFC3339, item.(map[string]interface{})["date"].(string))
		leaveRequestDate = append(leaveRequestDate,
			model.LeaveRequestDate{
				ID:             int(item.(map[string]interface{})["id"].(float64)),
				LeaveRequestID: int(item.(map[string]interface{})["leave_request_id"].(float64)),
				Date:           dateParsed,
				Period:         item.(map[string]interface{})["period"].(string),
				Hour:           item.(map[string]interface{})["hour"].(float64),
			},
		)
	})

	_, participantCheckOk := result["participants"].([]interface{})
	if participantCheckOk {
		if participantsErr := mapstructure.Decode(result["participants"].([]interface{}), &participants); participantsErr != nil {
			return model.LeaveRequest{}, errors.New("Can't not map leave request participants")
		}
	}
	_, attachmentCheckOk := result["attachments"].([]interface{})
	if attachmentCheckOk {
		if attachmentsErr := mapstructure.Decode(result["attachments"].([]interface{}), &attachments); attachmentsErr != nil {
			return model.LeaveRequest{}, errors.New("Can't not map leave request attachments")
		}
	}

	return model.LeaveRequest{
		ID:                int(result["id"].(float64)),
		EmployeeID:        int(result["employee_id"].(float64)),
		LeaveType:		   &leaveType,
		LeaveTypeID:       int(result["leave_type_id"].(float64)),
		LeaveRequestDates: leaveRequestDate,
		StartDate:         startDateParsed,
		EndDate:           endDateParsed,
		TotalDate:         result["total_date"].(float64),
		TotalHour:         result["total_hour"].(float64),
		Reason:            result["reason"].(string),
		Participants:      participants,
		Attachments:       attachments,
		Status:            result["status"].(string),
		CreatedAt:		   createdAt,
		UpdatedAt:		   updatedAt,
	}, nil
}

// CancelLeaveRequest -
func CancelLeaveRequest(id int) (model.LeaveRequest, error) {
	var leaveRequestDate []model.LeaveRequestDate
	var participants []model.Participant
	var attachments []model.Attachment
	var leaveType model.LeaveType

	url := fmt.Sprintf(URIEnvMapping.TMLInternalLeaveRequest+"/%d/cancel", id)
	res, err := coreRest.CallRestAPIWithOption("PUT", url, nil)
	if err != nil {
		return model.LeaveRequest{}, errors.New("Can't not get Leave Request")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.LeaveRequest{}, errors.New("Leave request not found")
	}

	data := res.Data.(map[string]interface{})

	leaveErr := mapstructure.Decode(data["leave_type"], &leaveType)
	if leaveErr != nil {
		return model.LeaveRequest{}, errors.New("Can't prase leave type")
	}

	startDateParsed, err := time.Parse(time.RFC3339, data["start_date"].(string))
	endDateParsed, err := time.Parse(time.RFC3339, data["end_date"].(string))

	funk.ForEach(data["leave_request_dates"].([]interface{}), func(item interface{}) {
		dateParsed, _ := time.Parse(time.RFC3339, item.(map[string]interface{})["date"].(string))
		leaveRequestDate = append(leaveRequestDate,
			model.LeaveRequestDate{
				ID:             int(item.(map[string]interface{})["id"].(float64)),
				LeaveRequestID: int(item.(map[string]interface{})["leave_request_id"].(float64)),
				Date:           dateParsed,
				Period:         item.(map[string]interface{})["period"].(string),
				Hour:           item.(map[string]interface{})["hour"].(float64),
			},
		)
	})

	_, participantCheckOk := data["participants"].([]interface{})
	if participantCheckOk {
		if participantsErr := mapstructure.Decode(data["participants"].([]interface{}), &participants); participantsErr != nil {
			return model.LeaveRequest{}, errors.New("Can't not map leave request participants")
		}
	}
	_, attachmentCheckOk := data["attachments"].([]interface{})
	if attachmentCheckOk {
		if attachmentsErr := mapstructure.Decode(data["attachments"].([]interface{}), &attachments); attachmentsErr != nil {
			return model.LeaveRequest{}, errors.New("Can't not map leave request attachments")
		}
	}

	return model.LeaveRequest{
		ID:                int(data["id"].(float64)),
		EmployeeID:        int(data["employee_id"].(float64)),
		LeaveTypeID:       int(data["leave_type_id"].(float64)),
		LeaveType:         &leaveType,
		LeaveRequestDates: leaveRequestDate,
		StartDate:         startDateParsed,
		EndDate:           endDateParsed,
		TotalDate:         data["total_date"].(float64),
		TotalHour:         data["total_hour"].(float64),
		Reason:            data["reason"].(string),
		Participants:      participants,
		Attachments:       attachments,
		Status:            data["status"].(string),
	}, nil
}

// GetLeaveRequestByID -
func GetLeaveRequestByID(id int) (model.LeaveRequest, error) {
	var leaveRequestDate []model.LeaveRequestDate
	var participants []model.Participant
	var attachments []model.Attachment
	var approvalTask []model.ApprovalTask
	var leaveType model.LeaveType

	url := fmt.Sprintf(URIEnvMapping.TMLInternalLeaveRequest+"/%d", id)
	res, err := coreRest.CallRestAPIWithOption("GET", url, nil)
	if err != nil {
		return model.LeaveRequest{}, errors.New("Can't not get Leave Request")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.LeaveRequest{}, errors.New("Leave request not found")
	}

	data := res.Data.(map[string]interface{})

	leaveErr := mapstructure.Decode(data["leave_type"], &leaveType)
	if leaveErr != nil {
		panic(leaveErr)
	}

	startDateParsed, err := time.Parse(time.RFC3339, data["start_date"].(string))
	if err != nil {
		return model.LeaveRequest{}, errors.New("Wrong time format")
	}

	endDateParsed, err := time.Parse(time.RFC3339, data["end_date"].(string))
	if err != nil {
		return model.LeaveRequest{}, errors.New("Wrong time format")
	}

	createdAtParsed, err := time.Parse(time.RFC3339, data["created_at"].(string))
	if err != nil {
		return model.LeaveRequest{}, errors.New("Wrong time format")
	}

	funk.ForEach(data["leave_request_dates"].([]interface{}), func(item interface{}) {
		dateParsed, _ := time.Parse(time.RFC3339, item.(map[string]interface{})["date"].(string))
		leaveRequestDate = append(leaveRequestDate,
			model.LeaveRequestDate{
				ID:             int(item.(map[string]interface{})["id"].(float64)),
				LeaveRequestID: int(item.(map[string]interface{})["leave_request_id"].(float64)),
				Date:           dateParsed,
				Period:         item.(map[string]interface{})["period"].(string),
				Hour:           item.(map[string]interface{})["hour"].(float64),
			},
		)
	})

	if participantsErr := mapstructure.Decode(data["participants"].([]interface{}), &participants); participantsErr != nil {
		return model.LeaveRequest{}, errors.New("Can't not map leave request participants")
	}

	if attachmentsErr := mapstructure.Decode(data["attachments"].([]interface{}), &attachments); attachmentsErr != nil {
		return model.LeaveRequest{}, errors.New("Can't not map leave request attachments")
	}

	if approvalTaskErr := mapstructure.Decode(data["approval_tasks"].([]interface{}), &approvalTask); approvalTaskErr != nil {
		return model.LeaveRequest{}, errors.New("Can't not map leave request approval list")
	}

	return model.LeaveRequest{
		ID:                int(data["id"].(float64)),
		EmployeeID:        int(data["employee_id"].(float64)),
		LeaveTypeID:       int(data["leave_type_id"].(float64)),
		LeaveType:         &leaveType,
		LeaveRequestDates: leaveRequestDate,
		StartDate:         startDateParsed,
		EndDate:           endDateParsed,
		TotalDate:         data["total_date"].(float64),
		TotalHour:         data["total_hour"].(float64),
		Reason:            data["reason"].(string),
		Participants:      participants,
		Attachments:       attachments,
		ApprovalTasks:     approvalTask,
		Status:            data["status"].(string),
		CreatedAt:         createdAtParsed,
	}, nil
}



