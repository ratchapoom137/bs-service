package service

import (
	"api/model"
	"api/request"
	"api/helpers"
	"errors"
	"fmt"
	"net/url"
	"time"

	coreRest "dv.co.th/hrms/core/rest"
	"github.com/thoas/go-funk"
	"github.com/mitchellh/mapstructure"
)

// GetApprovalLeaveRequestList -
func GetApprovalLeaveRequestList(employeeID int, limit int, offset int, leaveTypeID int, employeeIDs []int, status string,) (model.ApprovalTaskList, error) {
	approvalsURL := fmt.Sprintf(URIEnvMapping.TMLInternalApprovalTask+"/%d", employeeID)
	pathURL, _ := url.Parse(approvalsURL)
	q := pathURL.Query()

	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if leaveTypeID > 0 {
		q.Set("leave_type_id", fmt.Sprintf("%d", leaveTypeID))
	}
	if len(employeeIDs) > 0 {
		q.Set("employee_ids", helpers.ArrayToString(employeeIDs, ","))
	}
	if status != "" {
		q.Set("status", status)
	}
	pathURL.RawQuery = q.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), nil)

	if err != nil {
		return model.ApprovalTaskList{}, errors.New("Can't not get approval leave request list")
	}

	results := res.Data.([]interface{})

	var approvalTasks []model.ApprovalTask
	var total int

	funk.ForEach(results, func(result interface{}) {
		var leaveRequests interface{}
		var leaveType interface{}

		leaveRequests = result.(map[string]interface{})["leave_request"]
		leaveType = leaveRequests.(map[string]interface{})["leave_type"]

		startDate, _ := time.Parse(time.RFC3339, leaveRequests.(map[string]interface{})["start_date"].(string))
		endDate, _ := time.Parse(time.RFC3339, leaveRequests.(map[string]interface{})["end_date"].(string))

		item := result.(map[string]interface{})

		approvalTasks = append(approvalTasks,
			model.ApprovalTask{
				ID:         int(item["id"].(float64)),
				EmployeeID: int(item["employee_id"].(float64)),
				LeaveRequestID: int(item["leave_request_id"].(float64)),
				LeaveRequest: &model.LeaveRequest{
					ID:         int(leaveRequests.(map[string]interface{})["id"].(float64)),
					EmployeeID: int(leaveRequests.(map[string]interface{})["employee_id"].(float64)),
					LeaveTypeID: int(leaveRequests.(map[string]interface{})["leave_type_id"].(float64)),
					LeaveType: &model.LeaveType{
						ID:   int(leaveType.(map[string]interface{})["id"].(float64)),
						Name: leaveType.(map[string]interface{})["name"].(string),
					},
					StartDate: startDate,
					EndDate:   endDate,
					TotalDate: leaveRequests.(map[string]interface{})["total_date"].(float64),
					TotalHour: leaveRequests.(map[string]interface{})["total_hour"].(float64),
					Status:    leaveRequests.(map[string]interface{})["status"].(string),
				},
				Status: item["status"].(string),
			},
		)
	})

	if approvalTotalErr := mapstructure.Decode(res.Total, &total); approvalTotalErr != nil {
		return model.ApprovalTaskList{}, errors.New("Can't not map approval leave request list total")
	}

	return model.ApprovalTaskList{
		ApprovalTasks: approvalTasks,
		Total: total,
	}, nil
}

// CreateApprovalList - POST Create Aproval List
func CreateApprovalList(approvalList []request.TMLCreateApprovalList) ([]model.ApprovalTask, error){
	url := fmt.Sprintf(URIEnvMapping.TMLInternalApprovalTask)

	res, err := coreRest.CallRestAPIWithOption("POST", url, &approvalList)
	if err != nil {
		return []model.ApprovalTask{}, errors.New("Can't not create approval list")
	}

	results := res.Data.([]interface{})

	var approvalTasks []model.ApprovalTask

	if approvalTasksErr := mapstructure.Decode(results, &approvalTasks); approvalTasksErr != nil {
		return approvalTasks, errors.New("Can't map approval leave request list total")
	}

	return approvalTasks, nil
}

// ApproveLeaveRequest - PUT Approve Leave request with Call ApproveLeaveRequest API
func ApproveLeaveRequest(employeeID int, leaveRequestID int, approvalTask model.ApprovalTask) (model.ApprovalTask, error) {

	url := fmt.Sprintf(URIEnvMapping.TMLInternalApprovalTaskApprove, employeeID, leaveRequestID)

	res, err := coreRest.CallRestAPIWithOption("PUT", url, &approvalTask)
	if err != nil {
		return model.ApprovalTask{}, errors.New("Can't not get leave approval")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.ApprovalTask{}, errors.New("leave request approval not found")
	}

	data := res.Data.(map[string]interface{})

	approvalTask = model.ApprovalTask{
		ID:             int(data["id"].(float64)),
		EmployeeID:     int(data["employee_id"].(float64)),
		LeaveRequestID: int(data["leave_request_id"].(float64)),
		Comment:        data["comment"].(string),
		Status:         data["status"].(string),
	}
	return approvalTask, nil
}

// RejectLeaveRequest - PUT Reject Leave request with Call RejectLeaveRequest API
func RejectLeaveRequest(employeeID int, leaveRequestID int, approvalTask model.ApprovalTask) (model.ApprovalTask, error) {

	url := fmt.Sprintf(URIEnvMapping.TMLInternalApprovalTaskReject, employeeID, leaveRequestID)
	res, err := coreRest.CallRestAPIWithOption("PUT", url, approvalTask)
	if err != nil {
		return model.ApprovalTask{}, errors.New("Can't not get leave approval")
	}
	if res.Status.Code == "NOT_FOUND" {
		return model.ApprovalTask{}, errors.New("leave request approval not found")
	}

	data := res.Data.(map[string]interface{})

	approvalTask = model.ApprovalTask{
		ID:             int(data["id"].(float64)),
		EmployeeID:     int(data["employee_id"].(float64)),
		LeaveRequestID: int(data["leave_request_id"].(float64)),
		Comment:        data["comment"].(string),
		Status:         data["status"].(string),
	}
	return approvalTask, nil
}
