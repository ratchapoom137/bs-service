package service

import (
	"api/model"
	"time"
)

func GetAssetLatestOperation(assetID int) model.AssetOperation {
	filter := AssetOperationFilter{
		Limit:   1,
		Offset:  0,
		AssetID: assetID,
	}
	assetOperations, total, _ := GetAssetOperations(filter)

	if total > 0 {
		return assetOperations[0]
	}
	return model.AssetOperation{}
}

func GetAssetStatus(asset model.Asset) (string, error) {
	op := GetAssetLatestOperation(asset.ID)
	if op.ID == 0 {
		return "AVAILABLE", nil
	}
	if asset.ExpireDate.Before(time.Now()) {
		return "EXPIRED", nil
	}
	if op.ReturnDate != nil {
		return "AVAILABLE", nil
	}
	if op.DueDate != nil || op.EmployeeID != 0 {
		return "UNAVAILABLE", nil
	}
	return "UNAVAILABLE", nil
}
