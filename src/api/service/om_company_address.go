package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"dv.co.th/hrms/core/rest"
)

// GetAddress -
func GetAddress(id int) (model.CompanyAddress, error) {
	var address model.CompanyAddress

	url := fmt.Sprintf(URIEnvMapping.OMInternalCompanyAddress+"/%d", id)

	res, err := rest.CallRestAPIWithOption("GET", url, &address)
	if err != nil {
		return model.CompanyAddress{}, errors.New("cannot get address")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.CompanyAddress{}, errors.New("no such file: get address")
	}

	jsonAddress, _ := json.Marshal(result)
	json.Unmarshal(jsonAddress, &address)

	return address, nil
}

// GetAddressList -
func GetAddressList(limit int, offset int, compID int) (result []model.CompanyAddress, total int, err error) {
	var address []model.CompanyAddress

	pathURL, _ := url.Parse(URIEnvMapping.OMInternalCompanyAddresses)
	q := pathURL.Query()

	if offset > 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if compID > 0 {
		q.Set("company_id", fmt.Sprintf("%d", compID))
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), &address)

	if err != nil {
		err = errors.New("cannot get address")
		return
	}

	data, ok := res.Data.([]interface{})

	if !ok {
		return []model.CompanyAddress{}, 0, errors.New("no such file: get address list")
	}

	for _, address := range data {
		var jsonAddress model.CompanyAddress

		jsnonData, _ := json.Marshal(address)
		json.Unmarshal(jsnonData, &jsonAddress)

		result = append(result, jsonAddress)
	}
	total = res.Total

	return
}

// PostAddress -
func PostAddress(address model.CompanyAddress, addresses []model.CompanyAddress, compID int) (result []int, err error) {
	url := URIEnvMapping.OMInternalCompanyAddress

	if address.CompanyID != 0 {
		address.IsHeadOffice = true
		res, e := rest.CallRestAPIWithOption("POST", url, &address)
		if e != nil || res.Data == nil {
			err = errors.New("cannot create company address")
		}
		result = append(result, int(res.Data.(float64)))
	}
	for _, addr := range addresses {
		if compID != 0 {
			addr.CompanyID = compID
		}
		res, e := rest.CallRestAPIWithOption("POST", url, &addr)
		if e != nil || res.Data == nil {
			err = errors.New("cannot create company address")
			break
		}
		result = append(result, int(res.Data.(float64)))
	}

	return
}

// PutAddress -
func PutAddress(addresses []model.CompanyAddress) (result []int, err error) {

	for _, addr := range addresses {
		url := fmt.Sprintf(URIEnvMapping.OMInternalCompanyAddress+"/%d", addr.ID)
		res, e := rest.CallRestAPIWithOption("PUT", url, &addr)
		if e != nil || res.Data == nil {
			err = errors.New("cannot edit company address")
			break
		}
		result = append(result, int(res.Data.(float64)))
	}

	return
}

// DeleteCompanyAddress -
func DeleteCompanyAddress(idList []int) (result []int, err error) {

	for _, id := range idList {
		url := fmt.Sprintf(URIEnvMapping.OMInternalCompanyAddress+"/%d", id)
		res, e := rest.CallRestAPIWithOption("DELETE", url, &id)
		if e != nil || res.Data == nil {
			err = errors.New("cannot delete company address")
			break
		}
		result = append(result, int(res.Data.(float64)))
	}

	return
}
