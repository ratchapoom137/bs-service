package service

import (
	"api/model"
	"errors"
	"github.com/sethvargo/go-password/password"
)

//Create User and User detail
func CreateUserAndUserDetail(userInput UserInput, userDetailInput UserDetailInput) (userID int, err error) {

	userID, err = CreateUser(userInput)
	if err != nil {
		return
	}

	userDetailInput.UserID = userID

	_, err = CreateUserDetail(userDetailInput)
	if err != nil {
		return
	}

	// Create Password Session
	var passwordSession model.PasswordSessionInput
	passwordSession.UserID = userID
	_, err = CreatePasswordSession(passwordSession)
	if err != nil {
		return
	}

	return
}

func GetUserAndUserDetail(userID int) (result UserAndUserDetail, err error) {
	getUserDetailFilter := map[string]interface{}{}
	getUserDetailFilter["user_id"] = userID
	userDetailList, _, userDetailErr := GetUserDetailList(getUserDetailFilter)
	if userDetailErr != nil {
		err = userDetailErr
		return
	}
	userRes, userErr := GetUserFromID(userID)
	if userErr != nil {
		err = userErr
		return
	}

	result.User = userRes
	result.UserDetail = userDetailList[0]

	return
}

func GetUserAndUserDetailList(filter map[string]interface{}) (result []UserAndUserDetail, total int, err error) {
	var userAndUserDetail UserAndUserDetail
	var userDetailList []model.UserDetail
	var userList []model.User
	var filterList FilterList

	getUserDetailFilter := map[string]interface{}{}
	getUserFilter := map[string]interface{}{}

	offset, offsetOk := filter["offset"].(int)
	limit, limitOk := filter["limit"].(int)
	role_name_list, role_name_listOk := filter["role_name_list"].([]string)
	company_id, company_idOk := filter["company_id"].(int)
	status, statusOk := filter["status"].(string)
	name, nameOk := filter["name"].(string)
	company_name, company_nameOk := filter["company_name"].(string)
	username, usernameOk := filter["search_username"].(string)

	if offsetOk && offset >= 0 && (role_name_listOk && (company_idOk || statusOk || nameOk || company_nameOk)) {
		getUserDetailFilter["offset"] = offset
		getUserFilter["offset"] = offset
	}
	if limitOk && limit > 0 && (role_name_listOk && (company_idOk || statusOk || nameOk || company_nameOk)) {
		getUserDetailFilter["limit"] = limit
		getUserFilter["limit"] = limit
	}
	if nameOk && name != "" {
		getUserDetailFilter["name"] = name
	}
	if statusOk && status != "" {
		getUserDetailFilter["status"] = status
	}
	if company_idOk && company_id > 0 {
		getUserDetailFilter["company_id_list"] = []int{company_id}
	}
	if usernameOk && username != "" && !role_name_listOk {
		getUserFilter["search_username"] = username
		res, userTotal, userErr := GetUserList(getUserFilter)
		if userErr != nil {
			return
		}
		total = total + userTotal
		for _, item := range res {
			userList = append(userList, item)
			filterList.UserIDList = append(filterList.UserIDList, int(item.ID))
		}
	}
	if role_name_listOk && len(role_name_list) > 0 {
		if usernameOk && username != "" {
			getUserFilter["search_username"] = username
		}
		for index, item := range role_name_list {
			getUserFilter["role_name"] = item
			res, userTotal, _ := GetUserList(getUserFilter)
			total = total + userTotal
			for _, item := range res {
				userList = append(userList, item)
				filterList.UserIDList = append(filterList.UserIDList, int(item.ID))
			}
			if len(userList) == 0 && index+1 == len(role_name_list) {
				return
			}
		}
		getUserDetailFilter["user_id_list"] = filterList.UserIDList
	}
	if company_nameOk && company_name != "" {
		companyRes, comTotal, _ := GetCompanyList(filter["limit"].(int), filter["offset"].(int), company_name, "")
		if len(companyRes) == 0 {
			total = comTotal
			return
		}
		for _, item := range companyRes {
			filterList.CompanyIDList = append(filterList.CompanyIDList, int(item.ID))
		}
		getUserDetailFilter["company_id_list"] = filterList.CompanyIDList
	}
	userDetailList, total, _ = GetUserDetailList(getUserDetailFilter)
	if len(userList) == 0 {
		for _, item := range userDetailList {
			user, _ := GetUserFromID(item.UserID)
			userList = append(userList, user)
		}
	}
	for _, userDetail := range userDetailList {
		for _, user := range userList {
			if user.ID == userDetail.UserID {
				userAndUserDetail.User = user
				userAndUserDetail.UserDetail = userDetail
				result = append(result, userAndUserDetail)
			}
		}
	}
	if len(result) > 10 {
		total = len(result)
		if offset > total {
			result = []UserAndUserDetail{}
			total = 0
			return
		}
		endIndex := offset + limit
		if endIndex > total {
			endIndex = total
		}
		result = result[offset:endIndex]
	}

	return
}

func ChangePasswordHelper(userInfo model.User, newPassword model.UserInput, oldPassword model.UserPassword) (userID int, err error) {

	oldPassword.Username = userInfo.Username
	res := ValidatePassword(oldPassword)

	if res != true {
		return 0, errors.New("Password not match")
	}

	verifyPasswordErr := VerifyPassword(newPassword.Password)
	if verifyPasswordErr != nil {
		return 0, verifyPasswordErr
	}

	newPassword.ID = userInfo.ID
	userResult, userErr := PutUser(newPassword)

	if userErr != nil {
		return 0, userErr
	}
	return userResult, userErr
}

func PutUserAndPasswordSession(userInput model.UserInput) (result int, err error) {

	secret, err := password.Generate(32, 10, 0, false, false)
	userInput.Password = secret

	userResult, userErr := PutUser(userInput)
	if userErr != nil {
		return result, userErr
	}

	_, pwdSessionErr := PasswordSessionHelper(userInput.ID)
	if pwdSessionErr != nil {
		return result, pwdSessionErr
	}

	result = userResult
	return
}
