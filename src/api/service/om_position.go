package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"time"

	"dv.co.th/hrms/core/rest"
)

// GetPosition -
func GetPosition(id int) (model.Position, error) {
	var position model.Position
	url := fmt.Sprintf(URIEnvMapping.OMInternalPosition+"/%d", id)

	res, err := rest.CallRestAPIWithOption("GET", url, &position)
	if err != nil {
		return model.Position{}, errors.New("cannot get position")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.Position{}, errors.New("no such file: get position")
	}

	jsonPosition, _ := json.Marshal(result)
	json.Unmarshal(jsonPosition, &position)

	filter := map[string]interface{}{}
	filter["point_of_time"] = time.Now().Format("2006-01-02T15:04:05+07:00")
	filter["position_id"] = position.ID
	filter["limit"] = 1
	filter["offset"] = 0
	tmpRes, total, _ := GetEmployeeByFilter(filter)

	if total != 0 {
		position.EmployeeID = tmpRes[0].ID
	}

	return position, nil
}

// GetPositions -
func GetPositions(limit int, offset int, orgUnitID int, name string, pointOfTime string, isPending bool, isAvailable bool) (result []model.Position, total int, err error) {
	var positions []model.Position

	pathURL, _ := url.Parse(URIEnvMapping.OMInternalPositions)
	q := pathURL.Query()

	if offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if orgUnitID > 0 {
		q.Set("org_unit_id", fmt.Sprintf("%d", orgUnitID))
	}
	if name != "" {
		q.Set("name", name)
	}
	if pointOfTime == "" {
		pointOfTime = time.Now().Format("2006-01-02T15:04:05+07:00")
	}
	q.Set("point_of_time", pointOfTime)
	q.Set("is_pending", fmt.Sprintf("%v", isPending))

	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), &positions)
	if err != nil {
		err = errors.New("cannot get level")
		return
	}

	data, ok := res.Data.([]interface{})
	if !ok {
		return []model.Position{}, 0, errors.New("no such file: get position list")
	}

	for _, position := range data {
		var tmpPosition model.Position

		jsonData, _ := json.Marshal(position)
		json.Unmarshal(jsonData, &tmpPosition)

		// Get Employee's Position form PA
		filter := map[string]interface{}{}
		filter["point_of_time"] = pointOfTime
		filter["position_id"] = tmpPosition.ID
		filter["limit"] = 1
		filter["offset"] = 0
		tmpRes, total, _ := GetEmployeeByFilter(filter)

		if total != 0 {
			tmpPosition.EmployeeID = tmpRes[0].EmployeePersonnelInfo.ID
		}

		if isAvailable {
			if tmpPosition.EmployeeID == 0 {
				result = append(result, tmpPosition)
			}
			continue
		}
		result = append(result, tmpPosition)
	}
	total = res.Total

	return
}

// PostPosition -
func PostPosition(position model.Position) (int, error) {
	infiniteDate, _ := time.Parse(time.RFC3339, "9999-12-31T00:00:00+00:00")
	position.TerminateAt = &infiniteDate

	url := URIEnvMapping.OMInternalPosition
	res, err := rest.CallRestAPIWithOption("POST", url, &position)

	if err != nil {
		return 0, errors.New("cannot create position")
	}

	result := int(res.Data.(float64))

	return result, nil
}

// PutPosition -
func PutPosition(position model.Position) (int, error) {
	url := fmt.Sprintf(URIEnvMapping.OMInternalPosition+"/%d", position.ID)
	res, err := rest.CallRestAPIWithOption("PUT", url, &position)

	if err != nil {
		return 0, errors.New("cannot create position")
	}

	result := int(res.Data.(float64))

	return result, nil
}

// DeletePosition -
func DeletePosition(id int) (int, error) {
	url := fmt.Sprintf(URIEnvMapping.OMInternalPosition+"/%d", id)
	res, err := rest.CallRestAPIWithOption("DELETE", url, model.Position{})

	if err != nil {
		return 0, errors.New("cannot delete position")
	}

	result := int(res.Data.(float64))

	return result, nil
}
