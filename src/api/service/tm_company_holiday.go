package service

import (
	"api/model"
	"errors"
	"fmt"

	"time"

	coreRest "dv.co.th/hrms/core/rest"
	"github.com/mitchellh/mapstructure"
)

type CompanyHolidayQuery struct {
	ID        int    `json:"id`
	CompanyID int    `json:"company_id`
	Name      string `json:"id`
	Limit     int    `json:"limit`
	Offset    int    `json:"offset`
}

// HolidayCompany -
type HolidayCompany struct {
	ID                 int             `json:"id"`
	HolidayCompanyName string          `json:"holidaycompany_name" mapstructure:"holidaycompany_name"`
	HolidayDetails     []HolidayDetail `json:"holiday_details" mapstructure:"holiday_details"`
	HolidayAmount      int             `json:"holiday_amount" mapstructure:"holiday_amount"`
	CreatedAt          string          `json:"created_at" mapstructure:"created_at"`
}

type HolidayDetail struct {
	ID        int    `json:"id" mapstructure:"id"`
	Name      string `json:"name" mapstructure:"name"`
	Date      string `json:"date"`
	CreatedBy int    `json:"created_by" mapstructure:"created_by"`
	UpdatedBy int    `json:"updated_by" mapstructure:"updated_by"`
}

// HolidayCompanyList -
type HolidayCompanyList struct {
	HolidayCompany []HolidayCompany `json:"company_holidays"`
	Total          int              `json:"total"`
}

// GetHolidayCompanyList -
func GetHolidayCompanyList(queryInput CompanyHolidayQuery) (HolidayCompanyList, error) {
	var result []interface{}
	var ok bool
	var HolidayCompanyType []model.HolidayCompany
	url := fmt.Sprintf(URIEnvMapping.TMInternalCompanyHolidays + "?")
	url += fmt.Sprintf("&limit=%d", queryInput.Limit)
	url += fmt.Sprintf("&offset=%d", queryInput.Offset)
	url += fmt.Sprintf("&company_id=%d", queryInput.CompanyID)
	url += "&name=" + queryInput.Name
	res, err := coreRest.CallRestAPIWithOption("GET", url, &HolidayCompanyType)

	if err != nil {
		return HolidayCompanyList{}, errors.New("Can't get company holiday")
	}

	if result, ok = res.Data.([]interface{}); !ok {
		return HolidayCompanyList{}, errors.New("Can't get company holiday")
	}
	var parsedHolidayCompany []HolidayCompany

	for _, holiday := range result {
		var tempHoliday HolidayCompany
		decodeError := mapstructure.Decode(holiday, &tempHoliday)
		if decodeError != nil {
			return HolidayCompanyList{}, errors.New("Can't parse holiday")
		}

		parsedHolidayCompany = append(parsedHolidayCompany, tempHoliday)
	}

	totalAmount := res.Total
	holidayList := HolidayCompanyList{HolidayCompany: parsedHolidayCompany, Total: totalAmount}
	return holidayList, nil
}

// GetHolidayCompanyDetail -
func GetHolidayCompanyDetail(id string) (HolidayCompany, error) {
	var HolidaycompanyType HolidayCompany
	var result map[string]interface{}
	var ok bool
	inputQuery := fmt.Sprintf("%s%s", URIEnvMapping.TMInternalCompanyHoliday+"/", id)
	res, err := coreRest.CallRestAPIWithOption("GET", inputQuery, &HolidaycompanyType)

	if err != nil {
		return HolidayCompany{}, errors.New("Can't get company holiday")
	}
	if result, ok = res.Data.(map[string]interface{}); !ok {
		return HolidayCompany{}, errors.New("Can't get company holiday")
	}

	decodeError := mapstructure.Decode(result, &HolidaycompanyType)
	if decodeError != nil {
		return HolidayCompany{}, errors.New("Can't parse holiday")
	}

	return HolidaycompanyType, nil

}

//PostHolidayCompany -
func PostHolidayCompany(input map[string]interface{}) (HolidayCompany, error) {
	var parsedHolidays []model.HolidayDetail
	for _, holiday := range input["holidaydetails"].([]interface{}) {
		tempHoliday := holiday.(map[string]interface{})

		dateParsed, err := time.Parse(time.RFC3339, tempHoliday["date"].(string))

		if err != nil {
			return HolidayCompany{}, errors.New("Wrong time format")
		}

		parsedHolidays = append(parsedHolidays, model.HolidayDetail{
			Name:      tempHoliday["name"].(string),
			Date:      dateParsed,
			CreatedBy: input["created_by"].(int),
			UpdatedBy: input["updated_by"].(int),
		})
	}

	holidayCompanyType := model.HolidayCompany{
		CompanyID:          input["company_id"].(int),
		HolidayCompanyName: input["holidaycompany_name"].(string),
		HolidayAmount:      input["holiday_amount"].(int),
		HolidayDetails:     parsedHolidays,
		CreatedBy:          input["created_by"].(int),
		UpdatedBy:          input["updated_by"].(int),
	}
	res, err := coreRest.CallRestAPIWithOption("POST", URIEnvMapping.TMInternalCompanyHoliday, &holidayCompanyType)

	if err != nil {
		return HolidayCompany{}, errors.New("Can't create company holiday")
	}

	result := res.Data.(map[string]interface{})

	if err != nil {
		return HolidayCompany{}, errors.New("Can't parse time")
	}
	var resultHolidayCompany HolidayCompany
	decodeError := mapstructure.Decode(result, &resultHolidayCompany)
	if decodeError != nil {

		return HolidayCompany{}, errors.New("Can't parse holiday")
	}
	return resultHolidayCompany, nil

}

//PutHolidayCompany -
func PutHolidayCompany(input map[string]interface{}) (HolidayCompany, error) {

	type holidayCompanyUpdateType struct {
		ID                 int             `json:"id"`
		HolidayCompanyName string          `json:"holidaycompany_name" mapstructure:"holidaycompany_name"`
		HolidayAmount      int             `json:"holiday_amount" mapstructure:"holiday_amount"`
		UpdatedBy          int             `json:"updated_by" mapstructure:"updated_by"`
		HolidayDetails     []HolidayDetail `json:"holiday_details" mapstructure:"holiday_details"`
		Deleted            []int           `json:"delete_company_holiday_ids" mapstructure:"delete_company_holiday_ids"`
	}
	var updatedType holidayCompanyUpdateType

	decodeFinalErr := mapstructure.Decode(input, &updatedType)
	if decodeFinalErr != nil {
		return HolidayCompany{}, errors.New("Can't parse holiday")
	}

	holidayCompanyType := updatedType

	for i := 0; i < len(holidayCompanyType.HolidayDetails); i++ {
		if holidayCompanyType.HolidayDetails[i].ID == 0 {
			holidayCompanyType.HolidayDetails[i].CreatedBy = holidayCompanyType.UpdatedBy
		}
		holidayCompanyType.HolidayDetails[i].UpdatedBy = holidayCompanyType.UpdatedBy
	}

	res, err := coreRest.CallRestAPIWithOption("PUT", URIEnvMapping.TMInternalCompanyHoliday, &holidayCompanyType)

	if err != nil || res.Status.Code == "NOT_FOUND" {

		return HolidayCompany{}, errors.New("Can't not edit company holiday")
	}
	var result map[string]interface{}
	var ok bool
	if result, ok = res.Data.(map[string]interface{}); !ok {
		return HolidayCompany{}, errors.New("Can't edit company holiday")
	}

	var returnHolidayCompany HolidayCompany
	decodeErr := mapstructure.Decode(result, &returnHolidayCompany)
	if decodeErr != nil {
		return HolidayCompany{}, errors.New("Can't parse holiday")
	}
	return returnHolidayCompany, nil

}

//DeleteHolidayCompany -
func DeleteHolidayCompany(HolidayID int) (model.HolidayCompany, error) {
	inputQuery := fmt.Sprintf("%s%d", URIEnvMapping.TMInternalCompanyHoliday+"/", HolidayID)
	var holidayCompanyType []model.HolidayDetail

	res, err := coreRest.CallRestAPIWithOption("DELETE", inputQuery, &holidayCompanyType)

	if err != nil || res.Status.Code == "NOT_FOUND" {
		return model.HolidayCompany{}, errors.New("Can not delete holiday company")
	}

	return model.HolidayCompany{
		ID: HolidayID,
	}, nil

}
