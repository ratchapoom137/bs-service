package service

import (
	"api/model"
	"errors"
	"fmt"
	"net/url"
	coreRest "dv.co.th/hrms/core/rest"
	"github.com/mitchellh/mapstructure"
)

// GetLeaveTypeList -
func GetLeaveTypeList(limit int, offset int, name string, status string, year string, orderBy string, sort string, companyID int, chargeCodeID int) (model.LeaveTypeList, error) {
	pathURL, _ := url.Parse(URIEnvMapping.TMLInternalLeaveTypes)
	q := pathURL.Query()

	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if companyID > 0 {
		q.Set("company_id", fmt.Sprintf("%d", companyID))
	}
	if chargeCodeID > 0 {
		q.Set("charge_code_id", fmt.Sprintf("%d", chargeCodeID))
	}
	if name != "" {
		q.Set("name", name)
	}
	if status != "" {
		q.Set("status", status)
	}
	if year != "" {
		q.Set("year", year)
	}
	if orderBy != "" {
		q.Set("order_by", orderBy)
	}
	if sort != "" {
		q.Set("sort", sort)
	}
	pathURL.RawQuery = q.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), nil)

	if err != nil {
		return model.LeaveTypeList{}, errors.New("Can't get leave type list")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.LeaveTypeList{}, errors.New("leave type list not found")
	}

	var leaveType []model.LeaveType
	var total int

	if leaveErr := mapstructure.Decode(res.Data, &leaveType); leaveErr != nil {
		return model.LeaveTypeList{}, errors.New("Can't get leave type list")
	}

	if leaveTotalErr := mapstructure.Decode(res.Total, &total); leaveTotalErr != nil {
		return model.LeaveTypeList{}, errors.New("Can't get leave type list")
	}

	return model.LeaveTypeList{
		LeaveTypes: leaveType,
		Total:     total,
	}, nil
}

// GetLeaveType - GET Leave Type by id with Call LeaveType API
func GetLeaveType(id int) (model.LeaveType, error) {
	url := fmt.Sprintf(URIEnvMapping.TMLInternalLeaveType+"/%d", id)
	
	res, err := coreRest.CallRestAPIWithOption("GET", url, nil)
	if err != nil {
		return model.LeaveType{}, errors.New("Can't get leave type")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.LeaveType{}, errors.New("leave type not found")
	}
	var leaveType model.LeaveType
	leaveErr := mapstructure.Decode(res.Data, &leaveType)
	if leaveErr != nil {
		return model.LeaveType{}, errors.New("Can't mapstructure leave type")
	}
	return leaveType, nil
}

// CreateLeaveType -
func CreateLeaveType(leaveType model.LeaveType) (model.LeaveType, error) {
	url := fmt.Sprintf(URIEnvMapping.TMLInternalLeaveType)

	res, err := coreRest.CallRestAPIWithOption("POST", url, &leaveType)
	if err != nil {
		return model.LeaveType{}, errors.New("Can't create leave type")
	}

	result := res.Data.(map[string]interface{})

	leaveErr := mapstructure.Decode(result, &leaveType)
	if leaveErr != nil {
		return model.LeaveType{}, errors.New("Can't mapstructure leave type")
	}

	return leaveType, nil
}

// EditLeaveType - Edit Leave Type with Call LeaveType API
func EditLeaveType(id int, leaveType model.LeaveType) (model.LeaveType, error) {
	url := fmt.Sprintf(URIEnvMapping.TMLInternalLeaveType+"/%d", id)

	res, err := coreRest.CallRestAPIWithOption("PUT", url, &leaveType)
	if err != nil {
		return model.LeaveType{}, errors.New("Can't get leave type")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.LeaveType{}, errors.New("leave type not found")
	}

	result := res.Data.(map[string]interface{})

	leaveErr := mapstructure.Decode(result, &leaveType)
	if leaveErr != nil {
		return model.LeaveType{}, errors.New("Can't mapstructure leave type")
	}

	return leaveType, nil
}
