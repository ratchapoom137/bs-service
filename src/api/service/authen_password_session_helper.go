package service

import (
	"api/model"

	"github.com/sethvargo/go-password/password"
)

//Password Session Process for send email
func PasswordSessionHelper(userID int) (result int, err error) {

	filter := map[string]interface{}{}
	filter["user_id"] = userID
	//Get password session list by user id
	pwdSessionList, _, pwdSessionsErr := GetPasswordSessionList(filter)

	if pwdSessionsErr != nil {
		return result, pwdSessionsErr
	}

	if len(pwdSessionList) > 0 {
		for _, item := range pwdSessionList {
			_, deletePwdSessionErr := DeletePasswordSession(item.ID)
			if deletePwdSessionErr != nil {
				return result, deletePwdSessionErr
			}
		}
	}

	secret, err := password.Generate(32, 10, 0, false, false)
	passwordSession := model.PasswordSessionInput{
		Secret: secret,
		UserID: userID,
	}

	passwordSessiosID, passwordSessiosErr := CreatePasswordSession(passwordSession)
	if passwordSessiosErr != nil {
		return result, passwordSessiosErr
	}
	result = passwordSessiosID

	return
}
