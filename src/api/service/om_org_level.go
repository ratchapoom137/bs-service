package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"dv.co.th/hrms/core/rest"
)

// PutOrganizationLevel -
func PutOrganizationLevel(level model.OrgLevel) (int, error) {

	url := fmt.Sprintf(URIEnvMapping.OMInternalOrgLevel+"/%d", level.ID)
	res, err := rest.CallRestAPIWithOption("PUT", url, &level)

	if err != nil || res.Data == nil {
		return 0, errors.New("cannot edit organization Level")
	}

	result := int(res.Data.(float64))

	return result, nil

}

// PostLevel -
func PostLevel(level model.OrgLevel) (int, error) {
	url := URIEnvMapping.OMInternalOrgLevel
	res, err := rest.CallRestAPIWithOption("POST", url, &level)

	if err != nil {
		return 0, errors.New("cannot create org level")
	}

	result := int(res.Data.(float64))

	return result, nil
}

// GetLevelList -
func GetLevelList(compID int, name string, limit int, offset int) (result []model.OrgLevel, total int, err error) {
	var level []model.OrgLevel

	pathURL, _ := url.Parse(URIEnvMapping.OMInternalOrgLevels)
	q := pathURL.Query()

	if compID >= 0 {
		q.Set("company_id", fmt.Sprintf("%d", compID))
	}
	if offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if name != "" {
		q.Set("name", name)
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), &level)

	if err != nil {
		err = errors.New("cannot get level")
		return
	}

	data, ok := res.Data.([]interface{})
	if !ok {
		return []model.OrgLevel{}, 0, errors.New("no such file: get level list")
	}

	for _, level := range data {
		var jsonLevel model.OrgLevel

		jsonData, _ := json.Marshal(level)
		json.Unmarshal(jsonData, &jsonLevel)

		result = append(result, jsonLevel)
	}
	total = res.Total

	return
}

// GetLevel -
func GetLevel(id int) (model.OrgLevel, error) {
	var level model.OrgLevel
	url := fmt.Sprintf(URIEnvMapping.OMInternalOrgLevel+"/%d", id)
	res, err := rest.CallRestAPIWithOption("GET", url, &level)
	if err != nil {
		return model.OrgLevel{}, errors.New("cannot get org level")
	}
	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.OrgLevel{}, errors.New("no such file: get org level")
	}

	jsonOrglevel, _ := json.Marshal(result)
	json.Unmarshal(jsonOrglevel, &level)

	return level, nil
}

// DeleteOrganizationLevel -
func DeleteOrganizationLevel(id int) (int, error) {
	var level model.OrgLevel

	url := fmt.Sprintf(URIEnvMapping.OMInternalOrgLevel+"/%d", id)
	res, err := rest.CallRestAPIWithOption("DELETE", url, &level)

	if err != nil {
		return 0, errors.New("cannot delete org Level")
	}

	result := int(res.Data.(float64))

	return result, nil
}
