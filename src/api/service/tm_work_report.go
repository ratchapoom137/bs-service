package service

import (

	// "dv.co.th/hrms/core"
	"api/model"
	"errors"
	"fmt"
	"net/url"
	"sort"
	"time"

	coreRest "dv.co.th/hrms/core/rest"
	_ "github.com/davecgh/go-spew/spew"
	"github.com/mitchellh/mapstructure"
	"github.com/thoas/go-funk"
	_ "github.com/thoas/go-funk"
	// "strconv"
)

type HourWorkReport struct {
	Code        string `json:"code`
	Description string `json:"description`
	Hours       []int  `json:"hours`
}
type WorkReportQuery struct {
	ProjectIDs  []int     `json:"project_ids"`
	StartPeriod time.Time `json:"start_period"`
	EndPeriod   time.Time `json:"end_period"`
}
type WorkAdjustmentsInput struct {
	EmployeeID         int                   `json:"employee_id" mapstructure:"employee_id"`
	ProjectID          int                   `json:"project_id" mapstructure:"project_id"`
	PeriodID           int                   `json:"period_id" mapstructure:"period_id"`
	WorkAdjustments    []WorkAdjustmentInput `json:"work_adjustments" mapstructure:"work_adjustments"`
	AdjustmentPeriodID int                   `json:"adjust_period" mapstructure:"adjust_period"`
	Description        string                `json:"description" mapstructure:"description"`
}
type WorkAdjustmentInput struct {
	ID                int    `json:"id"`
	AdjustPeriodID    int    `json:"adjust_period"`
	EmployeeID        int    `json:"employee_id" mapstructure:"employee_id"`
	ProjectID         int    `json:"project_id" mapstructure:"project_id"`
	Date              string `json:"date"`
	OriginalWorkHours int    `json:"original_work_hours" mapstructure:"original_work_hours"`
	WorkHours         int    `json:"work_hours" mapstructure:"work_hours"`
	SubmitPeriodID    int    `json:"submit_period_id"`
}
type WorkReportQueryType struct {
	Status     int
	ReviewerID int
	EmployeeID int
	PeriodID   int
	Page       int
	Perpage    int
	CompanyID  int
}
type WorkReportProgressList struct {
	WorkReports []model.WorkProgressProjectOutput `json:"work_reports"`
	Total       int                               `json:"total"`
}
type WorkReportAdjustmentList struct {
	WorkAdjustments []model.WorkAdjustmentsProjectOutput `json:"work_adjustments" `
	Total           int                                  `json:"total"`
}

//WorkReportStatusOutput
type WorkReportStatusOutput struct {
	EmployeeID int    `json:"employee_id"  mapstructure:"employee_id"`
	PeriodID   int    `json:"period_id" mapstructure:"period_id"`
	Type       int    `json:"type"`
	Status     int    `json:"status"`
	SubmitDate string `json:"submit_date" mapstructure:"submit_date"`
	WorkHour   int    `json:"work_hour" mapstructure:"work_hour"`
	LeaveHour  int    `json:"leave_hour" mapstructure:"leave_hour"`
	TotalHour  int    `json:"total_hour" mapstructure:"total_hour"`
}

//WorkLogInput --
type WorkLogInput struct {
	ID                int                       `json:"id"`
	EmployeeID        int                       `json:"employee_id"  mapstructure:"employee_id"`
	ProjectID         int                       `json:"project_id"  mapstructure:"project_id"`
	PeriodID          int                       `json:"period_id"  mapstructure:"period_id"`
	WorkProgresses    []model.WorkProgressInput `json:"work_progresses"  mapstructure:"work_progresses"`
	Reviewers         []model.Reviewer          `json:"reviewers" mapstructure:"reviewers"`
	Files             []model.File              `json:"files" mapstructure:"files"`
	Description       string                    `json:"description"`
	DeleteReviewerIDs []int                     `json:"delete_reviewer_ids" mapstructure:"delete_reviewer_ids"`
	DeleteFileIDs     []int                     `json:"delete_file_ids" mapstructure:"delete_file_ids"`
}

type WorkSubmissionList struct {
	WorkReportStatusList []WorkReportStatusOutput `json:"work_submissions" mapstructure:"work_submissions"`
	Total                int                      `json:"total"`
}

type PeriodOutput struct {
	ID        int    `json:"id"`
	StartDate string `json:"start_date" mapstructure:"start_date"`
	EndDate   string `json:"end_date" mapstructure:"end_date"`
	TotalDate int    `json:"total_date" mapstructure:"total_date"`
}

//PostWorkAdjustment -
func PutWorkAdjustment(input map[string]interface{}) (bool, error) {
	var workAdjustmentType WorkAdjustmentsInput
	decodeError := mapstructure.Decode(input, &workAdjustmentType)
	if decodeError != nil {

		return false, errors.New("Cannot parse work report adjustment.")
	}
	res, err := coreRest.CallRestAPIWithOption("PUT",
		URIEnvMapping.TMInternalWorkAdjustment, &workAdjustmentType)
	if err != nil {
		return false, errors.New("Can't edit work adjustment.")
	}

	if res.Data == nil {
		return false, errors.New("Cannot parse put work adjustment.")
	}

	if !CallUpdateWork(workAdjustmentType.AdjustmentPeriodID, workAdjustmentType.EmployeeID) {
		return false, errors.New("Cannot update work report draft hour.")
	}
	return true, nil
}

//PostWorkAdjustment -
func PostWorkAdjustment(input map[string]interface{}) (bool, error) {
	var workAdjustmentType WorkAdjustmentsInput
	decodeError := mapstructure.Decode(input, &workAdjustmentType)
	if decodeError != nil {

		return false, errors.New("Cannot parse work report adjustment.")
	}
	res, err := coreRest.CallRestAPIWithOption("POST",
		URIEnvMapping.TMInternalWorkAdjustment, &workAdjustmentType)
	if err != nil {
		return false, errors.New("Can't create work adjustment.")
	}

	var output WorkAdjustmentsInput
	outputErr := mapstructure.Decode(res.Data.(map[string]interface{}), &output)
	if outputErr != nil {

		return false, errors.New("Cannot parse output work adjustment.")
	}

	if !CallUpdateWork(workAdjustmentType.AdjustmentPeriodID, workAdjustmentType.EmployeeID) {
		return false, errors.New("Cannot update work report draft hour.")
	}
	return true, nil
}

//PostWorkSubmitStatus -
func PostWorkSubmitStatus(input map[string]interface{}) (bool, error) {
	type reportSubmitInput struct {
		ReportIDs  []int `json:"report_ids" mapstructure:"report_ids"`
		EmployeeID int   `json:"employee_id" mapstructure:"employee_id"`
		PeriodID   int   `json:"period_id" mapstructure:"period_id"`
	}
	var reportInput reportSubmitInput
	decodeError := mapstructure.Decode(input, &reportInput)
	if decodeError != nil {
		return false, errors.New("Cannot post submit status.")
	}
	res, err := coreRest.CallRestAPIWithOption("POST",
		URIEnvMapping.TMInternalWorkStatusSubmit, &reportInput)
	if err != nil {
		return false, errors.New("Can't post submit status.")
	}
	if reportInput.EmployeeID > 0 {
		path := fmt.Sprintf("%s%d", URIEnvMapping.TMInternalWorkLogUpdate+"/", reportInput.PeriodID)
		pathURL, _ := url.Parse(path)
		pathQuery := pathURL.Query()
		pathQuery.Set("employee_id", fmt.Sprintf("%d", reportInput.EmployeeID))

		pathURL.RawQuery = pathQuery.Encode()

		_, updateErr := coreRest.CallRestAPIWithOption("POST",
			pathURL.String(), &reportInput)
		if updateErr != nil {
			return false, errors.New("Can't post submit status.")
		}
	}

	result := res.Data.(bool)

	return result, nil
}

//PutWorkReport -
func PutWorkReport(input map[string]interface{}) (bool, error) {

	var workReportType WorkLogInput
	decodeError := mapstructure.Decode(input, &workReportType)
	if decodeError != nil {

		return false, errors.New("Cannot parse work report input.")
	}

	res, err := coreRest.CallRestAPIWithOption("PUT",
		URIEnvMapping.TMInternalWorkProgress, &workReportType)
	if err != nil {

		return false, errors.New("Can't edit work report.")
	}
	//TODO remove result if error
	var output WorkAdjustmentsInput
	outputErr := mapstructure.Decode(res.Data.(map[string]interface{}), &output)
	if outputErr != nil {
		return false, errors.New("Cannot parse output work report.")
	}
	if !CallUpdateWork(workReportType.PeriodID, workReportType.EmployeeID) {
		return false, errors.New("Cannot update work report draft hour.")
	}
	//end of output return
	return true, nil
}

//PostWorkPreset -
func PostWorkReport(input map[string]interface{}) (bool, error) {
	var workReportType WorkLogInput
	decodeError := mapstructure.Decode(input, &workReportType)
	if decodeError != nil {

		return false, errors.New("Cannot parse work report.")
	}

	res, err := coreRest.CallRestAPIWithOption("POST",
		URIEnvMapping.TMInternalWorkProgress, &workReportType)
	if err != nil {
		return false, errors.New("Can't create work report.")
	}

	var output WorkAdjustmentsInput
	outputErr := mapstructure.Decode(res.Data.(map[string]interface{}), &output)
	if outputErr != nil {
		return false, errors.New("Cannot parse output work report.")
	}
	if !CallUpdateWork(workReportType.PeriodID, workReportType.EmployeeID) {
		return false, errors.New("Cannot update work report draft hour.")
	}
	return true, nil
}

//GetWorkReportByEmployeeIDPeriodID -
func GetWorkReportByEmployeeIDPeriodID(workReportQueryObject WorkReportQueryType) (WorkReportProgressList, error) {
	var workReportsType []model.WorkProgressProjectOutput

	url := fmt.Sprintf("%s%d%s%d", URIEnvMapping.TMInternal+"/employee/", workReportQueryObject.EmployeeID, "/work-progresses/", workReportQueryObject.PeriodID)

	res, err := coreRest.CallRestAPIWithOption("GET",
		url, &model.WorkLog{})
	if err != nil {

		return WorkReportProgressList{}, errors.New("Can't not get work report.")
	}

	var result []interface{}
	var ok bool

	if result, ok = res.Data.([]interface{}); !ok {
		return WorkReportProgressList{}, errors.New("Cannot get work report.")
	}

	decodeError := mapstructure.Decode(result, &workReportsType)
	if decodeError != nil {
		return WorkReportProgressList{}, errors.New("Cannot parse work report.")
	}

	workpresetList := WorkReportProgressList{WorkReports: workReportsType, Total: res.Total}
	return workpresetList, nil
}

//GetWorkAdjustmentByEmployeeIDPeriodID -
func GetWorkAdjustmentByEmployeeIDPeriodID(workReportQueryObject WorkReportQueryType) (WorkReportAdjustmentList, error) {
	var workAdjustmentsType []model.WorkAdjustmentsProjectOutput

	url := fmt.Sprintf("%s%d%s%d", URIEnvMapping.TMInternal+"/employee/", workReportQueryObject.EmployeeID, "/work-progress-adjustments/", workReportQueryObject.PeriodID)

	res, err := coreRest.CallRestAPIWithOption("GET",
		url, &workAdjustmentsType)
	if err != nil {

		return WorkReportAdjustmentList{}, errors.New("Can't not get work adjustment.")
	}

	var result []interface{}
	var ok bool

	if result, ok = res.Data.([]interface{}); !ok {
		return WorkReportAdjustmentList{}, errors.New("Cannot get work adjustment.")
	}

	decodeError := mapstructure.Decode(result, &workAdjustmentsType)
	if decodeError != nil {

		return WorkReportAdjustmentList{}, errors.New("Cannot parse work adjustment.")
	}

	workAdjustmentList := WorkReportAdjustmentList{WorkAdjustments: workAdjustmentsType, Total: res.Total}
	return workAdjustmentList, nil
}

//GetEmployeeWorkSubmissions -
func GetEmployeeWorkSubmissions(workReportQueryObject WorkReportQueryType) (WorkSubmissionList, error) {
	var workSubmissionType []WorkReportStatusOutput
	totalSubmissionAmount := 0
	var parsedEmpList []int
	//Get emp ids from PA for HR viewer
	if workReportQueryObject.EmployeeID < 1 && workReportQueryObject.ReviewerID < 1 {
		filter := make(map[string]interface{})
		if workReportQueryObject.CompanyID > 0 {
			filter["company_id"] = workReportQueryObject.CompanyID
			filter["limit"] = 1000
			filter["offset"] = 0
		}
		empList, _, _ := GetEmployeeByFilter(filter)
		if len(empList) > 0 {
			for i := 0; i < len(empList); i++ {
				parsedEmpList = append(parsedEmpList, empList[i].ID)
			}
		}
	}
	//Get employee ids for reviewer
	if workReportQueryObject.ReviewerID > 0 {
		url := fmt.Sprintf(URIEnvMapping.TMInternalEmpIDFromReviewerID+"?&reviewer_id=%d&period_id=%d", workReportQueryObject.ReviewerID, workReportQueryObject.PeriodID)
		res, err := coreRest.CallRestAPIWithOption("GET",
			url, &model.WorkPreset{})
		if err != nil {
			return WorkSubmissionList{}, errors.New("Can't not get work submissions.")
		}
		tempList := res.Data.([]interface{})
		for i := range tempList {
			parsedEmpList = append(parsedEmpList, int(tempList[i].(float64)))
		}
	}

	//Get submission from each employee ID with optional filtering
	if workReportQueryObject.EmployeeID == 0 {
		totalSubmissionAmount = len(funk.Uniq(parsedEmpList).([]int))
		SubmissionList := funk.Map(funk.Uniq(parsedEmpList), func(employeeID int) WorkReportStatusOutput {
			toQuery := WorkReportQueryType{
				Status:     0,
				EmployeeID: employeeID,
				PeriodID:   workReportQueryObject.PeriodID,
			}
			firstTry, _ := GetWorkSubmissionFromEmpID(toQuery)

			//If submission is not found, return empty submission
			if len(firstTry) < 1 {
				return WorkReportStatusOutput{
					EmployeeID: employeeID,
					PeriodID:   workReportQueryObject.PeriodID,
					Type:       1,
				}
			}
			return firstTry[0]
		}).([]WorkReportStatusOutput)
		//Sort list by status
		sort.Slice(SubmissionList, func(i, j int) bool { return SubmissionList[i].Status > SubmissionList[j].Status })
		//Filter
		SUBMITTED := []int{2, 5}
		if workReportQueryObject.Status > 0 {
			if workReportQueryObject.Status == 1 {
				SubmissionList = funk.Filter(SubmissionList, func(submission WorkReportStatusOutput) bool {
					return funk.Contains(SUBMITTED, submission.Status)
				}).([]WorkReportStatusOutput)
			}
			if workReportQueryObject.Status == 2 {
				SubmissionList = funk.Filter(SubmissionList, func(submission WorkReportStatusOutput) bool {
					return !funk.Contains(SUBMITTED, submission.Status)
				}).([]WorkReportStatusOutput)
			}
			totalSubmissionAmount = len(SubmissionList)
		}

		//Splice according to pagination
		for i := workReportQueryObject.Page; i < workReportQueryObject.Page+workReportQueryObject.Perpage; i++ {
			if i >= len(SubmissionList) {
				break
			}
			tempWorkSubmission := SubmissionList[i]

			if tempWorkSubmission.EmployeeID != 0 {
				workSubmissionType = append(workSubmissionType, tempWorkSubmission)
			}
		}
	}
	workSubmissionList := WorkSubmissionList{WorkReportStatusList: workSubmissionType, Total: totalSubmissionAmount}
	return workSubmissionList, nil
}

//GetMyWorkSubmissions -
func GetMyWorkSubmissions(workReportQueryObject WorkReportQueryType) (WorkSubmissionList, error) {
	var workSubmissionType []WorkReportStatusOutput

	//Logic for querying just for 1 employee

	url := fmt.Sprintf(URIEnvMapping.TMInternalWorkSubmissionSummary + "?")
	if workReportQueryObject.EmployeeID != 0 {
		url += fmt.Sprintf("&employee_id=%d", workReportQueryObject.EmployeeID)
	}

	if workReportQueryObject.Status > 0 {
		url += fmt.Sprintf("&status=%d", workReportQueryObject.Status)
	}
	url += fmt.Sprintf("&offset=%d", workReportQueryObject.Page)
	url += fmt.Sprintf("&limit=%d", workReportQueryObject.Perpage)
	res, err := coreRest.CallRestAPIWithOption("GET",
		url, &model.WorkPreset{})
	if err != nil {

		return WorkSubmissionList{}, errors.New("Can't not get work submissions.")
	}
	var result []interface{}
	var ok bool

	if result, ok = res.Data.([]interface{}); !ok {
		return WorkSubmissionList{}, errors.New("Cannot get work submissions.")
	}

	decodeError := mapstructure.Decode(result, &workSubmissionType)
	if decodeError != nil {
		return WorkSubmissionList{}, errors.New("Cannot parse work submissions.")
	}

	workSubmissionList := WorkSubmissionList{WorkReportStatusList: workSubmissionType, Total: res.Total}
	return workSubmissionList, nil
}

func GetWorkSubmissionFromEmpID(workReportQueryObject WorkReportQueryType) ([]WorkReportStatusOutput, error) {
	var workSubmissionType []WorkReportStatusOutput
	url := fmt.Sprintf(URIEnvMapping.TMInternalWorkSubmissionSummary + "?")

	url += fmt.Sprintf("&employee_id=%d", workReportQueryObject.EmployeeID)

	if workReportQueryObject.PeriodID > 0 {
		url += fmt.Sprintf("&period_id=%d", workReportQueryObject.PeriodID)
	}

	if workReportQueryObject.Status > 0 {
		url += fmt.Sprintf("&status=%d", workReportQueryObject.Status)
	}
	res, err := coreRest.CallRestAPIWithOption("GET",
		url, &model.WorkPreset{})
	if err != nil {

		return []WorkReportStatusOutput{}, errors.New("Can't not get work submissions.")
	}

	result, ok := res.Data.([]interface{})
	if !ok {
		return []WorkReportStatusOutput{}, errors.New("Cannot get work submissions.")
	}

	decodeError := mapstructure.Decode(result, &workSubmissionType)
	if decodeError != nil {
		return []WorkReportStatusOutput{}, errors.New("Cannot parse work submissions.")
	}
	return workSubmissionType, nil

}

//GetAvailablePeriod -
func GetAvailablePeriod() ([]PeriodOutput, error) {
	var periodsType []PeriodOutput

	url := fmt.Sprintf(URIEnvMapping.TMInternalPeriods)
	res, err := coreRest.CallRestAPIWithOption("GET",
		url, &[]PeriodOutput{})
	if err != nil {

		return []PeriodOutput{}, errors.New("Can't not get work submissions.")
	}

	var result []interface{}
	var ok bool

	if result, ok = res.Data.([]interface{}); !ok {
		return []PeriodOutput{}, errors.New("Cannot get work submissions.")
	}
	decodeError := mapstructure.Decode(result, &periodsType)
	if decodeError != nil {

		return []PeriodOutput{}, errors.New("Cannot parse work submissions.")
	}
	return periodsType, nil
}

//GetPeriodDetail -
func GetPeriodDetail(periodID int) (PeriodOutput, error) {
	var periodType PeriodOutput

	url := fmt.Sprintf(URIEnvMapping.TMInternalPeriod+"/%d", periodID)

	res, err := coreRest.CallRestAPIWithOption("GET",
		url, &PeriodOutput{})
	if err != nil {

		return PeriodOutput{}, errors.New("Can't not get period details.")
	}

	var result map[string]interface{}
	var ok bool

	if result, ok = res.Data.(map[string]interface{}); !ok {

		return PeriodOutput{}, errors.New("Cannot get period details.")
	}

	decodeError := mapstructure.Decode(result, &periodType)
	if decodeError != nil {

		return PeriodOutput{}, errors.New("Cannot parse period details.")
	}
	return periodType, nil
}

//GetWorkReport -
func GetWorkReport(workReportQueryObject WorkReportQuery) ([]HourWorkReport, error) {
	var hourWorkReportType []HourWorkReport
	url := fmt.Sprintf(URIEnvMapping.TMInternalWorkReports)
	res, err := coreRest.CallRestAPIWithOption("GET",
		url, &workReportQueryObject)
	if err != nil {
		return []HourWorkReport{}, errors.New("Can't not get monthly report.")
	}
	var result []interface{}
	var ok bool

	if result, ok = res.Data.([]interface{}); !ok {
		return []HourWorkReport{}, errors.New("Cannot get work submissions.")
	}

	decodeError := mapstructure.Decode(result, &hourWorkReportType)
	if decodeError != nil {

		return []HourWorkReport{}, errors.New("Cannot parse work submissions.")
	}

	return hourWorkReportType, nil
}

func CallUpdateWork(periodID int, employeeId int) bool {

	path := fmt.Sprintf("%s%d", URIEnvMapping.TMInternalWorkLogUpdate+"/", periodID)
	pathURL, _ := url.Parse(path)
	pathQuery := pathURL.Query()
	pathQuery.Set("employee_id", fmt.Sprintf("%d", employeeId))
	pathQuery.Set("is_draft", "true")
	pathURL.RawQuery = pathQuery.Encode()

	_, updateErr := coreRest.CallRestAPIWithOption("POST",
		pathURL.String(), &model.WorkLog{})
	if updateErr != nil {
		return false
	}
	return true
}
