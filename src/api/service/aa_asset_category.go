package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"

	"net/url"

	"dv.co.th/hrms/core/rest"
)

// PostCategory -
func PostCategory(category *model.AssetCategory) (catID int, err error) {
	url := URIEnvMapping.AAInternalCategory

	res, err := rest.CallRestAPIWithOption("POST", url, &category)

	if err != nil {
		return 0, errors.New("cannot create asset")
	}
	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return 0, errors.New("Invalid param")
	}
	catID = int(result["id"].(float64))

	return catID, nil
}

// GetCategoryList -
func GetCategoryList(name string, limit int, offset int, companyID int) (result []model.AssetCategory, total int, err error) {
	var category []model.AssetCategory

	pathURL, _ := url.Parse(URIEnvMapping.AAInternalCategories)
	q := pathURL.Query()

	if name != "" {
		q.Set("name", name)
	}
	if offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if companyID > 0 {
		q.Set("company_id", fmt.Sprintf("%d", companyID))
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), &category)

	if err != nil {
		err = errors.New("Cannot get categories")
		return
	}

	data, ok := res.Data.([]interface{})

	if !ok {
		return []model.AssetCategory{}, 0, errors.New("Not Found")
	}

	for _, category := range data {
		var jsonCategory model.AssetCategory

		jsonData, _ := json.Marshal(category)
		json.Unmarshal(jsonData, &jsonCategory)

		result = append(result, jsonCategory)
	}
	total = res.Total

	return
}

// GetCategory -
func GetCategory(id int) (model.AssetCategory, error) {
	var category model.AssetCategory
	url := fmt.Sprintf(URIEnvMapping.AAInternalCategory+"/%d", id)
	res, err := rest.CallRestAPIWithOption("GET", url, &category)
	if err != nil {
		return model.AssetCategory{}, errors.New("cannot get category")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.AssetCategory{}, errors.New("not found: get asset categoriy")
	}

	return model.AssetCategory{
		ID:        int(result["id"].(float64)),
		Name:      result["name"].(string),
		CreatedBy: int(result["created_by"].(float64)),
		UpdatedBy: int(result["updated_by"].(float64)),
	}, nil
}

// PutAssetCategory -
func PutAssetCategory(category *model.AssetCategory) (catID int, err error) {

	url := fmt.Sprintf(URIEnvMapping.AAInternalCategory+"/%d", category.ID)
	res, err := rest.CallRestAPIWithOption("PUT", url, &category)

	if err != nil {
		return 0, errors.New("cannot edit asset category")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return 0, errors.New("Invalid param")
	}
	catID = int(result["id"].(float64))
	return catID, nil
}

// DeleteAssetCategory -
func DeleteAssetCategory(id int) (message string, err error) {
	var assetCategory model.AssetCategory
	url := fmt.Sprintf(URIEnvMapping.AAInternalCategory+"/%d", id)
	res, err := rest.CallRestAPIWithOption("DELETE", url, &assetCategory)

	if err != nil {
		return "", errors.New("Cannot delete asset category")
	}

	result := res.Status.Message
	return result, nil
}
