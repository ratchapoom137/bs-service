package service

import (
	"api/base"
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"dv.co.th/hrms/core/rest"
	"github.com/sethvargo/go-password/password"
	"strings"
	"unicode"
)

type Role struct {
	ID int `json:"id"`
}

type RoleInfo struct {
	ID   int    `json:"id"`
	name string `json:"name"`
}

type UserInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
	RoleID   int    `json:"role_id"`
}

type UserAndUserDetail struct {
	User       model.User       `json:"user"`
	UserDetail model.UserDetail `json:"user_detail"`
}

type UserList struct {
	User  []UserAndUserDetail `json:"user_list"`
	Total int                 `json:"total"`
}

type ValidateUser struct {
	Username bool `json:"username"`
}

type FilterList struct {
	CompanyIDList []int `json:"company_id_list"`
	UserIDList    []int `json:"user_id_list"`
}

type errorString struct {
	s string
}

func (e *errorString) Error() string {
	return e.s
}

func VerifyPassword(password string) error {
	var uppercasePresent bool
	var lowercasePresent bool
	var numberPresent bool
	var specialCharPresent bool
	const minPassLength = 8
	var passLen int
	var errorString string

	for _, ch := range password {
		switch {
		case unicode.IsNumber(ch):
			numberPresent = true
			passLen++
		case unicode.IsUpper(ch):
			uppercasePresent = true
			passLen++
		case unicode.IsLower(ch):
			lowercasePresent = true
			passLen++
		case unicode.IsPunct(ch) || unicode.IsSymbol(ch):
			specialCharPresent = true
			passLen++
		case ch == ' ':
			passLen++
		}
	}
	appendError := func(err string) {
		if len(strings.TrimSpace(errorString)) != 0 {
			errorString += ", " + err
		} else {
			errorString = err
		}
	}
	if !lowercasePresent {
		appendError("lowercase letter missing")
	}
	if !uppercasePresent {
		appendError("uppercase letter missing")
	}
	if !numberPresent {
		appendError("atleast one numeric character required")
	}
	if !specialCharPresent {
		appendError("special character missing")
	}
	if !(minPassLength <= passLen) {
		appendError(fmt.Sprintf("password at least %d characters", minPassLength))
	}

	if len(errorString) != 0 {
		return fmt.Errorf(errorString)
	}
	return nil
}

func GetUserAuthFromLogin(username string, password string) (UserAuth model.UserAuth, err error) {
	formData := url.Values{
		"username": {username},
		"password": {password},
	}

	header := map[string]string{"Content-Type": "application/x-www-form-urlencoded"}
	result, _ := rest.CallPostAPIWithOption("POST", URIEnvMapping.AuthLoginInternal, formData,
		rest.WithHeader(header),
	)

	jsonData, err := json.Marshal(result.Data)
	if err != nil {
		return UserAuth, err
	}

	json.Unmarshal([]byte(jsonData), &UserAuth)

	return
}

//Create user
func CreateUser(user UserInput) (userID int, err error) {
	secret, err := password.Generate(16, 10, 0, false, false)
	user.Password = secret

	//[POST] API Create user
	createUserUrl := fmt.Sprintf(URIEnvMapping.USERInternal)
	userResult, _ := rest.CallRestAPIWithOption("POST", createUserUrl, &user)
	if userResult.Data == nil {
		return userID, errors.New(userResult.Status.Message)
	}
	userData := userResult.Data.(map[string]interface{})
	userID = ConvertFloatToInt(userData["id"].(float64))

	return
}

//DeleteUser -
func DeleteUser(userID int) (userIDResult int, err error) {
	url := fmt.Sprintf(URIEnvMapping.USERInternal+"/%d", userID)
	res, err := rest.CallRestAPIWithOption("DELETE", url, &userID)

	if err != nil {
		return 0, errors.New("Cannot delete user ")
	}

	userIDResult = int(res.Data.(float64))
	return userIDResult, err
}

func GetUserFromID(userID int) (User model.User, err error) {
	URL := fmt.Sprintf("%s/%d",
		URIEnvMapping.AuthGetUserInternal,
		userID,
	)

	app := base.GetApplication()
	app.Logger.Info(URL)
	result, err := rest.CallRestAPIWithOption("GET", URL, nil)
	if err != nil {
		return User, err
	}

	jsonData, err := json.Marshal(result.Data)
	if err != nil {
		return User, err
	}

	json.Unmarshal([]byte(jsonData), &User)

	return
}

func GetUserList(filter map[string]interface{}) (result []model.User, total int, err error) {
	pathURL, _ := url.Parse(URIEnvMapping.AuthGetUsersInternal)
	q := pathURL.Query()

	if offset, ok := filter["offset"].(int); ok && offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit, ok := filter["limit"].(int); ok && limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if username, ok := filter["username"].(string); ok && username != "" {
		q.Set("username", username)
	}
	if username, ok := filter["search_username"].(string); ok && username != "" {
		q.Set("search_username", username)
	}
	if role_name, ok := filter["role_name"].(string); ok && role_name != "" {
		q.Set("role_name", role_name)
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), nil)
	if err != nil {
		return
	}
	total = res.Total
	if total >= 1 {
		data := res.Data.([]interface{})
		for _, item := range data {
			var User model.User
			jsonUserData, _ := json.Marshal(item)
			json.Unmarshal(jsonUserData, &User)
			result = append(result, User)
		}
	}

	return
}

func GetRoles() (roles []model.Role, err error) {
	URL := fmt.Sprintf("%s", URIEnvMapping.AuthGetRolesInternal)

	app := base.GetApplication()
	app.Logger.Info(URL)
	result, err := rest.CallRestAPIWithOption("GET", URL, nil)

	if err != nil {
		return roles, err
	}

	jsonData, err := json.Marshal(result.Data)

	if err != nil {
		return roles, err
	}

	json.Unmarshal([]byte(jsonData), &roles)

	return
}

func PutUser(user model.UserInput) (userID int, err error) {
	url := fmt.Sprintf(URIEnvMapping.AuthPutUserInternal+"/%d", user.ID)
	res, err := rest.CallRestAPIWithOption("PUT", url, &user)

	if err != nil {
		return 0, errors.New("Cannot edit user ")
	}

	result := int(res.Data.(float64))
	return result, err
}

// ValidatePassword
func ValidatePassword(userInput model.UserPassword) bool {

	pathURL, _ := url.Parse(URIEnvMapping.AuthLoginInternal)
	q := pathURL.Query()

	if userInput.Username != "" {
		q.Set("username", userInput.Username)
	}
	if userInput.Password != "" {
		q.Set("password", userInput.Password)
	}

	pathURL.RawQuery = q.Encode()

	userResult, _ := rest.CallRestAPIWithOption("POST", pathURL.String(), &userInput)

	_, ok := userResult.Data.(map[string]interface{})

	if !ok {
		return false
	}
	return true
}
