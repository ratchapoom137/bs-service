package service

import (
	"api/helpers"
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"time"

	"dv.co.th/hrms/core/rest"
)

func PostTerminationRequest(terminateReq model.TerminationRequest) (terminateReqID int, err error) {

	terminateReq.RequestedDate = helpers.StartOfDateNow()

	infiniteDate, _ := time.Parse(time.RFC3339, "9999-12-31T00:00:00+00:00")
	terminateReq.ApprovedDate = infiniteDate
	terminateReq.CancelDate = infiniteDate

	url := URIEnvMapping.OMInternalTerminationRequest
	res, err := rest.CallRestAPIWithOption("POST", url, &terminateReq)

	if err != nil {
		return 0, errors.New("Cannot create termination request")
	}

	result := int(res.Data.(float64))
	return result, err
}

func GetTerminationRequestByID(terminateReqID int) (terminateReq model.TerminationRequest, err error) {
	url := fmt.Sprintf(URIEnvMapping.OMInternalTerminationRequest+"/%d", terminateReqID)
	res, err := rest.CallRestAPIWithOption("GET", url, &terminateReq)

	if err != nil {
		return terminateReq, errors.New("Cannot get termination request")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return terminateReq, errors.New("Cannot find termination request")
	}
	var terminateReqParsed model.TerminationRequest

	terminateReqJson, _ := json.Marshal(result)
	json.Unmarshal(terminateReqJson, &terminateReqParsed)

	return terminateReqParsed, nil
}

func GetTerminationRequestList(filter map[string]interface{}) (trlist []model.TerminationRequest, total int, err error) {
	pathURL, _ := url.Parse(URIEnvMapping.OMInternalTerminationRequests)
	q := pathURL.Query()

	if offset, ok := filter["offset"].(int); ok && offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit, ok := filter["limit"].(int); ok && limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if status, ok := filter["status"].(string); ok && status != "" {
		q.Set("status", status)
	}
	if requestDate, ok := filter["request_date"].(string); ok && requestDate != "" {
		q.Set("request_date", requestDate)
	}
	if companyName, ok := filter["company_name"].(string); ok && companyName != "" {
		q.Set("company_name", companyName)
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), []model.TerminationRequest{})

	if err != nil {
		err = errors.New("cannot get termination request list")
		return
	}
	data, ok := res.Data.([]interface{})
	if !ok {
		err = errors.New("no such file: get termination request list")
		return
	}

	for _, request := range data {
		var tmpRequest model.TerminationRequest

		jsonData, _ := json.Marshal(request)
		json.Unmarshal(jsonData, &tmpRequest)

		trlist = append(trlist, tmpRequest)
	}
	total = res.Total

	return
}

func PutTerminationRequest(terminateReq model.TerminationRequestInput) (terminateReqID int, err error) {
	url := fmt.Sprintf(URIEnvMapping.OMInternalTerminationRequest+"/%d", terminateReq.ID)
	res, err := rest.CallRestAPIWithOption("PUT", url, &terminateReq)

	if err != nil {
		return 0, errors.New("Cannot edit user detail")
	}

	result := int(res.Data.(float64))
	return result, err
}

//CheckTerminationRequestExist -
func CheckTerminationRequestExist(compID int) (bool, error) {
	pathURL, _ := url.Parse(URIEnvMapping.OMInternalTerminationRequests)
	q := pathURL.Query()

	if compID > 0 {
		q.Set("company_id", fmt.Sprintf("%d", compID))
	}
	q.Set("status", "PENDING")
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), []model.TerminationRequest{})
	if err != nil || res.Data == nil {
		return true, errors.New(res.Status.Message)
	}
	if total := int(res.Total); total > 0 {
		return true, nil
	}

	return false, nil
}
