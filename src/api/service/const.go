package service

import (
	"api/base"
	"os"

	_ "github.com/davecgh/go-spew/spew"
)

// URIEnv Mappging URL
type URIEnv struct {
	OAuthInternal              string
	OAuthTokenInternal         string
	OAuthTokenValidateInternal string

	AuthInternal         string
	AuthLoginInternal    string
	AuthGetUserInternal  string
	AuthGetUsersInternal string
	AuthGetRolesInternal string
	AuthPutUserInternal  string
	AuthPostPasswordSessionInternal  string
	AuthGetPasswordSessionInternal   string
	AuthValidatePassword string

	OMInternal                         string
	OMInternalCompany                  string
	OMInternalCompanies                string
	OMInternalCompanyAddress           string
	OMInternalCompanyAddresses         string
	OMInternalOrgLevel                 string
	OMInternalOrgLevels                string
	OMInternalOrgUnit                  string
	OMInternalOrgUnits                 string
	OMInternalPosition                 string
	OMInternalPositions                string
	OMInternalApprovalConfig           string
	OMInternalApprovalConfigs          string
	OMInternalApprovalConfigDetail     string
	OMInternalApprovalConfigDetails    string
	OMInternalApprovalConfigBudget     string
	OMInternalRegistrationRequest      string
	OMInternalRegistrationRequests     string
	OMInternalTerminationRequest       string
	OMInternalTerminationRequests      string
	OMInternalRegistrationRequestFile  string
	OMInternalRegistrationRequestFiles string
	OMInternalInbox                    string
	OMInternalInboxes                  string
	OMInternalInboxTemplate            string

	AAInternal                string
	AAInternalCategory        string
	AAInternalCategories      string
	AAInternalAsset           string
	AAInternalAssets          string
	AAInternalAssetOperation  string
	AAInternalAssetOperations string

	TMInternal                 string
	TMInternalChargeCode       string
	TMInternalChargeCodes      string
	TMInternalCompanyHoliday   string
	TMInternalCompanyHolidays  string
	TMInternalHolidayPreset    string
	TMInternalHolidayPresets   string
	TMInternalReportCodeSet    string
	TMInternalReportCodeSets   string
	TMInternalWorkPreset       string
	TMInternalWorkPresets      string
	TMInternalWorkAdjustment   string
	TMInternalWorkStatusSubmit string

	TMInternalWorkProgress           string
	TMInternalWorkSubmissionSummary  string
	TMInternalPeriod                 string
	TMInternalPeriods                string
	TMInternalWorkReports            string
	TMInternalWorkLogUpdate          string
	TMInternalEmpIDFromReviewerID    string
	TMLInternal                      string
	TMLInternalLeaveType             string
	TMLInternalLeaveTypes            string
	TMLInternalLeaveRequest          string
	TMLInternalLeaveRequests         string
	TMLInternalEmployeeQuota         string
	TMLInternalEmployeeQuotas        string
	TMLInternalApprovalTask          string
	TMLInternalApprovalTaskApprove   string
	TMLInternalApprovalTaskReject    string
	TMLInternalLeaveTypeEffectQuotas string
	TMLInternalInitEmployeeQuota     string
	TMLInternalResetEmployeeQuota    string

	PAInternal            string
	PAInternalEmployee    string
	PAInternalEmployees   string
	PAInternalUserDetail  string
	PAInternalUserDetails string

	RBInternal                            string
	RBInternalFoodApproveConfiguration    string
	RBInternalPerDiemApproveConfiguration string
	RBInternalTravelApproveConfiguration  string
	RBInternalOtherApproveConfiguration   string
	RBInternalReimbursementRequest        string
	RBInternalReimbursementRequests       string
	RBInternalReimbursementReports        string

	LOGSInternal string

	USERInternal string
}

// URIEnvMapping -
var URIEnvMapping URIEnv

func init() {
	app := base.GetApplication()

	AuthURL := app.Env.AuthInternalURL
	URIEnvMapping.AuthInternal = AuthURL
	URIEnvMapping.AuthLoginInternal = AuthURL + "/login"
	URIEnvMapping.AuthGetUserInternal = AuthURL + "/user"
	URIEnvMapping.AuthGetUsersInternal = AuthURL + "/users"
	URIEnvMapping.AuthGetRolesInternal = AuthURL + "/roles"
	URIEnvMapping.AuthPutUserInternal = AuthURL + "/user"
	URIEnvMapping.AuthPostPasswordSessionInternal = AuthURL + "/password-session"
	URIEnvMapping.AuthGetPasswordSessionInternal = AuthURL + "/password-sessions"
	URIEnvMapping.AuthValidatePassword = AuthURL + "/validate-password"

	OAuthURL := os.Getenv("OAUTH_INTERNAL_URL")
	URIEnvMapping.OAuthInternal = OAuthURL
	URIEnvMapping.OAuthTokenInternal = OAuthURL + "/token"
	URIEnvMapping.OAuthTokenValidateInternal = OAuthURL + "/validate"

	OMURL := os.Getenv("OM_SERVICE_INTERNAL_URL")
	URIEnvMapping.OMInternal = OMURL
	URIEnvMapping.OMInternalCompany = OMURL + "/company"
	URIEnvMapping.OMInternalCompanies = OMURL + "/companies"
	URIEnvMapping.OMInternalCompanyAddress = OMURL + "/company-address"
	URIEnvMapping.OMInternalCompanyAddresses = OMURL + "/company-addresses"
	URIEnvMapping.OMInternalOrgLevel = OMURL + "/org-level"
	URIEnvMapping.OMInternalOrgLevels = OMURL + "/org-levels"
	URIEnvMapping.OMInternalOrgUnit = OMURL + "/org-unit"
	URIEnvMapping.OMInternalOrgUnits = OMURL + "/org-units"
	URIEnvMapping.OMInternalPosition = OMURL + "/employee-position"
	URIEnvMapping.OMInternalPositions = OMURL + "/employee-positions"
	URIEnvMapping.OMInternalApprovalConfig = OMURL + "/approval-config"
	URIEnvMapping.OMInternalApprovalConfigs = OMURL + "/approval-configs"
	URIEnvMapping.OMInternalApprovalConfigDetail = OMURL + "/approval-config-detail"
	URIEnvMapping.OMInternalApprovalConfigDetails = OMURL + "/approval-config-details"
	URIEnvMapping.OMInternalApprovalConfigBudget = OMURL + "/approval-config-budget"
	URIEnvMapping.OMInternalRegistrationRequest = OMURL + "/registration-request"
	URIEnvMapping.OMInternalRegistrationRequests = OMURL + "/registration-requests"
	URIEnvMapping.OMInternalTerminationRequest = OMURL + "/termination-request"
	URIEnvMapping.OMInternalTerminationRequests = OMURL + "/termination-requests"
	URIEnvMapping.OMInternalRegistrationRequestFile = OMURL + "/attached-file"
	URIEnvMapping.OMInternalRegistrationRequestFiles = OMURL + "/attached-files"
	URIEnvMapping.OMInternalInbox = OMURL + "/inbox"
	URIEnvMapping.OMInternalInboxes = OMURL + "/inboxes"
	URIEnvMapping.OMInternalInboxTemplate = OMURL + "/inbox-template"

	AAURL := os.Getenv("AA_SERVICE_INTERNAL_URL")
	URIEnvMapping.AAInternal = AAURL
	URIEnvMapping.AAInternalCategory = AAURL + "/asset-category"
	URIEnvMapping.AAInternalCategories = AAURL + "/asset-categories"
	URIEnvMapping.AAInternalAsset = AAURL + "/asset"
	URIEnvMapping.AAInternalAssets = AAURL + "/assets"
	URIEnvMapping.AAInternalAssetOperation = AAURL + "/asset-operation"
	URIEnvMapping.AAInternalAssetOperations = AAURL + "/asset-operations"

	TMURL := os.Getenv("TM_SERVICE_INTERNAL_URL")
	URIEnvMapping.TMInternal = TMURL
	URIEnvMapping.TMInternalChargeCode = TMURL + "/chargecode"
	URIEnvMapping.TMInternalChargeCodes = TMURL + "/chargecodes"
	URIEnvMapping.TMInternalCompanyHoliday = TMURL + "/company-holiday"
	URIEnvMapping.TMInternalCompanyHolidays = TMURL + "/company-holidays"
	URIEnvMapping.TMInternalHolidayPreset = TMURL + "/holiday"
	URIEnvMapping.TMInternalHolidayPresets = TMURL + "/holidays"
	URIEnvMapping.TMInternalReportCodeSet = TMURL + "/report-charge-code-set"
	URIEnvMapping.TMInternalReportCodeSets = TMURL + "/report-charge-code-sets"
	URIEnvMapping.TMInternalWorkPreset = TMURL + "/workpreset"
	URIEnvMapping.TMInternalWorkPresets = TMURL + "/workpresets"
	URIEnvMapping.TMInternalWorkAdjustment = TMURL + "/work-adjustment"
	URIEnvMapping.TMInternalWorkProgress = TMURL + "/work-progress"
	URIEnvMapping.TMInternalWorkSubmissionSummary = TMURL + "/work-submission-summary"
	URIEnvMapping.TMInternalPeriod = TMURL + "/period"
	URIEnvMapping.TMInternalPeriods = TMURL + "/periods"
	URIEnvMapping.TMInternalWorkReports = TMURL + "/work-reports"
	URIEnvMapping.TMInternalWorkStatusSubmit = TMURL + "/submit-work-state"
	URIEnvMapping.TMInternalWorkLogUpdate = TMURL + "/process-work-summary/period"
	URIEnvMapping.TMInternalEmpIDFromReviewerID = TMURL + "/work-submission-reviewer"

	TMLURL := os.Getenv("TML_SERVICE_INTERNAL_URL")
	URIEnvMapping.TMLInternal = TMLURL
	URIEnvMapping.TMLInternalLeaveType = TMLURL + "/leave-type"
	URIEnvMapping.TMLInternalLeaveTypes = TMLURL + "/leave-types"
	URIEnvMapping.TMLInternalLeaveRequest = TMLURL + "/leave-request"
	URIEnvMapping.TMLInternalLeaveRequests = TMLURL + "/leave-requests"
	URIEnvMapping.TMLInternalEmployeeQuota = TMLURL + "/employee-leave-quota"
	URIEnvMapping.TMLInternalEmployeeQuotas = TMLURL + "/employee-leave-quotas"
	URIEnvMapping.TMLInternalApprovalTask = TMLURL + "/employee-approval"
	URIEnvMapping.TMLInternalApprovalTaskApprove = TMLURL + "/employee-approval/%d/leave-request/%d/approve"
	URIEnvMapping.TMLInternalApprovalTaskReject = TMLURL + "/employee-approval/%d/leave-request/%d/reject"
	URIEnvMapping.TMLInternalLeaveTypeEffectQuotas = TMLURL + "/leave-type/leave-quotas"
	URIEnvMapping.TMLInternalInitEmployeeQuota = TMLURL + "/employee-leave-quota/leave-quotas"
	URIEnvMapping.TMLInternalResetEmployeeQuota = TMLURL + "/employee-leave-quota/reset-leave-quotas"

	PAURL := os.Getenv("PA_SERVICE_INTERNAL_URL")
	URIEnvMapping.PAInternal = PAURL
	URIEnvMapping.PAInternalEmployee = PAURL + "/employee"
	URIEnvMapping.PAInternalEmployees = PAURL + "/employees"
	URIEnvMapping.PAInternalUserDetail = PAURL + "/user-detail"
	URIEnvMapping.PAInternalUserDetails = PAURL + "/user-details"

	PALOGSURL := os.Getenv("LOGS_SERVICE_INTERNAL_URL")
	URIEnvMapping.LOGSInternal = PALOGSURL + "/server/log"

	RBURL := os.Getenv("RB_SERVICE_INTERNAL_URL")
	URIEnvMapping.RBInternal = RBURL
	URIEnvMapping.RBInternalFoodApproveConfiguration = RBURL + "/reimbursementconfig/food"
	URIEnvMapping.RBInternalPerDiemApproveConfiguration = RBURL + "/reimbursementconfig/perdiem"
	URIEnvMapping.RBInternalTravelApproveConfiguration = RBURL + "/reimbursementconfig/travel"
	URIEnvMapping.RBInternalReimbursementRequest = RBURL + "/request"
	URIEnvMapping.RBInternalReimbursementRequests = RBURL + "/requests"
	URIEnvMapping.RBInternalReimbursementReports = RBURL + "/report/reimbursement"
	URIEnvMapping.RBInternalOtherApproveConfiguration = RBURL + "/reimbursementconfig/other"

	USERURL := os.Getenv("USER_SERVICE_INTERNAL_URL")
	URIEnvMapping.USERInternal = USERURL + "/auth/user"

}
