package service

import (
	"api/model"
	"errors"
	"fmt"

	"dv.co.th/hrms/core/rest"
	// "github.com/mitchellh/mapstructure"
	// "github.com/davecgh/go-spew/spew"
)

// CreateFodApproveConfiguration -
func CreateFoodApproveConfiguration(input *model.FoodApproveConfiguration) (interface{}, error) {
	url := fmt.Sprintf(URIEnvMapping.RBInternalFoodApproveConfiguration)

	res, err := rest.CallRestAPIWithOption("POST", url, input)
	if err != nil {
		return nil, err
	}
	result, _ := res.Data.(interface{})

	return result, err
}

// GetFoodConfiguration -
func GetFoodConfiguration(id int) (model.FoodApproveConfiguration, error) {
	var foodconfig model.FoodApproveConfiguration
	url := fmt.Sprintf(URIEnvMapping.RBInternalFoodApproveConfiguration+"?&company_id=%d", id)
	res, err := rest.CallRestAPIWithOption("GET", url, &foodconfig)
	if err != nil {
		return model.FoodApproveConfiguration{}, errors.New("cannot get food approve configuration")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.FoodApproveConfiguration{}, errors.New("not found: get food approve configuration")
	}

	return model.FoodApproveConfiguration{
		ID:            int(result["id"].(float64)),
		ApproveTypeID: int(result["approve_type_id"].(float64)),
		CreatedBy:     int(result["created_by"].(float64)),
		UpdatedBy:     int(result["updated_by"].(float64)),
	}, nil
}

// PutFodApproveConfiguration -
func PutFoodApproveConfiguration(input *model.FoodApproveConfiguration) (interface{}, error) {
	url := fmt.Sprintf(URIEnvMapping.RBInternalFoodApproveConfiguration)

	res, err := rest.CallRestAPIWithOption("PUT", url, input)
	if err != nil {
		return nil, err
	}

	result, _ := res.Data.(interface{})

	return result, err
}
