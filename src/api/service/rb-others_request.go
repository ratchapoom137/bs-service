package service

import (
	"api/helpers"
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	_ "net/url"
	"time"

	"dv.co.th/hrms/core/rest"
	coreRest "dv.co.th/hrms/core/rest"
	_ "github.com/davecgh/go-spew/spew"
	"github.com/mitchellh/mapstructure"
	"github.com/r3labs/diff"
	_ "github.com/thoas/go-funk"
)

func PostOthersReimbursementRequest(input map[string]interface{}, userLogin UserDetails) (requestID int, err error) {
	urlAddOther := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest) + "/others"
	currentrequestID, err := PostReimbursementRequestOnly(input)
	if err != nil {
		return 0, errors.New("cannot create request")
	}

	var otherDetail model.Others
	jsonData, _ := json.Marshal(input)
	json.Unmarshal(jsonData, &otherDetail)

	now := time.Now()
	bankokTime := helpers.GetBankokTimeZone(now).Format("2006-01-02T15:04:05+00:00")
	otherDetail.CreatedAt = bankokTime
	otherDetail.UpdatedAt = bankokTime
	otherDetail.RequestID = currentrequestID

	_, err = rest.CallRestAPIWithOption("POST", urlAddOther, &otherDetail)
	if err != nil {
		return 0, errors.New("cannot create other detail")
	}

	oldReimbursementRequest := model.OthersReimbursementRequest{}
	difObject, err := diff.Diff(oldReimbursementRequest, model.OthersReimbursementRequest{
		ID: currentrequestID,
	})

	errCreateChangelog := PostChangelog(difObject, currentrequestID, CREATE_ACTLOG, REIMBURSEMENT_REPORT, REIMBURSEMENT_CONTENT, userLogin)
	if errCreateChangelog != nil {
		return 0, errors.New("cannot create other detail")
	}
	return currentrequestID, nil
}

func GetOthersReimbursementRequestByID(id int) (model.OthersReimbursementRequest, error) {
	var reimbursementRequest model.OthersReimbursementRequest
	inputQuery := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/", id)

	res, err := coreRest.CallRestAPIWithOption("GET", inputQuery, nil)
	if err != nil {
		return model.OthersReimbursementRequest{}, errors.New("Can not get reimbursement request by id1")
	}
	result, ok := res.Data.(map[string]interface{})

	if !ok {
		return model.OthersReimbursementRequest{}, errors.New("Can not get request by id")
	}

	decodeError := mapstructure.Decode(result, &reimbursementRequest)
	if decodeError != nil {
		return model.OthersReimbursementRequest{}, errors.New("Cannot parse request")
	}
	var requestID = reimbursementRequest.ID
	inputQueryOthers := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/others/", id)

	response, err := coreRest.CallRestAPIWithOption("GET", inputQueryOthers, nil)
	if err != nil {
		return model.OthersReimbursementRequest{}, errors.New("Can not get reimbursement request by id2")
	}

	resultOthersDetail, ok := response.Data.(map[string]interface{})
	if !ok {
		return model.OthersReimbursementRequest{}, errors.New("Can not get food request by id")
	}

	jsonData, _ := json.Marshal(resultOthersDetail)
	json.Unmarshal(jsonData, &reimbursementRequest)
	reimbursementRequest.ID = requestID
	return reimbursementRequest, nil
}
func PutOthersReimbursementRequestWithLog(input map[string]interface{},loginUser UserDetails) (requestID int, err error) {

	// Step 1: Get old information
	before := func() (interface{}, error) {
		return GetPerdiemReimbursementRequestByID(input["id"].(int))
	}

	// Step 2: The service update or create process
	process := func() (interface{}, error) {
		return PutOthersReimbursementRequest(input)
	}

	// Step 3: Get new information
	after := func() (interface{}, error) {
		return GetPerdiemReimbursementRequestByID(input["id"].(int))
	}

	// Changelog Option
	var changelogOption = ChangelogOption{
		ContentID:   input["id"].(int),
		ActionType:  UPDATE_ACTLOG,
		InfoType:    REIMBURSEMENT_REPORT,
		ContentType: REIMBURSEMENT_CONTENT,
	}

	success, _ := ChangelogProcess(
		before,
		process,
		after,
		loginUser,
		changelogOption,
	)
	if success == false {
		err = errors.New("changelog process is fail")
		return
	}
	requestID=input["id"].(int)
	return
}

func PutOthersReimbursementRequest(input map[string]interface{}) (requestID int, err error) {

	var inputOthers model.FoodReimbursementRequest
	jsonData, _ := json.Marshal(input)
	json.Unmarshal(jsonData, &inputOthers)

	resEditRequestID, err := PutReimbursementRequestOnly(input)
	if err != nil {
		return 0, errors.New("Can not edit reimbursement request by id")
	}

	urlEditOthers := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest + "/others")
	inputQueryOthers := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/others/", resEditRequestID)

	resOthersDetail, err := rest.CallRestAPIWithOption("GET", inputQueryOthers, nil)
	if err != nil {
		return 0, errors.New("cannot edit food detail")
	}

	responeOthers, ok := resOthersDetail.Data.(map[string]interface{})
	if !ok {
		return 0, errors.New("Invalid food detail param")
	}

	var othersDetail model.Others
	jsonDataOthers, _ := json.Marshal(responeOthers)
	json.Unmarshal(jsonDataOthers, &othersDetail)

	now := time.Now()
	bankokTime := helpers.GetBankokTimeZone(now).Format(time.RFC3339)
	othersDetail.UpdatedAt = bankokTime
	othersDetail.Information = inputOthers.Information
	if inputOthers.Date != "" {
		othersDetail.Date = inputOthers.Date
	}
	othersDetail.UpdatedBy = inputOthers.UpdatedBy
	_, err = rest.CallRestAPIWithOption("PUT", urlEditOthers, &othersDetail)
	if err != nil {
		return 0, errors.New("cannot create food detail")
	}
	return resEditRequestID, nil
}
