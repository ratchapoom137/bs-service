package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"dv.co.th/hrms/core/rest"
)

// GetInboxTemplate -
func GetInboxTemplate(id int) (model.InboxTemplate, error) {
	var inboxTemplate model.InboxTemplate
	url := fmt.Sprintf(URIEnvMapping.OMInternalInboxTemplate+"/%d", id)

	res, err := rest.CallRestAPIWithOption("GET", url, model.InboxTemplate{})
	if err != nil {
		return model.InboxTemplate{}, errors.New("cannot get inbox template")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.InboxTemplate{}, errors.New("no such file: get inbox template")
	}

	jsonInbox, _ := json.Marshal(result)
	json.Unmarshal(jsonInbox, &inboxTemplate)

	return inboxTemplate, nil
}

// GetInbox -
func GetInbox(id int) (model.Inbox, error) {
	var inbox model.Inbox
	url := fmt.Sprintf(URIEnvMapping.OMInternalInbox+"/%d", id)

	res, err := rest.CallRestAPIWithOption("GET", url, model.Inbox{})
	if err != nil {
		return model.Inbox{}, errors.New("cannot get inbox")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.Inbox{}, errors.New("no such file: get inbox")
	}

	jsonInbox, _ := json.Marshal(result)
	json.Unmarshal(jsonInbox, &inbox)

	return inbox, nil
}

// GetInboxList -
func GetInboxList(filter map[string]interface{}) ([]model.Inbox, int, error) {
	pathURL, _ := url.Parse(URIEnvMapping.OMInternalInboxes)
	q := pathURL.Query()

	if offset, ok := filter["offset"].(int); ok && offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit, ok := filter["limit"].(int); ok && limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if receiver := filter["receiver"].(string); receiver != "" {
		q.Set("receiver", receiver)
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), []model.Inbox{})
	if err != nil {
		return []model.Inbox{}, 0, errors.New("cannot get inbox list")
	}

	data, ok := res.Data.([]interface{})
	if !ok {
		return []model.Inbox{}, 0, errors.New("no such file: get inbox list")
	}

	inboxList := []model.Inbox{}
	for _, inbox := range data {
		var tmpInbox model.Inbox

		jsonData, _ := json.Marshal(inbox)
		json.Unmarshal(jsonData, &tmpInbox)

		inboxList = append(inboxList, tmpInbox)
	}
	total := res.Total

	return inboxList, total, nil
}

func PostInbox(inbox model.Inbox) (int, error) {

	//TODO01: แยกเคสของการสร้าง email ตาม template ต่างๆ มีทั้งหมด 6 เคส

	url := URIEnvMapping.OMInternalInbox
	res, err := rest.CallRestAPIWithOption("POST", url, &inbox)

	if err != nil {
		return 0, errors.New("Cannot create inbox")
	}

	result := int(res.Data.(float64))
	return result, err
}
