package service

import (
	"errors"
	// "github.com/davecgh/go-spew/spew"
)

// EmployeeList -
type UserDetails struct {
	UserID     int    `json:"user_id"`
	PersonalID int    `json:"personal_id"`
	CompanyID  int    `json:"company_id"`
	FirstName  string `json:"first_name"`
	LastName   string `json:"last_name"`
}

func GetEmployeeByUserID(userID int) (result UserDetails, err error) {
	userFilter := map[string]interface{}{
		"limit":        10,
		"offset":       0,
		"user_id_list": []int{userID},
	}

	userInfo, total, err := GetUserDetailList(userFilter)
	if err != nil {
		return UserDetails{}, err
	}
	if total <= 0 {
		return UserDetails{}, errors.New("employee not found")
	}

	result.UserID = userInfo[0].UserID
	result.PersonalID = userInfo[0].EmployeeID
	result.CompanyID = userInfo[0].CompanyID
	result.FirstName = userInfo[0].FirstName
	result.LastName = userInfo[0].LastName
	return
}

func ConvertFloatToInt(float float64) int {
	return int(float)
}
