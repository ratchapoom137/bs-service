package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"dv.co.th/hrms/core/rest"
	"github.com/sethvargo/go-password/password"
)

// CreatePasswordSession
func CreatePasswordSession(passwordSession model.PasswordSessionInput) (passwordSessionID int, err error) {

	secret, err := password.Generate(32, 10, 0, false, false)
	passwordSession.Secret = secret

	passwordSessionUrl := fmt.Sprintf(URIEnvMapping.AuthPostPasswordSessionInternal)
	passwordSessionResult, passwordSessionErr := rest.CallRestAPIWithOption("POST", passwordSessionUrl, &passwordSession)

	if passwordSessionErr != nil {
		return passwordSessionID, passwordSessionErr
	}

	passwordSessionID = ConvertFloatToInt(passwordSessionResult.Data.(float64))

	return
}

//GetPasswordSessionList
func GetPasswordSessionList(filter map[string]interface{}) (result []model.PasswordSession, total int, err error) {
	pathURL, _ := url.Parse(URIEnvMapping.AuthGetPasswordSessionInternal)
	q := pathURL.Query()

	if secret, ok := filter["secret"].(string); ok && secret != "" {
		q.Set("secret", secret)
	}
	if user_id, ok := filter["user_id"].(int); ok && user_id >= 0 {
		q.Set("user_id", fmt.Sprintf("%d", user_id))
	}

	pathURL.RawQuery = q.Encode()

	res, _ := rest.CallRestAPIWithOption("GET", pathURL.String(), nil)
	if res.Data == nil {
		err = errors.New(res.Status.Message)
		return
	}

	data := res.Data.([]interface{})
	total = res.Total

	for _, item := range data {
		var passwordSession model.PasswordSession
		jsonPasswordSessionData, _ := json.Marshal(item)
		json.Unmarshal(jsonPasswordSessionData, &passwordSession)
		result = append(result, passwordSession)
	}

	return
}

//DeletePasswordSession -
func DeletePasswordSession(id int) (result int, err error) {
	url := fmt.Sprintf(URIEnvMapping.AuthPostPasswordSessionInternal+"/%d", id)
	res, err := rest.CallRestAPIWithOption("DELETE", url, nil)

	if err != nil {
		return result, errors.New("Cannot delete password session.")
	}

	result = int(res.Data.(float64))
	return
}