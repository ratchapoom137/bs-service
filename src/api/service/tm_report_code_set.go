package service

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"

	coreRest "dv.co.th/hrms/core/rest"
	"github.com/mitchellh/mapstructure"
)

// ReportCodeSet --
type ReportCodeSet struct {
	ID          int          `json:"id"`
	Name        string       `json:"name"`
	EmployeeID  int          `json:"employee_id" mapstructure:"employee_id"`
	ChargeCodes []ChargeCode `json:"charge_codes"  mapstructure:"charge_codes"`
	CreatedAt   string       `json:"created_at" mapstructure:"created_at"`
}

// ReportCodeSetList -
type ReportCodeSetList struct {
	ChargeCodeList []ReportCodeSet `json:"report_code_sets"`
	Total          int             `json:"total"`
}

// ReportChargeCode --
type ReportChargeCode struct {
	ID           int `json:"id"`
	ChargeCodeID int `json:"charge_code_id"`
	ReportCodeID int `json:"report_code_id"`
}

// ReportCodeSetQuery --
type ReportCodeSetQuery struct {
	Limit         int
	Offset        int
	EmployeeID    int
	SearchKeyword string
}

// PostReportCodeInput --
type PostReportCodeInput struct {
	Name        string `json:"name"`
	EmployeeID  int    `json:"employee_id" mapstructure:"employee_id"`
	ChargeCodes []int  `json:"charge_code_ids"  mapstructure:"charge_code_ids"`
	CreatedBy   int    `json:"created_by" mapstructure:"created_by"`
	UpdatedBy   int    `json:"updated_by" mapstructure:"updated_by"`
}

// PutCodeSetQuery -
type PutCodeSetQuery struct {
	ID                  int    `json:"id"`
	Name                string `json:"name"`
	ChargeCodeIDs       []int  `json:"charge_code_ids" mapstructure:"charge_code_ids"`
	ChargeCodeDeleteIDs []int  `json:"charge_code_delete_ids" mapstructure:"charge_code_delete_ids"`
	UpdatedBy           int    `json:"updated_by" mapstructure:"updated_by"`
}

// GetReportCodeSetList -
func GetReportCodeSetList(inputQuery ReportCodeSetQuery) (ReportCodeSetList, error) {
	var ReportCodeSetType []ReportCodeSet

	path := fmt.Sprintf(URIEnvMapping.TMInternalReportCodeSets)
	pathURL, _ := url.Parse(path)
	pathQuery := pathURL.Query()
	pathQuery.Set("offset", fmt.Sprintf("%d", inputQuery.Offset))
	pathQuery.Set("limit", fmt.Sprintf("%d", inputQuery.Limit))

	if inputQuery.SearchKeyword != "" {
		pathQuery.Set("search_keyword", fmt.Sprintf("%s", inputQuery.SearchKeyword))
	}

	pathURL.RawQuery = pathQuery.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), &ReportCodeSetType)
	if err != nil {
		return ReportCodeSetList{}, errors.New("Can't not create leave type")
	}

	decodeError := mapstructure.Decode(res.Data, &ReportCodeSetType)
	if decodeError != nil {
		return ReportCodeSetList{}, errors.New("Can't parse report code set")
	}

	return ReportCodeSetList{
			ChargeCodeList: ReportCodeSetType,
			Total:          res.Total},
		nil
}

// GetReportCodeSetDetail -
func GetReportCodeSetDetail(id int) (ReportCodeSet, error) {
	var reportCodeSetType ReportCodeSet

	inputQuery := fmt.Sprintf("%s%s", URIEnvMapping.TMInternalReportCodeSet+"/", strconv.Itoa(id))
	res, err := coreRest.CallRestAPIWithOption("GET", inputQuery, &reportCodeSetType)

	if err != nil {
		return ReportCodeSet{}, errors.New("Can't get report code set with the given id")
	}

	decodeError := mapstructure.Decode(res.Data, &reportCodeSetType)
	if decodeError != nil {
		return ReportCodeSet{}, errors.New("Can't parse report code set")
	}

	return reportCodeSetType, nil

}

// PostReportCodeSet -
func PostReportCodeSet(input map[string]interface{}) (ReportCodeSet, error) {
	var reportCodeSetType PostReportCodeInput
	decodeError := mapstructure.Decode(input, &reportCodeSetType)

	if decodeError != nil {
		return ReportCodeSet{}, errors.New("Can't parse report code set")
	}

	res, err := coreRest.CallRestAPIWithOption("POST", URIEnvMapping.TMInternalReportCodeSet, &reportCodeSetType)

	if err != nil {
		return ReportCodeSet{}, errors.New("Can't not create report code set")
	}

	var ResultReportCodeSet ReportCodeSet
	decodePostError := mapstructure.Decode(res.Data, &ResultReportCodeSet)
	if decodePostError != nil {
		return ReportCodeSet{}, errors.New("Can't parse received report code set")
	}

	return ResultReportCodeSet, nil

}

// PutReportCodeSet -
func PutReportCodeSet(input map[string]interface{}) (ReportCodeSet, error) {
	var newReportCodeSetType PutCodeSetQuery
	decodeError := mapstructure.Decode(input, &newReportCodeSetType)

	if decodeError != nil {
		return ReportCodeSet{}, errors.New("Can't edit code set type")
	}

	res, err := coreRest.CallRestAPIWithOption("PUT", URIEnvMapping.TMInternalReportCodeSet, &newReportCodeSetType)

	if err != nil || res.Status.Code == "NOT_FOUND" {
		return ReportCodeSet{}, errors.New("Can't edit report code set")
	}

	var resultReportCodeSet ReportCodeSet
	resultError := mapstructure.Decode(res.Data, &resultReportCodeSet)

	if resultError != nil {
		return ReportCodeSet{}, errors.New("Can't update")
	}

	return resultReportCodeSet, nil

}

// DeleteReportCodeSet -
func DeleteReportCodeSet(ReportCodeSetID int) (res ReportCodeSet, err error) {
	inputQuery := fmt.Sprintf("%s%d", URIEnvMapping.TMInternalReportCodeSet+"/", ReportCodeSetID)
	var reportCodeSetType []ReportCodeSet
	result, err := coreRest.CallRestAPIWithOption("DELETE", inputQuery, &reportCodeSetType)

	if err != nil {
		err = errors.New("Can not delete report code set")
		return
	}

	if result.Status.Code != "SUCCESS" {
		err = errors.New("Can not delete report code set")
		return
	}

	res = ReportCodeSet{
		ID: ReportCodeSetID,
	}

	return
}
