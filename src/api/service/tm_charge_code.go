package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"strconv"

	coreRest "dv.co.th/hrms/core/rest"
	_ "github.com/davecgh/go-spew/spew"
	"github.com/mitchellh/mapstructure"
)

// ChargeCodeList -
type ChargeCodeList struct {
	ChargeCodes []model.ChargeCode `json:"charge_codes"`
	Total       int                `json:"total"`
}
type ChargeCode struct {
	ID                int                       `json:"id"`
	Name              string                    `json:"name"`
	Code              string                    `json:"code"`
	StartDate         string                    `json:"start_date" mapstructure:"start_date"`
	EndDate           string                    `json:"end_date" mapstructure:"start_date"`
	IsActive          bool                      `json:"is_active" mapstructure:"is_active"`
	Employees         []EmployeeChargeCodeParse `json:"employees"`
	VisibilityPrivate bool                      `json:"visibility_private" mapstructure:"visibility_private"`
	Description       string                    `json:"description" mapstructure:"description"`
	CreatedBy         int                       `json:"created_by"`
	UpdatedBy         int                       `json:"updated_by"`
}
type EmployeeChargeCodeParse struct {
	ID                   int    `json:"id"`
	EmployeeID           int    `json:"employee_id" mapstructure:"employee_id"`
	Type                 string `json:"type" mapstructure:"type"`
	EmployeeChargeCodeID int    `json:"employee_charge_code_id" mapstructure:"employee_charge_code_id"`
	RollOn               string `json:"roll_on" mapstructure:"roll_on"`
	RollOff              string `json:"roll_off" mapstructure:"roll_off"`
	CreatedBy            int    `json:"created_by"`
	UpdatedBy            int    `json:"updated_by"`
}
type Pagination struct {
	Limit  int
	Offset int
}

//ChargeCodeQuery --

type ChargeCodeQuery struct {
	Limit         int
	Offset        int
	EmployeeID    int
	SearchKeyword string
	Status        string
	CompanyID     int
	IsNotWork     bool
}

// GetChargeCodeList -
func GetChargeCodeList(inputQuery ChargeCodeQuery) (ChargeCodeList, error) {
	var ChargeCodeType []model.ChargeCode

	path := fmt.Sprintf(URIEnvMapping.TMInternalChargeCodes + "/admin?")
	pathURL, _ := url.Parse(path)
	pathQuery := pathURL.Query()
	pathQuery.Set("offset", fmt.Sprintf("%d", inputQuery.Offset))
	pathQuery.Set("limit", fmt.Sprintf("%d", inputQuery.Limit))
	if inputQuery.SearchKeyword != "" {
		pathQuery.Set("searchKeyword", fmt.Sprintf("%s", inputQuery.SearchKeyword))
	}
	if inputQuery.Status != "" {
		pathQuery.Set("status", fmt.Sprintf("%s", inputQuery.Status))
	}
	if inputQuery.CompanyID > 0 {
		pathQuery.Set("company_id", fmt.Sprintf("%d", inputQuery.CompanyID))
	}
	pathURL.RawQuery = pathQuery.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), &ChargeCodeType)

	if err != nil {
		return ChargeCodeList{}, errors.New("Can't not get charge code")
	}

	result := res.Data.([]interface{})

	var parsedChargeCode []model.ChargeCode

	for _, chargeCode := range result {
		var jsonChargeCode model.ChargeCode

		jsonData, _ := json.Marshal(chargeCode)
		json.Unmarshal(jsonData, &jsonChargeCode)

		parsedChargeCode = append(parsedChargeCode, jsonChargeCode)
	}

	chargeCodeOutputList := ChargeCodeList{ChargeCodes: parsedChargeCode, Total: res.Total}
	return chargeCodeOutputList, nil
}

// GetChargeCodeListByEmployeeID -
func GetChargeCodeListByEmployeeID(inputQuery ChargeCodeQuery) (ChargeCodeList, error) {

	path := fmt.Sprintf(URIEnvMapping.TMInternalChargeCodes + "/employee?")
	pathURL, _ := url.Parse(path)
	pathQuery := pathURL.Query()
	pathQuery.Set("offset", fmt.Sprintf("%d", inputQuery.Offset))
	pathQuery.Set("limit", fmt.Sprintf("%d", inputQuery.Limit))
	pathQuery.Set("company_id", fmt.Sprintf("%d", inputQuery.CompanyID))
	if inputQuery.EmployeeID != 0 {
		pathQuery.Set("employee_id", fmt.Sprintf("%d", inputQuery.EmployeeID))
	}
	if inputQuery.SearchKeyword != "" {
		pathQuery.Set("searchKeyword", fmt.Sprintf("%s", inputQuery.SearchKeyword))
	}
	if inputQuery.Status != "" {
		pathQuery.Set("status", fmt.Sprintf("%s", inputQuery.Status))
	}
	pathURL.RawQuery = pathQuery.Encode()
	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), model.ChargeCode{})

	if err != nil {
		return ChargeCodeList{}, errors.New("Can't not get charge code")
	}
	result := res.Data.([]interface{})

	var parsedChargeCode []model.ChargeCode

	for _, chargeCode := range result {
		var jsonChargeCode model.ChargeCode
		jsonData, _ := json.Marshal(chargeCode)
		json.Unmarshal(jsonData, &jsonChargeCode)
		parsedChargeCode = append(parsedChargeCode, jsonChargeCode)
	}

	// totalAmount := len(parsedChargeCode)
	chargeCodeOutputList := ChargeCodeList{ChargeCodes: parsedChargeCode, Total: res.Total}

	return chargeCodeOutputList, nil
}

// GetChargeCodeDetail -
func GetChargeCodeDetail(id int) (model.ChargeCode, error) {
	var chargeCodeType model.ChargeCode

	inputQuery := fmt.Sprintf("%s%s", URIEnvMapping.TMInternalChargeCode+"/", strconv.Itoa(id))
	res, err := coreRest.CallRestAPIWithOption("GET", inputQuery, &chargeCodeType)

	if err != nil {
		return model.ChargeCode{}, errors.New("Can't get charge code with the given id")
	}

	decodeError := mapstructure.Decode(res.Data, &chargeCodeType)
	if decodeError != nil {
		return model.ChargeCode{}, errors.New("Can't parse charge code")
	}

	return chargeCodeType, nil

}

//PostChargeCode -
func PostChargeCode(input map[string]interface{}) (model.ChargeCode, error) {
	var chargeCodeType model.ChargeCode

	decodeError := mapstructure.Decode(input, &chargeCodeType)
	if decodeError != nil {
		return model.ChargeCode{}, errors.New("Can't parse charge code")
	}
	for i := 0; i < len(chargeCodeType.Employees); i++ {
		chargeCodeType.Employees[i].CreatedBy = chargeCodeType.CreatedBy
		chargeCodeType.Employees[i].UpdatedBy = chargeCodeType.UpdatedBy
	}
	res, err := coreRest.CallRestAPIWithOption("POST", URIEnvMapping.TMInternalChargeCode, &chargeCodeType)

	if err != nil {
		return model.ChargeCode{}, errors.New("Can't not create charge code")
	}
	var ResultChargeCode model.ChargeCode
	decodePostError := mapstructure.Decode(res.Data, &ResultChargeCode)
	if decodePostError != nil {
		return model.ChargeCode{}, errors.New("Can't parse received charge code")
	}

	return ResultChargeCode, nil

}

//PutChargeCode -
func PutChargeCode(input map[string]interface{}) (model.ChargeCode, error) {
	var newChargeCodeType model.PutChargeCodeQuery
	decodeError := mapstructure.Decode(input, &newChargeCodeType)
	if decodeError != nil {
		return model.ChargeCode{}, errors.New("Can't edit chargecode type")
	}
	for i := 0; i < len(newChargeCodeType.Employees); i++ {
		newChargeCodeType.Employees[i].UpdatedBy = newChargeCodeType.UpdatedBy
		if newChargeCodeType.Employees[i].CreatedBy < 1 {
			newChargeCodeType.Employees[i].CreatedBy = newChargeCodeType.UpdatedBy
		}
	}

	res, err := coreRest.CallRestAPIWithOption("PUT", URIEnvMapping.TMInternalChargeCode, &newChargeCodeType)

	if err != nil || res.Status.Code == "NOT_FOUND" {
		return model.ChargeCode{}, errors.New("Can't edit charge code")
	}

	var resultChargeCode model.ChargeCode
	resultError := mapstructure.Decode(res.Data, &resultChargeCode)

	if resultError != nil {
		return model.ChargeCode{}, errors.New("Can't update")
	}

	return resultChargeCode, nil

}

//DeleteChargeCode -
func DeleteChargeCode(ChargeCodeID int) (res model.ChargeCode, err error) {
	inputQuery := fmt.Sprintf("%s%d", URIEnvMapping.TMInternalChargeCode+"/", ChargeCodeID)
	var chargeCodeType []model.ChargeCode
	result, err := coreRest.CallRestAPIWithOption("DELETE", inputQuery, &chargeCodeType)

	if err != nil {

		err = errors.New("Can not delete charge code")
		return
	}
	if result.Status.Code != "SUCCESS" {
		err = errors.New("Can not delete charge code")
		return
	}

	res = model.ChargeCode{
		ID: ChargeCodeID,
	}

	return
}
