package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"strings"
	"time"

	"dv.co.th/hrms/core/rest"
)

type UserDetail struct {
	ID              int       `json:"id"`
	UserID          int       `json:"user_id"`
	EmployeeID      int       `json:"employee_id"`
	CompanyID       int       `json:"company_id"`
	FirstName       string    `json:"first_name"`
	LastName        string    `json:"last_name"`
	DeactivatedDate time.Time `json:"deactivated_date"`
	ExpiredDate		time.Time `json:"expired_date"`
}

type UserDetailInput struct {
	UserID          int       `json:"user_id"`
	EmployeeID      int       `json:"employee_id"`
	CompanyID       int       `json:"company_id"`
	FirstName       string    `json:"first_name"`
	LastName        string    `json:"last_name"`
	DeactivatedDate time.Time `json:"deactivated_date"`
}

type UserIDAndUserDetailID struct {
	UserID       int `json:"user_id"`
	UserDetailID int `json:"user_detail_id"`
}

//Create user detail
func CreateUserDetail(userDetail UserDetailInput) (userDetailID int, err error) {
	maxTime, _ := time.Parse(time.RFC3339, "9999-12-31T00:00:00+00:00")
	userDetail.DeactivatedDate = maxTime

	//[POST] API Create user detail
	createUseDetailrUrl := fmt.Sprintf(URIEnvMapping.PAInternalUserDetail)
	userDetailResult, userDetailResultErr := rest.CallRestAPIWithOption("POST", createUseDetailrUrl, &userDetail)
	if userDetailResultErr != nil {
		return userDetailID, userDetailResultErr
	}

	userDetailID = ConvertFloatToInt(userDetailResult.Data.(float64))

	return
}

func GetUserDetailByID(id int) (userDetail model.UserDetail, err error) {

	url := fmt.Sprintf(URIEnvMapping.PAInternalUserDetail+"/%d", id)
	res, err := rest.CallRestAPIWithOption("GET", url, &userDetail)

	if err != nil {
		return userDetail, errors.New("Cannot get user detail")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return userDetail, errors.New("Cannot find user")
	}
	var userDetailParsed model.UserDetail

	jsonUserDetail, _ := json.Marshal(result)
	json.Unmarshal(jsonUserDetail, &userDetailParsed)

	return userDetailParsed, nil
}

func GetUserDetailList(filter map[string]interface{}) (userDetailResult []model.UserDetail, total int, err error) {
	var filterList FilterList

	pathURL, _ := url.Parse(URIEnvMapping.PAInternalUserDetails)
	q := pathURL.Query()

	if user_id, ok := filter["user_id"].(int); ok && user_id > 0 {
		filterList.UserIDList = append(filterList.UserIDList, user_id)
	}
	if user_id_list, ok := filter["user_id_list"].([]int); ok && len(user_id_list) > 0 {
		filterList.UserIDList = user_id_list
	}
	if company_id_list, ok := filter["company_id_list"].([]int); ok && len(company_id_list) > 0 {
		filterList.CompanyIDList = company_id_list
	}
	if name, ok := filter["name"].(string); ok && name != "" {
		q.Set("name", name)
	}
	if offset, ok := filter["offset"].(int); ok && offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit, ok := filter["limit"].(int); ok && limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if status, ok := filter["status"].(string); ok && status != "" {
		status = strings.ToUpper(status)
		q.Set("status", status)
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), &filterList)

	if err != nil {
		err = errors.New("Cannot get user detail list")
		return
	}

	data, ok := res.Data.([]interface{})
	if !ok {
		return []model.UserDetail{}, 0, errors.New("Cannot find user detail")
	}

	for _, userDetails := range data {
		var jsonUserDetail model.UserDetail

		jsonOldinputUserDetailData, _ := json.Marshal(userDetails)
		json.Unmarshal(jsonOldinputUserDetailData, &jsonUserDetail)
		userDetailResult = append(userDetailResult, jsonUserDetail)
	}
	total = res.Total
	return
}

func PutUserDetail(userDetail model.UserDetailInput) (userDetailID int, err error) {
	url := fmt.Sprintf(URIEnvMapping.PAInternalUserDetail+"/%d", userDetail.ID)
	res, err := rest.CallRestAPIWithOption("PUT", url, &userDetail)

	if err != nil {
		return 0, errors.New("Cannot edit user detail")
	}

	result := int(res.Data.(float64))
	return result, err
}

func DeleteUserDetail(userDetailID int) (userDetailResult int, err error) {
	url := fmt.Sprintf(URIEnvMapping.PAInternalUserDetail+"/%d", userDetailID)
	res, err := rest.CallRestAPIWithOption("DELETE", url, &userDetailID)

	if err != nil {
		return 0, errors.New("Cannot delete user detail")
	}

	userDetailResult = int(res.Data.(float64))
	return userDetailResult, err
}
