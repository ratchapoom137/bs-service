package service

import (
	"api/model"

	// "dv.co.th/hrms/core"
	"errors"

	coreRest "dv.co.th/hrms/core/rest"
	"github.com/mitchellh/mapstructure"

	// "github.com/davecgh/go-spew/spew"
	// "strconv"
	"fmt"
)

// Work Preset -
type WorkPreset struct {
	ID        int    `json:"id"`
	CompanyID int    `json:"company_id" mapstructure:"company_id"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	StartTime string `json:"start_time" mapstructure:"start_time"`
	EndTime   string `json:"end_time" mapstructure:"end_time"`
	Workday   string `json:"work_day" mapstructure:"work_day"`
	CreatedBy int    `json:"created_by" mapstructure:"created_by"`
	UpdatedBy int    `json:"updated_by" mapstructure:"updated_by"`
}

type WorkPresetList struct {
	WorkPresets []WorkPreset `json:"work_presets"`
	Total       int          `json:"total"`
}

//GetWorkPreset -
func GetWorkPreset() model.WorkPreset {
	var workPreset model.WorkPreset

	return workPreset
}

//GetWorkPresetList -
func GetWorkPresetList(limit int, offset int, workType string, companyID int) (WorkPresetList, error) {
	var workPresetType []WorkPreset

	url := fmt.Sprintf(URIEnvMapping.TMInternalWorkPresets)

	url += fmt.Sprintf("?limit=%d", limit)
	url += fmt.Sprintf("&offset=%d", offset)
	url += "&work_type=" + workType
	url += fmt.Sprintf("&company_id=%d", companyID)
	res, err := coreRest.CallRestAPIWithOption("GET",
		url, &model.WorkPreset{})
	if err != nil {

		return WorkPresetList{}, errors.New("Can't not create work preset type")
	}

	var result []interface{}
	var ok bool
	if result, ok = res.Data.([]interface{}); !ok {
		return WorkPresetList{}, errors.New("Cannot get work preset")
	}

	decodeError := mapstructure.Decode(result, &workPresetType)
	if decodeError != nil {
		return WorkPresetList{}, errors.New("Cannot parse work preset")
	}

	workpresetList := WorkPresetList{WorkPresets: workPresetType, Total: res.Total}
	return workpresetList, nil
}

// GetWorkPresetDetail -
func GetWorkPresetDetail(id int) (WorkPreset, error) {

	var WorkPresetType WorkPreset
	inputQuery := fmt.Sprintf("%s%d", URIEnvMapping.TMInternalWorkPreset+"/", id)
	res, err := coreRest.CallRestAPIWithOption("GET",
		inputQuery, &WorkPresetType)

	if err != nil {
		return WorkPreset{}, errors.New("Can't get work preset type")
	}
	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return WorkPreset{}, errors.New("Cannot get work preset from rest")
	}
	decodeError := mapstructure.Decode(result, &WorkPresetType)
	if decodeError != nil {
		return WorkPreset{}, errors.New("Cannot parse work preset")
	}
	return WorkPresetType, nil
}

//DeleteWorkPreset -
func DeleteWorkPreset(WorkID int) (model.WorkPreset, error) {
	inputQuery := fmt.Sprintf("%s%d", URIEnvMapping.TMInternalWorkPreset+"/", WorkID)
	var workPresetType []model.WorkPreset
	res, err := coreRest.CallRestAPIWithOption("DELETE",
		inputQuery, &workPresetType)
	if err != nil {
		return model.WorkPreset{}, errors.New("Can not delete work preset")
	}

	if res.Status.Code != "SUCCESS" {
		return model.WorkPreset{}, errors.New("Can't delete work preset")
	}
	return model.WorkPreset{
		ID: WorkID,
	}, nil

}

//PutWorkPreset -
func PutWorkPreset(input map[string]interface{}) (WorkPreset, error) {
	var EditedWorkPreset WorkPreset

	decodeError := mapstructure.Decode(input, &EditedWorkPreset)
	if decodeError != nil {
		return WorkPreset{}, errors.New("Cannot parse work preset")
	}
	res, err := coreRest.CallRestAPIWithOption("PUT",
		URIEnvMapping.TMInternalWorkPreset, &EditedWorkPreset)

	if err != nil {
		return WorkPreset{}, errors.New("Can't update work preset")
	}

	var output WorkPreset
	outputErr := mapstructure.Decode(res.Data.(map[string]interface{}), &output)
	if outputErr != nil {
		return WorkPreset{}, errors.New("Cannot parse output work preset")
	}
	return output, nil

}

//PostWorkPreset -
func PostWorkPreset(input map[string]interface{}) (WorkPreset, error) {
	var workPresetType WorkPreset
	decodeError := mapstructure.Decode(input, &workPresetType)
	if decodeError != nil {
		return WorkPreset{}, errors.New("Cannot parse work preset")
	}
	res, err := coreRest.CallRestAPIWithOption("POST",
		URIEnvMapping.TMInternalWorkPreset+"/", &workPresetType)
	if err != nil {
		return WorkPreset{}, errors.New("Can't create work preset")
	}
	var output WorkPreset
	outputErr := mapstructure.Decode(res.Data.(map[string]interface{}), &output)
	if outputErr != nil {
		return WorkPreset{}, errors.New("Cannot parse output work preset")
	}
	return output, nil
}
