package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"dv.co.th/hrms/core/rest"
)

// GetAssetByID -
func GetAssetByID(id int) (model.Asset, error) {
	var asset model.Asset

	url := fmt.Sprintf(URIEnvMapping.AAInternalAsset+"/%d", id)

	res, err := rest.CallRestAPIWithOption("GET", url, &asset)
	if err != nil {
		return model.Asset{}, errors.New("cannot get asset")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.Asset{}, errors.New("not found: get asset")
	}

	var assetOut model.Asset

	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &assetOut)

	return assetOut, nil
}

// GetAssetList -
func GetAssetList(name string, status string, assetCategoryName string, serialNumber string, startDate string, endDate string,
	limit int, offset int, categoryID int, companyID int) (result []model.Asset, total int, err error) {
	var assets []model.Asset

	pathURL, _ := url.Parse(URIEnvMapping.AAInternalAssets)
	q := pathURL.Query()

	if name != "" {
		q.Set("name", name)
	}
	if status != "" {
		q.Set("status", status)
	}
	if assetCategoryName != "" {
		q.Set("asset_category_name", assetCategoryName)
	}
	if serialNumber != "" {
		q.Set("serial_number", serialNumber)
	}
	if startDate != "" {
		q.Set("start_date", startDate)
	}
	if endDate != "" {
		q.Set("end_date", endDate)
	}
	if offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if categoryID > 0 {
		q.Set("asset_category_id", fmt.Sprintf("%d", categoryID))
	}
	if companyID > 0 {
		q.Set("company_id", fmt.Sprintf("%d", companyID))
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), &assets)
	if err != nil {
		err = errors.New("cannot get asset list")
		return
	}

	data, ok := res.Data.([]interface{})
	if !ok {
		return []model.Asset{}, 0, errors.New("not found: get asset list")
	}

	for _, item := range data {
		var asset model.Asset

		jsonData, _ := json.Marshal(item)
		json.Unmarshal(jsonData, &asset)

		result = append(result, asset)
	}
	total = res.Total
	return
}

// PostAsset -
func PostAsset(asset *model.AssetInput) (assetID int, err error) {
	url := URIEnvMapping.AAInternalAsset

	res, err := rest.CallRestAPIWithOption("POST", url, &asset)
	if err != nil {
		return 0, errors.New("cannot create asset")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return 0, errors.New("Invalid param")
	}

	assetID = int(result["id"].(float64))
	return assetID, nil
}

// PutAsset -
func PutAsset(asset *model.AssetInput) (int, error) {
	url := fmt.Sprintf(URIEnvMapping.AAInternalAsset+"/%d", asset.ID)

	res, err := rest.CallRestAPIWithOption("PUT", url, &asset)
	if err != nil {
		return 0, errors.New("cannot edit asset")
	}

	return int(res.Data.(float64)), nil
}

// DeleteAsset -
func DeleteAsset(id int) (int, error) {
	url := fmt.Sprintf(URIEnvMapping.AAInternalAsset+"/%d", id)
	res, err := rest.CallRestAPIWithOption("DELETE", url, nil)

	if err != nil {
		return 0, errors.New("cannot delete asset")
	}

	result := int(res.Data.(float64))

	return result, nil
}
