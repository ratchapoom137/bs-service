package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"sort"

	"dv.co.th/hrms/core/rest"
	"github.com/thoas/go-funk"
	_"github.com/davecgh/go-spew/spew"
)

// GetApprovalConfig -
func GetApprovalConfig(id int) (model.ApprovalConfig, error) {
	var approvalConfig model.ApprovalConfig

	url := fmt.Sprintf(URIEnvMapping.OMInternalApprovalConfig+"/%d", id)

	res, err := rest.CallRestAPIWithOption("GET", url, &approvalConfig)
	if err != nil {
		return model.ApprovalConfig{}, errors.New("cannot get approval config")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.ApprovalConfig{}, errors.New("no such file: get approval config")
	}

	jsonApprovalConfig, _ := json.Marshal(result)
	json.Unmarshal(jsonApprovalConfig, &approvalConfig)

	return approvalConfig, nil
}

// GetApprovalConfigList -
func GetApprovalConfigList(limit int, offset int, companyID int, name string) ([]model.ApprovalConfig, int, error) {
	pathURL, _ := url.Parse(URIEnvMapping.OMInternalApprovalConfigs)
	q := pathURL.Query()

	if offset > 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if companyID > 0 {
		q.Set("company_id", fmt.Sprintf("%d", companyID))
	}
	if name != "" {
		q.Set("name", name)
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), nil)
	if err != nil {
		return []model.ApprovalConfig{}, 0, errors.New("cannot get approval config list")
	}

	data, ok := res.Data.([]interface{})
	if !ok {
		return []model.ApprovalConfig{}, 0, errors.New("no such file: get approval config list")
	}

	var approvalConfigs []model.ApprovalConfig

	for _, item := range data {
		var approvalConfig model.ApprovalConfig

		jsonData, _ := json.Marshal(item)
		json.Unmarshal(jsonData, &approvalConfig)

		approvalConfigs = append(approvalConfigs, approvalConfig)
	}
	total := res.Total

	return approvalConfigs, total, nil
}

// PostApprovalConfig -
func PostApprovalConfig(approvalConfigInput model.ApprovalConfigInput) (approvalID int, err error) {

	url := URIEnvMapping.OMInternalApprovalConfig

	var approvalConfig model.ApprovalConfig
	var approvalConfigBudgets []model.ApprovalConfigBudget

	jsonData, _ := json.Marshal(approvalConfigInput)
	json.Unmarshal(jsonData, &approvalConfig)

	approvalConfigBudgets = approvalConfigInput.EmployeeLevelBudget
	res, e := rest.CallRestAPIWithOption("POST", url, &approvalConfig)
	if e != nil || res.Data == nil {
		err = errors.New("cannot create approval config")
	}

	approvalID = int(res.Data.(float64))

	url = URIEnvMapping.OMInternalApprovalConfigDetail

	approvalConfigDetail := model.ApprovalConfigDetail{
		ApprovalConfigID: approvalID,
	}

	
	if approvalConfig.ConfigType == "EMPLOYEE_LEVEL" {
		response,errorAdd :=PostApprovalConfigBudget(approvalID , approvalConfigBudgets)
		if errorAdd != nil {
		
			err = errorAdd
		}
		approvalConfigInput.ValueID = response
	}

	funk.ForEach(approvalConfigInput.ValueID, func(value int) {
		approvalConfigDetail.ValueID = value
		
		res, e := rest.CallRestAPIWithOption("POST", url, &approvalConfigDetail)
		if e != nil || res.Data == nil {
			err = errors.New("cannot create approval config detail")
		}
	})


	return
}

// PutApprovalConfig -
func PutApprovalConfig(approvalConfigInput model.ApprovalConfigInput) (approvalID int, err error) {
	approvalConfigURL := fmt.Sprintf(URIEnvMapping.OMInternalApprovalConfig+"/%d", approvalConfigInput.ID)

	var approvalConfigBudgets []model.ApprovalConfigBudget
	var approvalConfig model.ApprovalConfig

	jsnonData, _ := json.Marshal(approvalConfigInput)
	json.Unmarshal(jsnonData, &approvalConfig)

    approvalConfigBudgets = approvalConfigInput.EmployeeLevelBudget
	res, e := rest.CallRestAPIWithOption("PUT", approvalConfigURL, &approvalConfig)
	if e != nil || res.Data == nil {
		err = errors.New("cannot edit approval config detail")
	}

	approvalID = approvalConfig.ID

	// ***ATTENTION***
	// For Now: the edit method is deleting old configs and create new ones.
	// Still looking for a better way than this.
	currentConfigDetails, err := GetApprovalConfigDetail(approvalConfigInput.ID)
	if err != nil {
		return
	}

	funk.ForEach(currentConfigDetails, func(config model.ApprovalConfigDetail) {
		_, err = DeleteApprovalConfigDetail(config.ID)
		if err != nil {
			return
		}
	})

	approvalConfigDetailURL := URIEnvMapping.OMInternalApprovalConfigDetail

	approvalConfigDetail := model.ApprovalConfigDetail{
		ApprovalConfigID: approvalConfigInput.ID,
	}

	funk.ForEach(approvalConfigInput.ValueID, func(value int) {
		approvalConfigDetail.ValueID = value

		res, e := rest.CallRestAPIWithOption("POST", approvalConfigDetailURL, &approvalConfigDetail)
		if e != nil || res.Data == nil {
			err = errors.New("cannot create approval config detail")
		}
	})

	if approvalConfig.ConfigType == "EMPLOYEE_LEVEL" {
		_,errorEdit :=editApprovalConfigBudget(approvalID , approvalConfigBudgets)
		if errorEdit != nil {
			err = errorEdit
		}
	}

	return
}

// DeleteApprovalConfig -
func DeleteApprovalConfig(id int) (configID int, err error) {
	configDetails, e := GetApprovalConfigDetail(id)
	if e != nil {
		return 0, e
	}

	funk.ForEach(configDetails, func(config model.ApprovalConfigDetail) {
		_, e := DeleteApprovalConfigDetail(config.ID)
		if e != nil {
			err = errors.New("cannot delete approval config detail")
		}
	})

	url := fmt.Sprintf(URIEnvMapping.OMInternalApprovalConfig+"/%d", id)

	res, err := rest.CallRestAPIWithOption("DELETE", url, nil)
	if err != nil {
		err = errors.New("cannot delete approval config")
		configID = 0
	}

	configID = int(res.Data.(float64))

	return
}

// GetApprovalConfigDetail -
func GetApprovalConfigDetail(approvalConfigID int) ([]model.ApprovalConfigDetail, error) {
	pathURL, _ := url.Parse(URIEnvMapping.OMInternalApprovalConfigDetails)
	q := pathURL.Query()
	
	if approvalConfigID > 0 {
		q.Set("approval_config_id", fmt.Sprintf("%d", approvalConfigID))
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), nil)
	if err != nil {
		return []model.ApprovalConfigDetail{}, errors.New("cannot get approval config details")
	}

	data, ok := res.Data.([]interface{})
	if !ok {
		return []model.ApprovalConfigDetail{}, errors.New("no such file: get approval config detail")
	}

	var approvalConfigDetails []model.ApprovalConfigDetail

	for _, configDetail := range data {
		var approvalConfigDetail model.ApprovalConfigDetail

		jsonData, _ := json.Marshal(configDetail)
		json.Unmarshal(jsonData, &approvalConfigDetail)

		approvalConfigDetails = append(approvalConfigDetails, approvalConfigDetail)
	}
	
	return approvalConfigDetails, nil
}

// DeleteApprovalConfigDetail -
func DeleteApprovalConfigDetail(approvalConfigDetailID int) (int, error) {
	url := fmt.Sprintf(URIEnvMapping.OMInternalApprovalConfigDetail+"/%d", approvalConfigDetailID)

	res, err := rest.CallRestAPIWithOption("DELETE", url, nil)
	if err != nil {
		return 0, errors.New("cannot delete approval config detail")
	}

	IDResult := int(res.Data.(float64))

	return IDResult, nil
}

// GetApprovalPositions - TM LEAVE
func GetApprovalPositions(positionID int, approvalID int) (approvalPositions []model.Position, e error) {
	requestedPosition, err := GetPosition(positionID)
	if err != nil {
		return nil, err
	}
	requestedApproval, err := GetApprovalConfig(approvalID)
	if err != nil {
		return nil, err
	}
	configs, err := GetApprovalConfigDetail(approvalID)
	if err != nil {
		return nil, err
	}
	currentOrg, err := GetOrgUnit(requestedPosition.OrgUnitID)
	if err != nil {
		return nil, err
	}
	parentOrg := model.OrgUnit{}
	if currentOrg.ParentOrgID != 0 {
		parentOrg, err = GetOrgUnit(currentOrg.ParentOrgID)
		if err != nil {
			return nil, err
		}
	}
	currentLevel, err := GetLevel(currentOrg.OrgLevelID)
	if err != nil {
		return nil, err
	}

	switch requestedApproval.ConfigType {
	case "MANAGER":
		approvalPositions, e = defaultApprovalConfig(requestedPosition)
		return

	case "SPECIFIC_POSITION":
		var position model.Position
		funk.ForEach(configs, func(config model.ApprovalConfigDetail) {
			position, err = GetPosition(config.ValueID)
			if err != nil {
				e = err
			}
		})
		if position.ID == requestedPosition.ID {
			positionsInOrg, _, _ := GetPositions(-1, -1, requestedPosition.OrgUnitID, "", "", false, false)
			managers := findManagers(positionsInOrg)
			for _, pos := range managers {
				if pos.ID == requestedPosition.ID {
					continue
				}
				position = pos
				approvalPositions = append(approvalPositions, pos)
				return
			}
			approvalPositions, e = defaultApprovalConfig(requestedPosition)
			return
		}

		approvalPositions = append(approvalPositions, position)

		return

	case "SPECIFIC_LEVEL":
		orgLevel := findLevels(configs)[0]

		if currentLevel.Value == orgLevel.Value {
			approvalPositions, e = defaultApprovalConfig(requestedPosition)
			return
		}

		for {
			if parentOrg.OrgLevelID == orgLevel.ID {
				positionsInOrg, _, err := GetPositions(-1, -1, parentOrg.ID, "", "", false, false)
				if err != nil {
					return nil, err
				}
				// Temporary Get Only First Found Manager
				if len(findManagers(positionsInOrg)) > 0 {
					approvalPositions = append(approvalPositions, findManagers(positionsInOrg)[0])
				}
				break
			}
			parentOrg, err = GetOrgUnit(parentOrg.ParentOrgID)
			if err != nil {
				return nil, err
			}
		}

		return

	case "MULTI_LEVEL":
		orgLevels := findLevels(configs)
		sort.Slice(orgLevels, func(i, j int) bool {
			return orgLevels[i].Value > orgLevels[j].Value
		})

		for {
			for _, tmpLvl := range orgLevels {
				if currentLevel.Value == tmpLvl.Value {
					if requestedPosition.OrgUnitID == currentOrg.ID && requestedPosition.IsManager {
						break
					}
					positionsInOrg, _, _ := GetPositions(-1, -1, currentOrg.ID, "", "", false, false)
					managers := findManagers(positionsInOrg)
					// Temporary Get Only First Found Manager
					if len(managers) > 0 {
						approvalPositions = append(approvalPositions, managers[0])
					}
				}
			}
			if currentOrg.ParentOrgID == 0 {
				if len(approvalPositions) < 1 {
					positionsInOrg, _, _ := GetPositions(-1, -1, currentOrg.ID, "", "", false, false)
					managers := findManagers(positionsInOrg)
					if len(managers) > 0 {
						approvalPositions = append(approvalPositions, managers[0])
					}
				}
				break
			}
			currentOrg, err = GetOrgUnit(currentOrg.ParentOrgID)
			if err != nil {
				return nil, err
			}
			currentLevel, err = GetLevel(currentOrg.OrgLevelID)
			if err != nil {
				return nil, err
			}
		}

		return

	case "MAX_LEVEL":
		maxLevel := findLevels(configs)[0]

		for {
			iteratedLevel, _ := GetLevel(currentOrg.OrgLevelID)
			if iteratedLevel.Value >= maxLevel.Value {
				if currentOrg.ID == requestedPosition.OrgUnitID && requestedPosition.IsManager {
					currentOrg, _ = GetOrgUnit(currentOrg.ParentOrgID)
					continue
				}
				positionsInOrg, _, _ := GetPositions(-1, -1, currentOrg.ID, "", "", false, false)
				managers := findManagers(positionsInOrg)
				// Temporary Get Only First Found Manager
				if len(managers) > 0 {
					approvalPositions = append(approvalPositions, managers[0])
				}
			}
			if currentOrg.ParentOrgID == 0 {
				if len(approvalPositions) < 1 {
					positionsInOrg, _, _ := GetPositions(-1, -1, currentOrg.ID, "", "", false, false)
					managers := findManagers(positionsInOrg)
					if len(managers) > 0 {
						approvalPositions = append(approvalPositions, managers[0])
					}
				}
				break
			}
			currentOrg, _ = GetOrgUnit(currentOrg.ParentOrgID)
		}

		return
	case "EMPLOYEE_LEVEL":

		requestorEmployeeLevel := getEmployeeLevelByPosition(requestedPosition.EmployeeID)	
		inOrgManager,err := defaultApprovalConfig(requestedPosition)
		if err != nil {
			return nil, err
		}
		for  _, m := range inOrgManager {
			inOrgManagerEmpLevel := getEmployeeLevelByPosition(m.EmployeeID)
			if inOrgManagerEmpLevel < requestorEmployeeLevel && inOrgManagerEmpLevel != 0 {
				approvalPositions = append(approvalPositions, m)
			}else if inOrgManagerEmpLevel == requestorEmployeeLevel {
				checkManagerORg, err := GetOrgUnit(m.OrgUnitID)
				if err != nil {
					return nil, err
				}
				checkmOrgLevel, _ := GetLevel(checkManagerORg.OrgLevelID)
				if (currentLevel.Value < checkmOrgLevel.Value){
					approvalPositions = append(approvalPositions, m)
				}
			}
		}
		for currentOrg.ParentOrgID != 0 {
			positionsInOrg, _, err := GetPositions(-1, -1, currentOrg.ParentOrgID, "", "", false, false)
			if err != nil {
				return nil, err
			}

			managersInOrg := findManagers(positionsInOrg)
			
			for  _, m := range managersInOrg {
				managerEmpLevel := getEmployeeLevelByPosition(m.EmployeeID)
				
				if managerEmpLevel < requestorEmployeeLevel && managerEmpLevel != 0 {
					approvalPositions = append(approvalPositions, m)
				}else if managerEmpLevel == requestorEmployeeLevel {
					managerORg, err := GetOrgUnit(m.OrgUnitID)
					if err != nil {
						return nil, err
					}
					mOrgLevel, _ := GetLevel(managerORg.OrgLevelID)
					if (currentLevel.Value <= mOrgLevel.Value){
						approvalPositions = append(approvalPositions, m)
					}
				}
			}
			currentOrg, _ = GetOrgUnit(currentOrg.ParentOrgID)
		}
		return
	default:
		break
	}
	return nil, errors.New("Request Failed")
}

func defaultApprovalConfig(requestedPosition model.Position) (approvalPositions []model.Position, err error) {
	var positionsInOrg []model.Position
	var totalPosition int
	var parentOrg model.OrgUnit

	currentOrg, err := GetOrgUnit(requestedPosition.OrgUnitID)
	if err != nil {
		return nil, err
	}

	if currentOrg.ParentOrgID != 0 {
		parentOrg, err = GetOrgUnit(currentOrg.ParentOrgID)
		if err != nil {
			return nil, err
		}
	}
	if requestedPosition.IsManager {
		if parentOrg.ID == 0 {
			approvalPositions = append(approvalPositions, requestedPosition)
			return
		}
	}

	if !requestedPosition.IsManager {
		positionsInOrg, _, err = GetPositions(-1, -1, requestedPosition.OrgUnitID, "", "", false, false)
		if err != nil {
			return nil, errors.New("cannot get positions in org")
		}

		managersInOrg := findManagers(positionsInOrg)
		// Temporary Get Only First Found Manager
		if len(managersInOrg) > 0 {
			approvalPositions = append(approvalPositions, managersInOrg[0])
		}

		if len(approvalPositions) > 0 {
			return
		}
	}

	for {
		positionsInOrg, totalPosition, _ = GetPositions(-1, -1, parentOrg.ID, "", "", false, false)

		if totalPosition > 0 {
			managersInOrg := findManagers(positionsInOrg)
			// Temporary Get Only First Found Manager
			if len(managersInOrg) > 0 {
				approvalPositions = append(approvalPositions, managersInOrg[0])
			}
			if totalManager := len(approvalPositions); totalManager > 0 {
				break
			}
		}
		if parentOrg.ID != 0 {
			parentOrg, err = GetOrgUnit(parentOrg.ParentOrgID)
			if err != nil {
				break
			}
		}
	}
	return
}

func findManagers(positions []model.Position) (managers []model.Position) {
	funk.ForEach(positions, func(position model.Position) {
		if position.IsManager && position.EmployeeID != 0 {
			managers = append(managers, position)
		}
	})
	return
}

func findLevels(configs []model.ApprovalConfigDetail) (orgLevels []model.OrgLevel) {
	funk.ForEach(configs, func(config model.ApprovalConfigDetail) {
		lvl, _ := GetLevel(config.ValueID)
		orgLevels = append(orgLevels, lvl)
	})
	return
}

func compareEmployeeLevel(employeeLevel string) (empLevelIndex int) {
	switch employeeLevel {
	case "Executive":	
		 empLevelIndex = 1
	case "MiddleManager":	
		 empLevelIndex = 2
	case "ManagerAndAdvicer":	
		empLevelIndex = 3
	case "Staff":	
		empLevelIndex = 4
	default: 
		break;
	}
	return 
}

func getEmployeeLevelByPosition(positionID int)(employeeLevelNumber int){
	requestorEmployee, err := GetEmployeeByPersonalID(positionID)
	if err != nil {
		employeeLevelNumber = 0
	}
	
	requestorEmployeeLevel := requestorEmployee.EmployeeGeneralInfo.EmployeeLevel

	currentEmployeeLevel:= compareEmployeeLevel(requestorEmployeeLevel)
	employeeLevelNumber = currentEmployeeLevel
	return
}

// GetApprovalBudgetDetail -
// GetApprovalConfigDetail -
func GetApprovalConfigBudget(approvalConfigBudgetID int) ( model.ApprovalConfigBudget, error) {

	inputQuery := fmt.Sprintf("%s%d", URIEnvMapping.OMInternalApprovalConfigBudget+"/", approvalConfigBudgetID) 
	var resultApprovalConfigBudget model.ApprovalConfigBudget

	res, err := rest.CallRestAPIWithOption("GET",inputQuery, &resultApprovalConfigBudget)
	if err != nil {
		return model.ApprovalConfigBudget{}, errors.New("cannot get approval config budget")
	}

	data, ok := res.Data.(interface{})
	if !ok {
		return model.ApprovalConfigBudget{}, errors.New("no such file: get approval config budget")
	}

	jsonData, _ := json.Marshal(data)
	json.Unmarshal(jsonData, &resultApprovalConfigBudget)

	return resultApprovalConfigBudget, nil
}

func editApprovalConfigBudget(approverID int ,approvalConfigBudgets []model.ApprovalConfigBudget) (budgetID int, err error) {
	for _, each := range approvalConfigBudgets {
		each.ApprovalConfigID = approverID
		inputQuery := fmt.Sprintf("%s%d", URIEnvMapping.OMInternalApprovalConfigBudget+"/", each.ID ) 
		res, err := rest.CallRestAPIWithOption("PUT",inputQuery, &each)
		if err != nil || res.Data == nil {
			err = errors.New("cannot create approval config budget")
			return 0, err
		}
		budgetID = int(res.Data.(float64))
	}
	return budgetID,nil

}
func PostApprovalConfigBudget(approvalConfigId int ,approvalConfigBudgetInputs []model.ApprovalConfigBudget) ( approvalBudgetID []int, err error) {
	url := fmt.Sprintf(URIEnvMapping.OMInternalApprovalConfigBudget) 
	for _, each := range approvalConfigBudgetInputs {
		each.ApprovalConfigID = approvalConfigId

		res, e := rest.CallRestAPIWithOption("POST", url, &each)
		if e != nil || res.Data == nil {
		
			err = errors.New("cannot create approval config budget")
		}
		approvalBudgetID = append(approvalBudgetID,int(res.Data.(float64)))

	}
	
	return approvalBudgetID,nil
}