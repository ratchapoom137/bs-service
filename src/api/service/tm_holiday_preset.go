package service

import (
	"api/base"
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"time"

	coreRest "dv.co.th/hrms/core/rest"
	_ "github.com/davecgh/go-spew/spew"
	"github.com/mitchellh/mapstructure"
)

// HolidayPreset -
type HolidayPreset struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Amount    int    `json:"amount"`
	CreatedAt string `json:"created_at"`
}

//HolidayPresetList -
type HolidayPresetList struct {
	Presets []model.HolidayPreset `json:"preset_list"`
	Total   int                   `json:"total"`
}

//HolidayPresetUpdateType -
type HolidayPresetUpdateType struct {
	ID        int             `json:"id"`
	Name      string          `json:"name"`
	Amount    int             `json:"amount"`
	UpdatedBy int             `json:"updated_by"`
	Holidays  []HolidayOutput `json:"holidays" mapstructure:"holidays"`
	Deleted   []int           `json:"deleted" mapstructure:"deleted"`
}

// HolidayPresetOutput model
type HolidayPresetOutput struct {
	ID        int             `json:"id" mapstructure:"id"`
	Name      string          `json:"name" mapstructure:"name"`
	Amount    int             `json:"amount" mapstructure:"amount"`
	Holidays  []HolidayOutput `json:"holidays"`
	CreatedBy int             `json:"created_by"`
	UpdatedBy int             `json:"updated_by"`
	CreatedAt string          `json:"created_at" mapstructure:"created_at"`
	UpdatedAt string          `json:"updated_at" mapstructure:"updated_at"`
	DeletedAt string          `json:"deleted_at,omitempty"`
}

// Holiday table model
type HolidayOutput struct {
	ID          int    `json:"id"`
	HolidayName string `json:"holiday_name"  mapstructure:"holiday_name"`
	HolidayDate string `json:"holiday_date"  mapstructure:"holiday_date"`
	MemberID    int
	CreatedBy   int    `json:"created_by"`
	UpdatedBy   int    `json:"updated_by"`
	CreatedAt   string `json:"created_at" mapstructure:"created_at"`
	UpdatedAt   string `json:"updated_at" mapstructure:"updated_at"`
	DeletedAt   string `json:"deleted_at,omitempty"`
}

//GetHolidayPresetList -
func GetHolidayPresetList(limit int, offset int) (HolidayPresetList, error) {
	var HolidayPresetType []model.HolidayPreset

	path := fmt.Sprintf(URIEnvMapping.TMInternalHolidayPresets)
	pathURL, _ := url.Parse(path)
	pathQuery := pathURL.Query()
	pathQuery.Set("offset", fmt.Sprintf("%d", offset))
	pathQuery.Set("limit", fmt.Sprintf("%d", limit))
	pathURL.RawQuery = pathQuery.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), &HolidayPresetType)

	if err != nil {
		return HolidayPresetList{}, errors.New("Can't not create leave type")
	}

	result := res.Data.([]interface{})

	var parsedHolidayPresets []model.HolidayPreset

	for _, holiday := range result {
		tempHoliday := holiday.(map[string]interface{})

		dateStamp, err := time.Parse(time.RFC3339, tempHoliday["created_at"].(string))

		if err != nil {
			return HolidayPresetList{}, errors.New("Can't parse time")
		}

		parsedHolidayPresets = append(parsedHolidayPresets, model.HolidayPreset{
			ID:        int(tempHoliday["id"].(float64)),
			Name:      tempHoliday["name"].(string),
			Amount:    int(tempHoliday["amount"].(float64)),
			CreatedAt: &dateStamp,
		})

	}

	totalAmount := res.Total
	holidayList := HolidayPresetList{Presets: parsedHolidayPresets, Total: totalAmount}
	return holidayList, nil
}

// GetHolidayPresetDetail -
func GetHolidayPresetDetail(id string) (HolidayPresetOutput, error) {
	var HolidayPresetType HolidayPreset
	inputQuery := fmt.Sprintf("%s%s", URIEnvMapping.TMInternalHolidayPreset+"/", id)

	res, err := coreRest.CallRestAPIWithOption("GET", inputQuery, &HolidayPresetType)
	if err != nil {
		return HolidayPresetOutput{}, errors.New("Can't connect to server")
	}

	var outputHol HolidayPresetOutput

	decodeError := mapstructure.Decode(res.Data, &outputHol)

	if decodeError != nil {
		return HolidayPresetOutput{}, errors.New("Cannot parse holiday preset")
	}

	return outputHol, nil

}

// PostHolidayPreset -
func PostHolidayPreset(input map[string]interface{}) (model.HolidayPreset, error) {
	// var parsedHolidays []model.Holiday
	holidayPresetType := model.HolidayPreset{}
	app := base.GetApplication()

	jsontextInput, _ := json.Marshal(input)

	err := json.Unmarshal([]byte(jsontextInput), &holidayPresetType)

	if err != nil {
		return model.HolidayPreset{}, err
	}

	url := fmt.Sprintf(URIEnvMapping.TMInternalHolidayPreset + "/")
	app.Logger.Info(url)

	for i := 0; i < len(holidayPresetType.Holidays); i++ {
		if holidayPresetType.Holidays[i].ID == 0 {
			holidayPresetType.Holidays[i].CreatedBy = holidayPresetType.CreatedBy
		}
		holidayPresetType.Holidays[i].UpdatedBy = holidayPresetType.UpdatedBy
	}

	res, err := coreRest.CallRestAPIWithOption("POST", url, &holidayPresetType)

	if err != nil {
		app.Logger.Error(holidayPresetType)
		return model.HolidayPreset{}, errors.New("Can't not create leave type")
	}
	result := res.Data.(map[string]interface{})

	dateStamp, err := time.Parse(time.RFC3339, result["created_at"].(string))
	if err != nil {
		return model.HolidayPreset{}, errors.New("Can't parse time")
	}

	return model.HolidayPreset{
		ID:        int(result["id"].(float64)),
		Name:      result["name"].(string),
		Amount:    int(result["amount"].(float64)),
		CreatedAt: &dateStamp,
	}, nil
}

// PutHolidayPreset -
func PutHolidayPreset(input map[string]interface{}) (model.HolidayPreset, error) {
	var parsedHolidays []HolidayOutput

	for _, holiday := range input["holidays"].([]interface{}) {
		tempHoliday := holiday.(map[string]interface{})
		if tempHoliday["id"] == nil {
			parsedHolidays = append(parsedHolidays, HolidayOutput{
				HolidayName: tempHoliday["holiday_name"].(string),
				HolidayDate: tempHoliday["holiday_date"].(string),
				CreatedBy:   input["created_by"].(int),
				UpdatedBy:   input["updated_by"].(int),
			})
		} else {
			parsedHolidays = append(parsedHolidays, HolidayOutput{
				ID:          tempHoliday["id"].(int),
				HolidayName: tempHoliday["holiday_name"].(string),
				HolidayDate: tempHoliday["holiday_date"].(string),
				UpdatedBy:   input["updated_by"].(int),
			})
		}

	}
	var deleteCellList []int

	if input["delete_holiday_ids"] != nil {
		for _, toBeDeleted := range input["delete_holiday_ids"].([]interface{}) {
			deleteCellList = append(deleteCellList, toBeDeleted.(int))
		}
	}

	holidayPresetType := HolidayPresetUpdateType{
		ID:        input["id"].(int),
		Name:      input["name"].(string),
		Amount:    input["amount"].(int),
		Holidays:  parsedHolidays,
		UpdatedBy: input["updated_by"].(int),
		Deleted:   deleteCellList,
	}

	var outputHol HolidayPresetUpdateType

	decodeError := mapstructure.Decode(input, &outputHol)

	if decodeError != nil {
		return model.HolidayPreset{}, errors.New("Cannot parse holiday preset")
	}

	url := fmt.Sprintf(URIEnvMapping.TMInternalHolidayPreset + "/")
	res, err := coreRest.CallRestAPIWithOption("PUT", url, &holidayPresetType)

	if err != nil {
		return model.HolidayPreset{}, errors.New("Can't not create leave type")
	}

	result := res.Data.(map[string]interface{})

	dateStamp, err := time.Parse(time.RFC3339, result["created_at"].(string))

	if err != nil {
		return model.HolidayPreset{}, errors.New("Can't parse time")
	}

	return model.HolidayPreset{
		ID:        int(result["id"].(float64)),
		Name:      result["name"].(string),
		Amount:    int(result["amount"].(float64)),
		CreatedAt: &dateStamp,
	}, nil

}

//DeleteHolidayPreset -
func DeleteHolidayPreset(HolidayID int) (res model.HolidayPreset, err error) {
	inputQuery := fmt.Sprintf("%s%d", URIEnvMapping.TMInternalHolidayPreset+"/", HolidayID)
	var holidayPresetType []model.Holiday
	result, err := coreRest.CallRestAPIWithOption("DELETE", inputQuery, &holidayPresetType)

	if err != nil {
		err = errors.New("Can not delete holiday preset")
		return
	}

	_, ok := result.Data.(string)

	if !ok {
		err = errors.New("Can not parse delete holiday preset result")
		return
	}

	res = model.HolidayPreset{
		ID: HolidayID,
	}

	return
}
