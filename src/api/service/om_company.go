package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"dv.co.th/hrms/core/rest"
)

// GetCompanyDetail -
func GetCompanyDetail(id int) (model.Company, error) {
	var company model.Company

	url := fmt.Sprintf(URIEnvMapping.OMInternalCompany+"/%d", id)

	res, err := rest.CallRestAPIWithOption("GET", url, &company)

	if err != nil {
		return model.Company{}, errors.New("cannot get company")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.Company{}, errors.New("no such file: get company detail")
	}

	jsonCompany, _ := json.Marshal(result)
	json.Unmarshal(jsonCompany, &company)

	return company, nil
}

// GetCompanyList -
func GetCompanyList(limit int, offset int, name string, status string) (result []model.Company, total int, err error) {
	var company []model.Company

	pathURL, _ := url.Parse(URIEnvMapping.OMInternalCompanies)
	q := pathURL.Query()

	if name != "" {
		q.Set("name", name)
	}
	if offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if status != "" {
		q.Set("status", status)
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), &company)

	if err != nil {
		err = errors.New("cannot get company")
		return
	}

	data, ok := res.Data.([]interface{})
	if !ok {
		return []model.Company{}, 0, errors.New("no such file: get company list")
	}

	for _, company := range data {
		var jsonCompany model.Company

		jsonData, _ := json.Marshal(company)
		json.Unmarshal(jsonData, &jsonCompany)

		result = append(result, jsonCompany)
	}
	total = res.Total

	return
}

// PostCompany -
func PostCompany(company model.Company) (int, error) {

	res, err := rest.CallRestAPIWithOption("POST", URIEnvMapping.OMInternalCompany, &company)
	if err != nil || res.Data == nil {
		return 0, errors.New(res.Status.Message)
	}

	result := int(res.Data.(float64))

	return result, nil
}

// PutCompany -
func PutCompany(company model.Company) (int, error) {
	url := fmt.Sprintf(URIEnvMapping.OMInternalCompany+"/%d", company.ID)

	res, err := rest.CallRestAPIWithOption("PUT", url, &company)
	if err != nil || res.Data == nil {
		return 0, errors.New(res.Status.Message)
	}

	result := int(res.Data.(float64))

	return result, nil
}

//CheckDuplicateTaxID -
func CheckDuplicateTaxID(taxID string) (bool, error) {
	pathURL, _ := url.Parse(URIEnvMapping.OMInternalCompanies)
	q := pathURL.Query()

	if taxID != "" {
		q.Set("tax_id", taxID)
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), []model.Company{})
	if err != nil || res.Data == nil {
		return true, errors.New(res.Status.Message)
	}

	if total := int(res.Total); total > 0 {
		return true, nil
	}

	return false, nil
}
