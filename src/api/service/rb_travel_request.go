package service

import (
	_ "api/helpers"
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	// _ "net/url"
	_ "time"

	"dv.co.th/hrms/core/rest"
	// // coreRest "dv.co.th/hrms/core/rest"
	_ "github.com/davecgh/go-spew/spew"
	// _ "github.com/mitchellh/mapstructure"
	"github.com/r3labs/diff"
	"github.com/thoas/go-funk"
)

func PostTravelAllowanceReimbursementRequest(input map[string]interface{}, userLogin UserDetails) (requestID int, err error) {
	urlAddTravelAllowance := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest) + "/travel"
	currentrequestID, err := PostReimbursementRequestOnly(input)
	if err != nil {
		return 0, errors.New("cannot create request")
	}

	for _, item := range input["travel_allowances"].([]interface{}) {
		var inputTravel model.TravelAllowance
		jsonData, _ := json.Marshal(item)
		json.Unmarshal(jsonData, &inputTravel)
		inputTravel.RequestID = currentrequestID

		_, err = rest.CallRestAPIWithOption("POST", urlAddTravelAllowance, &inputTravel)
		if err != nil {
			return 0, errors.New("cannot create travel allowance")
		}
	}
	oldReimbursementRequest := model.PerDiemReimbursementRequest{}
	difObject, err := diff.Diff(oldReimbursementRequest, model.PerDiemReimbursementRequest{
		ID: currentrequestID,
	})

	errCreateChangelog := PostChangelog(difObject, currentrequestID, CREATE_ACTLOG, REIMBURSEMENT_REPORT, REIMBURSEMENT_CONTENT, userLogin)
	if errCreateChangelog != nil {
		return 0, errors.New("cannot create travel detail")
	}
	return currentrequestID, nil
}

//GetTravelAllowanceByRequestID
func GetTravelAllowanceReimbursementRequestByID(id int) (model.TravelReimbursementRequest, error) {
	var travelAllowancesRequest model.TravelReimbursementRequest
	url := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/", id)

	res, err := rest.CallRestAPIWithOption("GET", url, nil)
	if err != nil {
		return model.TravelReimbursementRequest{}, errors.New("cannot get traval allowance request by ID")
	}
	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.TravelReimbursementRequest{}, errors.New("not found: get traval allowance request By ID")
	}

	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &travelAllowancesRequest)

	var travelAllowanceTable []model.TravelAllowance
	urlTable := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/travel/", id)
	resTable, err := rest.CallRestAPIWithOption("GET", urlTable, nil)
	if err != nil {
		return model.TravelReimbursementRequest{}, errors.New("cannot get perdiem by id")
	}
	jsonData2, _ := json.Marshal(resTable.Data)
	json.Unmarshal(jsonData2, &travelAllowanceTable)
	travelAllowancesRequest.TravelAllowances = travelAllowanceTable

	return travelAllowancesRequest, nil
}
func PutTravelAllowanceReimbursementRequestWithLog(input map[string]interface{},loginUser UserDetails) (requestID int, err error) {

	// Step 1: Get old information
	before := func() (interface{}, error) {
		return GetPerdiemReimbursementRequestByID(input["id"].(int))
	}

	// Step 2: The service update or create process
	process := func() (interface{}, error) {
		return PutTravelAllowanceReimbursementRequest(input)
	}

	// Step 3: Get new information
	after := func() (interface{}, error) {
		return GetPerdiemReimbursementRequestByID(input["id"].(int))
	}

	// Changelog Option
	var changelogOption = ChangelogOption{
		ContentID:   input["id"].(int),
		ActionType:  UPDATE_ACTLOG,
		InfoType:    REIMBURSEMENT_REPORT,
		ContentType: REIMBURSEMENT_CONTENT,
	}

	success, _ := ChangelogProcess(
		before,
		process,
		after,
		loginUser,
		changelogOption,
	)
	if success == false {
		err = errors.New("changelog process is fail")
		return
	}
	requestID=input["id"].(int)
	return
}
func PutTravelAllowanceReimbursementRequest(input map[string]interface{}) (requestID int, err error) {

	var inputTravelAllowanceRequest model.TravelReimbursementRequest
	jsonData, _ := json.Marshal(input)
	json.Unmarshal(jsonData, &inputTravelAllowanceRequest)

	currentRequest, err := GetTravelAllowanceReimbursementRequestByID(inputTravelAllowanceRequest.ID)
	if err != nil {
		return 0, errors.New("Can not get per diem reimbursement request by id")
	}

	var newTravelAllowanceIDs []int
	for _, item := range inputTravelAllowanceRequest.TravelAllowances {
		if item.ID > 0 {
			newTravelAllowanceIDs = append(newTravelAllowanceIDs, item.ID)
		}
	}
	for _, item := range currentRequest.TravelAllowances {
		if item.ID > 0 {
			if !funk.Contains(newTravelAllowanceIDs, item.ID) {
				DeleteTravelAllowance(item.ID)
			}

		}
	}

	var totals = SumRequestTotals(inputTravelAllowanceRequest.TravelAllowances)
	currentRequest.TravelAllowances = inputTravelAllowanceRequest.TravelAllowances

	input["total_distance"] = totals[0]
	input["cost"] = totals[1]
	resEditRequestID, err := PutReimbursementRequestOnly(input)
	if err != nil {
		return 0, errors.New("Can not edit reimbursement request by id")
	}

	urlTravelAllowance := fmt.Sprintf(URIEnvMapping.RBInternalReimbursementRequest + "/travel")

	for _, travelAllowance := range currentRequest.TravelAllowances {
		travelAllowance.RequestID = resEditRequestID
		if travelAllowance.ID != 0 {
			_, err = rest.CallRestAPIWithOption("PUT", urlTravelAllowance, &travelAllowance)
			if err != nil {
				return 0, errors.New("cannot edit perdiem detail")
			}
		} else {
			_, err = rest.CallRestAPIWithOption("POST", urlTravelAllowance, &travelAllowance)
			if err != nil {
				return 0, errors.New("cannot add perdiem detail")
			}
		}

	}

	return resEditRequestID, nil
}

func DeleteTravelAllowance(id int) (ok bool) {
	urlDeleteTravelAllowance := fmt.Sprintf("%s%d", URIEnvMapping.RBInternalReimbursementRequest+"/travel/", id)
	_, err := rest.CallRestAPIWithOption("DELETE", urlDeleteTravelAllowance, nil)
	if err != nil {
		return
	}
	ok = true
	return
}

func SumRequestTotals(arr []model.TravelAllowance) (result []float64) {
	var sumExpend float64
	var sumDistance float64

	for _, eachTravelAllowance := range arr {
		sumExpend += eachTravelAllowance.Expend
		sumDistance += eachTravelAllowance.Distance
	}
	result = append(result, sumDistance)
	result = append(result, sumExpend)
	return result
}

func SumPerDiemTotalRequestTotals(arr []model.PerDiem) (result model.PerDiem) {
	var resultPerDiem model.PerDiem
	var sumExpend float64
	var sumHour int

	for _, each := range arr {
		sumExpend += each.Expend
		sumHour += each.Hour
	}
	resultPerDiem.Expend = sumExpend
	resultPerDiem.Hour = sumHour
	return resultPerDiem
}
