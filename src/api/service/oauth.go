package service

import (
	"api/base"
	"api/model"
	"encoding/json"
	"errors"
	"fmt"

	"dv.co.th/hrms/core/rest"
)

// GetToken exchange access token with auth code
func GetTokenFromAuthCode(code string, redirectURI string, ClientID string, ClientSecret string) (ret model.OAuthToken, err error) {
	URL := fmt.Sprintf("%s?grant_type=%s&code=%s&redirect_uri=%s&client_id=%s&client_secret=%s",
		URIEnvMapping.OAuthTokenInternal,
		"authorization_code",
		code,
		redirectURI,
		ClientID,
		ClientSecret,
	)

	app := base.GetApplication()
	app.Logger.Info(URL)
	result, _ := rest.CallAPI("GET", URL, nil)

	json.Unmarshal(result, &ret)
	if ret.ErrorDescription != "" {
		err = errors.New(ret.ErrorDescription)
	}

	return
}

// ValidateTokenFromAuthCode check access_token validity
func ValidateTokenFromAuthCode(accessToken string) (ret model.OAuthTokenInfo, err error) {
	URL := fmt.Sprintf("%s?access_token=%s",
		URIEnvMapping.OAuthTokenValidateInternal,
		accessToken,
	)

	app := base.GetApplication()
	app.Logger.Info(URL)
	result, _ := rest.CallAPI("GET", URL, nil)

	json.Unmarshal(result, &ret)

	return
}
