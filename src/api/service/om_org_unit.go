package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"time"

	"dv.co.th/hrms/core/rest"
)

// GetOrgUnit -
func GetOrgUnit(id int) (model.OrgUnit, error) {
	var orgUnit model.OrgUnit
	url := fmt.Sprintf(URIEnvMapping.OMInternalOrgUnit+"/%d", id)

	res, err := rest.CallRestAPIWithOption("GET", url, &orgUnit)
	if err != nil {
		return model.OrgUnit{}, errors.New("cannot get org unit")
	}

	result, ok := res.Data.(map[string]interface{})
	if !ok {
		return model.OrgUnit{}, errors.New("no such file: get org unit")
	}

	jsonOrgUnit, _ := json.Marshal(result)
	json.Unmarshal(jsonOrgUnit, &orgUnit)

	return orgUnit, nil
}

// GetOrgUnits -
func GetOrgUnits(limit int, offset int, companyID int, orgLevelID int, name string, minLvl int, maxLvl int, pointOfTime string, isPending bool, parentOrgID int) (result []model.OrgUnit, total int, err error) {
	var orgUnits []model.OrgUnit

	pathURL, _ := url.Parse(URIEnvMapping.OMInternalOrgUnits)
	q := pathURL.Query()

	if offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if companyID > 0 {
		q.Set("company_id", fmt.Sprintf("%d", companyID))
	}
	if orgLevelID > 0 {
		q.Set("org_level_id", fmt.Sprintf("%d", orgLevelID))
	}
	if name != "" {
		q.Set("name", name)
	}
	if minLvl >= 0 {
		q.Set("min_level", fmt.Sprintf("%d", minLvl))
	}
	if maxLvl >= 0 {
		q.Set("max_level", fmt.Sprintf("%d", maxLvl))
	}
	if pointOfTime == "" {
		pointOfTime = time.Now().Format("2006-01-02T15:04:05+07:00")
	}
	q.Set("point_of_time", pointOfTime)
	q.Set("is_pending", fmt.Sprintf("%v", isPending))
	if parentOrgID != 0 {
		q.Set("parent_org_id", fmt.Sprintf("%d", parentOrgID))
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), &orgUnits)
	if err != nil {
		err = errors.New("cannot get org unit list")
		return
	}

	data, ok := res.Data.([]interface{})
	if !ok {
		return []model.OrgUnit{}, 0, errors.New("no such file: get org unit list")
	}

	for _, orgUnit := range data {
		var tmpOrg model.OrgUnit

		jsonData, _ := json.Marshal(orgUnit)
		json.Unmarshal(jsonData, &tmpOrg)

		result = append(result, tmpOrg)
	}
	total = res.Total

	return
}

// PostOrgUnit -
func PostOrgUnit(orgUnit model.OrgUnit) (int, error) {
	infiniteDate, _ := time.Parse(time.RFC3339, "9999-12-31T00:00:00+00:00")
	orgUnit.TerminateAt = &infiniteDate

	url := URIEnvMapping.OMInternalOrgUnit
	res, err := rest.CallRestAPIWithOption("POST", url, &orgUnit)

	if err != nil {
		return 0, errors.New("cannot create org unit")
	}

	result := int(res.Data.(float64))

	return result, nil
}

// PutOrgUnit -
func PutOrgUnit(orgUnit model.OrgUnit) (int, error) {
	url := fmt.Sprintf(URIEnvMapping.OMInternalOrgUnit+"/%d", orgUnit.ID)
	res, err := rest.CallRestAPIWithOption("PUT", url, &orgUnit)

	if err != nil {
		return 0, errors.New("cannot edit org unit")
	}

	result := int(res.Data.(float64))

	return result, nil
}

// DeleteOrgUnit -
func DeleteOrgUnit(id int) (int, error) {
	url := fmt.Sprintf(URIEnvMapping.OMInternalOrgUnit+"/%d", id)
	res, err := rest.CallRestAPIWithOption("DELETE", url, model.OrgUnit{})

	if err != nil {
		return 0, errors.New("cannot delete org unit")
	}

	result := int(res.Data.(float64))

	return result, nil
}

// TerminateOrgUnit -
func TerminateOrgUnit(id int, terminate string) (result int, message string, e error) {

	terminateDate, _ := time.Parse(time.RFC3339, terminate)
	now := time.Now()

	orgUnit, err := GetOrgUnit(id)
	if err != nil {
		e = err
		return
	}

	if orgUnit.ParentOrgID == 0 {
		message = "cannot terminate root org unit"
		return
	}

	if orgUnit.EffectiveAt.After(now) {
		result, e = DeleteOrgUnit(id)
		message = "delete success"
		return
	}

	if IsOrgContainsEmployeeRecur(id) {
		message = "org unit contains employees"
		return
	}

	orgUnit.TerminateAt = &terminateDate
	result, e = PutOrgUnit(orgUnit)
	message = "terminate success"
	return
}

// TransferOrgUnit -
func TransferOrgUnit(orgID int, targetParentOrgID int, processDateString string) (result int, message string, err error) {
	result, err = transferOrgUnitRecur(orgID, targetParentOrgID, processDateString)
	if err != nil {
		message = "cannot transfer org unit"
	}

	return
}

func isOrgContainsEmployee(orgID int) bool {
	orgUnit, _ := GetOrgUnit(orgID)

	positions, _, _ := GetPositions(-1, -1, orgUnit.ID, "", "", false, false)
	for _, position := range positions {
		if position.EmployeeID == 0 {
			continue
		}
		return true
	}

	return false
}

func IsOrgContainsEmployeeRecur(orgID int) bool {
	if isOrgContainsEmployee(orgID) {
		return true
	}
	orgUnit, _ := GetOrgUnit(orgID)
	for _, child := range orgUnit.ChildrenOrg {
		if !IsOrgContainsEmployeeRecur(child.ID) {
			continue
		}
		return true
	}
	return false
}

func transferOrgUnitRecur(orgID int, targetParentOrgID int, processDateString string) (newOrgUnitID int, err error) {
	processDate, _ := time.Parse(time.RFC3339, processDateString)

	orgUnit, _ := GetOrgUnit(orgID)
	newOrgUnit := model.OrgUnit{
		Name:        orgUnit.Name,
		Description: orgUnit.Description,
		OrgLevelID:  orgUnit.OrgLevelID,
		ParentOrgID: targetParentOrgID,
		EffectiveAt: processDate,
	}

	newOrgUnitID, err = PostOrgUnit(newOrgUnit)
	if err != nil {
		return 0, err
	}
	// Terminate Old OrgUnit
	_, _, err = TerminateOrgUnit(orgUnit.ID, processDateString)

	positions, total, _ := GetPositions(-1, -1, orgUnit.ID, "", "", false, false)

	if total > 0 {
		for _, position := range positions {
			newPosition := model.Position{
				Name:        position.Name,
				Description: position.Description,
				IsManager:   position.IsManager,
				EffectiveAt: processDate,
				OrgUnitID:   newOrgUnitID,
			}
			_, err = PostPosition(newPosition)
			if err != nil {
				return 0, err
			}
			// Terminate Old Position
			position.TerminateAt = &processDate
			_, err = PutPosition(position)
			if err != nil {
				return 0, err
			}
		}
	}

	for _, co := range orgUnit.ChildrenOrg {
		_, err = transferOrgUnitRecur(co.ID, newOrgUnitID, processDateString)
		if err != nil {
			return 0, err
		}
	}

	return newOrgUnitID, nil
}
