package service

import (
	"api/model"
	"api/helpers"
	"api/request"
	"net/url"
	"strings"
	"errors"
	"fmt"
	coreRest "dv.co.th/hrms/core/rest"
	"github.com/mitchellh/mapstructure"
)

// InitEmployeeQuotaByID - GET Initialize EmployeeQuota with Call Employee API
func InitEmployeeQuotaByID(employeeID int, gender string, companyID int) (model.EmployeeQuotaDetail, error) {
	var employeeInput request.APITMLInitEmployeeQuota
	var employeeQuota []model.EmployeeQuota
	var employeeQuotaErr error

	url := fmt.Sprintf(URIEnvMapping.TMLInternalInitEmployeeQuota)
	
	employeeInput = request.APITMLInitEmployeeQuota {
		EmployeeID: employeeID,
		Gender: strings.ToLower(gender),
		CompanyID: companyID,
	}

	res, err := coreRest.CallRestAPIWithOption("POST", url, employeeInput)
	if err != nil {
		return model.EmployeeQuotaDetail{}, errors.New("Can't get Employee")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.EmployeeQuotaDetail{}, errors.New("Employee not found")
	}

	if employeeQuotaErr = mapstructure.Decode(res.Data, &employeeQuota); employeeQuotaErr != nil {
		return model.EmployeeQuotaDetail{}, errors.New("Can't prase employee quota")
	}

	employeeQuotaDetail := model.EmployeeQuotaDetail{
		EmployeeID:	employeeID,
		Quotas:		employeeQuota,
	}

	return employeeQuotaDetail, nil
}

// GetEmployeeQuotaByID - GET EmployeeQuota with Call Employee API
func GetEmployeeQuotaByID(employeeID int, status string) (model.EmployeeQuotaDetail, error) {
	var employeeQuota []model.EmployeeQuota
	var employeeQuotaErr error

	pathURL, _ := url.Parse(fmt.Sprintf(URIEnvMapping.TMLInternalEmployeeQuota+"/%d", employeeID))
	q := pathURL.Query()

	if status != "" {
		q.Set("status", status)
	}

	pathURL.RawQuery = q.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), nil)
	if err != nil {
		return model.EmployeeQuotaDetail{}, errors.New("Can't get Employee")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.EmployeeQuotaDetail{}, errors.New("Employee not found")
	}

	if employeeQuotaErr = mapstructure.Decode(res.Data, &employeeQuota); employeeQuotaErr != nil {
		return model.EmployeeQuotaDetail{}, errors.New("Can't prase employee quota")
	}

	employeeQuotaDetail := model.EmployeeQuotaDetail{
		EmployeeID:	employeeID,
		Quotas:		employeeQuota,
	}

	return employeeQuotaDetail, nil
}

// GetEmployeeQuotaList -
func GetEmployeeQuotaList(employeeIDs []int, year string, status string) (model.EmployeeQuotaList, error) {
	var employeeQuotas []model.EmployeeQuota
	var total int

	pathURL, _ := url.Parse(URIEnvMapping.TMLInternalEmployeeQuotas)
	q := pathURL.Query()

	if len(employeeIDs) > 0 {
		q.Set("employee_id", helpers.ArrayToString(employeeIDs, ","))
	}

	if year != "" {
		q.Set("year", year)
	}

	if status != "" {
		q.Set("status", status)
	}

	pathURL.RawQuery = q.Encode()

	res, err := coreRest.CallRestAPIWithOption("GET", pathURL.String(), nil)

	if err != nil {
		return model.EmployeeQuotaList{}, errors.New("Can't not get employee quota list")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.EmployeeQuotaList{}, errors.New("employee quota list not found")
	}

	results := res.Data.([]interface{})

	if employeeQuotaErr := mapstructure.Decode(results, &employeeQuotas); employeeQuotaErr != nil {
		return model.EmployeeQuotaList{}, errors.New("Can't mapstructure employee quota list")
	}

	if employeeQuotaTotalErr := mapstructure.Decode(res.Total, &total); employeeQuotaTotalErr != nil {
		return model.EmployeeQuotaList{}, errors.New("Can't mapstructure employee quota total")
	}

	employeeQuotaList := model.EmployeeQuotaList{
		EmployeeQuotas:	employeeQuotas,
		Total:			total,
	}

	return employeeQuotaList, nil
}


// EditEmployeeQuotaByID -
func EditEmployeeQuotaByID(employeeID int, employeeQuota []model.EmployeeQuota) (model.EmployeeQuotaDetail, error) {
	url := fmt.Sprintf(URIEnvMapping.TMLInternalEmployeeQuota+"/%d", employeeID)

	res, err := coreRest.CallRestAPIWithOption("PUT", url, &employeeQuota)

	if err != nil {
		return model.EmployeeQuotaDetail{}, errors.New("Can't not get Employee")
	}

	if res.Status.Code == "NOT_FOUND" {
		return model.EmployeeQuotaDetail{}, errors.New("Employee quota not found")
	}

	if res.Data == nil {
		return model.EmployeeQuotaDetail{}, errors.New("Can't get employee quota form api")
	}

	result := res.Data.([]interface{})

	if employeeQuotaErr := mapstructure.Decode(result, &employeeQuota); employeeQuotaErr != nil {
		return model.EmployeeQuotaDetail{}, errors.New("Can't prase employee quota")
	}

	employeeQuotaDetail := model.EmployeeQuotaDetail{
		EmployeeID:	employeeID,
		Quotas:		employeeQuota,
	}

	return employeeQuotaDetail, nil
}

// CreateEmployeeQuotaAfterLeaveType -
func CreateEmployeeQuotaAfterLeaveType(employeeQuotaRequest request.TMLCreateEmployeeQuota) ([]model.EmployeeQuota, error) {
	var employeeQuota []model.EmployeeQuota

	url := fmt.Sprintf(URIEnvMapping.TMLInternalLeaveTypeEffectQuotas)

	res, err := coreRest.CallRestAPIWithOption("POST", url, &employeeQuotaRequest)
	if err != nil {
		return []model.EmployeeQuota{}, errors.New("Can't create employee quotas after leave type")
	}

	result := res.Data.([]interface{})

	if employeeQuotaErr := mapstructure.Decode(result, &employeeQuota); employeeQuotaErr != nil {
		return []model.EmployeeQuota{}, errors.New("Can't prase employee quota")
	}

	return employeeQuota, nil
}

// EditEmployeeQuotasAfterLeaveType -
func EditEmployeeQuotasAfterLeaveType(employeeQuotasRequest request.TMLEditEmployeeQuotaAfterLeaveType) ([]model.EmployeeQuota, error) {
	var employeeQuotas []model.EmployeeQuota

	url := fmt.Sprintf(URIEnvMapping.TMLInternalLeaveTypeEffectQuotas)

	res, err := coreRest.CallRestAPIWithOption("PUT", url, &employeeQuotasRequest)
	if err != nil {
		return []model.EmployeeQuota{}, errors.New("Can't edit employee quotas after leave type")
	}

	result := res.Data.([]interface{})

	if employeeQuotasErr := mapstructure.Decode(result, &employeeQuotas); employeeQuotasErr != nil {
		return []model.EmployeeQuota{}, errors.New("Can't prase employee quota")
	}

	return employeeQuotas, nil
}

// ResetEmployeeQuotaByID -
func ResetEmployeeQuotaByID(employeeReset []request.APITMLInitEmployeeQuota) ([]model.EmployeeQuota, error) {
	var employeeQuota []model.EmployeeQuota

	url := fmt.Sprintf(URIEnvMapping.TMLInternalResetEmployeeQuota)

	res, err := coreRest.CallRestAPIWithOption("PUT", url, &employeeReset)
	if err != nil {
		return []model.EmployeeQuota{}, errors.New("Can't reset leave type")
	}

	result := res.Data.([]interface{})

	if employeeQuotaErr := mapstructure.Decode(result, &employeeQuota); employeeQuotaErr != nil {
		return []model.EmployeeQuota{}, errors.New("Can't prase employee quota")
	}

	return employeeQuota, nil
}