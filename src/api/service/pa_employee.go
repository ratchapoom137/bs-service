package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"strings"
	"time"
	"api/helpers"

	"dv.co.th/hrms/core/rest"

	// "github.com/davecgh/go-spew/spew"
	"github.com/mohae/deepcopy"
	"github.com/r3labs/diff"
	"github.com/sethvargo/go-password/password"
)

// EmployeeList -
type EmployeeList struct {
	Employees []model.Employee `json:"employee_list"`
	Total     int              `json:"total"`
}

// Employee Edit -
type EmployeeEdit struct {
	EndDate   time.Time `json:"end_date"`
	UpdatedBy int       `json:"updated_by"`
}

// EmployeeGeneralInfoEdit -
type EmployeeGeneralInfoEdit struct {
	CompanyEmployeeID string     `json:"company_employee_id" mapstructure:"company_employee_id"`
	TypeOfStaff       string     `json:"type_of_staff" mapstructure:"type_of_staff"`
	EmployeeLevel     string     `json:"employee_level" mapstructure:"employee_level"`
	WorkTypeID        int        `json:"work_type_id"`
	ProbationEndDate  *time.Time `json:"probation_end_date" mapstructure:"probation_end_date"`
	PositionID        int        `json:"position_id" mapstructure:"position_id"`
	OrgUnitID         int        `json:"org_unit_id" mapstructure:"org_unit_id"`
	CompanyBranchID   int        `json:"company_branch_id" mapstructure:"company_branch_id"`
	CompanyID         int        `json:"company_id" mapstructure:"company_id"`
	StartDate         time.Time  `json:"start_date" mapstructure:"start_date"`
	UpdatedBy         int        `json:"updated_by" mapstructure:"updated_by"`
}

// EmployeePersonnelInfoEdit -
type EmployeePersonnelInfoEdit struct {
	TitleNameTh     string    `json:"titlename_th"`
	FirstNameTh     string    `json:"firstname_th"`
	LastNameTh      string    `json:"lastname_th"`
	TitleNameEn     string    `json:"titlename_en"`
	FirstNameEn     string    `json:"firstname_en"`
	LastNameEn      string    `json:"lastname_en"`
	DateOfBirth     time.Time `json:"date_of_birth"`
	Gender          string    `json:"gender"`
	Height          float64   `json:"height"`
	Weight          float64   `json:"weight"`
	BloodType       string    `json:"blood_type"`
	PregnancyStatus string    `json:"pregnancy_status"`
	Nationality     string    `json:"nationality"`
	Religion        string    `json:"religion"`
	MaritalStatus   string    `json:"marital_status"`
	UpdatedBy       int       `json:"updated_by"`
}

// EmployeeAddressInfoEdit -
type EmployeeAddressInfoEdit struct {
	ID          int    `json:"id"`
	AddressType string `json:"address_type"`
	Country     string `json:"country"`
	Province    string `json:"province"`
	Address     string `json:"address"`
	PostalCode  string `json:"postal_code"`
	UpdatedBy   int    `json:"updated_by"`
}

// EmployeeContactInfoEdit -
type EmployeeContactInfoEdit struct {
	CompanyEmail               string                           `json:"company_email"`
	ContactInfoMobilePhoneInfo []ContactInfoMobilePhoneInfoEdit `json:"contact_info_mobile_phone_info"`
	UpdatedBy                  int                              `json:"updated_by"`
}

//ContactInfoMobilePhoneInfoEdit table model -
type ContactInfoMobilePhoneInfoEdit struct {
	ID                int    `json:"id"`
	MobilePhoneNumber string `json:"mobile_phone_number"`
	UpdatedBy         int    `json:"updated_by"`
}

// EmployeeEmergencyContactInfo -
type EmployeeEmergencyContactInfo struct {
	ID                int    `json:"id"`
	Name              string `json:"name"`
	Relative          string `json:"relative"`
	MobilePhoneNumber string `json:"mobile_phone_number"`
	UpdatedBy         int    `json:"updated_by"`
}

// CreateEmployee -
func CreateEmployee(employee *model.Employee, user UserInput, userLogin UserDetails) (model.Employee, error) {
	roleFilter := map[string]interface{}{}
	positionResult, _ := GetPosition(employee.EmployeeGeneralInfo.PositionID)
	if positionResult.IsManager {
		roleFilter["name"] = "manager"

		getRoleUrl, _ := url.Parse(URIEnvMapping.AuthGetRolesInternal)
		q := getRoleUrl.Query()
		if name, ok := roleFilter["name"].(string); ok && name != "" {
			q.Set("name", name)
		}
		getRoleUrl.RawQuery = q.Encode()
		roleRes, roleErr := rest.CallRestAPIWithOption("GET", getRoleUrl.String(), nil)
		if roleErr != nil {
			return model.Employee{}, errors.New("can't get role")
		}
		roleResult := roleRes.Data.([]interface{})
		var role Role
		for _, item := range roleResult {
			jsonRole, _ := json.Marshal(item)
			json.Unmarshal(jsonRole, &role)
		}
		user.RoleID = role.ID

	}
	secret, err := password.Generate(32, 10, 0, false, false)
	user.Password = secret

	userID, createUserErr := CreateUser(user)
	if createUserErr != nil {
		if createUserErr.Error() == "UserCreationFail" {
			return model.Employee{}, createUserErr
		}
		if createUserErr.Error() == "Username already exists" {
			//Get user by username
			filter := map[string]interface{}{}
			filter["username"] = employee.EmployeeContactInfo.CompanyEmail
			res, _, _ := GetUserList(filter)
			userID = res[0].ID
		}
	}

	employee.StartDate = helpers.StartOfDateNow()
	employee.UserID = userID

	employee.CreatedBy = userLogin.UserID
	employee.UpdatedBy = userLogin.UserID

	employee.EmployeePersonnelInfo.CreatedBy = userLogin.UserID
	employee.EmployeePersonnelInfo.UpdatedBy = userLogin.UserID

	var employeeAddress []model.EmployeeAddressInfo
	for _, item := range employee.EmployeeAddressInfo {
		item.CreatedBy = userLogin.UserID
		item.UpdatedBy = userLogin.UserID
		employeeAddress = append(employeeAddress, item)
	}
	employee.EmployeeAddressInfo = employeeAddress

	employee.EmployeeContactInfo.CreatedBy = userLogin.UserID
	employee.EmployeeContactInfo.UpdatedBy = userLogin.UserID

	var employeeContactMobile []model.ContactInfoMobilePhoneInfo
	for _, item := range employee.EmployeeContactInfo.ContactInfoMobilePhoneInfo {
		item.CreatedBy = userLogin.UserID
		item.UpdatedBy = userLogin.UserID
		employeeContactMobile = append(employeeContactMobile, item)
	}
	employee.EmployeeContactInfo.ContactInfoMobilePhoneInfo = employeeContactMobile

	var employeeEmergencyContactInfo []model.EmployeeEmergencyContactInfo
	for _, item := range employee.EmployeeEmergencyContactInfo {
		item.CreatedBy = userLogin.UserID
		item.UpdatedBy = userLogin.UserID
		employeeEmergencyContactInfo = append(employeeEmergencyContactInfo, item)
	}
	employee.EmployeeEmergencyContactInfo = employeeEmergencyContactInfo

	employee.EmployeeGeneralInfo.CreatedBy = userLogin.UserID
	employee.EmployeeGeneralInfo.UpdatedBy = userLogin.UserID

	url := fmt.Sprintf(URIEnvMapping.PAInternalEmployee)
	res, err := rest.CallRestAPIWithOption("POST", url, &employee)
	if err != nil {
		return model.Employee{}, errors.New("can't create employee")
	}

	result := res.Data.(map[string]interface{})
	var employeeParsed model.Employee
	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &employeeParsed)

	//Personnel information
	var employeePersonnelInfoParsed model.EmployeePersonnelInfo
	jsonEmployeePersonnelData, _ := json.Marshal(employeeParsed.EmployeePersonnelInfo)
	json.Unmarshal(jsonEmployeePersonnelData, &employeePersonnelInfoParsed)

	var oldDataEmployeePersonnel model.EmployeePersonnelInfo

	//Diff with blank object
	personnelInfoChangelog, err := diff.Diff(oldDataEmployeePersonnel, employeePersonnelInfoParsed)
	if personnelInfoChangelog == nil {
		return model.Employee{}, errors.New("employee input values ​​have not changed")
	}

	errPersonnelChangelog := PostChangelog(personnelInfoChangelog, employeeParsed.ID, CREATE_ACTLOG, EMP_PERSONNEL_IFT, EMPLOYEE_CONTENT, userLogin)
	if errPersonnelChangelog != nil {
		return model.Employee{}, errors.New("create changelog not success")
	}

	//General information
	var employeeGeneralInfoParsed model.EmployeeGeneralInfo
	jsonEmployeeGeneralData, _ := json.Marshal(employeeParsed.EmployeeGeneralInfo)
	json.Unmarshal(jsonEmployeeGeneralData, &employeeGeneralInfoParsed)

	var oldDataEmployeeGeneral model.EmployeeGeneralInfo

	//Diff with blank object
	generalInfoChangelog, err := diff.Diff(oldDataEmployeeGeneral, employeeGeneralInfoParsed)
	if generalInfoChangelog == nil {
		return model.Employee{}, errors.New("employee input values ​​have not changed")
	}

	errGeneralChangelog := PostChangelog(generalInfoChangelog, employeeParsed.ID, CREATE_ACTLOG, EMP_GENERAL_IFT, EMPLOYEE_CONTENT, userLogin)
	if errGeneralChangelog != nil {
		return model.Employee{}, errors.New("create changelog not success")
	}

	//Address information
	var employeeAddressInfoParsed []model.EmployeeAddressInfo
	jsonEmployeeAddressData, _ := json.Marshal(employeeParsed.EmployeeAddressInfo)
	json.Unmarshal(jsonEmployeeAddressData, &employeeAddressInfoParsed)

	var oldDataEmployeeAddress []model.EmployeeAddressInfo

	//Diff with blank object
	addressInfoChangelog, err := diff.Diff(oldDataEmployeeAddress, employeeAddressInfoParsed)
	if addressInfoChangelog == nil {
		return model.Employee{}, errors.New("employee input values ​​have not changed")
	}

	errAddressChangelog := PostChangelog(addressInfoChangelog, employeeParsed.ID, CREATE_ACTLOG, EMP_ADDRESS_IFT, EMPLOYEE_CONTENT, userLogin)
	if errAddressChangelog != nil {
		return model.Employee{}, errors.New("create changelog not success")
	}

	//Contact information
	var employeeContactInfoParsed model.EmployeeContactInfo
	jsonEmployeeContactData, _ := json.Marshal(employeeParsed.EmployeeContactInfo)
	json.Unmarshal(jsonEmployeeContactData, &employeeContactInfoParsed)

	var oldDataEmployeeContact model.EmployeeContactInfo

	//Diff with blank object
	contactInfoChangelog, err := diff.Diff(oldDataEmployeeContact, employeeContactInfoParsed)
	if contactInfoChangelog == nil {
		return model.Employee{}, errors.New("employee input values ​​have not changed")
	}

	errContactChangelog := PostChangelog(contactInfoChangelog, employeeParsed.ID, CREATE_ACTLOG, EMP_CONTACT_IFT, EMPLOYEE_CONTENT, userLogin)
	if errContactChangelog != nil {
		return model.Employee{}, errors.New("create changelog not success")
	}

	//Emergency Contact information
	var employeeEmergencyContactInfoParsed model.EmployeeEmergencyContactInfo
	jsonEmployeeEmergencyContactData, _ := json.Marshal(employeeParsed.EmployeeEmergencyContactInfo)
	json.Unmarshal(jsonEmployeeEmergencyContactData, &employeeEmergencyContactInfoParsed)

	var oldDataEmergencyEmployeeContact model.EmployeeEmergencyContactInfo

	//Diff with blank object
	emergencyContactInfoChangelog, err := diff.Diff(oldDataEmergencyEmployeeContact, employeeEmergencyContactInfoParsed)

	errEmergencyContactChangelog := PostChangelog(emergencyContactInfoChangelog, employeeParsed.ID, CREATE_ACTLOG, EMP_EMERGENCY_CONTACT_IFT, EMPLOYEE_CONTENT, userLogin)
	if errEmergencyContactChangelog != nil {
		return model.Employee{}, errors.New("create changelog not success")
	}

	// Create user detail struct for create user detail
	if createUserErr != nil {
		if createUserErr.Error() == "Username already exists" {
			//UpdateUserDetail(employee_id)
			filter := map[string]interface{}{}
			filter["user_id"] = userID
			userDetailRes, _, getUserDetailerr := GetUserDetailList(filter)
			if getUserDetailerr != nil {
				return model.Employee{}, getUserDetailerr
			}
			userDetail := model.UserDetailInput{
				ID:         userDetailRes[0].ID,
				EmployeeID: employeeParsed.ID,
			}
			_, putUserErr := PutUserDetail(userDetail)
			if putUserErr != nil {
				return model.Employee{}, putUserErr
			}
			return employeeParsed, nil
		}
	}

	var userDetail = UserDetailInput{
		UserID:     userID,
		EmployeeID: employeeParsed.ID,
		CompanyID:  employeeGeneralInfoParsed.CompanyID,
	}
	_, _ = CreateUserDetail(userDetail) //create user detail (not sure for return)

	// Create Password Session
	secretForPwds, err := password.Generate(32, 10, 0, false, false)
	passwordSession := model.PasswordSessionInput{
		Secret: secretForPwds,
		UserID: userID,
	}
	_, pwdsErr := CreatePasswordSession(passwordSession)
	if pwdsErr != nil {
		return model.Employee{}, pwdsErr
	}

	// Send Email
	// Create user Template ID: 7
	inboxTemplate, err := GetInboxTemplate(7)
	if err != nil {
		return model.Employee{}, err
	}

	inboxTemplate.Description = strings.Replace(inboxTemplate.Description, "--username--", employee.EmployeeContactInfo.CompanyEmail, 1)

	inbox := mapInboxWithTemplate(inboxTemplate)

	inbox.Receiver = employee.EmployeeContactInfo.CompanyEmail
	//TODO: เพิ่ม secret จาก password session
	filter := map[string]interface{}{}
	filter["username"] = employee.EmployeeContactInfo.CompanyEmail
	userForEmail, _, _ := GetUserList(filter)
	filter["user_id"] = userForEmail[0].ID
	pwds, _, _ := GetPasswordSessionList(filter)
	secretForEmail := pwds[0].Secret

	inbox.Link = inbox.Link + "secret?" + secretForEmail

	inboxID, err := PostInbox(inbox)
	if err != nil || inboxID == 0 {
		return model.Employee{}, err
	}
	// End Send Email

	return employeeParsed, nil
}

//GetEmployeeByPersonalID Employee information by personal id -
func GetEmployeeByPersonalID(id int) (employee model.Employee, err error) {

	url := fmt.Sprintf(URIEnvMapping.PAInternalEmployee+"/%d", id)
	res, _ := rest.CallRestAPIWithOption("GET", url, &employee)
	if res.Total <= 0 {
		return employee, errors.New("Cannot get employee")
	}

	result := res.Data.(map[string]interface{})
	var employeeParsed model.Employee
	jsonData, _ := json.Marshal(result)
	json.Unmarshal(jsonData, &employeeParsed)

	return employeeParsed, nil
}

//GetEmployeeList -
func GetEmployeeByFilter(filter map[string]interface{}) (result []model.Employee, total int, err error) {
	var employee []model.Employee

	pathURL, _ := url.Parse(URIEnvMapping.PAInternalEmployees)
	q := pathURL.Query()

	if personal_id, ok := filter["personal_id"].(int); ok && personal_id > 0 {
		q.Set("personal_id", fmt.Sprintf("%d", personal_id))
	}
	if offset, ok := filter["offset"].(int); ok && offset >= 0 {
		q.Set("offset", fmt.Sprintf("%d", offset))
	}
	if limit, ok := filter["limit"].(int); ok && limit > 0 {
		q.Set("limit", fmt.Sprintf("%d", limit))
	}
	if name, ok := filter["name"].(string); ok && name != "" {
		q.Set("name", name)
	}
	if company_employee_id, ok := filter["company_employee_id"].(string); ok && company_employee_id != "" {
		q.Set("company_employee_id", company_employee_id)
	}
	if company_id, ok := filter["company_id"].(int); ok && company_id > 0 {
		q.Set("company_id", fmt.Sprintf("%d", company_id))
	}
	if user_id, ok := filter["user_id"].(int); ok && user_id > 0 {
		q.Set("user_id", fmt.Sprintf("%d", user_id))
	}
	if gender, ok := filter["gender"].(string); ok && gender != "" {
		q.Set("gender", gender)
	}
	if status, ok := filter["status"].(string); ok && status != "" {
		q.Set("status", status)
	}
	if position_id, ok := filter["position_id"].(int); ok && position_id > 0 {
		q.Set("position_id", fmt.Sprintf("%d", position_id))
	}
	if point_of_time, ok := filter["point_of_time"].(string); ok && point_of_time != "" {
		q.Set("point_of_time", point_of_time)
	}
	if citizen_id, ok := filter["citizen_id"].(string); ok && citizen_id != "" {
		q.Set("citizen_id", citizen_id)
	}
	if company_email, ok := filter["company_email"].(string); ok && company_email != "" {
		q.Set("company_email", company_email)
	}
	if work_type_id, ok := filter["work_type_id"].(int); ok && work_type_id > 0 {
		q.Set("work_type_id", fmt.Sprintf("%d", work_type_id))
	}
	pathURL.RawQuery = q.Encode()

	res, err := rest.CallRestAPIWithOption("GET", pathURL.String(), &employee)
	
	if res.Data == nil {
		result = []model.Employee{}
		return
	}

	data := res.Data.([]interface{})
	total = res.Total
	for _, employeeInformation := range data {
		var jsonEmployee model.Employee
		// decodeError := mapstructure.Decode(employeeInformation, &jsonEmployee)
		// if decodeError != nil {
		// 	err = errors.New("Cannot get employee 2")
		// 	return
		// }
		jsonOldinputEmployeeData, _ := json.Marshal(employeeInformation)
		json.Unmarshal(jsonOldinputEmployeeData, &jsonEmployee)
		result = append(result, jsonEmployee)
	}
	return
}

func GetEmployeeByInfoType(id int, infoType InfoTypeGet) (result *EmployeePersonnelInfoEdit, err error) {
	urlGetEmployee := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/%s/%d", infoType.String(), id)
	resGetEmployee, err := rest.CallRestAPIWithOption("GET", urlGetEmployee, &result)

	if err != nil {
		err = errors.New("cannot get employee")
		return
	}

	getEmployeeData := resGetEmployee.Data.(interface{})
	jsonGetEmployeeData, _ := json.Marshal(getEmployeeData)
	json.Unmarshal(jsonGetEmployeeData, &result)
	return
}

//PutEmployeePersonalInformation employee peresonal information
func PutEmployeePersonalInformation(id int, inputEmployee map[string]interface{}, userLogin UserDetails) (result EmployeePersonnelInfoEdit, err error) {
	urlGetEmployee := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/personnel_info/%d", id)
	resGetEmployee, err := rest.CallRestAPIWithOption("GET", urlGetEmployee, &result)
	if err != nil {
		err = errors.New("cannot get employee")
		return
	}

	if inputEmployee["date_of_birth"] != nil {
		dateOfBirth, _ := time.Parse(time.RFC3339, inputEmployee["date_of_birth"].(string))
		inputEmployee["date_of_birth"] = dateOfBirth
	}

	//Marshal input employee information
	var inputEmployeeParsed EmployeePersonnelInfoEdit
	jsonOldinputEmployeeData, _ := json.Marshal(inputEmployee)
	json.Unmarshal(jsonOldinputEmployeeData, &inputEmployeeParsed)
	inputEmployeeParsed.UpdatedBy = userLogin.UserID

	//Marshal get employee information
	getEmployeeData := resGetEmployee.Data.(interface{})
	var getEmployeeDataParsed *EmployeePersonnelInfoEdit
	jsonGetEmployeeData, _ := json.Marshal(getEmployeeData)
	json.Unmarshal(jsonGetEmployeeData, &getEmployeeDataParsed)

	getEmployeeDataParsed.TitleNameTh = ifStr(inputEmployeeParsed.TitleNameTh != "", inputEmployeeParsed.TitleNameTh, getEmployeeDataParsed.TitleNameTh)
	getEmployeeDataParsed.FirstNameTh = ifStr(inputEmployeeParsed.FirstNameTh != "", inputEmployeeParsed.FirstNameTh, getEmployeeDataParsed.FirstNameTh)
	getEmployeeDataParsed.LastNameTh = ifStr(inputEmployeeParsed.LastNameTh != "", inputEmployeeParsed.LastNameTh, getEmployeeDataParsed.LastNameTh)
	getEmployeeDataParsed.TitleNameEn = ifStr(inputEmployeeParsed.TitleNameEn != "", inputEmployeeParsed.TitleNameEn, getEmployeeDataParsed.TitleNameEn)
	getEmployeeDataParsed.FirstNameEn = ifStr(inputEmployeeParsed.FirstNameEn != "", inputEmployeeParsed.FirstNameEn, getEmployeeDataParsed.FirstNameEn)
	getEmployeeDataParsed.DateOfBirth = ifDate(!inputEmployeeParsed.DateOfBirth.IsZero(), inputEmployeeParsed.DateOfBirth, getEmployeeDataParsed.DateOfBirth)
	getEmployeeDataParsed.LastNameEn = ifStr(inputEmployeeParsed.LastNameEn != "", inputEmployeeParsed.LastNameEn, getEmployeeDataParsed.LastNameEn)
	getEmployeeDataParsed.Gender = ifStr(inputEmployeeParsed.Gender != "", inputEmployeeParsed.Gender, getEmployeeDataParsed.Gender)
	getEmployeeDataParsed.Height = ifFloat(inputEmployeeParsed.Height > 0.0, inputEmployeeParsed.Height, getEmployeeDataParsed.Height)
	getEmployeeDataParsed.Weight = ifFloat(inputEmployeeParsed.Weight > 0.0, inputEmployeeParsed.Weight, getEmployeeDataParsed.Weight)
	getEmployeeDataParsed.BloodType = ifStr(inputEmployeeParsed.BloodType != "", inputEmployeeParsed.BloodType, getEmployeeDataParsed.BloodType)
	getEmployeeDataParsed.PregnancyStatus = ifStr(inputEmployeeParsed.PregnancyStatus != "", inputEmployeeParsed.PregnancyStatus, getEmployeeDataParsed.PregnancyStatus)
	getEmployeeDataParsed.Nationality = ifStr(inputEmployeeParsed.Nationality != "", inputEmployeeParsed.Nationality, getEmployeeDataParsed.Nationality)
	getEmployeeDataParsed.Religion = ifStr(inputEmployeeParsed.Religion != "", inputEmployeeParsed.Religion, getEmployeeDataParsed.Religion)
	getEmployeeDataParsed.MaritalStatus = ifStr(inputEmployeeParsed.MaritalStatus != "", inputEmployeeParsed.MaritalStatus, getEmployeeDataParsed.MaritalStatus)
	getEmployeeDataParsed.UpdatedBy = ifInt(inputEmployeeParsed.UpdatedBy > 0, inputEmployeeParsed.UpdatedBy, getEmployeeDataParsed.UpdatedBy)

	//Update
	url := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/personnel_info/%d", id)
	res, _ := rest.CallRestAPIWithOption("PUT", url, &getEmployeeDataParsed)

	if res.Data == true {
		err = errors.New("cannot update employee personnel information")
		return
	}

	return *getEmployeeDataParsed, err
}

//PutEmployeePersonalInformationWithChangelog -
func PutEmployeePersonalInformationWithChangelog(id int, inputEmployee map[string]interface{}, loginUser UserDetails) (result bool, err error) {

	// Step 1: Get old information
	before := func() (interface{}, error) {
		return GetEmployeeByInfoType(id, PERSONNEL_IFT_NAME)
	}

	// Step 2: The service update or create process
	process := func() (interface{}, error) {
		return PutEmployeePersonalInformation(id, inputEmployee, loginUser)
	}

	// Step 3: Get new information
	after := func() (interface{}, error) {
		return GetEmployeeByInfoType(id, PERSONNEL_IFT_NAME)
	}

	// Changelog Option
	var changelogOption = ChangelogOption{
		ContentID:   id,
		ActionType:  UPDATE_ACTLOG,
		InfoType:    EMP_PERSONNEL_IFT,
		ContentType: EMPLOYEE_CONTENT,
	}

	success, _ := ChangelogProcess(
		before,
		process,
		after,
		loginUser,
		changelogOption,
	)
	if success == false {
		return result, errors.New("changelog process is fail")
	}

	return true, err
}

//PutEmployeeAddressInformation employee address information
func PutEmployeeAddressInformation(id int, inputEmployee []interface{}, userLogin UserDetails) (result []EmployeeAddressInfoEdit, err error) {
	urlGetEmployee := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/address_info/%d", id)
	resGetEmployee, err := rest.CallRestAPIWithOption("GET", urlGetEmployee, &result)
	if err != nil {
		err = errors.New("cannot get employee")
		return
	}

	//Marshal input employee information
	var inputEmployeeAddressDataParsedList []EmployeeAddressInfoEdit
	for _, item := range inputEmployee {
		var address EmployeeAddressInfoEdit
		jsonInputEmployeeAddressData, _ := json.Marshal(item)
		json.Unmarshal(jsonInputEmployeeAddressData, &address)
		address.UpdatedBy = userLogin.UserID
		inputEmployeeAddressDataParsedList = append(inputEmployeeAddressDataParsedList, address)
	}

	//Marshal get employee information
	getEmployeeData := resGetEmployee.Data.([]interface{})
	var getEmployeeDataParsed []EmployeeAddressInfoEdit
	jsonGetEmployeeData, _ := json.Marshal(getEmployeeData)
	json.Unmarshal(jsonGetEmployeeData, &getEmployeeDataParsed)

	//Copy get employee information
	oldDataGetEmployeeParsed := deepcopy.Copy(getEmployeeDataParsed)

	for index, inputValue := range inputEmployeeAddressDataParsedList {
		getEmployeeDataParsed[index].AddressType = ifStr(inputValue.AddressType != "", inputValue.AddressType, getEmployeeDataParsed[index].AddressType)
		getEmployeeDataParsed[index].Country = ifStr(inputValue.Country != "", inputValue.Country, getEmployeeDataParsed[index].Country)
		getEmployeeDataParsed[index].Province = ifStr(inputValue.Province != "", inputValue.Province, getEmployeeDataParsed[index].Province)
		getEmployeeDataParsed[index].Address = ifStr(inputValue.Address != "", inputValue.Address, getEmployeeDataParsed[index].Address)
		getEmployeeDataParsed[index].PostalCode = ifStr(inputValue.PostalCode != "", inputValue.PostalCode, getEmployeeDataParsed[index].PostalCode)
		getEmployeeDataParsed[index].UpdatedBy = ifInt(inputValue.UpdatedBy > 0, inputValue.UpdatedBy, getEmployeeDataParsed[index].UpdatedBy)
	}

	//Diff 2 object
	changelog, err := diff.Diff(oldDataGetEmployeeParsed, getEmployeeDataParsed)
	if changelog == nil {
		return result, errors.New("employee input values ​​have not changed")
	}

	//Update
	url := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/address_info/%d", id)
	res, _ := rest.CallRestAPIWithOption("PUT", url, &getEmployeeDataParsed)

	if res.Data == true {
		err = errors.New("cannot update employee address information")
		return
	}

	//Post Changelog API
	errChangelog := PostChangelog(changelog, id, UPDATE_ACTLOG, EMP_ADDRESS_IFT, EMPLOYEE_CONTENT, userLogin)
	if errChangelog != nil {
		return result, errors.New("create changelog not success")
	}

	return getEmployeeDataParsed, err
}

//PutEmployeeContactInformation employee contact information
func PutEmployeeContactInformation(id int, inputEmployee map[string]interface{}, userLogin UserDetails) (result EmployeeContactInfoEdit, err error) {
	urlGetEmployee := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/contact_info/%d", id)
	resGetEmployee, err := rest.CallRestAPIWithOption("GET", urlGetEmployee, &result)
	if err != nil {
		err = errors.New("cannot get employee")
		return
	}

	//Marshal input employee information
	var inputEmployeeContactDataParsed EmployeeContactInfoEdit
	jsonInputEmployeeContactData, _ := json.Marshal(inputEmployee)
	json.Unmarshal(jsonInputEmployeeContactData, &inputEmployeeContactDataParsed)
	var employeeContactMobile []ContactInfoMobilePhoneInfoEdit
	inputEmployeeContactDataParsed.UpdatedBy = userLogin.UserID
	for _, item := range inputEmployeeContactDataParsed.ContactInfoMobilePhoneInfo {
		item.UpdatedBy = userLogin.UserID
		employeeContactMobile = append(employeeContactMobile, item)
	}
	inputEmployeeContactDataParsed.ContactInfoMobilePhoneInfo = employeeContactMobile

	//Marshal get employee information
	getEmployeeData := resGetEmployee.Data.(interface{})
	var getEmployeeDataParsed *EmployeeContactInfoEdit
	jsonGetEmployeeData, _ := json.Marshal(getEmployeeData)
	json.Unmarshal(jsonGetEmployeeData, &getEmployeeDataParsed)

	oldDataGetEmployeeParsed := deepcopy.Copy(getEmployeeDataParsed)

	getEmployeeDataParsed.CompanyEmail = ifStr(inputEmployeeContactDataParsed.CompanyEmail != "", inputEmployeeContactDataParsed.CompanyEmail, getEmployeeDataParsed.CompanyEmail)
	lenContactGet := len(getEmployeeDataParsed.ContactInfoMobilePhoneInfo) - 1
	lenContactInput := len(inputEmployeeContactDataParsed.ContactInfoMobilePhoneInfo) - 1
	for index, inputValue := range inputEmployeeContactDataParsed.ContactInfoMobilePhoneInfo {
		if index <= lenContactGet {
			getEmployeeDataParsed.ContactInfoMobilePhoneInfo[index].MobilePhoneNumber = ifStr(inputValue.MobilePhoneNumber != "", inputValue.MobilePhoneNumber, getEmployeeDataParsed.ContactInfoMobilePhoneInfo[index].MobilePhoneNumber)
			getEmployeeDataParsed.ContactInfoMobilePhoneInfo[index].UpdatedBy = ifInt(inputValue.UpdatedBy > 0, inputValue.UpdatedBy, getEmployeeDataParsed.ContactInfoMobilePhoneInfo[index].UpdatedBy)
		}
		if index > lenContactGet {
			getEmployeeDataParsed.ContactInfoMobilePhoneInfo = append(getEmployeeDataParsed.ContactInfoMobilePhoneInfo, inputValue)
		}
	}
	getEmployeeDataParsed.UpdatedBy = ifInt(inputEmployeeContactDataParsed.UpdatedBy > 0, inputEmployeeContactDataParsed.UpdatedBy, getEmployeeDataParsed.UpdatedBy)
	var employeeContact []ContactInfoMobilePhoneInfoEdit
	if lenContactInput < lenContactGet {
		for index, contactInfoGet := range getEmployeeDataParsed.ContactInfoMobilePhoneInfo {
			if index <= lenContactInput {
				employeeContact = append(employeeContact, contactInfoGet)
			}
		}
		getEmployeeDataParsed.ContactInfoMobilePhoneInfo = employeeContact
	}

	//Diff 2 object
	changelog, err := diff.Diff(oldDataGetEmployeeParsed, getEmployeeDataParsed)
	if changelog == nil {
		return result, errors.New("employee input values ​​have not changed")
	}

	//Update
	url := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/contact_info/%d", id)
	res, _ := rest.CallRestAPIWithOption("PUT", url, &getEmployeeDataParsed)

	if res.Data == true {
		err = errors.New("cannot update employee address information")
		return
	}

	employee, employeeErr := GetEmployeeByPersonalID(id)
	if employeeErr != nil {
		return EmployeeContactInfoEdit{}, employeeErr
	}

	var userInput = model.UserInput{
		ID:       employee.UserID,
		Username: inputEmployeeContactDataParsed.CompanyEmail,
	}

	_, userErr := PutUser(userInput)
	if userErr != nil {
		return EmployeeContactInfoEdit{}, userErr
	}

	//Post Changelog API
	errChangelog := PostChangelog(changelog, id, UPDATE_ACTLOG, EMP_CONTACT_IFT, EMPLOYEE_CONTENT, userLogin)
	if errChangelog != nil {
		return result, errors.New("create changelog not success")
	}

	return *getEmployeeDataParsed, err
}

//PutEmployeeEmergencyContactInformation employee emergency contact information
func PutEmployeeEmergencyContactInformation(id int, inputEmployee []interface{}, userLogin UserDetails) (result []EmployeeEmergencyContactInfo, err error) {
	urlGetEmployee := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/emergncy_contact_info/%d", id)
	resGetEmployee, err := rest.CallRestAPIWithOption("GET", urlGetEmployee, &result)
	if err != nil {
		err = errors.New("cannot get employee")
		return
	}

	//Marshal input employee information
	var inputEmployeeEmergencyContactDataParsed []EmployeeEmergencyContactInfo
	for _, item := range inputEmployee {
		var emergency EmployeeEmergencyContactInfo
		jsonInputEmployeeEmergencyContactData, _ := json.Marshal(item)
		json.Unmarshal(jsonInputEmployeeEmergencyContactData, &emergency)
		emergency.UpdatedBy = userLogin.UserID
		inputEmployeeEmergencyContactDataParsed = append(inputEmployeeEmergencyContactDataParsed, emergency)
	}

	//Marshal get employee information
	getEmployeeData := resGetEmployee.Data.([]interface{})
	var getEmployeeDataParsed []EmployeeEmergencyContactInfo
	jsonGetEmployeeData, _ := json.Marshal(getEmployeeData)
	json.Unmarshal(jsonGetEmployeeData, &getEmployeeDataParsed)

	//Copy get employee information
	oldDataGetEmployeeParsed := deepcopy.Copy(getEmployeeDataParsed)

	for index, inputValue := range inputEmployeeEmergencyContactDataParsed {
		getEmployeeDataParsed[index].Name = ifStr(inputValue.Name != "", inputValue.Name, getEmployeeDataParsed[index].Name)
		getEmployeeDataParsed[index].Relative = ifStr(inputValue.Relative != "", inputValue.Relative, getEmployeeDataParsed[index].Relative)
		getEmployeeDataParsed[index].MobilePhoneNumber = ifStr(inputValue.MobilePhoneNumber != "", inputValue.MobilePhoneNumber, getEmployeeDataParsed[index].MobilePhoneNumber)
		getEmployeeDataParsed[index].UpdatedBy = ifInt(inputValue.UpdatedBy > 0, inputValue.UpdatedBy, getEmployeeDataParsed[index].UpdatedBy)
	}

	//Diff 2 object
	changelog, err := diff.Diff(oldDataGetEmployeeParsed, getEmployeeDataParsed)
	if changelog == nil {
		return result, errors.New("employee input values ​​have not changed")
	}

	//Update
	url := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/emergncy_contact_info/%d", id)
	res, _ := rest.CallRestAPIWithOption("PUT", url, &getEmployeeDataParsed)

	if res.Data == true {
		err = errors.New("cannot update employee emergency contact information")
		return
	}

	//Post Changelog API
	errChangelog := PostChangelog(changelog, id, UPDATE_ACTLOG, EMP_EMERGENCY_CONTACT_IFT, EMPLOYEE_CONTENT, userLogin)
	if errChangelog != nil {
		return result, errors.New("create changelog not success")
	}

	return getEmployeeDataParsed, err
}

//PutEmployeeByAction employee peresonal information
func PutEmployeeByAction(id int, inputEmployee map[string]interface{}, userLogin UserDetails) (result EmployeeEdit, err error) {
	urlGetEmployee := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/%d", id)
	resGetEmployee, err := rest.CallRestAPIWithOption("GET", urlGetEmployee, &result)
	if err != nil {
		err = errors.New("cannot get employee")
		return
	}

	var inputEmployeeGeneralDataParsed EmployeeEdit
	jsoninputEmployeeGeneralDataParsed, _ := json.Marshal(inputEmployee)
	json.Unmarshal(jsoninputEmployeeGeneralDataParsed, &inputEmployeeGeneralDataParsed)
	inputEmployeeGeneralDataParsed.UpdatedBy = userLogin.UserID

	getEmployeeData := resGetEmployee.Data.(interface{})
	var getEmployeeDataParsed *EmployeeEdit
	jsonGetEmployeeData, _ := json.Marshal(getEmployeeData)
	json.Unmarshal(jsonGetEmployeeData, &getEmployeeDataParsed)

	//Copy get employee information
	oldDataGetEmployeeParsed := deepcopy.Copy(getEmployeeDataParsed)

	getEmployeeDataParsed.EndDate = ifDate(!inputEmployeeGeneralDataParsed.EndDate.IsZero(), inputEmployeeGeneralDataParsed.EndDate, getEmployeeDataParsed.EndDate)
	getEmployeeDataParsed.UpdatedBy = ifInt(inputEmployeeGeneralDataParsed.UpdatedBy > 0, inputEmployeeGeneralDataParsed.UpdatedBy, getEmployeeDataParsed.UpdatedBy)

	//Diff
	changelog, err := diff.Diff(oldDataGetEmployeeParsed, getEmployeeDataParsed)
	if changelog == nil {
		return result, errors.New("employee input values ​​have not changed")
	}

	//Update
	url := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/general_info/%d", id)
	res, _ := rest.CallRestAPIWithOption("PUT", url, &getEmployeeDataParsed)

	if res.Data == true {
		err = errors.New("cannot update employee personnel information")
		return
	}

	//Post Changelog API
	errChangelog := PostChangelog(changelog, id, UPDATE_ACTLOG, EMP_GENERAL_IFT, EMPLOYEE_CONTENT, userLogin)
	if errChangelog != nil {
		return result, errors.New("create changelog not success")
	}

	return *getEmployeeDataParsed, err
}

//PutEmployeeGeneralInformation employee general information
func PutEmployeeGeneralInformation(id int, inputEmployee map[string]interface{}, userLogin UserDetails) (result EmployeeGeneralInfoEdit, err error) {
	urlGetEmployee := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/general_info/%d", id)
	resGetEmployee, err := rest.CallRestAPIWithOption("GET", urlGetEmployee, &result)
	if err != nil {
		err = errors.New("cannot get employee")
		return
	}

	if inputEmployee["probation_end_date"] != nil {
		ProbationEndDate, _ := time.Parse(time.RFC3339, inputEmployee["probation_end_date"].(string))
		inputEmployee["probation_end_date"] = ProbationEndDate
	}
	if inputEmployee["start_date"] != nil {
		StartDate, _ := time.Parse(time.RFC3339, inputEmployee["start_date"].(string))
		inputEmployee["start_date"] = StartDate
	}

	//Marshal input employee information
	var inputEmployeeParsed EmployeeGeneralInfoEdit
	jsonOldinputEmployeeData, _ := json.Marshal(inputEmployee)
	json.Unmarshal(jsonOldinputEmployeeData, &inputEmployeeParsed)
	inputEmployeeParsed.UpdatedBy = userLogin.UserID

	//Marshal get employee information
	getEmployeeData := resGetEmployee.Data.(interface{})
	var getEmployeeDataParsed *EmployeeGeneralInfoEdit
	jsonGetEmployeeData, _ := json.Marshal(getEmployeeData)
	json.Unmarshal(jsonGetEmployeeData, &getEmployeeDataParsed)

	//Copy get employee information
	oldDataGetEmployeeParsed := deepcopy.Copy(getEmployeeDataParsed)

	getEmployeeDataParsed.CompanyEmployeeID = ifStr(inputEmployeeParsed.CompanyEmployeeID != "", inputEmployeeParsed.CompanyEmployeeID, getEmployeeDataParsed.CompanyEmployeeID)
	getEmployeeDataParsed.TypeOfStaff = ifStr(inputEmployeeParsed.TypeOfStaff != "", inputEmployeeParsed.TypeOfStaff, getEmployeeDataParsed.TypeOfStaff)
	getEmployeeDataParsed.EmployeeLevel = ifStr(inputEmployeeParsed.EmployeeLevel != "", inputEmployeeParsed.EmployeeLevel, getEmployeeDataParsed.EmployeeLevel)
	getEmployeeDataParsed.WorkTypeID = ifInt(inputEmployeeParsed.WorkTypeID > 0, inputEmployeeParsed.WorkTypeID, getEmployeeDataParsed.WorkTypeID)
	if inputEmployeeParsed.ProbationEndDate != nil {
		getEmployeeDataParsed.ProbationEndDate = ifDatePointer(!inputEmployeeParsed.ProbationEndDate.IsZero(), inputEmployeeParsed.ProbationEndDate, getEmployeeDataParsed.ProbationEndDate)
	}
	getEmployeeDataParsed.PositionID = ifInt(inputEmployeeParsed.PositionID > 0, inputEmployeeParsed.PositionID, getEmployeeDataParsed.PositionID)
	getEmployeeDataParsed.OrgUnitID = ifInt(inputEmployeeParsed.OrgUnitID > 0, inputEmployeeParsed.OrgUnitID, getEmployeeDataParsed.OrgUnitID)
	getEmployeeDataParsed.CompanyBranchID = ifInt(inputEmployeeParsed.CompanyBranchID > 0, inputEmployeeParsed.CompanyBranchID, getEmployeeDataParsed.CompanyBranchID)
	getEmployeeDataParsed.CompanyID = ifInt(inputEmployeeParsed.CompanyID > 0, inputEmployeeParsed.CompanyID, getEmployeeDataParsed.CompanyID)
	getEmployeeDataParsed.UpdatedBy = ifInt(inputEmployeeParsed.UpdatedBy > 0, inputEmployeeParsed.UpdatedBy, getEmployeeDataParsed.UpdatedBy)
	getEmployeeDataParsed.StartDate = ifDate(!inputEmployeeParsed.StartDate.IsZero(), inputEmployeeParsed.StartDate, getEmployeeDataParsed.StartDate)
	//Diff
	changelog, err := diff.Diff(oldDataGetEmployeeParsed, getEmployeeDataParsed)
	if changelog == nil {
		return result, errors.New("employee input values ​​have not changed")
	}

	//Update
	url := fmt.Sprintf(URIEnvMapping.PAInternal+"/employee/general_info/%d", id)
	res, _ := rest.CallRestAPIWithOption("PUT", url, &getEmployeeDataParsed)

	if res.Data == true {
		err = errors.New("cannot update employee personnel information")
		return
	}

	//Post Changelog API
	errChangelog := PostChangelog(changelog, id, UPDATE_ACTLOG, EMP_GENERAL_IFT, EMPLOYEE_CONTENT, userLogin)
	if errChangelog != nil {
		return result, errors.New("create changelog not success")
	}

	return *getEmployeeDataParsed, err
}

func mapInboxWithTemplate(inboxTemplate model.InboxTemplate) (inbox model.Inbox) {
	inbox.Title = inboxTemplate.Title
	inbox.Sender = inboxTemplate.Sender
	inbox.Description = inboxTemplate.Description
	inbox.Footer = inboxTemplate.Footer
	inbox.ButtonName = inboxTemplate.ButtonName
	inbox.Link = inboxTemplate.Link

	return
}

func ifStr(condition bool, ifTrue string, ifFalse string) string {
	if condition {
		return ifTrue
	}
	return ifFalse
}

func ifFloat(condition bool, ifTrue float64, ifFalse float64) float64 {
	if condition {
		return ifTrue
	}
	return ifFalse
}

func ifInt(condition bool, ifTrue int, ifFalse int) int {
	if condition {
		return ifTrue
	}
	return ifFalse
}

func ifDate(condition bool, ifTrue time.Time, ifFalse time.Time) time.Time {
	if condition {
		return ifTrue
	}
	return ifFalse
}

func ifDatePointer(condition bool, ifTrue *time.Time, ifFalse *time.Time) *time.Time {
	if condition {
		return ifTrue
	}
	return ifFalse
}
