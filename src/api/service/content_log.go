package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"dv.co.th/hrms/core/rest"

	// "github.com/davecgh/go-spew/spew"
	"github.com/r3labs/diff"
)

// ChangelogFieldPost -
type ChangelogFieldPost struct {
	Field string `json:"field"`
	From  string `json:"from"`
	To    string `json:"to"`
}

// ChangelogRecordInput -
type ChangelogRecordInput struct {
	Action      string               `json:"action"`
	ContentID   string               `json:"content_id"`
	ContentType string               `json:"content_type"`
	EditBy      ChangelogEditer      `json:"edit_by"`
	Logs        []ChangelogFieldPost `json:"logs"`
	SecretHash  string               `json:"secret_hash"`
}

// ChangelogRecordGet -
type ChangelogRecordGet struct {
	Hash      string               `json:"hash"`
	Action    string               `json:"action"`
	EditBy    ChangelogEditer      `json:"edit_by"`
	Timestamp string               `json:"timestamp"`
	Logs      []ChangelogFieldPost `json:"logs"`
}

// ChangelogContentGet -
type ChangelogContentGet struct {
	ID          string               `json:"id"`
	ContentType string               `json:"content_type"`
	ContentID   string               `json:"content_id"`
	Record      []ChangelogRecordGet `json:"record"`
	SecretHash  string               `json:"secret_hash"`
}

// ChangelogList -
type ChangelogList struct {
	Changelog ChangelogContentGet `json:"changelog"`
	Total     int                 `json:"total"`
}

// ChangelogEditer -
type ChangelogEditer struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type ChangelogOption struct {
	ContentID   int            `json:"content_id"`
	ActionType  ActionLog      `json:"action_changelog"`
	InfoType    InfoTypeLog    `json:"info_type"`
	ContentType ContentTypeLog `json:"content_type"`
}

// Enum action and infoType for changelog
type ActionLog int
type InfoTypeLog int
type ContentTypeLog int
type InfoTypeGet int

const (
	DEFAULT_ACTLOG ActionLog = iota
	CREATE_ACTLOG
	UPDATE_ACTLOG
)

const (
	DEFAULT_IFT InfoTypeLog = iota
	EMP_PERSONNEL_IFT
	EMP_GENERAL_IFT
	EMP_ADDRESS_IFT
	EMP_CONTACT_IFT
	EMP_EMERGENCY_CONTACT_IFT
	REIMBURSEMENT_REPORT
)

const (
	DEFAULT_INFOTYPE InfoTypeGet = iota
	PERSONNEL_IFT_NAME
	GENERAL_IFT_NAME
	ADDRESS_IFT_NAME
	CONTACT_IFT_NAME
	EMERGENCY_CONTACT_IFT_NAME
)

const (
	EMPLOYEE_CONTENT ContentTypeLog = iota
	REIMBURSEMENT_CONTENT
)

var actionLog = [...]string{
	"undefined",
	"create",
	"update",
}

var infoTypeLog = [...]string{
	"undefined",
	"EmployeePersonnelInfo",
	"EmployeeGeneralInfo",
	"EmployeeAddressInfo",
	"EmployeeContactInfo",
	"EmployeeEmergencyContactInfo",
	"FoodReimbursementRequest",
}

var contentTypeLog = [...]string{
	"employee",
	"reimbursement_report",
}

var infoTypeGet = [...]string{
	"undefined",
	"personnel_info",
	"general_info",
	"address_info",
	"contact_info",
	"emergncy_contact_info",
}

func (a ActionLog) String() string      { return actionLog[a] }
func (a InfoTypeLog) String() string    { return infoTypeLog[a] }
func (a ContentTypeLog) String() string { return contentTypeLog[a] }
func (a InfoTypeGet) String() string    { return infoTypeGet[a] }

//postChangelog changelog
func PostChangelog(changelog diff.Changelog, id int, actionChangelog ActionLog, infoType InfoTypeLog, contentType ContentTypeLog, loginUser UserDetails) (err error) {

	//Append item to changelogparsed
	var changelogParsed []ChangelogFieldPost

	for _, item := range changelog {
		fromStrValue := fmt.Sprintf("%v", item.From)
		toStrValue := fmt.Sprintf("%v", item.To)
		var tmp ChangelogFieldPost
		field := infoType.String()

		if fromStrValue == "" || fromStrValue == "0" || fromStrValue == "0001-01-01 00:00:00 +0000 UTC" || fromStrValue == "<nil>" {
			fromStrValue = "-"
		}

		for _, path := range item.Path {
			field = field + "." + path
		}

		tmp = ChangelogFieldPost{
			Field: field,
			From:  fromStrValue,
			To:    toStrValue,
		}

		changelogParsed = append(changelogParsed, tmp)
	}

	loginUserID := strconv.Itoa(loginUser.UserID)

	//Summary Changelog
	var ChangelogInformation *ChangelogRecordInput
	mapChangelog := ChangelogRecordInput{
		Action:      actionChangelog.String(),
		ContentID:   strconv.Itoa(id),
		ContentType: contentType.String(),
		EditBy: ChangelogEditer{
			ID:   loginUserID,
			Name: loginUser.FirstName + " " + loginUser.LastName,
		},
		Logs: changelogParsed,
	}
	ChangelogInformation = &mapChangelog

	//Post Changelog API
	urlpostChangelog := fmt.Sprintf(URIEnvMapping.LOGSInternal)
	_, err = rest.CallRestAPIWithOption("POST", urlpostChangelog, &ChangelogInformation)

	return
}

//GetEmployeeList -
func GetChangelogList(contentType ContentTypeLog, contentId int) (result ChangelogContentGet, total int, err error) {
	var changelog interface{}

	url := fmt.Sprintf(URIEnvMapping.LOGSInternal+"/%s/%d", contentType.String(), contentId)
	res, err := rest.CallRestAPIWithOption("GET", url, &changelog)

	if err != nil {
		err = errors.New("Cannot get change log")
		return
	}

	data,ok := res.Data.(interface{})
	if !ok {
		err = errors.New("Cannot get change log")
		return
	}
	total = res.Total

	jsonChangelogContentGetParsed, _ := json.Marshal(data)
	json.Unmarshal(jsonChangelogContentGetParsed, &result)

	return
}

//ChangelogProcess -
func ChangelogProcess(
	before func() (interface{}, error),
	process func() (interface{}, error),
	after func() (interface{}, error),
	loginUser UserDetails,
	changelogOption ChangelogOption,
) (bool, interface{}) {
	old, err := before()
	if err != nil {
		return false, err
	}
	if _, err := process(); err == nil {
		new, err := after()
		if err != nil {
			return false, err
		}

		// The process of comparing new and old data
		// Diff 2 struct
		changelog, err := diff.Diff(old, new)
		if changelog == nil {
			return false, errors.New("input values ​​have not changed")
		}
		//Post Changelog API
		changelogErr := PostChangelog(changelog, changelogOption.ContentID, changelogOption.ActionType, changelogOption.InfoType, changelogOption.ContentType, loginUser)
		if changelogErr != nil {
			return false, errors.New("create changelog not success")
		}
		// End Process

		return true, nil
	}
	err = err
	return false, nil
}
