module api

go 1.12

require (
	dv.co.th/hrms/core v0.0.0
	github.com/ajg/form v1.5.1 // indirect
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/davecgh/go-spew v1.1.1
	github.com/gavv/monotime v0.0.0-20190418164738-30dba4353424 // indirect
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/graphql-go/graphql v0.7.8
	github.com/graphql-go/handler v0.2.3
	github.com/imkira/go-interpol v1.1.0 // indirect
	github.com/iris-contrib/httpexpect v0.0.0-20180314041918-ebe99fcebbce // indirect
	github.com/iris-contrib/middleware v0.0.0-20190720041902-5f3a20f489c4
	github.com/jinzhu/gorm v1.9.10 // indirect
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/kataras/iris v11.1.1+incompatible
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mitchellh/mapstructure v1.1.2
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/onsi/ginkgo v1.10.1 // indirect
	github.com/onsi/gomega v1.7.0 // indirect
	github.com/r3labs/diff v0.0.0-20190801153147-a71de73c46ad
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/sethvargo/go-password v0.1.3
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/swaggo/swag v1.6.2
	github.com/thoas/go-funk v0.4.0
	github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xeipuuv/gojsonschema v1.1.0 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
	github.com/yudai/gojsondiff v1.0.0 // indirect
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	github.com/yudai/pp v2.0.1+incompatible // indirect
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/sys v0.0.0-20190602015325-4c4f7f33c9ed // indirect
)

replace dv.co.th/hrms/core v0.0.0 => /go/src/core
