package request

// TMLCreateApprovalList - Create Approval List for TM Leave
type TMLCreateApprovalList struct {
	EmployeeID 		int          `json:"employee_id"`
	LeaveRequestID  int          `json:"leave_request_id"`
}
