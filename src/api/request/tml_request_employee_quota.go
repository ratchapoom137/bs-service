package request

import "api/model"

//TMLCreateEmployeeQuota - request
type TMLCreateEmployeeQuota struct {
	LeaveTypeID int               `json:"leave_type_id"`
	Employees   []TMLEmployeeInfo `json:"employees"`
}

//TMLEditEmployeeQuotaAfterLeaveType - request
type TMLEditEmployeeQuotaAfterLeaveType struct {
	Employees []TMLEmployeeInfo `json:"employees"`
	LeaveType model.LeaveType   `json:"leave_type"`
}

// TMLEmployeeInfo -
type TMLEmployeeInfo struct {
	EmployeeID int    `json:"employee_id"`
	Gender     string `json:"gender"`
}
