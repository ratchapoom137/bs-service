package request

//APITMLInitEmployeeQuota - 
type APITMLInitEmployeeQuota struct {
	EmployeeID int       `json:"employee_id"`
	Gender 	   string    `json:"gender"`
	CompanyID  int 		 `json:"company_id"`
}
