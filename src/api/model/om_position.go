package model

import (
	"time"
)

type Position struct {
	ID          int        `json:"id"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	OrgUnitID   int        `json:"org_unit_id"`
	EmployeeID  int        `json:"employee_id"`
	IsManager   bool       `json:"is_manager"`
	EffectiveAt time.Time  `json:"effective_at,omitempty"`
	TerminateAt *time.Time `json:"terminate_at,omitempty"`
	CreatedAt   time.Time  `json:"created_at"`
	UpdatedAt   time.Time  `json:"updated_at"`
}

type PositionList struct {
	Positions []Position `json:"org_units"`
	Total     int        `json:"total"`
}

type PositionResponseMessage struct {
	PositionID int    `json:"position_id"`
	Message    string `json:"message"`
}

type PositionListResponseMessage struct {
	PositionsID []int  `json:"org_units_id"`
	Message     string `json:"message"`
}
