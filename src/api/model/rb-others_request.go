package model

import (
	"time"
)

type Others struct {
	ID          int        `json:"id" mapstructure:"id"`
	RequestID   int        `json:"request_id" mapstructure:"request_id"`
	Information string     `json:"information" mapstructure:"information"`
	Date        string     `json:"date" mapstructure:"date"`
	CreatedBy   int        `json:"created_by" mapstructure:"created_by"`
	UpdatedBy   int        `json:"updated_by" mapstructure:"updated_by"`
	CreatedAt   string     `json:"created_at" mapstructure:"created_at"`
	UpdatedAt   string     `json:"updated_at" mapstructure:"updated_at"`
	DeletedAt   *time.Time `json:"deleted_at,omitempty"`
}

type OthersReimbursementRequest struct {
	ID            int                            `json:"id" mapstructure:"id"`
	RequesterID   int                            `json:"requester_id" mapstructure:"requester_id"`
	ChargeCode    string                         `json:"charge_code" mapstructure:"charge_code"`
	Type          string                         `json:"type" mapstructure:"type"`
	Cost          float64                        `json:"cost" mapstructure:"cost"`
	Status        string                         `json:"status" mapstructure:"status"`
	ApproverIDs   []Approver                     `json:"approvers" mapstructure:"approvers"`
	ApproverID    int                            `json:"approver_id" mapstructure:"approver_id"`
	ReviewerID    int                            `json:"reviewer_id" mapstructure:"reviewer_id" `
	ReviewerIDs   []ReimbursementRequestReviewer `json:"reviewers" mapstructure:"reviewers"`
	FileIDs       []ReimbursementRequestFile     `json:"file_ids" mapstructure:"file_ids"`
	ProcessedDate string                         `json:"processed_date" mapstructure:"processed_date"`
	Date          string                         `json:"date" mapstructure:"date"`
	Description   string                         `json:"description" mapstructure:"description"`
	Remark        string                         `json:"remark" mapstructure:"remark"`
	CreatedBy     int                            `json:"created_by" mapstructure:"created_by"`
	UpdatedBy     int                            `json:"updated_by" mapstructure:"updated_by"`
	CreatedAt     string                         `json:"created_at" mapstructure:"created_at"`
	UpdatedAt     string                         `json:"updated_at" mapstructure:"updated_at"`
	DeletedAt     *time.Time                     `json:"deleted_at,omitempty"`
	Information   string                         `json:"information" mapstructure:"information"`
}