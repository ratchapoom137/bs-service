package model

import (
	"time"
)

type AssetCategory struct {
	ID        int        `json:"id"`
	Name      string     `json:"name"`
	CompanyID int        `json:"company_id"`
	CreatedBy int        `json:"created_by"`
	UpdatedBy int        `json:"updated_by"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at,omitempty"`
}

type AssetCategoryList struct {
	Categories []AssetCategory `json:"categories"`
	Total      int             `json:"total"`
}
