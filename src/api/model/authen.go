package model

import "time"

// UserAuth -
type UserAuth struct {
	ID int `json:"id"`
}

// User -
type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Role     Role   `json:"role"`
}

// Role -
type Role struct {
	ID          int          `json:"id,omitempty"`
	Name        string       `json:"name"`
	Permissions []Permission `json:"permissions,omitempty"`
}

// Permission -
type Permission struct {
	Module   string `json:"module"`
	Function string `json:"function"`
}

type UserResponse struct {
	ID      int    `json:"id"`
	Message string `json:json:"message"`
}

type UserResponseMessage struct {
	Message string `json:json:"message"`
}

type UserInput struct {
	ID       int    `json:"id"`
	RoleID   int    `json:"role_id"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type PasswordSession struct {
	ID        int        `json:"id" gorm:"column:id; type:int unsigned auto_increment; primary_key; unique;"`
	Secret    string     `json:"secret" sql:"not null" gorm:"column:secret"`
	UserID    int        `json:"user_id" sql:"not null" gorm:"column:user_id"`
	CreatedAt time.Time  `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt time.Time  `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

type PasswordSessionInput struct {
	ID     int    `json:"id"`
	Secret string `json:"secret"`
	UserID int    `json:"user_id"`
}

type UserCreationResponse struct {
	UserID          int    `json:"user_id"`
	UserDetailID    int    `json:"user_detail_id"`
	PasswordSession int    `json:"password_session_id"`
	Message         string `json:json:"message"`
}
 
type UserPassword struct {
	Username	string		`json:"username"`
	Password	string		`json:"password"`
}

type SecretResponse struct {
	User		[]PasswordSession	`json:"user"`
	Message     string  `json:json:"message"`
}