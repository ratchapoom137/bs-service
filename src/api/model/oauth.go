package model

// OAuthToken -
type OAuthToken struct {
	AccessToken      string `json:"access_token,omitempty"`
	RefreshToken     string `json:"refresh_token,omitempty"`
	ExpiresIn        int    `json:"expires_in,omitempty"`
	ErrorDescription string `json:"error_description,omitempty"`
}

// OAuthTokenInfo -
type OAuthTokenInfo struct {
	UserID    string `json:"user_id,omitempty"`
	ClientID  string `json:"client_id,omitempty"`
	Scope     string `json:"scope,omitempty"`
	ExpiresIn int    `json:"expires_in,omitempty"`
}
