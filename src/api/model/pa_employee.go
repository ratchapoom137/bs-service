package model

import (
	"time"
)

//Employee table model -
type Employee struct {
	ID                           int                            `json:"id"`
	CitizenID                    string                         `json:"citizen_id"`
	UserID					     int							`json:"user_id"`
	EmployeePersonnelInfo        *EmployeePersonnelInfo         `json:"personnel_info"`
	EmployeeAddressInfo          []EmployeeAddressInfo          `json:"address_info"`
	EmployeeContactInfo          *EmployeeContactInfo           `json:"contact_info"`
	EmployeeEmergencyContactInfo []EmployeeEmergencyContactInfo `json:"emergency_contact_info"`
	EmployeeGeneralInfo          *EmployeeGeneralInfo           `json:"general_info"`
	CreatedBy                    int                            `json:"created_by"`
	UpdatedBy                    int                            `json:"updated_by"`
	CreatedAt                    time.Time                      `json:"created_at"`
	UpdatedAt                    time.Time                      `json:"updated_at"`
	DeletedAt                    *time.Time                     `json:"deleted_at"`
	StartDate					 time.Time 						`json:"start_date"`
	EndDate					     time.Time 						`json:"end_date"`	
}

//EmployeePersonnelInfo table model -
type EmployeePersonnelInfo struct {
	ID              int        `json:"id"`
	EmployeeInfoID  int        `json:"employee_info_id"`
	TitleNameTh     string     `json:"titlename_th"`
	FirstNameTh     string     `json:"firstname_th"`
	LastNameTh      string     `json:"lastname_th"`
	TitleNameEn     string     `json:"titlename_en"`
	FirstNameEn     string     `json:"firstname_en"`
	LastNameEn      string     `json:"lastname_en"`
	DateOfBirth     time.Time  `json:"date_of_birth"`
	Gender          string     `json:"gender"`
	Height          float64    `json:"height"`
	Weight          float64    `json:"weight"`
	BloodType       *string    `json:"blood_type"`
	PregnancyStatus string     `json:"pregnancy_status"`
	Nationality     *string    `json:"nationality"`
	Religion        *string    `json:"religion"`
	MaritalStatus   *string    `json:"marital_status"`
	CreatedBy       int        `json:"created_by"`
	UpdatedBy       int        `json:"updated_by"`
	CreatedAt       time.Time  `json:"created_at"`
	UpdatedAt       time.Time  `json:"updated_at"`
	DeletedAt       *time.Time `json:"deleted_at"`
}

//EmployeeGeneralInfo table model -
type EmployeeGeneralInfo struct {
	ID                int        `json:"id"`
	EmployeeInfoID    int        `json:"employee_info_id"`
	CompanyEmployeeID string     `json:"company_employee_id"`
	TypeOfStaff       string     `json:"type_of_staff"`
	EmployeeLevel     string     `json:"employee_level"`
	WorkTypeID        int        `json:"work_type_id"`
	ProbationEndDate  *time.Time `json:"probation_end_date"`
	PositionID        int        `json:"position_id"`
	OrgUnitID         int        `json:"org_unit_id"`
	CompanyBranchID   int        `json:"company_branch_id"`
	CompanyID         int        `json:"company_id"`
	CreatedBy         int        `json:"created_by"`
	UpdatedBy         int        `json:"updated_by"`
	CreatedAt         time.Time  `json:"created_at"`
	UpdatedAt         time.Time  `json:"updated_at"`
	DeletedAt         *time.Time `json:"deleted_at,omitempty"`
}

//EmployeeEmergencyContactInfo table model -
type EmployeeEmergencyContactInfo struct {
	ID                int        `json:"id"`
	EmployeeInfoID    int        `json:"employee_info_id"`
	Name              string     `json:"name"`
	Relative          string     `json:"relative"`
	MobilePhoneNumber string     `json:"mobile_phone_number"`
	CreatedBy         int        `json:"created_by"`
	UpdatedBy         int        `json:"updated_by"`
	CreatedAt         time.Time  `json:"created_at"`
	UpdatedAt         time.Time  `json:"updated_at"`
	DeletedAt         *time.Time `json:"deleted_at"`
}

//EmployeeContactInfo table model -
type EmployeeContactInfo struct {
	ID                         int                          `json:"id"`
	EmployeeInfoID             int                          `json:"employee_info_id"`
	CompanyEmail               string                       `json:"company_email"`
	ContactInfoMobilePhoneInfo []ContactInfoMobilePhoneInfo `json:"contact_info_mobile_phone_info"`
	CreatedBy                  int                          `json:"created_by"`
	UpdatedBy                  int                          `json:"updated_by"`
	CreatedAt                  time.Time                    `json:"created_at"`
	UpdatedAt                  time.Time                    `json:"updated_at"`
	DeletedAt                  *time.Time                   `json:"deleted_at"`
}

//ContactInfoMobilePhoneInfo table model -
type ContactInfoMobilePhoneInfo struct {
	ID                int        `json:"id"`
	MobileInfoID      int        `json:"mobile_info_id"`
	MobilePhoneNumber string     `json:"mobile_phone_number"`
	CreatedBy         int        `json:"created_by"`
	UpdatedBy         int        `json:"updated_by"`
	CreatedAt         time.Time  `json:"created_at"`
	UpdatedAt         time.Time  `json:"updated_at"`
	DeletedAt         *time.Time `json:"deleted_at"`
}

type UserDetail struct {
	ID					int					`json:"id"`
	UserID				int					`json:"user_id"`
	EmployeeID			int					`json:"employee_id"`
	CompanyID			int					`json:"company_id"`
	FirstName			string				`json:"first_name"`
	LastName			string				`json:"last_name"`
	DeactivatedDate	    time.Time			`json:"deactivated_date"`
	ExpiredDate			time.Time			`json:"expired_date"`
	CreatedAt           time.Time       	`json:"created_at"`
	UpdatedAt           time.Time       	`json:"updated_at"`
	DeletedAt           *time.Time      	`json:"deleted_at"`
}

type UserDetailList struct {
	UserDetails []UserDetail `json:"user_details"`
	Total       int          `json:"total"`
}

type UserDetailInput struct {
	ID					int					`json:"id"`
	UserID				int					`json:"user_id"`
	EmployeeID			int					`json:"employee_id"`
	CompanyID			int					`json:"company_id"`
	FirstName			string				`json:"first_name"`
	LastName			string				`json:"last_name"`
	DeactivatedDate	    time.Time			`json:"deactivated_date"`
	ExpiredDate			time.Time			`json:"expired_date"`
}

type UserDetailAndExpiredDateInput struct {
	ID					int					`json:"id"`
	UserID				int					`json:"user_id"`
	EmployeeID			int					`json:"employee_id"`
	CompanyID			int					`json:"company_id"`
	FirstName			string				`json:"first_name"`
	LastName			string				`json:"last_name"`
	DeactivatedDate	    time.Time			`json:"deactivated_date"`
	ExpiredDate			time.Time			`json:"expired_date"`
}

type UserDetailResponse struct {
	ID          int 		 `json:"id"`
	Message     string       `json:json:"message"`
}