package model

import (
	"time"
)

type EmployeeType int

const (
	OWNER EmployeeType = 1 + iota
	MEMBER
)

var employeeTypes = [...]string{
	"OWNER",
	"MEMBER",
}

// String returns the English name of the employee type ("Owner", "Member").
func (m EmployeeType) String() string { return employeeTypes[m-1] }

type ChargeCode struct {
	ID                int                  `json:"id"`
	Name              string               `json:"name"`
	CompanyID         int                  `json:"company_id"  mapstructure:"company_id"`
	Code              string               `json:"code"`
	StartDate         string               `json:"start_date" mapstructure:"start_date"`
	EndDate           string               `json:"end_date" mapstructure:"end_date"`
	IsActive          bool                 `json:"is_active" mapstructure:"is_active"`
	IsNotWork         bool                 `json:"is_not_work"  mapstructure:"is_not_work"`
	Employees         []EmployeeChargeCode `json:"employees"`
	VisibilityPrivate bool                 `json:"visibility_private" mapstructure:"visibility_private"`
	Description       string               `json:"description" mapstructure:"description"`
	CreatedBy         int                  `json:"created_by" mapstructure:"created_by"`
	UpdatedBy         int                  `json:"updated_by" mapstructure:"updated_by"`
	CreatedAt         time.Time            `json:"created_at"`
	UpdatedAt         time.Time            `json:"updated_at"`
	DeletedAt         *time.Time           `json:"deleted_at,omitempty"`
}
type EmployeeChargeCode struct {
	ID                   int          `json:"id"`
	EmployeeID           int          `json:"employee_id" mapstructure:"employee_id"`
	Type                 EmployeeType `json:"type" mapstructure:"type"`
	EmployeeChargeCodeID int          `json:"employee_charge_code_id" mapstructure:"employee_charge_code_id"`
	RollOn               string       `json:"roll_on" mapstructure:"roll_on"`
	RollOff              string       `json:"roll_off" mapstructure:"roll_off"`
	CreatedBy            int          `json:"created_by"`
	UpdatedBy            int          `json:"updated_by"`
	CreatedAt            time.Time    `json:"created_at"`
	UpdatedAt            time.Time    `json:"updated_at"`
	DeletedAt            *time.Time   `json:"deleted_at,omitempty"`
}

type PutChargeCodeQuery struct {
	ID                 int
	Name               string               `json:"name"`
	StartDate          string               `json:"start_date" mapstructure:"start_date"`
	EndDate            string               `json:"end_date" mapstructure:"end_date"`
	IsActive           bool                 `json:"is_active" mapstructure:"is_active"`
	Employees          []EmployeeChargeCode `json:"employees"`
	VisibilityPrivate  bool                 `json:"visibility_private" mapstructure:"visibility_private"`
	Description        string               `json:"description" mapstructure:"description"`
	DeletedEmployeeIDs []int                `json:"deleted_employee_ids" mapstructure:"deleted_employee_ids"`
	UpdatedBy          int                  `json:"updated_by" mapstructure:"updated_by"`
}
