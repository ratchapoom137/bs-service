package model

import (
	"time"
)

type ApprovalConfig struct {
	ID         int        `json:"id"`
	Name       string     `json:"name"`
	CompanyID  int        `json:"company_id"`
	ConfigType string     `json:"config_type"`
	CreatedBy  int        `json:"created_by"`
	UpdatedBy  int        `json:"updated_by"`
	CreatedAt  time.Time  `json:"created_at"`
	UpdatedAt  time.Time  `json:"updated_at"`
	DeletedAt  *time.Time `json:"deleted_at,omitempty"`
}

type ApprovalConfigDetail struct {
	ID               int        `json:"id"`
	ApprovalConfigID int        `json:"approval_config_id"`
	ValueID          int        `json:"value_id"`
	CreatedAt        time.Time  `json:"created_at"`
	UpdatedAt        time.Time  `json:"updated_at"`
	DeletedAt        *time.Time `json:"deleted_at,omitempty"`
}

type ApprovalConfigInput struct {
	ID         int    `json:"id" mapstructure:"id"`
	Name       string `json:"name"  `
	CompanyID  int    `json:"company_id" `
	ConfigType string `json:"config_type"  mapstructure:"config_type"`
	ValueID    []int  `json:"value_id"  mapstructure:"value_id"`
	CreatedBy  int    `json:"created_by"  mapstructure:"created_by"`
	UpdatedBy  int    `json:"updated_by"  mapstructure:"updated_by"`
	EmployeeLevelBudget	[]ApprovalConfigBudget `json:"employee_level_budget"  mapstructure:"employee_level_budget"`
}

type ApprovalConfigList struct {
	ApprovalConfigs []ApprovalConfig `json:"approval_configs"`
	Total           int              `json:"total"`
}

type ApprovalConfigResponseMessage struct {
	ApprovalConfigID int    `json:"approval_config_id"`
	Message          string `json:"message"`
}

type ApprovalConfigBudget struct {
	ID                 int             			  `json:"id" mapstructure:"id"`
	ApproverEmployeeLevel      string             `json:"approver_employee_level" mapstructure:"approver_employee_level"`
	Budget		       float64				      `json:"budget"  mapstructure:"budget"`
	ApprovalConfigID int                          `json:"approval_config_id"  mapstructure:"approval_config_id"`
	CreatedBy            int                     `json:"created_by"  mapstructure:"created_by"`
	UpdatedBy            int                     `json:"updated_by"  mapstructure:"updated_by"`
}
