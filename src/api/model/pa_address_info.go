package model

import "time"

//EmployeeAddressInfo table model -
type EmployeeAddressInfo struct {
	ID             int        `json:"id"`
	EmployeeInfoID int        `json:"employee_info_id"`
	AddressType    string     `json:"address_type"`
	Country        string     `json:"country"` 
	Province       string     `json:"province"`
	Address        string     `json:"address"`
	PostalCode     string     `json:"postal_code"`
	CreatedBy      int        `json:"created_by"`
	UpdatedBy      int        `json:"updated_by"`
	CreatedAt      time.Time  `json:"created_at"`
	UpdatedAt      time.Time  `json:"updated_at"`
	DeletedAt      *time.Time `json:"deleted_at"`
}