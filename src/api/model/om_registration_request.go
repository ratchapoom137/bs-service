package model

import "time"

type RegistrationRequest struct {
	ID               int       `json:"id"`
	FirstName        string    `json:"first_name"`
	LastName         string    `json:"last_name"`
	Email            string    `json:"email"`
	PhoneNumber      string    `json:"phone_number"`
	CompanyNameTH    string    `json:"company_name_th"`
	CompanyNameEN    string    `json:"company_name_en"`
	CompanyEmail     string    `json:"company_email"`
	CompanyTelephone string    `json:"company_telephone"`
	CompanyCode      string    `json:"company_code"`
	TaxID            string    `json:"tax_id"`
	Description      string    `json:"description"`
	SecretCode       string    `json:"secret_code"`
	Status           string    `json:"status"`
	RequestDate      time.Time `json:"request_date"`
	CreatedAt        time.Time `json:"created_at"`
	UpdatedAt        time.Time `json:"updated_at"`
	DeletedAt        time.Time `json:"deleted_at"`
}

type RegistrationRequestInput struct {
	ID               int    `json:"id"`
	FirstName        string `json:"first_name"`
	LastName         string `json:"last_name"`
	Email            string `json:"email"`
	PhoneNumber      string `json:"phone_number"`
	CompanyNameTH    string `json:"company_name_th"`
	CompanyNameEN    string `json:"company_name_en"`
	CompanyEmail     string `json:"company_email"`
	CompanyTelephone string `json:"company_telephone"`
	CompanyCode      string `json:"company_code"`
	TaxID            string `json:"tax_id"`
	Description      string `json:"description"`
	SecretCode       string `json:"secret_code"`
	Status           string `json:"status"`
	RequestDate      string `json:"request_date"`
}

type RegistrationRequestList struct {
	RegistrationRequests []RegistrationRequest `json:"registration_requests"`
	Total                int                   `json:"total"`
}

type RegistrationRequestResponseMessage struct {
	RegistrationRequestID int    `json:"id"`
	Message               string `json:"message"`
}

type RegistrationRequestFile struct {
	ID         int    `json:"id"`
	RequestID  int    `json:"request_id"`
	FileSource string `json:"file_source"`
	Name       string `json:"name"`
}

type RegistrationRequestFileList struct {
	RequestFiles []RegistrationRequestFile `json:"request_files"`
	Total        int                       `json:"total"`
}

type RegistrationRequestFileResponseMessage struct {
	RequestFileID int    `json:"id"`
	Message       string `json:"message"`
}

type RegistrationRequestFileListResponseMessage struct {
	RequestFilesID []int  `json:"request_files_id"`
	Message        string `json:"message"`
}
