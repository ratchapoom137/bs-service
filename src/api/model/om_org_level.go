package model

import (
	"time"
	// validator "gopkg.in/go-playground/validator.v9"
)

type OrgLevel struct {
	ID        int        `json:"id"`
	Name      string     `json:"name"`
	Value     int        `json:"value"`
	CompanyID int        `json:"company_id"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at,omitempty"`
}

type OrgLevelList struct {
	OrgLevels []OrgLevel `json:"org_levels"`
	Total     int        `json:"total"`
}
