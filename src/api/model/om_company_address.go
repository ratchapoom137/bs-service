package model

import (
	"time"
)

type CompanyAddress struct {
	ID           int        `json:"id"`
	Name         string     `json:"name"`
	Detail       string     `json:"detail"`
	Postcode     string     `json:"postcode"`
	Province     string     `json:"province"`
	Country      string     `json:"country"`
	Latitude     float64    `json:"latitude"`
	Longitude    float64    `json:"longitude"`
	CompanyID    int        `json:"company_id"`
	IsHeadOffice bool       `json:"is_head_office"`
	CreatedAt    time.Time  `json:"created_at"`
	UpdatedAt    time.Time  `json:"updated_at"`
	DeletedAt    *time.Time `json:"deleted_at,omitempty"`
}

type CompanyAddressList struct {
	Addresses []CompanyAddress `json:"addresses"`
	Total     int              `json:"total"`
}

type CompanyAddressResponseMessage struct {
	AddressID int    `json:"address_id"`
	Message   string `json:"message"`
}

type CompanyAddressListResponseMessage struct {
	AddressesID []int  `json:"addresses_id"`
	Message     string `json:"message"`
}
