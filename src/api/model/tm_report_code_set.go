package model

import (
	"time"
)

//ReportCodeSet table model
type ReportCodeSet struct {
	ID          int                `json:"id"`
	Name        string             `json:"name" `
	EmployeeID  int                `json:"employee_id" mapstructure:"employee_id"`
	ChargeCodes []ReportChargeCode `json:"codes" `
	CreatedBy   int                `json:"created_by" mapstructure:"created_by"`
	UpdatedBy   int                `json:"updated_by"`
	CreatedAt   time.Time          `json:"created_at" mapstructure:"created_at"`
	UpdatedAt   time.Time          `json:"updated_at"`
	DeletedAt   *time.Time         `json:"deleted_at,omitempty"`
}

//ReportChargeCode table model
type ReportChargeCode struct {
	ID           int `json:"id"`
	ChargeCodeID int `json:"charge_code_id" mapstructure:"charge_code_id"`
	ReportCodeID int `json:"report_code_id" mapstructure:"report_code_id"`
}
