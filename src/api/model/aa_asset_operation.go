package model

import "time"

type AssetOperation struct {
	ID         int        `json:"id"`
	AssetID    int        `json:"asset_id"`
	EmployeeID int        `json:"employee_id"`
	StartDate  time.Time  `json:"start_date"`
	DueDate    *time.Time `json:"due_date,omitempty"`
	ReturnDate *time.Time `json:"return_date,omitempty"`
	AssignNote string     `json:"assign_note"`
	ReturnNote string     `json:"return_note"`
	CreatedBy  int        `json:"created_by"`
	UpdatedBy  int        `json:"updated_by"`
	CreatedAt  time.Time  `json:"created_at"`
	UpdatedAt  time.Time  `json:"updated_at"`
}

type AssetOperationInput struct {
	ID         int        `json:"id"`
	AssetID    int        `json:"asset_id"`
	EmployeeID int        `json:"employee_id"`
	StartDate  time.Time  `json:"start_date"`
	DueDate    *time.Time `json:"due_date"`
	ReturnDate *time.Time `json:"return_date"`
	AssignNote string     `json:"assign_note"`
	ReturnNote string     `json:"return_note"`
	CreatedBy  int        `json:"created_by"`
	UpdatedBy  int        `json:"updated_by"`
}

type AssetOperationList struct {
	AssetOperations []AssetOperation `json:"asset_operations"`
	Total           int              `json:"total"`
}

type AssetOperationResponseMessage struct {
	AssetOperationID int    `json:"asset_operation_id`
	Message          string `json:"message"`
}

type AssetOperationListResponseMessage struct {
	AssetOperationsID []int  `json:"asset_operations_id`
	Message           string `json:"message"`
}
