package model

import (
	"time"
)

// HolidayPreset table model
type HolidayPreset struct {
	ID        int        `json:"id" mapstructure:"id"`
	Name      string     `json:"name" mapstructure:"name"`
	Amount    int        `json:"amount" mapstructure:"amount"`
	Holidays  []Holiday  `json:"holidays"`
	CreatedBy int        `json:"created_by" mapstructure:"created_by"`
	UpdatedBy int        `json:"updated_by" mapstructure:"updated_by"`
	CreatedAt *time.Time `json:"created_at" mapstructure:"created_at"`
	UpdatedAt *time.Time `json:"updated_at" mapstructure:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at,omitempty"`
}

// Holiday table model
type Holiday struct {
	ID          int    `json:"id"`
	HolidayName string `json:"holiday_name"  mapstructure:"holiday_name"`
	HolidayDate string `json:"holiday_date"  mapstructure:"holiday_date"`
	MemberID    int
	CreatedBy   int        `json:"created_by"`
	UpdatedBy   int        `json:"updated_by"`
	CreatedAt   *time.Time `json:"created_at" mapstructure:"created_at"`
	UpdatedAt   *time.Time `json:"updated_at" mapstructure:"updated_at"`
	DeletedAt   *time.Time `json:"deleted_at,omitempty"`
}
