package model

import (
	"time"
)

//LeaveTypeStatus - leave type status enum
type LeaveTypeStatus int

const (
	ALL_LEAVE_TYPE LeaveTypeStatus = iota
	ACTIVE
	INACTIVE
)

var leaveTypeStatus = [...]string{
	"all",
	"active",
	"inactive",
}

//ENUM leave type status -
func (a LeaveTypeStatus) String() string { return leaveTypeStatus[a] }

// LeaveType -
type LeaveType struct {
	ID                  int        `json:"id"`
	CompanyID			int		   `json:"company_id"`
	Name                string     `json:"name"`
	Description         string     `json:"description"`
	Gender              string     `json:"gender"`
	MaritalStatus       string     `json:"marital_status" mapstructure:"marital_status"`
	IsLeaveAfterAbsence bool       `json:"is_leave_after_absence" mapstructure:"is_leave_after_absence"`
	IsHolidayCalculate  bool       `json:"is_holiday_calculate" mapstructure:"is_holiday_calculate"`
	IsPerYear           bool       `json:"is_per_year" mapstructure:"is_per_year"`
	IsPerTimes          bool       `json:"is_per_times" mapstructure:"is_per_times"`
	Times               int        `json:"times" mapstructure:"times"`
	QuotaDayPerTimes    float64    `json:"quota_day_per_times" mapstructure:"quota_day_per_times"`
	QuotaHourPerTimes   float64    `json:"quota_hour_per_times" mapstructure:"quota_hour_per_times"`
	QuotaDayPerYear     float64    `json:"quota_day_per_year" mapstructure:"quota_day_per_year"`
	QuotaHourPerYear    float64    `json:"quota_hour_per_year" mapstructure:"quota_hour_per_year"`
	MaxCarriedDay       float64    `json:"max_carried_day" mapstructure:"max_carried_day"`
	MaxCarriedHour      float64    `json:"max_carried_hour" mapstructure:"max_carried_hour"`
	ApprovalConfigID    int        `json:"approval_config_id" mapstructure:"approval_config_id"`
	ChargeCodeID    	int        `json:"charge_code_id" mapstructure:"charge_code_id"`
	Status              bool       `json:"status" mapstructure:"status"`
	CreatedBy           int        `json:"create_by"`
	UpdatedBy           int        `json:"updated_by"`
	CreatedAt           time.Time  `json:"create_at"`
	UpdatedAt           time.Time  `json:"updated_at"`
	DeletedAt           *time.Time `json:"deleted_at,omitempty"`
}

// LeaveTypeList -
type LeaveTypeList struct {
	LeaveTypes   []LeaveType  `json:"leave_types"`
	Total       int           `json:"total"`
}