package model

import (
	"time"
)

// EmployeeQuota -
type EmployeeQuota struct {
	ID          	int        	`json:"id"`
	EmployeeID  	int       	`json:"employee_id" mapstructure:"employee_id"`
	LeaveTypeID 	int        	`json:"leave_type_id" mapstructure:"leave_type_id"`
	LeaveType   	*LeaveType 	`json:"leave_type" mapstructure:"leave_type"`
	QuotaDays       float64    	`json:"quota_days" mapstructure:"quota_days"`
	QuotaHours      float64    	`json:"quota_hours" mapstructure:"quota_hours"`
	TakenDays       float64    	`json:"taken_days" mapstructure:"taken_days"`
	TakenHours      float64    	`json:"taken_hours" mapstructure:"taken_hours"`
	CarriedDays 	float64    	`json:"carried_days" mapstructure:"carried_days"`
	CarriedHours    float64    	`json:"carried_hours" mapstructure:"carried_hours"`
	TimesLeft		int			`json:"times_left" mapstructure:"times_left"`
	TimesTaken		int			`json:"times_taken" mapstructure:"times_taken"`
	CreatedBy  		int        	`json:"created_by"`
	UpdatedBy   	int        	`json:"updated_by"`
	CreatedAt   	time.Time  	`json:"created_at"`
	UpdatedAt   	time.Time  	`json:"updated_at"`
	DeletedAt   	*time.Time 	`json:"deleted_at,omitempty"`
}

// EmployeeQuotaList -
type EmployeeQuotaList struct {
	EmployeeQuotas   []EmployeeQuota	`json:"employee_quotas"`
	Total       	 int				`json:"total"`
}

// EmployeeQuotaDetail -
type EmployeeQuotaDetail struct {
	EmployeeID	int   				`json:"employee_id"`
	Quotas		[]EmployeeQuota     `json:"quotas"`
}