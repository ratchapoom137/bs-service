package model

import (
	"time"
)

type TerminationRequest struct {
	ID            int        `json:"id""`
	CompanyID     int        `json:"company_id"`
	UserID        int        `json:"user_id"`
	RequestedDate time.Time  `json:"requested_date"`
	ApprovedDate  time.Time  `json:"approved_date"`
	CancelDate    time.Time  `json:"cancel_date"`
	CreatedAt     time.Time  `json:"created_at"`
	UpdatedAt     time.Time  `json:"updated_at"`
	DeletedAt     *time.Time `json:"deleted_at`
}

type TerminationRequestInput struct {
	ID            int    `json:"id""`
	CompanyID     int    `json:"company_id"`
	UserID        int    `json:"user_id"`
	RequestedDate string `json:"requested_date"`
	ApprovedDate  string `json:"approved_date"`
	CancelDate    string `json:"cancel_date"`
}

type TerminationRequestResponse struct {
	ID      int    `json:"id"`
	Message string `json:json:"message"`
}

type TerminationRequestList struct {
	TerminationRequests []TerminationRequest `json:"termination_requests"`
	Total               int                  `json:"total"`
}
