package model

import (
	"time"
)

type ReimbursementRequest struct {
	ID            int                            `json:"id" mapstructure:"id"`
	CompanyID     int                            `json:"company_id" mapstructure:"company_id"`
	RequesterID   int                            `json:"requester_id" mapstructure:"requester_id"`
	ChargeCode    string                         `json:"charge_code" mapstructure:"charge_code"`
	Type          string                         `json:"type" mapstructure:"type"`
	Status        string                         `json:"status" mapstructure:"status"`
	Cost          float64                        `json:"cost" mapstructure:"cost"`
	ApproverID    int                            `json:"approver_id" mapstructure:"approver_id"`
	ApproverIDs   []Approver                     `json:"approvers" mapstructure:"approvers"`
	ReviewerID    int                            `json:"reviewer_id" mapstructure:"reviewer_id" `
	ReviewerIDs   []ReimbursementRequestReviewer `json:"reviewers" mapstructure:"reviewers"`
	ProcessedDate string                         `json:"processed_date" mapstructure:"processed_date"`
	Description   string                         `json:"description" mapstructure:"description"`
	FileIDs       []ReimbursementRequestFile     `json:"file_ids" mapstructure:"file_ids"`
	TotalHours    int                            `json:"total_hours" mapstructure:"total_hours" `
	TotalDistance float64                        `json:"total_distance"  mapstructure:"total_distance"`
	Remark        string                         `json:"remark" mapstructure:"remark"`
	BackOfficeID  int                            `json:"backoffice_id"  mapstructure:"backoffice_id"`
	CreatedBy     int                            `json:"created_by" mapstructure:"created_by"`
	UpdatedBy     int                            `json:"updated_by" mapstructure:"updated_by"`
	CreatedAt     time.Time                      `json:"created_at" `
	UpdatedAt     time.Time                      `json:"updated_at"  `
	DeletedAt     *time.Time                     `json:"deleted_at,omitempty"`
}

type ReimbursementRequestFile struct {
	ID        int    `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	Name      string `json:"name" sql:"not null"`
	URL       string `json:"url" sql:"not null"`
	CreatedBy int    `json:"created_by" sql:"not null"`
	UpdatedBy int    `json:"updated_by" sql:"not null"`
}

type FoodReimbursementRequest struct {
	ID            int                            `json:"id" mapstructure:"id"`
	RequesterID   int                            `json:"requester_id" mapstructure:"requester_id"`
	CompanyID     int                            `json:"company_id" mapstructure:"company_id"`
	ChargeCode    string                         `json:"charge_code" mapstructure:"charge_code"`
	Type          string                         `json:"type" mapstructure:"type"`
	Cost          float64                        `json:"cost" `
	Status        string                         `json:"status" mapstructure:"status"`
	ApproverIDs   []Approver                     `json:"approvers" mapstructure:"approvers"`
	ApproverID    int                            `json:"approver_id" mapstructure:"approver_id"`
	ReviewerID    int                            `json:"reviewer_id" mapstructure:"reviewer_id" `
	ReviewerIDs   []ReimbursementRequestReviewer `json:"reviewers" mapstructure:"reviewers"`
	FileIDs       []ReimbursementRequestFile     `json:"file_ids" mapstructure:"file_ids"`
	ProcessedDate string                         `json:"processed_date" mapstructure:"processed_date"`
	Date          string                         `json:"date" mapstructure:"date"`
	Description   string                         `json:"description" mapstructure:"description"`
	Remark        string                         `json:"remark" mapstructure:"remark"`
	CreatedBy     int                            `json:"created_by" mapstructure:"created_by"`
	UpdatedBy     int                            `json:"updated_by" mapstructure:"updated_by"`
	CreatedAt     string                         `json:"created_at" mapstructure:"created_at"`
	UpdatedAt     string                         `json:"updated_at" mapstructure:"updated_at"`
	DeletedAt     *time.Time                     `json:"deleted_at,omitempty"`
	Information   string                         `json:"information" mapstructure:"information"`
}

type FoodAllowance struct {
	ID          int        `json:"id" mapstructure:"id"`
	RequestID   int        `json:"request_id" mapstructure:"request_id"`
	Information string     `json:"information" mapstructure:"information"`
	Date        string     `json:"date" mapstructure:"date"`
	CreatedBy   int        `json:"created_by" `
	UpdatedBy   int        `json:"updated_by" mapstructure:"updated_by"`
	CreatedAt   string     `json:"created_at" `
	UpdatedAt   string     `json:"updated_at" `
	DeletedAt   *time.Time `json:"deleted_at,omitempty"`
}

type FoodReimbursemetList struct {
	FoodRequests []FoodReimbursementRequest `json:"reimbursement_requests"`
	Total        int                        `json:"total"`
}

type ReimbursementRequestList struct {
	Requests []ReimbursementRequest `json:"reimbursement_requests"`
	Total    int                    `json:"total"`
}

type Approver struct {
	ID int `json:"id"`
}

type ReimbursementRequestReviewer struct {
	ID int `json:"id"`
}

type EmployeeLevelValue struct {
	EmployeeLevel int `json:"empployee_level"`
	Value         int `json:"value"`
}

type ReimbursementReport struct {
	Code    string
	Food    []float64
	Travel  []float64
	Perdiem []float64
	Other   []float64
}

type ReimbursementReportList struct {
	Reports []ReimbursementReport `json:"reimbursement_requests" mapstructure::"reimbursement_requests"`
	Total   int                   `json:"total" mapstructure::"total"`
}
