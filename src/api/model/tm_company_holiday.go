package model

import (
	"time"
)

// HolidayCompany table model
type HolidayCompany struct {
	ID                 int             `json:"id"`
	CompanyID          int             `json:"company_id"`
	HolidayCompanyName string          `json:"holidaycompany_name" mapstructure:"holidaycompany_name"`
	HolidayAmount      int             `json:"holiday_amount" mapstructure:"holiday_amount"`
	HolidayDetails     []HolidayDetail `json:"holiday_details" mapstructure:"holiday_details"`
	CreatedBy          int             `json:"created_by" mapstructure:"created_by"`
	UpdatedBy          int             `json:"updated_by" mapstructure:"updated_by"`
	CreatedAt          time.Time       `json:"created_at" `
	UpdatedAt          time.Time       `json:"updated_at"`
	DeletedAt          *time.Time      `json:"deleted_at,omitempty"`
}

// HolidayDetail table model
type HolidayDetail struct {
	ID        int       `json:"id"`
	Name      string    `json:"name"`
	Date      time.Time `json:"date"`
	MemberID  int
	CreatedBy int        `json:"created_by"`
	UpdatedBy int        `json:"updated_by"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at,omitempty"`
}
