package model

import (
	"time"
)

// ApprovalTask -
type ApprovalTask struct {
	ID             int           `json:"id"`
	EmployeeID     int           `json:"employee_id" mapstructure:"employee_id"`
	LeaveRequest   *LeaveRequest `json:"leave_request"`
	LeaveRequestID int           `json:"leave_request_id"`
	Status         string        `json:"status"`
	Comment        string        `json:"comment"`
	CreatedBy      int           `json:"created_by"`
	UpdatedBy      int           `json:"updated_by"`
	CreatedAt      time.Time     `json:"created_at"`
	UpdatedAt      time.Time     `json:"updated_at"`
	DeletedAt      *time.Time    `json:"deleted_at,omitempty"`
}

// ApprovalTaskList -
type ApprovalTaskList struct {
	ApprovalTasks  []ApprovalTask   	`json:"approval_tasks"`
	Total          int                  `json:"total"`
}