package model

import (
	"time"
)

// LeaveRequestDate -
type LeaveRequestDate struct {
	ID             int           `json:"id"`
	LeaveRequestID int           `json:"leave_request_id"`
	Date           time.Time     `json:"date"`
	Period         string        `json:"period"`
	Hour           float64       `json:"hour"`
	CreatedBy      int           `json:"created_by"`
	UpdatedBy      int           `json:"updated_by"`
	CreatedAt      time.Time     `json:"created_at"`
	UpdatedAt      time.Time     `json:"updated_at"`
	DeletedAt      *time.Time    `json:"deleted_at,omitempty"`
}

// Participant -
type Participant struct {
	ID             int           `json:"id"`
	LeaveRequestID int           `json:"leave_request_id"`
	LeaveRequest   *LeaveRequest `json:"leave_request"`
	Email          string        `json:"email"`
	CreatedBy      int           `json:"created_by"`
	UpdatedBy      int           `json:"updated_by"`
	CreatedAt      time.Time     `json:"created_at"`
	UpdatedAt      time.Time     `json:"updated_at"`
	DeletedAt      *time.Time    `json:"deleted_at,omitempty"`
}

// Attachment -
type Attachment struct {
	ID             int           `json:"id"`
	LeaveRequestID int           `json:"leave_request_id"`
	LeaveRequest   *LeaveRequest `json:"leave_request"`
	File           string        `json:"file"`
	CreatedBy      int           `json:"created_by"`
	UpdatedBy      int           `json:"updated_by"`
	CreatedAt      time.Time     `json:"created_at"`
	UpdatedAt      time.Time     `json:"updated_at"`
	DeletedAt      *time.Time    `json:"deleted_at,omitempty"`
}

// LeaveRequest -
type LeaveRequest struct {
	ID               	int                `json:"id"`
	EmployeeID       	int                `json:"employee_id"`
	LeaveTypeID      	int                `json:"leave_type_id"`
	LeaveType        	*LeaveType         `json:"leave_type"`
	LeaveRequestDates 	[]LeaveRequestDate `json:"leave_request_dates"`
	StartDate        	time.Time          `json:"start_date"`
	EndDate          	time.Time          `json:"end_date"`
	TotalDate        	float64            `json:"total_date"`
	TotalHour        	float64            `json:"total_hour"`
	Reason           	string             `json:"reason"`
	ApprovalTasks	 	[]ApprovalTask	   `json:"approval_tasks"`
	Participants     	[]Participant      `json:"participants"`
	Attachments      	[]Attachment       `json:"attachments"`
	Status           	string             `json:"status"`
	CreatedBy        	int                `json:"created_by"`
	UpdatedBy        	int                `json:"updated_by"`
	CreatedAt        	time.Time          `json:"created_at"`
	UpdatedAt        	time.Time          `json:"updated_at"`
	DeletedAt        	*time.Time         `json:"deleted_at,omitempty"`
}

// LeaveRequestList -
type LeaveRequestList struct {
	LeaveRequests   []LeaveRequest   	`json:"leave_requests"`
	Total          int                  `json:"total"`
}