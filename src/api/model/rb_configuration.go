package model

import (
	"time"
)

type FoodApproveConfiguration struct {
	ID              int       `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	ConfigurationID int       `json:"configuration_id" gorm:"unique_index:unique_reimbursement_food_config"`
	ApproveTypeID   int       `json:"approve_type_id"`
	CompanyID       int       `json:"company_id" gorm:"unique_index:unique_reimbursement_food_config"`
	CreatedBy       int       `json:"created_by" sql:"not null"`
	UpdatedBy       int       `json:"updated_by" sql:"not null"`
	CreatedAt       time.Time `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt       time.Time `json:"updated_at" sql:"type:datetime(6)"`
}

//PerDiemApproveConfiguration table model -
type PerDiemApproveConfiguration struct {
	ID              int                                `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	ConfigurationID int                                `json:"configuration_id" gorm:"unique_index:unique_reimbursement_perdiem_config"`
	ApproveTypeID   int                                `json:"approve_type_id"`
	CompanyID       int                                `json:"company_id" gorm:"unique_index:unique_reimbursement_perdiem_config"`
	CreatedBy       int                                `json:"created_by" sql:"not null"`
	UpdatedBy       int                                `json:"updated_by" sql:"not null"`
	CreatedAt       time.Time                          `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt       time.Time                          `json:"updated_at" sql:"type:datetime(6)"`
	PerDiemTable    []PerDiemApproveConfigurationTable `json:"perdiem_table" sql:"not null"`
}

//PerDiemApproveConfigurationTable table model -
type PerDiemApproveConfigurationTable struct {
	ID              int       `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	ConfigurationID int       `json:"configuration_id" gorm:"unique_index:unique_reimbursement_perdiem_config"`
	EmployeeLevelID int       `json:"employee_level_id" gorm:"unique_index:unique_reimbursement_perdiem_config"`
	Rate            float64   `json:"rate"`
	CompanyID       int       `json:"company_id" `
	CreatedBy       int       `json:"created_by" sql:"not null"`
	UpdatedBy       int       `json:"updated_by" sql:"not null"`
	CreatedAt       time.Time `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt       time.Time `json:"updated_at" sql:"type:datetime(6)"`
}

//TravelApproveConfiguration table model -
type TravelApproveConfiguration struct {
	ID              int                               `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	ConfigurationID int                               `json:"configuration_id" gorm:"unique_index:unique_reimbursement_travel_config"`
	ApproveTypeID   int                               `json:"approve_type_id"`
	CompanyID       int                               `json:"company_id" gorm:"unique_index:unique_reimbursement_travel_config"`
	CreatedBy       int                               `json:"created_by" sql:"not null"`
	UpdatedBy       int                               `json:"updated_by" sql:"not null"`
	CreatedAt       time.Time                         `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt       time.Time                         `json:"updated_at" sql:"type:datetime(6)"`
	TravelExpense   TravelApproveConfigurationExpense `json:"travel_expense" sql:"not null"`
}

//TravelApproveConfigurationExpense table model -
type TravelApproveConfigurationExpense struct {
	ID              int       `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	ConfigurationID int       `json:"configuration_id" gorm:"unique_index:unique_reimbursement_travel_config"`
	Expense         float64   `json:"expense"`
	CompanyID       int       `json:"company_id" gorm:"unique_index:unique_reimbursement_travel_config"`
	CreatedBy       int       `json:"created_by" sql:"not null"`
	UpdatedBy       int       `json:"updated_by" sql:"not null"`
	CreatedAt       time.Time `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt       time.Time `json:"updated_at" sql:"type:datetime(6)"`
}

//OtherApproveConfiguration table model -
type OtherApproveConfiguration struct {
	ID              int       `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	ConfigurationID int       `json:"configuration_id" gorm:"unique_index:unique_reimbursement_other_config"`
	ApproveTypeID   int       `json:"approve_type_id"`
	CompanyID       int       `json:"company_id" gorm:"unique_index:unique_reimbursement_other_config"`
	CreatedBy       int       `json:"created_by" sql:"not null"`
	UpdatedBy       int       `json:"updated_by" sql:"not null"`
	CreatedAt       time.Time `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt       time.Time `json:"updated_at" sql:"type:datetime(6)"`
}
