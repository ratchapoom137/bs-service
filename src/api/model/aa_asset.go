package model

import "time"

type Asset struct {
	ID              int       `json:"id"`
	SerialNumber    string    `json:"serial_number"`
	Name            string    `json:"name"`
	Detail          string    `json:"detail"`
	AssetCategoryID int       `json:"asset_category_id"`
	Residual        int       `json:"residual"`
	Status          string    `json:"status"`
	PurchaseDate    time.Time `json:"purchase_date"`
	ExpireDate      time.Time `json:"expire_date"`
	CreatedBy       int       `json:"created_by"`
	UpdatedBy       int       `json:"updated_by"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
}

type AssetInput struct {
	ID              int    `json:"id"`
	SerialNumber    string `json:"serial_number"`
	Name            string `json:"name"`
	Detail          string `json:"detail"`
	AssetCategoryID int    `json:"asset_category_id"`
	Residual        int    `json:"residual"`
	Status          string `json:"status"`
	PurchaseDate    string `json:"purchase_date"` // date string format
	ExpireDate      string `json:"expire_date"`   // date string format
	CreatedBy       int    `json:"created_by"`
	UpdatedBy       int    `json:"updated_by"`
}

type AssetList struct {
	Assets []Asset `json:"assets"`
	Total  int     `json:"total"`
}
