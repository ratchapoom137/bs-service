package model

import (
	"time"
)

type TravelAllowance struct {
	ID                 int       	     `json:"id" mapstructure:"id"`
	RequestID   	   int               `json:"request_id" mapstructure:"request_id"`
	Expend        	   float64           `json:"expend"  mapstructure:"expend" `
	TravelDescription  string            `json:"travel_description" mapstructure:"travel_description"`
	Distance 		   float64     		 `json:"distance" mapstructure:"distance"`
	Date               time.Time         `json:"date" mapstructure:"date"`
	CreatedBy          int               `json:"created_by" mapstructure:"created_by"`
	UpdatedBy          int               `json:"updated_by" mapstructure:"updated_by"`
	CreatedAt          time.Time         `json:"created_at" mapstructure:"created_at"`
	UpdatedAt          time.Time         `json:"updated_at" mapstructure:"updated_at"`
	DeletedAt          *time.Time        `json:"deleted_at,omitempty" `
}

type TravelReimbursementRequest struct {
	ID            int        						`json:"id"   mapstructure:"id"`
	RequesterID   int        						`json:"request_id" mapstructure:"request_id" `
	ChargeCode    string     						`json:"charge_code"  mapstructure:"charge_code"`
	Type          string     						`json:"type"  mapstructure:"type"`
	Cost          float64    						`json:"cost"  mapstructure:"cost"`
	Status        string     						`json:"status"  mapstructure:"status"`
	ApproverIDs   []Approver 						`json:"approvers"  mapstructure:"approvers"`
	ApproverID    int        						`json:"approver_id"  mapstructure:"approver_id"`
	ProcessedDate string     						`json:"processed_date" mapstructure:"processed_date" `
	Date 		  string     						`json:"date"  mapstructure:"date"`
	Description   string     						`json:"description" mapstructure:"description" `
	Remark		  string	 						`json:"remark"  mapstructure:"remark"`
	CreatedBy     int        						`json:"created_by" mapstructure:"created_by"`
	UpdatedBy     int        						`json:"updated_by"  mapstructure:"updated_by"`
	CreatedAt     string     						`json:"created_at"`
	UpdatedAt     string     						`json:"updated_at"  mapstructure:"updated_at"`
	DeletedAt     *time.Time 						`json:"deleted_at,omitempty"`
	TravelAllowances   []TravelAllowance			`json:"travel_allowances"  mapstructure:"travel_allowances"`
	TotalDistance  float64   						`json:"total_distance"  mapstructure:"total_distance"`
	FileIDs       []ReimbursementRequestFile		`json:"file_ids" mapstructure:"file_ids"`
	ReviewerIDs   []ReimbursementRequestReviewer 	`json:"reviewers" mapstructure:"reviewers"`
	ReviewerID    int        						`json:"reviewer_id" mapstructure:"reviewer_id" `
}