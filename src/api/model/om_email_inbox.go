package model

import "time"

type Inbox struct {
	ID          int       `json:"id"`
	Title       string    `json:"title"`
	Sender      string    `json:"sender"`
	Receiver    string    `json:"receiver"`
	Description string    `json:"description"`
	Footer      string    `json:"footer"`
	ButtonName  string    `json:"button_name"`
	Link        string    `json:"link"`
	CreatedAt   time.Time `json:"created_at"`
}

type InboxTemplate struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Sender      string `json:"sender"`
	Description string `json:"description"`
	Footer      string `json:"footer"`
	ButtonName  string `json:"button_name"`
	Link        string `json:"link"`
}

type InboxList struct {
	InboxList []Inbox `json:"inbox_list"`
	Total     int     `json:"total"`
}

type InboxResponseMessage struct {
	InboxID int    `json:"inbox_id"`
	Message string `json:"message"`
}
