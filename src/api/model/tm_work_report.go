package model

import (
	"time"
)

// StatusType --
type StatusType int

const (
	DRAFT StatusType = 1 + iota
	PENDING
	PROCESSED
	ADJUSTMENT_DRAFT
	ADJUSTMENT
	ADJUSTMENT_PROCESSED
)

var statusTypes = [...]string{
	"DRAFT",
	"PENDING",
	"PROCESSED",
	"ADJUSTMENT_DRAFT",
	"ADJUSTMENT",
	"ADJUSTMENT_PROCESSED",
}

// String returns the English name of the status type ("DRAFT", "PENDING").
func (m StatusType) String() string { return statusTypes[m-1] }

///Input Output Models

//WorkProgressInput --
type WorkProgressInput struct {
	ID         int    `json:"id"`
	Date       string `json:"date" `
	WorkHours  int    `json:"work_hours" mapstructure:"work_hours"`
	PeriodID   int    `json:"period_id" mapstructure:"period_id"`
	EmployeeID int    `json:"employee_id" mapstructure:"employee_id"`
	ProjectID  int    `json:"project_id" mapstructure:"project_id"`
}

//WorkAdjustmentsInput --
type WorkAdjustmentsInput struct {
	EmployeeID         int              `json:"employee_id"`
	ProjectID          int              `json:"project_id"`
	PeriodID           int              `json:"period_id"`
	WorkAdjustments    []WorkAdjustment `json:"work_adjustments"`
	AdjustmentPeriodID int              `json:"adjust_period"`
}

//WorkLogInput --
type WorkLogInput struct {
	EmployeeID     int            `json:"employee_id"`
	ProjectID      int            `json:"project_id"`
	PeriodID       int            `json:"period_id"`
	WorkProgresses []WorkProgress `json:"work_progresses"`
	Reviewers      []Reviewer     `json:"reviewers"`
	Files          []File         `json:"files"`
	Description    string         `json:"description" mapstructure:"description"`
}

//WorkProgressProjectOutput --
type WorkProgressProjectOutput struct {
	Status         StatusType           `json:"status"`
	ReportID       int                  `json:"report_id" mapstructure:"report_id"`
	WorkProgresses []WorkProgressOutput `json:"work_progresses" mapstructure:"work_progresses"`
	PeriodID       int                  `json:"period_id"  mapstructure:"period_id"`
	Files          []File               `json:"files"`
	Description    string               `json:"description"`
	Reviewer       []Reviewer           `json:"reviewers" mapstructure:"reviewers"`
	ProjectID      int                  `json:"project_id"  mapstructure:"project_id" `
}

//WorkAdjustmentsProjectOutput --
type WorkAdjustmentsProjectOutput struct {
	Status             StatusType       `json:"status"`
	WorkAdjustments    []WorkAdjustment `json:"work_adjustments" mapstructure:"work_adjustments"`
	PeriodID           int              `json:"period_id" mapstructure:"period_id"`
	Files              []File           `json:"files"`
	Description        string           `json:"description" mapstructure:"description"`
	Reviewer           []Reviewer       `json:"reviewers"`
	ProjectID          int              `json:"project_id" mapstructure:"project_id"`
	AdjustmentPeriodID int              `json:"adjust_period" mapstructure:"adjust_period"`
}

//WorkProgressOutput --
type WorkProgressOutput struct {
	ID         int    `json:"id"`
	Date       string `json:"date"`
	WorkHours  int    `json:"work_hours" mapstructure:"work_hours"`
	PeriodID   int    `json:"period_id" mapstructure:"period_id"`
	EmployeeID int    `json:"employee_id" mapstructure:"employee_id"`
	ProjectID  int    `json:"project_id" mapstructure:"project_id"`
}

//Database Models

//WorkLog --
type WorkLog struct {
	ID          int        `json:"id"`
	Status      StatusType `json:"status" mapstructure:"status"`
	EmployeeID  int        `json:"employee_id" mapstructure:"employee_id"`
	PeriodID    int        `json:"period_id"`
	Period      Period     `json:"period"`
	DraftHour   int        `json:"draft_hour" mapstructure:"draft_hour"`
	TotalHour   int        `json:"total_hour" mapstructure:"total_hour"`
	ProjectID   int        `json:"project_id" mapstructure:"project_id"`
	Description string     `json:"description"`
}

//WorkProgress --
type WorkProgress struct {
	ID         int       `json:"id"`
	Date       time.Time `json:"date"`
	WorkHours  int       `json:"work_hours" mapstructure:"work_hours"`
	PeriodID   int       `json:"period_id" mapstructure:"period_id"`
	EmployeeID int       `json:"employee_id" mapstructure:"employee_id"`
	ProjectID  int       `json:"project_id" mapstructure:"project_id"`
}

//WorkAdjustment --
type WorkAdjustment struct {
	ID                int    `json:"id"`
	IsPendingDelete   bool   `json:"is_pending_delete" mapstructure:"is_pending_delete"`
	AdjustPeriodID    int    `json:"adjust_period" mapstructure:"adjust_period"`
	EmployeeID        int    `json:"employee_id" mapstructure:"employee_id"`
	ProjectID         int    `json:"project_id" mapstructure:"project_id"`
	Date              string `json:"date"`
	OriginalWorkHours int    `json:"original_work_hours" mapstructure:"original_work_hours"`
	WorkHours         int    `json:"work_hours" mapstructure:"work_hours"`
	SubmitPeriodID    int    `json:"submit_period_id" mapstructure:"submit_period_id"`
}

//Reviewer --
type Reviewer struct {
	ID         int `json:"id"`
	PeriodID   int `json:"period_id" mapstructure:"period_id"`
	EmployeeID int `json:"employee_id" mapstructure:"employee_id"`
	ProjectID  int `json:"project_id" mapstructure:"project_id"`
	ReviewerID int `json:"reviewer_id" mapstructure:"reviewer_id"`
}

//File --
type File struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	URL        string `json:"url"`
	PeriodID   int    `json:"period_id" mapstructure:"period_id"`
	EmployeeID int    `json:"employee_id" mapstructure:"employee_id"`
	ProjectID  int    `json:"project_id" mapstructure:"project_id"`
}

//Period --
type Period struct {
	ID        int       `json:"id"`
	StartDate time.Time `json:"start_date" mapstructure:"start_date"`
	EndDate   time.Time `json:"end_date" mapstructure:"end_date"`
	TotalDate int       `json:"total_date" mapstructure:"total_date"`
}
