package model

import "time"

type Company struct {
	ID              int       `json:"id"`
	NameTH          string    `json:"name_th" mapstructure:"name_th"`
	NameEN          string    `json:"name_en" mapstructure:"name_en"`
	Code            string    `json:"code"`
	Tax             string    `json:"tax"`
	Email           string    `json:"email"`
	Website         string    `json:"website"`
	Telephone       string    `json:"telephone"`
	Fax             string    `json:"fax"`
	About           string    `json:"about"`
	CreatedBy       int       `json:"created_by"`
	UpdatedBy       int       `json:"updated_by"`
	DeactivatedDate time.Time `json:"deactivated_date"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
}

type CompanyList struct {
	Companies []Company `json:"companies"`
	Total     int       `json:"total"`
}

type CompanyResponseMessage struct {
	CompanyID int    `json:"company_id"`
	Message   string `json:"message"`
}
