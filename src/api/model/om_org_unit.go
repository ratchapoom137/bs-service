package model

import (
	"time"
)

type OrgUnit struct {
	ID          int        `json:"id"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	OrgLevelID  int        `json:"org_level_id"`
	ParentOrgID int        `json:"parent_org_id"`
	ChildrenOrg []OrgUnit  `json:"children_org"`
	EffectiveAt time.Time  `json:"effective_at"`
	TerminateAt *time.Time `json:"terminate_at"`
	CreatedAt   time.Time  `json:"created_at"`
	UpdatedAt   time.Time  `json:"updated_at"`
}

type OrgUnitInput struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	OrgLevelID  int    `json:"org_level_id"`
	ParentOrgID int    `json:"parent_org_id"`
	EffectiveAt string `json:"effective_at"`
	TerminateAt string `json:"terminate_at"`
}

type OrgUnitList struct {
	OrgUnits []OrgUnit `json:"org_units"`
	Total    int       `json:"total"`
}

type OrgUnitResponseMessage struct {
	OrgUnitID int    `json:"org_unit_id"`
	Message   string `json:"message"`
}

type OrgUnitListResponseMessage struct {
	OrgUnitsID []int  `json:"org_units_id"`
	Message    string `json:"message"`
}
